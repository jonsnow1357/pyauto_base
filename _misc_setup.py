#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

from __future__ import annotations  # for python 3.8
#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import stat
import shutil
import glob
import zipfile

logger = logging.getLogger("lib")

def _rm_file_ro(action, name, exc):
  os.chmod(name, stat.S_IWRITE)
  os.remove(name)

def preBuildCleanup(folders=None, files=None):
  if (folders is None):
    folders = []
  if (files is None):
    files = []
  if (not isinstance(folders, list)):
    msg = "folders MUST BE a list"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isinstance(files, list)):
    msg = "files MUST BE a list"
    logger.error(msg)
    raise RuntimeError(msg)

  for fld in folders:
    if (os.path.exists(fld)):
      logger.info("== remove old folder: {}".format(fld))
      shutil.rmtree(fld, onerror=_rm_file_ro)

  for f in files:
    if (os.path.exists(f)):
      logger.info("== remove old file: {}".format(f))
      os.remove(f)

def postBuildCleanup(folders=None, files=None):
  if (folders is None):
    folders = []
  if (files is None):
    files = []
  if (not isinstance(folders, list)):
    msg = "folders MUST BE a list"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isinstance(files, list)):
    msg = "files MUST BE a list"
    logger.error(msg)
    raise RuntimeError(msg)

  for fld in folders:
    if (os.path.exists(fld)):
      logger.info("== remove temp folder: {}".format(fld))
      shutil.rmtree(fld, onerror=_rm_file_ro)

  for f in files:
    logger.info("== remove temp file: {}".format(f))
    if (os.path.exists(f)):
      os.remove(f)

def addDataFiles_VisualC(data_files):
  if (sys.argv[1] != "py2exe"):
    return
  if (sys.platform.startswith("win")):
    return

  if (data_files is None):
    data_files = []
  if (not isinstance(data_files, list)):
    msg = "data_files MUST BE a list"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("== add Microsoft Visual C++ 2008 Redistributable Package dlls")
  if (os.path.exists(
      r"C:/Windows/winsxs/x86_microsoft.vc90.crt_1fc8b3b9a1e18e3b_9.0.30729.6161_none_50934f2ebcb7eb57"
  )):
    sys.path.append(
        r"C:/Windows/winsxs/x86_microsoft.vc90.crt_1fc8b3b9a1e18e3b_9.0.30729.6161_none_50934f2ebcb7eb57"
    )

  if (not os.path.exists(
      r"C:\Windows\winsxs\x86_microsoft.vc90.crt_1fc8b3b9a1e18e3b_9.0.21022.8_none_bcb86ed6ac711f91"
  )):
    raise RuntimeError("CANNOT find path")
  if (not os.path.exists(
      r"C:\Windows\winsxs\Manifests\x86_microsoft.vc90.crt_1fc8b3b9a1e18e3b_9.0.21022.8_none_bcb86ed6ac711f91.manifest"
  )):
    raise RuntimeError("CANNOT find path")
  lstTemp = glob.glob(
      r"C:\Windows\winsxs\x86_microsoft.vc90.crt_1fc8b3b9a1e18e3b_9.0.21022.8_none_bcb86ed6ac711f91\*.*"
  )
  lstTemp += glob.glob(
      r"C:\Windows\winsxs\Manifests\x86_microsoft.vc90.crt_1fc8b3b9a1e18e3b_9.0.21022.8_none_bcb86ed6ac711f91.manifest"
  )
  data_files.append(("Microsoft.VC90.CRT", lstTemp))
  return data_files

def addDataFiles_local(data_files, folders=None, files=None):
  if (folders is None):
    folders = []
  if (files is None):
    files = []
  if (not isinstance(folders, list)):
    msg = "folders MUST BE a list"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isinstance(files, list)):
    msg = "files MUST BE a list"
    logger.error(msg)
    raise RuntimeError(msg)

  if (data_files is None):
    data_files = []
  if (not isinstance(data_files, list)):
    msg = "data_files MUST BE a list"
    logger.error(msg)
    raise RuntimeError(msg)

  for fld in folders:
    if (os.path.exists(fld)):
      logger.info("== add folder: {}".format(fld))
      lstTemp = []
      for f in os.listdir(fld):
        lstTemp.append(os.path.join(fld, f))
      data_files.append((os.path.basename(fld), lstTemp))

  for f in files:
    if (os.path.isfile(f)):
      logger.info("== add file: {}".format(f))
      dirPath = os.path.dirname(f)
      if (dirPath == ""):
        data_files.append((".", [f]))
      else:
        data_files.append((os.path.basename(dirPath), [f]))
  return data_files

def getTargetAppVersion(appPath=".", bAddBuild=True):
  res = []

  for f in os.listdir(appPath):
    if (f.endswith(".py") and (f != "setup.py")):
      with open(os.path.join(appPath, f), "r") as fIn:
        for ln in fIn.readlines():
          if (ln.startswith("appVer")):
            logger.info("appVer found in {}: {}".format(f, ln))
            tmp = re.sub(r" ?#.*", "", ln)  # remove any comments from the line
            res.append(tmp.split()[-1].replace("\"", ""))
  #logger.info(res)
  if (len(res) == 0):
    logger.warning("== NO appVer found")
    return ""
  elif (len(res) > 1):
    logger.warning("== MULTIPLE appVer found: {}".format(res))
    return ""

  if (bAddBuild):
    now = datetime.datetime.now()
    if (res[0] == ""):
      return "{}.{}".format(
          "0.0", (now.year + now.timetuple().tm_yday + now.hour + now.minute + now.second))
    else:
      return "{}.{}".format(
          res[0], (now.year + now.timetuple().tm_yday + now.hour + now.minute + now.second))
  else:
    if (res[0] == ""):
      return "0.0"
    else:
      return res[0]

def getFolderFromPythonPath(fPath):
  res = None
  for pathEl in sys.path:
    if (os.path.isdir(pathEl)):
      for dirpath, dirs, files in os.walk(pathEl):
        for d in dirs:
          if (fPath in os.path.join(dirpath, d)):
            res = os.path.join(dirpath, d)
            break
        if (res is not None):
          break
    if (res is not None):
      break

  if (res is None):
    msg = "CANNOT find {} in PYTHONPATH".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)
  return res

def addToLibraryZip(pyPath, zipPath, folders=None, files=None):
  """
  :param pyPath: source path (in PYTHONPATH)
  :param zipPath:
  :param folders:
  :param files:
  :return:
  """
  if (sys.argv[1] != "py2exe"):
    return
  if (sys.platform.startswith("win")):
    return

  if (folders is None):
    folders = []
  if (files is None):
    files = []
  if (not isinstance(folders, list)):
    msg = "folders MUST BE a list"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isinstance(files, list)):
    msg = "files MUST BE a list"
    logger.error(msg)
    raise RuntimeError(msg)

  srcOK = False
  srcPath = None
  for pathEl in sys.path:
    if (os.path.isdir(pathEl)):
      for f in os.listdir(pathEl):
        if (f == pyPath):
          srcPath = os.path.join(pathEl, f)
          srcOK = True
          break
    if (srcOK):
      break

  if (srcPath is None):
    msg = "CANNOT find {} in PYTHONPATH"
    logger.error(msg)
    raise RuntimeError(msg)

  with zipfile.ZipFile(zipPath, "a") as libZip:
    for fld in folders:
      fPath = os.path.join(srcPath, fld)
      if (os.path.isdir(fPath)):
        logger.info("== add to library.zip folder: {}".format(fPath))
        for f in os.listdir(fPath):
          libZip.write(os.path.join(fPath, f), arcname=os.path.join(pyPath, fld, f))
