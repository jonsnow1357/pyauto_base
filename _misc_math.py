#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

from __future__ import annotations  # for python 3.8
#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv
import typing
import random

logger = logging.getLogger("lib")
try:
  import pint
  _ureg = pint.UnitRegistry()  # create one instance per module
except ImportError as ex:
  logger.error(ex)
  pass

def chkInt(val: int,
           valId: typing.Optional[str] = None,
           minVal: typing.Optional[int] = None,
           maxVal: typing.Optional[int] = None) -> int:
  if (not isinstance(val, int)):
    val = int(val)
  if (not isinstance(val, int)):
    msg = "INCORRECT '{}' type: {}".format(valId, type(val))
    logger.error(msg)
    raise RuntimeError(msg)

  if (valId is None):
    valId = "number"
  if ((minVal is not None) and (val < minVal)):
    msg = "INCORRECT '{}' value: {} < {}".format(valId, val, minVal)
    logger.error(msg)
    raise RuntimeError(msg)
  if ((maxVal is not None) and (val > maxVal)):
    msg = "INCORRECT '{}' value: {} > {}".format(valId, val, maxVal)
    logger.error(msg)
    raise RuntimeError(msg)

  return val

def chkFloat(val: float,
             valId: typing.Optional[str] = None,
             minVal: typing.Optional[float] = None,
             maxVal: typing.Optional[float] = None) -> float:
  if (not isinstance(val, float)):
    val = float(val)
  if (not isinstance(val, float)):
    msg = "INCORRECT '{}' type: {}".format(valId, type(val))
    logger.error(msg)
    raise RuntimeError(msg)

  if (valId is None):
    valId = "number"
  if ((minVal is not None) and (val < minVal)):
    msg = "INCORRECT '{}' value: {} < {}".format(valId, val, minVal)
    logger.error(msg)
    raise RuntimeError(msg)
  if ((maxVal is not None) and (val > maxVal)):
    msg = "INCORRECT '{}' value: {} > {}".format(valId, val, maxVal)
    logger.error(msg)
    raise RuntimeError(msg)

  return val

def getRndFloat(minVal: float, maxVal: float) -> float:
  if (minVal == maxVal):
    return minVal
  elif (maxVal < minVal):
    tmp = minVal
    minVal = maxVal
    maxVal = tmp

  res = random.random()  # floating point number in the range [0.0, 1.0)
  return (maxVal - minVal) * res + minVal

def valueWithUnit(val: typing.Union[int, float, str], unit: str):
  if ((unit == "mil") or (unit == "mils")):
    unit = "thou"
  _unit = _ureg(unit)

  if (isinstance(val, int)):
    _val = float(val) * _unit
  elif (isinstance(val, float)):
    _val = val * _unit
  else:
    _val = float(val) * _unit

  # if (_val.units != _unit.units):
  #   _val.ito(_unit)
  return _val

def hex2float(hexVal: str, Nb: int, Vref: float, scale: float = 1.0) -> float:
  """
  Converts hex values from ADCs into numbers

  :param hexVal: the value read from the ADC [hex string]
  :param Nb: number of resolution bits of the ADC
  :param Vref: reference voltage of the ADC
  :param scale: additional scale factor [optional]
  :return: float value (> 0)
  """
  tmp = (int(hexVal, 16) / (2.0**Nb)) * Vref
  return (tmp * scale)

def float2hex(val: float, Nb: int, Vref: float, scale: float = 1.0) -> str:
  """
  Converts float values to DAC values

  :param val: the float value (> 0)
  :param Nb: number of resolution bits of the DAC
  :param Vref: reference voltage of the DAC
  :param scale: additional scale factor [optional]
  :return: hex string
  """
  maxint = (2**Nb - 1)
  tmp = ((val * maxint) / Vref) * scale
  tmp = int(round(tmp))
  if (tmp >= maxint):
    tmp = maxint
  fmt = "0x{:0" + str(Nb // 4) + "X}"
  return fmt.format(tmp)

def val2dB(val: float, refLvl: typing.Union[float, str] = "dBm") -> float:
  if (refLvl == 0.0):
    msg = "reference level MUST NOT be 0"
    raise RuntimeError(msg)
  elif (refLvl == "dBm"):
    refLvl = 1.0e-3
  else:
    refLvl = float(refLvl)

  return (10.0 * math.log10(val / refLvl))

def dB2val(db: float, refLvl: typing.Union[float, str] = "dBm") -> float:
  if (refLvl == "dBm"):
    refLvl = 1.0e-3
  else:
    refLvl = float(refLvl)

  return (refLvl * math.pow(10.0, (db / 10.0)))

class SparseMatrix(object):
  """
  implementation using coordinate list
  """

  def __init__(self, nr, nc):
    if (nr < 1):
      raise RuntimeError
    self.nRows = nr
    if (nc < 1):
      raise RuntimeError
    self.nCols = nc

    self._r: list[int] = []
    self._c: list[int] = []
    self._v: list[typing.Union[int, float]] = []

  def clear(self) -> None:
    self._r = []
    self._c = []
    self._v = []

  def shape(self) -> list[int]:
    return [self.nRows, self.nCols]

  @property
  def nValues(self) -> int:
    return len(self._v)

  @property
  def rowsWithVal(self):
    return list(set(self._r))

  @property
  def colsWithVal(self):
    return list(set(self._c))

  def get(self, row: int, col: int) -> typing.Union[int, float]:
    res = self.getRow(row)
    return res[col]

  def set(self, row: int, col: int, val: typing.Union[int, float]) -> None:
    if (isinstance(val, int) and (val == 0)):
      return
    elif (isinstance(val, float) and (val == 0.0)):
      return
    elif (not isinstance(val, int) and not isinstance(val, float)):
      raise NotImplementedError

    if (row >= self.nRows):
      raise RuntimeError
    if (col >= self.nCols):
      raise RuntimeError

    for i in range(0, len(self._r)):
      if ((self._r[i] == row) and (self._c[i] == col)):
        self._v[i] = val
        return

    self._r.append(row)
    self._c.append(col)
    self._v.append(val)

  def getRow(self, row: int) -> list[typing.Union[int, float]]:
    res: list[typing.Union[int, float]] = [0] * self.nCols
    for i, r in enumerate(self._r):
      if (r == row):
        res[self._c[i]] = self._v[i]
    return res

  def getCol(self, col: int) -> list[typing.Union[int, float]]:
    res: list[typing.Union[int, float]] = [0] * self.nRows
    for i, c in enumerate(self._c):
      if (c == col):
        res[self._r[i]] = self._v[i]
    return res

  def remove(self, row: int, col: int) -> None:
    idx = -1
    for i, v in enumerate(self._v):
      if ((self._r[i] == row) and (self._c[i] == col)):
        idx = i
        break
    del self._r[idx]
    del self._c[idx]
    del self._v[idx]

  def getAll(self) -> list[tuple[int, int, typing.Union[int, float]]]:
    return [t for t in zip(self._r, self._c, self._v)]

  def keepMax_row(self) -> None:
    """if the matrix has more than 1 col, keep only the maximum value in each row, set all else to 0"""
    if ((self.nRows == 1) or (self.nCols == 1)):
      return

    idx_del = []
    for i in range(0, self.nRows):
      row = self.getRow(i)
      min_r = min(row)
      max_r = max(row)
      if ((min_r == 0) and (max_r == 0)):
        continue
      elif (min_r == max_r):
        for j in range(0, self.nCols):
          val = self.get(i, j)
          if (val != 0):
            idx_del.append([i, j])
      else:
        for j in range(0, self.nCols):
          val = self.get(i, j)
          if ((val != 0) and (val < max_r)):
            idx_del.append([i, j])

    for tmp in idx_del:
      self.remove(tmp[0], tmp[1])

  def keepMax_col(self) -> None:
    """if the matrix has more than 1 row, keep only the maximum value in each column, set all else to 0"""
    if ((self.nRows == 1) or (self.nCols == 1)):
      return

    idx_del = []
    for i in range(0, self.nCols):
      col = self.getCol(i)
      min_c = min(col)
      max_c = max(col)
      if ((min_c == 0) and (max_c == 0)):
        continue
      elif (min_c == max_c):
        for j in range(0, self.nRows):
          val = self.get(j, i)
          if (val != 0):
            idx_del.append([j, i])
      else:
        for j in range(0, self.nRows):
          val = self.get(j, i)
          if ((val != 0) and (val < max_c)):
            idx_del.append([j, i])

    for tmp in idx_del:
      self.remove(tmp[0], tmp[1])
