#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_base.svg"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_base.svg
import pyauto_base.tests.tools as tt

class Test_svg(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  def _test_base_rect(self, xg, yg):
    x0 = xg * self.dx
    y0 = yg * self.dy
    yb = (yg + 1) * self.dy
    sh = pyauto_base.svg.svgText(x=(x0 + 20), y=(yb - 5), text="<rect>")
    logger.info(sh)
    self.img.addElement(sh)

    sh = pyauto_base.svg.svgRectangle(x=(x0 + 50),
                                      y=(y0 + 50),
                                      width=int(self.dx / 2),
                                      height=int(self.dy / 4))
    logger.info(sh)
    sh.rx = 10
    sh.ry = 5
    logger.info(sh)
    sh.fill = "#00ff00"
    sh.stroke = "#ff0000"
    logger.info(sh)
    self.img.addElement(sh)

  def _test_base_circle(self, xg, yg):
    x0 = xg * self.dx
    y0 = yg * self.dy
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2
    yb = (yg + 1) * self.dy
    sh = pyauto_base.svg.svgText(x=(x0 + 20), y=(yb - 5), text="<circle>")
    #logger.info(sh)
    self.img.addElement(sh)

    sh = pyauto_base.svg.svgCircle(cx=xm, cy=ym, r=(self.dy / 4))
    logger.info(sh)
    sh.fill = "rgb(255, 128, 0)"
    sh.stroke = "rgb(0, 0, 255)"
    logger.info(sh)
    self.img.addElement(sh)

  def _test_base_ellipse(self, xg, yg):
    x0 = xg * self.dx
    y0 = yg * self.dy
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2
    yb = (yg + 1) * self.dy
    sh = pyauto_base.svg.svgText(x=(x0 + 20), y=(yb - 5), text="<ellipse>")
    #logger.info(sh)
    self.img.addElement(sh)

    sh = pyauto_base.svg.svgEllipse(cx=xm, cy=ym, rx=(self.dx / 4), ry=(self.dy / 6))
    logger.info(sh)
    sh.fill = "rgb(100%, 100%, 0%)"
    sh.stroke = "rgb(100%, 0%, 100%)"
    logger.info(sh)
    self.img.addElement(sh)

  def _test_base_polyline(self, xg, yg):
    x0 = xg * self.dx
    y0 = yg * self.dy
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2
    yb = (yg + 1) * self.dy
    sh = pyauto_base.svg.svgText(x=(x0 + 20), y=(yb - 5), text="<polyline>")
    #logger.info(sh)
    self.img.addElement(sh)

    sh = pyauto_base.svg.svgPolyLine()
    sh.stroke = "rgb(0, 255, 255)"
    sh.addPoint(x=(x0 + 10), y=ym)
    sh.addPoint(x=(x0 + 20), y=ym)
    sh.addPoint(x=(x0 + 20), y=(ym - 10))
    sh.addPoint(x=(x0 + 30), y=(ym - 10))
    sh.addPoint(x=(x0 + 30), y=ym)
    sh.addPoint(x=(x0 + 40), y=ym)
    sh.addPoint(x=(x0 + 50), y=(ym - 40))
    sh.addPoint(x=(x0 + 50), y=(ym + 40))
    sh.addPoint(x=(x0 + 60), y=ym)
    sh.addPoint(x=(x0 + 70), y=ym)
    logger.info(sh)
    self.img.addElement(sh)

  def _test_base_polygon(self, xg, yg):
    x0 = xg * self.dx
    y0 = yg * self.dy
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2
    yb = (yg + 1) * self.dy
    sh = pyauto_base.svg.svgText(x=(x0 + 20), y=(yb - 5), text="<polygon>")
    #logger.info(sh)
    self.img.addElement(sh)

    sh = pyauto_base.svg.svgPolygon()
    sh.stroke = "rgb(0, 128, 128)"
    n = 9
    for i in range(n):
      x = (self.dx / 4) * math.cos((2 * i * math.pi) / n)
      y = (self.dx / 4) * math.sin((2 * i * math.pi) / n)
      sh.addPoint(x=(xm + int(x)), y=(ym + int(y)))
    logger.info(sh)
    self.img.addElement(sh)

  def _test_base_path(self, xg, yg):
    x0 = xg * self.dx
    y0 = yg * self.dy
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2
    yb = (yg + 1) * self.dy
    sh = pyauto_base.svg.svgText(x=(x0 + 20), y=(yb - 5), text="<path>")
    #logger.info(sh)
    self.img.addElement(sh)

    sh = pyauto_base.svg.svgPath(x=xm, y=ym)
    sh.lineto(x=(xm - 40), y=(ym - 40))
    sh.lineto(x=(xm - 20), y=ym)
    sh.lineto(x=(xm - 40), y=(ym + 40))
    sh.lineto(x=(xm + 40), y=(ym - 40))
    sh.lineto(x=(xm + 20), y=ym)
    sh.lineto(x=(xm + 40), y=(ym + 40))
    sh.lineto(x=(xm + 5), y=(ym + 5))
    logger.info(sh)
    self.img.addElement(sh)

  #@unittest.skip("")
  def test_base(self):
    fPath = os.path.join(self.outFolder, "test_base.svg")
    self.dx = 200
    self.dy = 200
    nx = 3
    ny = 3
    self.img = pyauto_base.svg.svgImage(fPath, width=(nx * self.dx), height=(ny * self.dy))
    self.img.title = "image title"

    logger.info(self.img)
    lstHash = [
        "2009fea4ef09b3561575f77621ee6a87ee1d6f94442eab96cc73e93b9c181ebf",  # linux
        "75505b7f6e67fc153fe4ef3bd749341ee27b3ac2f53bb7a0bd128c7b7eaba557",  # win
    ]
    tt.chkHash_str(self, str(self.img), lstHash)

    # draw grid
    for i in range(1, nx):
      sh = pyauto_base.svg.svgLine(x1=(i * self.dx),
                                   y1=0,
                                   x2=(i * self.dx),
                                   y2=(ny * self.dy))
      if (i == 1):
        logger.info(sh)
      self.img.addElement(sh)
    for i in range(1, ny):
      sh = pyauto_base.svg.svgLine(x1=0,
                                   y1=(i * self.dy),
                                   x2=(nx * self.dx),
                                   y2=(i * self.dy))
      self.img.addElement(sh)

    self._test_base_rect(xg=0, yg=0)
    self._test_base_circle(xg=1, yg=0)
    self._test_base_ellipse(xg=2, yg=0)

    self._test_base_polyline(xg=0, yg=1)
    self._test_base_polygon(xg=1, yg=1)
    self._test_base_path(xg=2, yg=1)

    self.img.showInfo()
    self.img.write()
    lstHash = [
        "512e85179125b49378aaef9ccbda8c50357c8b642d96936c676fe38137029de8",  # py2 linux
        "7295d925453d3f16253c032be0c71c294270fe94c6e02a43c9a5343be9e9b3af",  # py3 linux
        "d6a9e467bb9c16111a153945170bf2cc5bac1b58e57fc9e639358e1edf270307",  # win
    ]
    tt.chkHash_file(self, fPath, lstHash)

  def _test_rectangle_round(self, xg, yg):
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2

    sh = pyauto_base.svg.svgRectangle(x=(xm - self.rw / 2),
                                      y=(ym - self.rh / 2),
                                      width=self.rw,
                                      height=self.rh)
    if (xg == 1):
      sh.rx = self.rw / 4
      sh.ry = self.rh / 4
    elif (xg == 2):
      sh.rx = self.rw / 10
      sh.ry = self.rh / 4
    elif (xg == 3):
      sh.rx = self.rw / 4
      sh.ry = self.rh / 10
    self.img.addElement(sh)

  def _test_rectangle_fill(self, xg, yg):
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2

    sh = pyauto_base.svg.svgRectangle(x=(xm - self.rw / 2),
                                      y=(ym - self.rh / 2),
                                      width=self.rw,
                                      height=self.rh)
    sh.fill = "rgb(0, 0, 255)"
    if (xg == 1):
      sh.fill_opacity = 0.75
    elif (xg == 2):
      sh.fill_opacity = 0.5
    elif (xg == 3):
      sh.fill_opacity = 0.25
    self.img.addElement(sh)

  def _test_rectangle_stroke_width(self, xg, yg):
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2

    sh = pyauto_base.svg.svgRectangle(x=(xm - self.rw / 2),
                                      y=(ym - self.rh / 2),
                                      width=self.rw,
                                      height=self.rh)
    sh.fill = "rgb(0, 255, 0)"
    sh.stroke = "rgb(255, 0, 0)"
    if (xg == 1):
      sh.stroke_width = 2
    elif (xg == 2):
      sh.stroke_width = 4
    elif (xg == 3):
      sh.stroke_width = 8
    self.img.addElement(sh)

  def _test_rectangle_stroke_opacity(self, xg, yg):
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2

    sh = pyauto_base.svg.svgRectangle(x=(xm - self.rw / 2),
                                      y=(ym - self.rh / 2),
                                      width=self.rw,
                                      height=self.rh)
    sh.fill = "rgb(0, 255, 0)"
    sh.stroke = "rgb(255, 0, 0)"
    sh.stroke_width = 8
    sh.stroke_linejoin = "round"
    if (xg == 1):
      sh.stroke_linejoin = "bevel"
      sh.stroke_opacity = 0.75
    elif (xg == 2):
      sh.stroke_opacity = 0.5
    elif (xg == 3):
      sh.stroke_linejoin = "bevel"
      sh.stroke_opacity = 0.25
    self.img.addElement(sh)

  #@unittest.skip("")
  def test_rectangle(self):
    fPath = os.path.join(self.outFolder, "test_rectangle.svg")
    self.dx = 200
    self.dy = 200
    nx = 4
    ny = 4
    self.img = pyauto_base.svg.svgImage(fPath, width=(nx * self.dx), height=(ny * self.dy))

    # draw grid
    for i in range(1, nx):
      sh = pyauto_base.svg.svgLine(x1=(i * self.dx),
                                   y1=0,
                                   x2=(i * self.dx),
                                   y2=(ny * self.dy))
      self.img.addElement(sh)
    for i in range(1, ny):
      sh = pyauto_base.svg.svgLine(x1=0,
                                   y1=(i * self.dy),
                                   x2=(nx * self.dx),
                                   y2=(i * self.dy))
      self.img.addElement(sh)

    self.rw = 100
    self.rh = 60
    self._test_rectangle_round(xg=0, yg=0)
    self._test_rectangle_round(xg=1, yg=0)
    self._test_rectangle_round(xg=2, yg=0)
    self._test_rectangle_round(xg=3, yg=0)

    self.rw = 60
    self.rh = 100
    self._test_rectangle_fill(xg=0, yg=1)
    self._test_rectangle_fill(xg=1, yg=1)
    self._test_rectangle_fill(xg=2, yg=1)
    self._test_rectangle_fill(xg=3, yg=1)

    self.rw = 120
    self.rh = 40
    self._test_rectangle_stroke_width(xg=0, yg=2)
    self._test_rectangle_stroke_width(xg=1, yg=2)
    self._test_rectangle_stroke_width(xg=2, yg=2)
    self._test_rectangle_stroke_width(xg=3, yg=2)

    self.rw = 40
    self.rh = 120
    self._test_rectangle_stroke_opacity(xg=0, yg=3)
    self._test_rectangle_stroke_opacity(xg=1, yg=3)
    self._test_rectangle_stroke_opacity(xg=2, yg=3)
    self._test_rectangle_stroke_opacity(xg=3, yg=3)

    self.img.showInfo()
    self.img.write()
    lstHash = [
        "85fe35fe706fa10a428daedddb0242931b8ff0b0459e7a9fb9ba38f8beab1e1a",  # py2 linux
        "edae6ff9451fc4612a67147ba74f5d21afb166d51d22ea7928e9d8123b210722",  # py3 linux
        "98aa79809c1e1acba056bfc3b5d063187bd4a96115039878ba2690bd59a32b4f",  # win
    ]
    tt.chkHash_file(self, fPath, lstHash)

  def _test_circle_fill(self, xg, yg):
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2

    sh = pyauto_base.svg.svgCircle(cx=xm, cy=ym, r=self.r)
    sh.fill = "rgb(128, 0, 128)"
    if (xg == 1):
      sh.fill_opacity = 0.75
    elif (xg == 2):
      sh.fill_opacity = 0.5
    elif (xg == 3):
      sh.fill_opacity = 0.25
    self.img.addElement(sh)

  def _test_circle_stroke(self, xg, yg):
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2

    sh = pyauto_base.svg.svgCircle(cx=xm, cy=ym, r=self.r)
    sh.fill = "rgb(0, 128, 128)"
    sh.stroke = "rgb(255, 64, 0)"
    sh.stroke_dasharray = "5,5"
    sh.stroke_width = 8
    if (xg == 1):
      sh.stroke_dasharray = "10,10"
    elif (xg == 2):
      sh.stroke_dasharray = "15,5"
    elif (xg == 3):
      sh.stroke_dasharray = "5,15"
    self.img.addElement(sh)

  #@unittest.skip("")
  def test_circle(self):
    fPath = os.path.join(self.outFolder, "test_circle.svg")
    self.dx = 200
    self.dy = 200
    nx = 4
    ny = 2
    self.img = pyauto_base.svg.svgImage(fPath, width=(nx * self.dx), height=(ny * self.dy))

    # draw grid
    for i in range(1, nx):
      sh = pyauto_base.svg.svgLine(x1=(i * self.dx),
                                   y1=0,
                                   x2=(i * self.dx),
                                   y2=(ny * self.dy))
      self.img.addElement(sh)
    for i in range(1, ny):
      sh = pyauto_base.svg.svgLine(x1=0,
                                   y1=(i * self.dy),
                                   x2=(nx * self.dx),
                                   y2=(i * self.dy))
      self.img.addElement(sh)

    self.r = 40
    self._test_circle_fill(xg=0, yg=0)
    self._test_circle_fill(xg=1, yg=0)
    self._test_circle_fill(xg=2, yg=0)
    self._test_circle_fill(xg=3, yg=0)

    self.r = 50
    self._test_circle_stroke(xg=0, yg=1)
    self._test_circle_stroke(xg=1, yg=1)
    self._test_circle_stroke(xg=2, yg=1)
    self._test_circle_stroke(xg=3, yg=1)

    self.img.showInfo()
    self.img.write()
    lstHash = [
        "253ea3de183fde32b1d9d40aea7ec1c803996dee37a4914140be91e8d40c39fc",  # py2 linux
        "8d54bdfd38628812225782dc8b78f594b44c03238f38aa49e161b413f481da54",  # py3 linux
        "94937d51dfc75c4a13369bb73f001d7d2f907a5346919fd74acc60a3c7e41b21",  # win
    ]
    tt.chkHash_file(self, fPath, lstHash)

  def _test_polyline_linecap(self, xg, yg):
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2

    sh = pyauto_base.svg.svgPolyLine()
    sh.stroke_width = 8
    sh.addPoint(x=xm, y=ym)
    sh.addPoint(x=xm, y=(ym - 20))
    sh.addPoint(x=(xm - 20), y=(ym - 20))
    sh.addPoint(x=(xm - 20), y=(ym + 20))
    sh.addPoint(x=(xm + 20), y=(ym + 20))
    sh.addPoint(x=(xm + 20), y=(ym - 40))
    sh.addPoint(x=(xm - 40), y=(ym - 40))
    if (xg == 1):
      sh.stroke_linecap = "round"
    elif (xg == 2):
      sh.fill = "rgb(0, 255, 0)"
    elif (xg == 3):
      sh.stroke_linecap = "square"
      sh.fill = "rgb(0, 255, 0)"
    self.img.addElement(sh)

  def _test_polyline_linejoin(self, xg, yg):
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2

    sh = pyauto_base.svg.svgPolyLine()
    sh.stroke = "rgb(255, 0, 0)"
    sh.stroke_width = 8
    sh.addPoint(x=xm, y=ym)
    sh.addPoint(x=xm, y=(ym + 20))
    sh.addPoint(x=(xm + 20), y=(ym + 20))
    sh.addPoint(x=(xm + 20), y=(ym - 20))
    sh.addPoint(x=(xm - 20), y=(ym - 20))
    sh.addPoint(x=(xm - 20), y=(ym + 40))
    sh.addPoint(x=(xm + 40), y=(ym + 40))
    if (xg == 1):
      sh.stroke_linecap = "round"
      sh.stroke_linejoin = "round"
    elif (xg == 2):
      sh.stroke_linejoin = "bevel"
    elif (xg == 3):
      sh.stroke_linecap = "round"
      sh.stroke_linejoin = "bevel"
    self.img.addElement(sh)

  def _test_polygon_convex(self, xg, yg):
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2

    sh = pyauto_base.svg.svgPolygon()
    sh.regular_convex(x=xm, y=ym, n=3, r=self.r)
    if (xg == 1):
      sh.regular_convex(x=xm, y=ym, n=3, r=self.r, rot=(math.pi / 2))
    elif (xg == 2):
      sh.regular_convex(x=xm, y=ym, n=6, r=self.r)
    elif (xg == 3):
      sh.regular_convex(x=xm, y=ym, n=6, r=self.r, rot=(math.pi / 6))
    self.img.addElement(sh)

  #@unittest.skip("")
  def test_poly(self):
    fPath = os.path.join(self.outFolder, "test_poly.svg")
    self.dx = 200
    self.dy = 200
    nx = 4
    ny = 3
    self.img = pyauto_base.svg.svgImage(fPath, width=(nx * self.dx), height=(ny * self.dy))

    # draw grid
    for i in range(1, nx):
      sh = pyauto_base.svg.svgLine(x1=(i * self.dx),
                                   y1=0,
                                   x2=(i * self.dx),
                                   y2=(ny * self.dy))
      self.img.addElement(sh)
    for i in range(1, ny):
      sh = pyauto_base.svg.svgLine(x1=0,
                                   y1=(i * self.dy),
                                   x2=(nx * self.dx),
                                   y2=(i * self.dy))
      self.img.addElement(sh)

    self._test_polyline_linecap(xg=0, yg=0)
    self._test_polyline_linecap(xg=1, yg=0)
    self._test_polyline_linecap(xg=2, yg=0)
    self._test_polyline_linecap(xg=3, yg=0)

    self._test_polyline_linejoin(xg=0, yg=1)
    self._test_polyline_linejoin(xg=1, yg=1)
    self._test_polyline_linejoin(xg=2, yg=1)
    self._test_polyline_linejoin(xg=3, yg=1)

    self.r = 40
    self._test_polygon_convex(xg=0, yg=2)
    self._test_polygon_convex(xg=1, yg=2)
    self._test_polygon_convex(xg=2, yg=2)
    self._test_polygon_convex(xg=3, yg=2)

    self.img.showInfo()
    self.img.write()
    lstHash = [
        "7de752f7d3c12eebe61a078e661243635e14ac07abbcde47f37578f79d30498b",  # py2 linux
        "2495be3448be9ca01a5760d881f1f3f392cfe23fa5430eba60ff17d8b88f9ac2",  # py3 linux
        "7c18f19813dc46540022bded89d3fbeef24d6d8288595265906a49cd6cb5d25a",  # win
    ]
    tt.chkHash_file(self, fPath, lstHash)

  def _test_path_line(self, xg, yg):
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2

    sh = pyauto_base.svg.svgPath(x=xm, y=ym)
    sh.fill = "rgb(255, 255, 0)"
    sh.stroke = "rgb(255, 0, 255)"
    sh.stroke_width = 2
    if (xg % 2):
      sh.lineto_h(x=(xm + 10))
      sh.lineto_v(y=(ym + 10))
      sh.lineto_h(x=(xm + 20))
      sh.lineto_v(y=(ym - 10))
      sh.lineto_h(x=(xm + 30))
      sh.lineto_v(y=ym)
      sh.lineto_h(x=(xm + 40))
      sh.lineto_v(y=(ym + 40))
      sh.lineto_h(x=(xm + 50))
      sh.lineto_v(y=(ym + 50))
      sh.lineto_h(x=(xm + 60))
      sh.lineto_v(y=(ym + 30))
      sh.lineto_h(x=(xm + 70))
      sh.lineto_v(y=(ym + 40))
    else:
      sh.lineto(x=(xm + 20), y=(ym - 20))
      sh.lineto(x=(xm + 40), y=ym)
      sh.lineto(x=(xm + 60), y=(ym - 20))
      sh.lineto(x=(xm + 80), y=ym)
      sh.lineto(x=(xm + 90), y=(ym + 20))
      sh.lineto(x=(xm + 80), y=(ym + 40))
      sh.lineto(x=(xm + 60), y=(ym + 60))
      sh.lineto(x=(xm + 40), y=(ym + 40))
      sh.lineto(x=(xm + 20), y=(ym + 60))
      sh.lineto(x=xm, y=(ym + 40))
    if (xg == 2):
      sh.closepath()
    elif (xg == 3):
      sh.closepath()
    self.img.addElement(sh)

  def _test_path_arc(self, xg, yg):
    xm = xg * self.dx + self.dx / 2
    ym = yg * self.dy + self.dy / 2

    sh = pyauto_base.svg.svgPath(x=xm, y=ym)
    sh.fill = "rgb(255, 128, 0)"
    sh.stroke = "rgb(0, 0, 255)"
    sh.stroke_width = 2
    if (xg == 0):
      r = self.rx + self.ry
      sh.lineto_v(y=(ym - r))
      sh.arc(rx=r, ry=r, x=(xm + r), y=ym)
      sh.lineto_h(x=xm)
    elif (xg == 1):
      r = self.rx + self.ry
      sh.lineto_v(y=(ym - r))
      sh.arc(rx=r, ry=r, x=(xm + r), y=ym, bSweep=True)
      sh.lineto_h(x=xm)
    elif (xg == 2):
      sh.lineto_v(y=(ym - self.ry))
      sh.arc(rx=self.rx, ry=self.ry, x=(xm + self.rx), y=ym, bLargeArc=True)
      sh.lineto_h(x=xm)
    elif (xg == 3):
      sh.lineto_v(y=(ym - self.ry))
      sh.arc(rx=self.rx, ry=self.ry, x=(xm + self.rx), y=ym, bLargeArc=True, bSweep=True)
      sh.lineto_h(x=xm)
    self.img.addElement(sh)

  #@unittest.skip("")
  def test_path(self):
    fPath = os.path.join(self.outFolder, "test_path.svg")
    self.dx = 200
    self.dy = 200
    nx = 4
    ny = 2
    self.img = pyauto_base.svg.svgImage(fPath, width=(nx * self.dx), height=(ny * self.dy))

    # draw grid
    for i in range(1, nx):
      sh = pyauto_base.svg.svgLine(x1=(i * self.dx),
                                   y1=0,
                                   x2=(i * self.dx),
                                   y2=(ny * self.dy))
      self.img.addElement(sh)
    for i in range(1, ny):
      sh = pyauto_base.svg.svgLine(x1=0,
                                   y1=(i * self.dy),
                                   x2=(nx * self.dx),
                                   y2=(i * self.dy))
      self.img.addElement(sh)

    self._test_path_line(xg=0, yg=0)
    self._test_path_line(xg=1, yg=0)
    self._test_path_line(xg=2, yg=0)
    self._test_path_line(xg=3, yg=0)

    self.rx = 20
    self.ry = 40
    self._test_path_arc(xg=0, yg=1)
    self._test_path_arc(xg=1, yg=1)
    self._test_path_arc(xg=2, yg=1)
    self._test_path_arc(xg=3, yg=1)

    self.img.showInfo()
    self.img.write()
    lstHash = [
        "f6e059d83398df4490529f98d0dedb1c8627ee03ded224972afc3a2e33ee878d",  # py2 linux
        "c434b1d41b7a3c4a2a270c6a71dfe81d9e29bb96caf0636a9e43db59b750fac1",  # py3 linux
        "210b6a74f9419c2b14de9be28b275e7225cbf8a6752bc95b2b8b4864504abcdf",  # win
    ]
    tt.chkHash_file(self, fPath, lstHash)

  #@unittest.skip("")
  def test_units(self):
    fPath = os.path.join(self.outFolder, "test_units_mm.svg")
    self.dx = 10
    self.dy = 15
    self.img = pyauto_base.svg.svgImage(fPath, width=self.dx, height=self.dy, unit="mm")
    self.img.title = "image title"

    for i in range(1, self.dx):
      sh = pyauto_base.svg.svgLine(x1=i, y1=0, x2=i, y2=self.dy)
      sh.stroke_width = 0.1
      self.img.addElement(sh)
    for i in range(1, self.dy):
      sh = pyauto_base.svg.svgLine(x1=0, y1=i, x2=self.dx, y2=i)
      sh.stroke_width = 0.1
      sh.stroke = "rgb(255, 0, 0)"
      self.img.addElement(sh)

    self.img.showInfo()
    self.img.write()
    lstHash = [
        "214722afd7cdcd8293c92a374a3d601a1314e0f7829f945fa7ee47a3477bb528",  # linux
        "bd46b574aed55805df21aa30b1b2d4e73ec1dfd499b846a3eea298fc3ab47f43",  # win
    ]
    tt.chkHash_file(self, fPath, lstHash)

    fPath = os.path.join(self.outFolder, "test_units_cm.svg")
    self.dx = 10
    self.dy = 15
    self.img = pyauto_base.svg.svgImage(fPath, width=self.dx, height=self.dy, unit="cm")
    self.img.title = "image title"

    for i in range(1, self.dx):
      sh = pyauto_base.svg.svgLine(x1=i, y1=0, x2=i, y2=self.dy)
      sh.stroke_width = 0.1
      self.img.addElement(sh)
    for i in range(1, self.dy):
      sh = pyauto_base.svg.svgLine(x1=0, y1=i, x2=self.dx, y2=i)
      sh.stroke_width = 0.1
      sh.stroke = "rgb(255, 0, 0)"
      self.img.addElement(sh)

    self.img.showInfo()
    self.img.write()
    lstHash = [
        "ee7276a42deeb3865a331fcbb12e8f05ae39b86e9f3d2d454fa7144828d4b612",  # linux
        "2c17d4e3db4280cb3bdf7f1185ce72b99e517cc2a14abfa8aaf6742479e72878",  # win
    ]
    tt.chkHash_file(self, fPath, lstHash)

    fPath = os.path.join(self.outFolder, "test_units_in.svg")
    self.dx = 1
    self.dy = 2
    self.img = pyauto_base.svg.svgImage(fPath, width=self.dx, height=self.dy, unit="in")
    self.img.title = "image title"

    x = 0.1
    while (x < self.dx):
      sh = pyauto_base.svg.svgLine(x1=x, y1=0, x2=x, y2=self.dy)
      sh.stroke_width = 0.01
      self.img.addElement(sh)
      x += 0.1
    y = 0.1
    while (y < self.dy):
      sh = pyauto_base.svg.svgLine(x1=0, y1=y, x2=self.dx, y2=y)
      sh.stroke_width = 0.01
      sh.stroke = "rgb(255, 0, 0)"
      self.img.addElement(sh)
      y += 0.1

    self.img.showInfo()
    self.img.write()
    lstHash = [
        "79352a8ecc93a990b1edbb617039d06ea3eb7569737ea3f6f8a987166648d310",  # py2 linux
        "490a49f7a389e7bdc1924baaad60266d60c5b0aaffdabf2489415096347e28ce",  # py3 linux
        "47305a2fc396f49d55c1ec524518b685eb23164019861bddd6f686f2aac199eb",  # win
    ]
    tt.chkHash_file(self, fPath, lstHash)
