#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_base.imagemagick"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
skipAll = False
try:
  import pyauto_base.imagemagick
except RuntimeError:
  skipAll = True
except IOError:
  skipAll = True
import pyauto_base.svg

class Test_convert(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    if (skipAll):
      self.skipTest("")

    p1 = complex(1000, 1000)
    self.imgPath = os.path.join(self.outFolder, "test_gray.png")
    pyauto_base.imagemagick.makeImg(self.imgPath, p1, "gray")
    self.assertTrue(os.path.isfile(self.imgPath),
                    "file DOES NOT exist: {}".format(self.imgPath))

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_version(self):
    pyauto_base.imagemagick.showVersion()

  #@unittest.skip("")
  def test_crop(self):
    imgOut = os.path.join(self.outFolder, "test_crop.png")

    p1 = complex(100, 250)
    p2 = p1 + complex(666, 666)
    pyauto_base.imagemagick.cropImg(self.imgPath, p1, p2)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    self.assertTrue(os.path.isfile(imgOut), "file DOES NOT exist: {}".format(imgOut))

  #@unittest.skip("")
  def test_lines_mirror(self):
    imgOut = os.path.join(self.outFolder, "test_lines_mirror.png")

    p1 = complex(0, 500)
    p2 = complex(1000, 600)
    pyauto_base.imagemagick.drawLine(self.imgPath, p1, p2, "red", 8)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    p2 = complex(1000, 800)
    pyauto_base.imagemagick.drawLine(imgOut, p1, p2, "green", 6)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    p2 = complex(1000, 1000)
    pyauto_base.imagemagick.drawLine(imgOut, p1, p2, "blue", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)

    p1 = complex(500, 300)
    for i in range(3):
      rotCoef = pyauto_base.imagemagick.getRotationFactor(30 * i)
      p2 = (complex(200, 0) * rotCoef) + p1
      pyauto_base.imagemagick.drawLine(imgOut, p1, p2, "red", 2)
      pyauto_base.imagemagick.saveTempPic(imgOut)
    for i in range(3):
      rotCoef = pyauto_base.imagemagick.getRotationFactor((30 * i) + 90)
      p2 = (complex(200, 0) * rotCoef) + p1
      pyauto_base.imagemagick.drawLine(imgOut, p1, p2, "blue", 2)
      pyauto_base.imagemagick.saveTempPic(imgOut)
    for i in range(3):
      rotCoef = pyauto_base.imagemagick.getRotationFactor((30 * i) + 180)
      p2 = (complex(200, 0) * rotCoef) + p1
      pyauto_base.imagemagick.drawLine(imgOut, p1, p2, "yellow", 2)
      pyauto_base.imagemagick.saveTempPic(imgOut)
    for i in range(3):
      rotCoef = pyauto_base.imagemagick.getRotationFactor((30 * i) + 270)
      p2 = (complex(200, 0) * rotCoef) + p1
      pyauto_base.imagemagick.drawLine(imgOut, p1, p2, "green", 2)
      pyauto_base.imagemagick.saveTempPic(imgOut)

    pyauto_base.imagemagick.mirrorImg(imgOut)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    self.assertTrue(os.path.isfile(imgOut), "file DOES NOT exist: {}".format(imgOut))

  #@unittest.skip("")
  def test_shapes(self):
    imgOut = os.path.join(self.outFolder, "test_shapes.png")

    p1 = complex(200, 600)
    pyauto_base.imagemagick.drawCircle(self.imgPath, p1, 50, "red", 2, "yellow")
    pyauto_base.imagemagick.saveTempPic(imgOut)
    pyauto_base.imagemagick.drawCircle(imgOut, p1, 80, "green", 8)
    pyauto_base.imagemagick.saveTempPic(imgOut)

    p1 = complex(400, 300)
    p2 = p1 + complex(50, 100)
    pyauto_base.imagemagick.drawRect(imgOut, p1, p2, "orange", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    p1 = complex(500, 300)
    p2 = p1 + complex(50, 100)
    pyauto_base.imagemagick.drawRect(imgOut, p1, p2, "black", 6, "white")
    pyauto_base.imagemagick.saveTempPic(imgOut)

    p1 = complex(600, 500)
    p2 = p1 + complex(50, 100)
    pyauto_base.imagemagick.drawRRect(imgOut, p1, p2, "blue", 2)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    p1 = complex(700, 500)
    p2 = p1 + complex(50, 100)
    pyauto_base.imagemagick.drawRRect(imgOut, p1, p2, "magenta", 12, "cyan")
    pyauto_base.imagemagick.saveTempPic(imgOut)
    self.assertTrue(os.path.isfile(imgOut), "file DOES NOT exist: {}".format(imgOut))

  #@unittest.skip("")
  def test_text_path(self):
    imgOut = os.path.join(self.outFolder, "test_text_path.png")

    p1 = complex(200, 200)
    pyauto_base.imagemagick.drawText(self.imgPath, p1, "bottom right", "yellow", 36)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    p1 = complex(200, -200)
    pyauto_base.imagemagick.drawText(imgOut, p1, "top right", "magenta", 24)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    p1 = complex(-250, -250)
    pyauto_base.imagemagick.drawText(imgOut, p1, "top left", "red", 12)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    p1 = complex(-250, 250)
    pyauto_base.imagemagick.drawText(imgOut, p1, "bottom left", "blue", 16)
    pyauto_base.imagemagick.saveTempPic(imgOut)

    pts = [
        complex(300, 600),
        complex(350, 550),
        complex(400, 650),
        complex(450, 550),
        complex(500, 650),
        complex(550, 550),
        complex(600, 650),
        complex(650, 550),
        complex(700, 600)
    ]

    svgp = pyauto_base.svg.svgPath()
    for i, pt in enumerate(pts):
      if (i == 0):
        svgp.moveto(pt.real, pt.imag)
      else:
        svgp.lineto(pt.real, pt.imag)
    pyauto_base.imagemagick.drawPath(imgOut, svgp.path, "cyan", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    self.assertTrue(os.path.isfile(imgOut), "file DOES NOT exist: {}".format(imgOut))

  #@unittest.skip("")
  def test_arrow_cross(self):
    imgOut = os.path.join(self.outFolder, "test_arrow_cross.png")

    p1 = complex(300, 150)
    p2 = complex(300, 250)
    pyauto_base.imagemagick.drawLine(self.imgPath, p1, p2, "red", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    pyauto_base.imagemagick.drawArrow(imgOut, p1, p2, 20, "cyan", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    p1 = complex(150, 300)
    p2 = complex(250, 300)
    pyauto_base.imagemagick.drawLine(imgOut, p1, p2, "blue", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    pyauto_base.imagemagick.drawArrow(imgOut, p1, p2, 20, "cyan", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    p1 = complex(300, 450)
    p2 = complex(300, 350)
    pyauto_base.imagemagick.drawLine(imgOut, p1, p2, "red", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    pyauto_base.imagemagick.drawArrow(imgOut, p1, p2, 20, "magenta", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    p1 = complex(450, 300)
    p2 = complex(350, 300)
    pyauto_base.imagemagick.drawLine(imgOut, p1, p2, "blue", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    pyauto_base.imagemagick.drawArrow(imgOut, p1, p2, 20, "magenta", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)

    p1 = complex(450, 150)
    p2 = complex(350, 250)
    pyauto_base.imagemagick.drawLine(imgOut, p1, p2, "red", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    pyauto_base.imagemagick.drawArrow(imgOut, p1, p2, 20, "magenta", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    p1 = complex(150, 150)
    p2 = complex(250, 250)
    pyauto_base.imagemagick.drawLine(imgOut, p1, p2, "blue", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    pyauto_base.imagemagick.drawArrow(imgOut, p1, p2, 20, "cyan", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    p1 = complex(150, 450)
    p2 = complex(250, 350)
    pyauto_base.imagemagick.drawLine(imgOut, p1, p2, "red", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    pyauto_base.imagemagick.drawArrow(imgOut, p1, p2, 20, "cyan", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    p1 = complex(450, 450)
    p2 = complex(350, 350)
    pyauto_base.imagemagick.drawLine(imgOut, p1, p2, "blue", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)
    pyauto_base.imagemagick.drawArrow(imgOut, p1, p2, 20, "magenta", 4)
    pyauto_base.imagemagick.saveTempPic(imgOut)

    p1 = complex(700, 300)
    for i in range(8):
      p2 = 150.0 * complex(math.cos(i * (math.pi / 4)), math.sin(i * (math.pi / 4)))
      pyauto_base.imagemagick.drawLine(imgOut, p1, (p1 + p2), "green", 4)
      pyauto_base.imagemagick.saveTempPic(imgOut)
      pyauto_base.imagemagick.drawArrow(imgOut, p1, (p1 + p2), 20, "red", 4)
      pyauto_base.imagemagick.saveTempPic(imgOut)

    p1 = complex(300, 700)
    po1 = complex(-30, -30)
    po2 = complex(30, 30)
    for i in range(5):
      ptl = (p1 + complex((100 * i), 0)) + po1
      pbr = (p1 + complex((100 * i), 0)) + po2
      pyauto_base.imagemagick.drawCross(imgOut, ptl, pbr, "yellow" if
                                        ((i % 2) == 0) else "orange", 6)
      pyauto_base.imagemagick.saveTempPic(imgOut)

    self.assertTrue(os.path.isfile(imgOut), "file DOES NOT exist: {}".format(imgOut))

  #@unittest.skip("")
  def test_rectangle(self):
    imgOut = os.path.join(self.outFolder, "test_rectangle.png")

    p1 = complex(500, 300)
    pyauto_base.imagemagick.drawLine(self.imgPath, p1, p1, "red", 0)
    pyauto_base.imagemagick.saveTempPic(imgOut)

    for i in range(4):
      cnt = complex(200, (200 * (i + 1)))
      pyauto_base.imagemagick.drawRectGeneric(imgOut, cnt, 100, 20, (30 * i), "red", 4)
      pyauto_base.imagemagick.saveTempPic(imgOut)
    for i in range(4):
      cnt = complex(400, (200 * (i + 1)))
      pyauto_base.imagemagick.drawRectGeneric(imgOut, cnt, 100, 20, ((30 * i) + 90), "blue",
                                              4)
      pyauto_base.imagemagick.saveTempPic(imgOut)
    for i in range(4):
      cnt = complex(600, (200 * (i + 1)))
      pyauto_base.imagemagick.drawRectGeneric(imgOut, cnt, 100, 20, ((30 * i) + 180),
                                              "yellow", 4)
      pyauto_base.imagemagick.saveTempPic(imgOut)
    for i in range(4):
      cnt = complex(800, (200 * (i + 1)))
      pyauto_base.imagemagick.drawRectGeneric(imgOut, cnt, 100, 20, ((30 * i) + 270),
                                              "green", 4)
      pyauto_base.imagemagick.saveTempPic(imgOut)
    self.assertTrue(os.path.isfile(imgOut), "file DOES NOT exist: {}".format(imgOut))

class Test_montage(unittest.TestCase):
  cwd = ""
  lclDir = ""
  imgName = 2 * [
      "test_lines_mirror.png", "test_shapes.png", "test_text_path.png",
      "test_arrow_cross.png"
  ]

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    if (skipAll):
      self.skipTest("")

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_join(self):
    imgIn = [os.path.join(self.outFolder, t) for t in self.imgName]
    imgOut = [
        os.path.join(self.outFolder, "test_join_1x2.png"),
        os.path.join(self.outFolder, "test_join_2x1.png"),
        os.path.join(self.outFolder, "test_join_3x1.png"),
        os.path.join(self.outFolder, "test_join_4x1.png"),
        os.path.join(self.outFolder, "test_join_2x2.png"),
        os.path.join(self.outFolder, "test_join_5x1.png"),
        os.path.join(self.outFolder, "test_join_6x1.png"),
        os.path.join(self.outFolder, "test_join-2x3.png"),
    ]

    pyauto_base.imagemagick.join2Img_1r2c(imgIn[:2], 2)
    pyauto_base.imagemagick.saveTempPic(imgOut[0])
    pyauto_base.imagemagick.join2Img_2r1c(imgIn[:2], 2)
    pyauto_base.imagemagick.saveTempPic(imgOut[1])
    pyauto_base.imagemagick.join3Img_3r1c(imgIn[:3], 2)
    pyauto_base.imagemagick.saveTempPic(imgOut[2])
    pyauto_base.imagemagick.join4Img_4r1c(imgIn[:4], 2)
    pyauto_base.imagemagick.saveTempPic(imgOut[3])
    pyauto_base.imagemagick.join4Img_2r2c(imgIn[:4], 2)
    pyauto_base.imagemagick.saveTempPic(imgOut[4])
    pyauto_base.imagemagick.join5Img_5r1c(imgIn[:5], 2)
    pyauto_base.imagemagick.saveTempPic(imgOut[5])
    pyauto_base.imagemagick.join6Img_6r1c(imgIn[:6], 2)
    pyauto_base.imagemagick.saveTempPic(imgOut[6])
    pyauto_base.imagemagick.join6Img_2r3c(imgIn[:6], 2)
    pyauto_base.imagemagick.saveTempPic(imgOut[7])

    for path in imgOut:
      self.assertTrue(os.path.isfile(path), "file DOES NOT exist: {}".format(path))
