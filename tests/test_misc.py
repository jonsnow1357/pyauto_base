#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_base.misc"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import random
import unittest
import xml.etree.ElementTree as ET

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_base.tests.tools as tt

regexDate = r"^[0-9]{4,}-[0-9][0-9]-[0-9][0-9]$"
regexTime1 = r"^[0-9][0-9]:[0-9][0-9]:[0-9][0-9]$"
regexTime2 = r"^[0-9][0-9]:[0-9][0-9]:[0-9][0-9]\.[0-9]{6,}$"
#regexTime3 = r"^[0-9][0-9]-[0-9][0-9]-[0-9][0-9]$"
#regexTime4 = r"^[0-9][0-9]-[0-9][0-9]-[0-9][0-9]\.[0-9]{6,}$"
regexDateTime1 = r"^[0-9]{4,}-[0-9][0-9]-[0-9][0-9]T[0-9]{6}$"
regexDateTime2 = r"^[0-9]{4,}-[0-9][0-9]-[0-9][0-9]T[0-9]{6}Z$"
regexTimestamp1 = r"^[0-9]{4,}-[0-9][0-9]-[0-9][0-9]T[0-9][0-9]:[0-9][0-9]:[0-9][0-9]$"
regexTimestamp2 = r"^[0-9]{4,}-[0-9][0-9]-[0-9][0-9]T[0-9][0-9]:[0-9][0-9]:[0-9][0-9]Z$"

nTests = 100

class Test_misc(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_getDate(self):
    tmp = pyauto_base.misc.getDate()
    logger.info(tmp)
    self.assertNotEqual(re.match(regexDate, tmp), None)

    tmp = pyauto_base.misc.getDate(bUTC=True)
    logger.info(tmp)
    self.assertNotEqual(re.match(regexDate, tmp), None)

  #@unittest.skip("")
  def test_getTime(self):
    tmp = pyauto_base.misc.getTime()
    logger.info(tmp)
    self.assertNotEqual(re.match(regexTime2, tmp), None)

    tmp = pyauto_base.misc.getTime(bUTC=True)
    logger.info(tmp)
    self.assertNotEqual(re.match(regexTime2, tmp), None)

  #@unittest.skip("")
  def test_getDateTime(self):
    tmp = pyauto_base.misc.getDateTime()
    logger.info(tmp)
    self.assertNotEqual(re.match(regexDateTime1, tmp), None)

    tmp = pyauto_base.misc.getDateTime(bUTC=True)
    logger.info(tmp)
    self.assertNotEqual(re.match(regexDateTime2, tmp), None)

  #@unittest.skip("")
  def test_getTimestamp(self):
    tmp = pyauto_base.misc.getTimestamp()
    logger.info(tmp)
    self.assertNotEqual(re.match(regexTimestamp1, tmp), None)

    tmp = pyauto_base.misc.getTimestamp(bUTC=True)
    logger.info(tmp)
    self.assertNotEqual(re.match(regexTimestamp2, tmp), None)

  #@unittest.skip("")
  def test_getRndFloat(self):
    res = pyauto_base.misc.getRndFloat(1.5, 1.5)
    self.assertEqual(res, 1.5)

    for i in range(nTests):
      res = pyauto_base.misc.getRndFloat(2.0, 6.5)
      self.assertGreaterEqual(res, 2.0)
      self.assertLess(res, 6.5)

      res = pyauto_base.misc.getRndFloat(4.5, 1.2)
      self.assertGreaterEqual(res, 1.2)
      self.assertLess(res, 4.5)

      res = pyauto_base.misc.getRndFloat(-0.5, 2.2)
      self.assertGreaterEqual(res, -0.5)
      self.assertLess(res, 2.2)

      res = pyauto_base.misc.getRndFloat(-3.1, 1.6)
      self.assertGreaterEqual(res, -3.1)
      self.assertLess(res, 1.6)

      res = pyauto_base.misc.getRndFloat(-10.0, -5.2)
      self.assertGreaterEqual(res, -10.0)
      self.assertLess(res, -5.2)

      res = pyauto_base.misc.getRndFloat(-4.7, -8.8)
      self.assertGreaterEqual(res, -8.8)
      self.assertLess(res, -4.7)

  #@unittest.skip("")
  # def test_chkEqualLists(self):
  #   row1 = ["asdkh", "ueyrwioh", "skdjoewijf", "ksdfj kasj", "powie"]
  #   row2 = ["asdkh", "ueyrwioh", "skdjoewijf", "ksdfj kasj", "powie"]
  #   self.assertTrue(pyauto_base.misc.chkEqualLists(row1, row2))
  #   row2 = ["asdkh", "ueyrwioh", "skdjoewijf", "ksdfj kasj", "powiehhh"]
  #   self.assertFalse(pyauto_base.misc.chkEqualLists(row1, row2))
  #   row2 = ["asdkh", "ueyrwioh", "skdjoewijf", "ksdfj kasj", "powie", "yyreurhe"]
  #   self.assertFalse(pyauto_base.misc.chkEqualLists(row1, row2))

  #@unittest.skip("")
  def test_getListIndex(self):
    lst = ["val1", "val4", "val16", "val2"]
    res = pyauto_base.misc.getListIndex(lst)
    self.assertCountEqual(res, {"val1": 0, "val4": 1, "val16": 2, "val2": 3})

  #@unittest.skip("")
  def test_getListIndexByRegex(self):
    names = ["col1", "col2", "col3", "col4"]
    regex = {"index1": ".*1", "index4": "c.*4", "index3": "co.3"}
    res = pyauto_base.misc.getListIndexByRegex(names, regex)
    self.assertCountEqual(res, {"index1": 0, "index4": 3, "index3": 2})

  #@unittest.skip("")
  def test_getListIndexByName(self):
    names = ["col1", "col2", "col3", "col4"]

    with self.assertRaises(RuntimeError):
      pyauto_base.misc.getListIndexByName(names, [])

    req = ["col1", "col4"]
    opt = ["col2", "col8", "col13"]
    res = pyauto_base.misc.getListIndexByName(names, req)
    self.assertCountEqual(res, {"col1": 0, "col4": 3})

    res = pyauto_base.misc.getListIndexByName(names, req, opt)
    self.assertCountEqual(res, {"col1": 0, "col4": 3, "col2": 1})

    req = ["col1", "col4"]
    opt = ["col1", "col2"]
    with self.assertRaises(RuntimeError):
      pyauto_base.misc.getListIndexByName(names, req, opt)

  #@unittest.skip("")
  def test_hasAllKeys(self):
    d = {"k1": "v1", "k2": "v2", "k5": None}
    self.assertTrue(pyauto_base.misc.hasAllKeys(d, ["k1", "k2"]))
    self.assertFalse(pyauto_base.misc.hasAllKeys(d, ["k1", "k2", "k3"]))

    self.assertTrue(pyauto_base.misc.hasAllKeys(d, ["k1", "k2", "k5"]))
    self.assertFalse(pyauto_base.misc.hasAllKeys(d, ["k1", "k2", "k5"], bEmptyValues=False))

  #@unittest.skip("")
  # def test_compareLists(self):
  #   self.assertEqual(pyauto_base.misc.compareLists([], 1), -2)
  #   self.assertEqual(pyauto_base.misc.compareLists("a", []), -2)
  #
  #   self.assertEqual(pyauto_base.misc.compareLists([], []), 0)
  #   self.assertEqual(pyauto_base.misc.compareLists(["a"], ["a"]), 0)
  #
  #   self.assertEqual(pyauto_base.misc.compareLists(["a"], ["1"]), 2)
  #   self.assertEqual(pyauto_base.misc.compareLists(["1"], ["a"]), 2)
  #   self.assertEqual(pyauto_base.misc.compareLists(["a", "b"], ["a"]), 1)
  #   self.assertEqual(pyauto_base.misc.compareLists(["a"], ["a", "b"]), -1)

  #@unittest.skip("")
  def test_chkXMLElementHasTag(self):
    f_in_path = os.path.join(self.inFolder, "test_read.xml")
    dom = ET.parse(f_in_path)
    root = dom.getroot()

    self.assertTrue(pyauto_base.misc.chkXMLElementHasTag(root, "tag1"))
    self.assertFalse(pyauto_base.misc.chkXMLElementHasTag(root, "notag"))

  #@unittest.skip("")
  def test_chkXMLElementHasAttr(self):
    f_in_path = os.path.join(self.inFolder, "test_read.xml")
    dom = ET.parse(f_in_path)
    root = dom.getroot()
    el_tag1 = root.find("tag1")

    self.assertTrue(pyauto_base.misc.chkXMLElementHasAttr(el_tag1, "key1"))
    self.assertTrue(pyauto_base.misc.chkXMLElementHasAttr(el_tag1, "key1_1"))
    self.assertFalse(pyauto_base.misc.chkXMLElementHasAttr(el_tag1, "noattr"))

  #@unittest.skip("")
  def test_chkXMLElementHasAttrWithVal(self):
    f_in_path = os.path.join(self.inFolder, "test_read.xml")
    dom = ET.parse(f_in_path)
    root = dom.getroot()
    el_tag1 = root.find("tag1")

    self.assertTrue(pyauto_base.misc.chkXMLElementHasAttrWithVal(el_tag1, "key1"))
    self.assertFalse(pyauto_base.misc.chkXMLElementHasAttrWithVal(el_tag1, "key1_1"))

  #@unittest.skip("")
  def test_chkXMLElementHasText(self):
    f_in_path = os.path.join(self.inFolder, "test_read.xml")
    dom = ET.parse(f_in_path)
    root = dom.getroot()
    el_tag0 = root.find("tag0")
    el_tag2 = root.find("tag2")

    self.assertFalse(pyauto_base.misc.chkXMLElementHasText(el_tag0))
    self.assertTrue(pyauto_base.misc.chkXMLElementHasText(el_tag2))

  #@unittest.skip("")
  def test_StatCounter(self):
    cnt = pyauto_base.misc.StatCounter()
    self.assertEqual(cnt.test("pass"), False)
    self.assertEqual(cnt.test("fail"), False)

    nPass = random.randint(10, 20)
    nFail = random.randint(10, 20)
    for i in range(nPass):
      cnt.inc("pass")
    for i in range(nFail):
      cnt.inc("fail")
    logger.info(cnt)
    self.assertEqual(cnt.get("pass"), [nPass, (nPass + nFail)])
    self.assertEqual(cnt.get("fail"), [nFail, (nPass + nFail)])
    self.assertEqual(cnt.test("pass"), False)
    self.assertEqual(cnt.test("fail"), False)

    cnt.reset()
    self.assertEqual(cnt.get("pass"), [0, 0])
    self.assertEqual(cnt.get("fail"), [0, 0])
    cnt.reset(hard=True)
    self.assertEqual(cnt.test("pass"), False)
    self.assertEqual(cnt.test("fail"), False)

    nPass = random.randint(10, 20)
    for i in range(nPass):
      cnt.inc("pass")
    logger.info(cnt)
    self.assertEqual(cnt.get("pass"), [nPass, nPass])
    self.assertEqual(cnt.test("pass"), True)

    cnt.inc("fail")
    self.assertEqual(cnt.test("pass"), False)

class Test_misc_math(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_chkInt(self):
    self.assertEqual(pyauto_base.misc.chkInt(0), 0)
    self.assertEqual(pyauto_base.misc.chkInt(-25), -25)
    self.assertEqual(pyauto_base.misc.chkInt(6), 6)
    self.assertEqual(pyauto_base.misc.chkInt(1), 1)
    pyauto_base.misc.chkInt(0, minVal=-1)
    pyauto_base.misc.chkInt(0, maxVal=1)
    pyauto_base.misc.chkInt(0, minVal=-2, maxVal=2)

    #with self.assertRaises(TypeError):
    #  pyauto_base.misc.chkInt(None)
    #with self.assertRaises(ValueError):
    #  pyauto_base.misc.chkInt("asdf")
    with self.assertRaises(RuntimeError):
      pyauto_base.misc.chkInt(-2, minVal=7)
    with self.assertRaises(RuntimeError):
      pyauto_base.misc.chkInt(-2, maxVal=-5)
    with self.assertRaises(RuntimeError):
      pyauto_base.misc.chkInt(-2, minVal=-1, maxVal=3)

  #@unittest.skip("")
  def test_chkFloat(self):
    self.assertEqual(pyauto_base.misc.chkFloat(0), 0.0)
    self.assertEqual(pyauto_base.misc.chkFloat(-12.25), -12.25)
    self.assertEqual(pyauto_base.misc.chkFloat(2.6), 2.6)
    self.assertEqual(pyauto_base.misc.chkFloat(1.5), 1.5)
    pyauto_base.misc.chkFloat(0.1, minVal=-1.0)
    pyauto_base.misc.chkFloat(0.1, maxVal=1.0)
    pyauto_base.misc.chkFloat(0.1, minVal=-0.5, maxVal=0.8)

    #with self.assertRaises(TypeError):
    #  pyauto_base.misc.chkFloat(None)
    #with self.assertRaises(ValueError):
    #  pyauto_base.misc.chkFloat("qwer")
    with self.assertRaises(RuntimeError):
      pyauto_base.misc.chkFloat(1.0, minVal=10.0)
    with self.assertRaises(RuntimeError):
      pyauto_base.misc.chkFloat(1.0, maxVal=-2.0)
    with self.assertRaises(RuntimeError):
      pyauto_base.misc.chkFloat(1.0, minVal=2.0, maxVal=3.0)

  #@unittest.skip("")
  def test_getRndStr(self):
    self.assertEqual(len(pyauto_base.misc.getRndStr(n=0)), 0)
    self.assertEqual(pyauto_base.misc.getRndStr(n=0), "")

    for _ in range(nTests):
      n = random.randint(1, 2048)
      self.assertEqual(len(pyauto_base.misc.getRndStr(n=n)), n)

    for _ in range(nTests):
      n = random.randint(1, 2048)
      tmp = pyauto_base.misc.getRndStr_Pat(prefix="PRE", maxLen=n)
      self.assertLessEqual(len(tmp), (n + 3))
      self.assertIsNotNone(re.match(r"PRE[0-9]+", tmp))
      tmp = pyauto_base.misc.getRndStr_Pat(prefix="PRE", maxLen=n, bNum=False)
      self.assertLessEqual(len(tmp), (n + 3))
      self.assertIsNotNone(re.match(r"PRE[0-9a-zA-Z]+", tmp))

  #@unittest.skip("")
  def test_valueWithUnit(self):
    v_unit = pyauto_base.misc.valueWithUnit(1, "m")
    self.assertEqual(v_unit.magnitude, 1)
    self.assertEqual(v_unit.units, "meter")

    v_unit = pyauto_base.misc.valueWithUnit(0.7, "V")
    self.assertEqual(v_unit.magnitude, 0.7)
    self.assertEqual(v_unit.units, "volt")

    v_unit = pyauto_base.misc.valueWithUnit(16.16, "mil")
    self.assertEqual(v_unit.magnitude, 16.16)
    self.assertEqual(v_unit.units, "thou")

  #@unittest.skip("")
  def test_ADC_DAC(self):
    self.assertEqual(pyauto_base.misc.hex2float("0x00", 8, 2.5), 0.0)
    self.assertEqual(pyauto_base.misc.hex2float("0xFF", 8, 2.5), 2.490234375)

    self.assertEqual(pyauto_base.misc.hex2float("0x1", 12, 2.5), 0.0006103515625)
    self.assertEqual(pyauto_base.misc.hex2float("0x10", 12, 2.5), 0.009765625)

    self.assertEqual(pyauto_base.misc.float2hex(0.0, 8, 2.5), "0x00")
    self.assertEqual(pyauto_base.misc.float2hex(2.5, 8, 2.5), "0xFF")
    self.assertEqual(pyauto_base.misc.float2hex(3.2, 8, 2.5), "0xFF")

    self.assertEqual(pyauto_base.misc.float2hex(0.1, 12, 2.5), "0x0A4")
    self.assertEqual(pyauto_base.misc.float2hex(1.56, 12, 2.5), "0x9FB")

  #@unittest.skip("")
  def test_val2dB(self):
    self.assertEqual(pyauto_base.misc.val2dB(1.0e-3), 0.0)
    self.assertEqual(pyauto_base.misc.val2dB(0.5e-3), -3.010299956639812)
    self.assertEqual(pyauto_base.misc.val2dB(2.0e-3), 3.010299956639812)
    self.assertEqual(pyauto_base.misc.val2dB(1.0, refLvl=1.0), 0.0)
    self.assertEqual(pyauto_base.misc.val2dB(0.5, refLvl=1.0), -3.010299956639812)
    self.assertEqual(pyauto_base.misc.val2dB(2.0, refLvl=1.0), 3.010299956639812)

  #@unittest.skip("")
  def test_dB2val(self):
    self.assertEqual(pyauto_base.misc.dB2val(0.0), 1.0e-3)
    self.assertEqual(pyauto_base.misc.dB2val(0.0, refLvl=1.0), 1.0)

class Test_SparseMatrix(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    m = pyauto_base.misc.SparseMatrix(4, 3)
    m.set(1, 0, 5)
    m.set(1, 1, 8)
    m.set(2, 2, 3)
    m.set(3, 1, 6)
    #print("DBG {}\n     {}\n     {}".format(m._v, m._r, m._c))

    sh = m.shape()
    print("matrix by rows:")
    for i in range(sh[0]):
      print(m.getRow(i))
    print("matrix by cols:")
    for i in range(sh[1]):
      print(m.getCol(i))

    self.assertEqual(sh, [4, 3])
    self.assertEqual(m.nValues, 4)
    self.assertEqual(m.get(0, 0), 0)
    self.assertEqual(m.get(0, 1), 0)
    self.assertEqual(m.get(1, 0), 5)
    self.assertEqual(m.get(1, 1), 8)
    self.assertEqual(m.get(2, 1), 0)
    self.assertEqual(m.get(2, 2), 3)
    self.assertEqual(m.get(3, 1), 6)
    self.assertEqual(m.get(3, 2), 0)

    m.clear()
    self.assertEqual(sh, [4, 3])
    self.assertEqual(m.nValues, 0)

    m = pyauto_base.misc.SparseMatrix(4, 6)
    m.set(0, 0, 1)
    m.set(0, 1, 2)
    m.set(1, 1, 3)
    m.set(1, 3, 4)
    m.set(2, 2, 5)
    m.set(2, 3, 6)
    m.set(2, 4, 7)
    m.set(3, 5, 8)

    sh = m.shape()
    print("matrix by rows:")
    for i in range(sh[0]):
      print(m.getRow(i))
    print("matrix by cols:")
    for i in range(sh[1]):
      print(m.getCol(i))

    self.assertEqual(sh, [4, 6])
    self.assertEqual(m.nValues, 8)
    self.assertEqual(m.get(0, 0), 1)
    self.assertEqual(m.get(0, 1), 2)
    self.assertEqual(m.get(1, 0), 0)
    self.assertEqual(m.get(1, 1), 3)
    self.assertEqual(m.get(1, 2), 0)
    self.assertEqual(m.get(1, 3), 4)
    self.assertEqual(m.get(2, 2), 5)
    self.assertEqual(m.get(2, 3), 6)
    self.assertEqual(m.get(2, 4), 7)
    self.assertEqual(m.get(2, 5), 0)
    self.assertEqual(m.get(3, 4), 0)
    self.assertEqual(m.get(3, 5), 8)

    m.clear()
    self.assertEqual(sh, [4, 6])
    self.assertEqual(m.nValues, 0)

  def test_keepMax(self):
    m = pyauto_base.misc.SparseMatrix(3, 5)
    m.set(0, 0, -3)
    m.set(1, 0, -8)
    m.set(2, 1, 4)
    m.set(2, 2, 6)
    m.set(2, 3, 8)
    m.set(0, 4, 6)
    m.set(1, 4, 5)

    sh = m.shape()
    print("matrix by rows:")
    for i in range(sh[0]):
      print(m.getRow(i))

    m.keepMax_row()
    print("matrix by rows:")
    for i in range(sh[0]):
      print(m.getRow(i))

    self.assertEqual(m.nValues, 3)
    self.assertEqual(m.get(0, 0), 0)
    self.assertEqual(m.get(1, 0), 0)
    self.assertEqual(m.get(2, 1), 0)
    self.assertEqual(m.get(2, 2), 0)
    self.assertEqual(m.get(2, 3), 8)
    self.assertEqual(m.get(0, 4), 6)
    self.assertEqual(m.get(1, 4), 5)

    m = pyauto_base.misc.SparseMatrix(3, 5)
    m.set(0, 0, -3)
    m.set(1, 0, -8)
    m.set(2, 1, 4)
    m.set(2, 2, 6)
    m.set(2, 3, 8)
    m.set(0, 4, 6)
    m.set(1, 4, 5)

    sh = m.shape()
    print("matrix by rows:")
    for i in range(sh[0]):
      print(m.getRow(i))

    m.keepMax_col()
    print("matrix by rows:")
    for i in range(sh[0]):
      print(m.getRow(i))

    self.assertEqual(m.nValues, 4)
    self.assertEqual(m.get(0, 0), 0)
    self.assertEqual(m.get(1, 0), 0)
    self.assertEqual(m.get(2, 1), 4)
    self.assertEqual(m.get(2, 2), 6)
    self.assertEqual(m.get(2, 3), 8)
    self.assertEqual(m.get(0, 4), 6)
    self.assertEqual(m.get(1, 4), 0)

    m = pyauto_base.misc.SparseMatrix(3, 5)
    m.set(0, 0, -3)
    m.set(1, 0, -8)
    m.set(2, 1, 7)
    m.set(2, 2, 6)
    m.set(2, 3, 7)
    m.set(0, 4, 2)
    m.set(1, 4, 2)

    sh = m.shape()
    print("matrix by rows:")
    for i in range(sh[0]):
      print(m.getRow(i))

    m.keepMax_row()
    print("matrix by rows:")
    for i in range(sh[0]):
      print(m.getRow(i))

    self.assertEqual(m.nValues, 4)
    self.assertEqual(m.get(0, 0), 0)
    self.assertEqual(m.get(1, 0), 0)
    self.assertEqual(m.get(2, 1), 7)
    self.assertEqual(m.get(2, 2), 0)
    self.assertEqual(m.get(2, 3), 7)
    self.assertEqual(m.get(0, 4), 2)
    self.assertEqual(m.get(1, 4), 2)

    m = pyauto_base.misc.SparseMatrix(3, 5)
    m.set(0, 0, -3)
    m.set(1, 0, -3)
    m.set(2, 1, 4)
    m.set(2, 2, 6)
    m.set(2, 3, 8)
    m.set(0, 4, 6)
    m.set(1, 4, 5)

    sh = m.shape()
    print("matrix by rows:")
    for i in range(sh[0]):
      print(m.getRow(i))

    m.keepMax_col()
    print("matrix by rows:")
    for i in range(sh[0]):
      print(m.getRow(i))

    self.assertEqual(m.nValues, 4)
    self.assertEqual(m.get(0, 0), 0)
    self.assertEqual(m.get(1, 0), 0)
    self.assertEqual(m.get(2, 1), 4)
    self.assertEqual(m.get(2, 2), 6)
    self.assertEqual(m.get(2, 3), 8)
    self.assertEqual(m.get(0, 4), 6)
    self.assertEqual(m.get(1, 4), 0)

class Test_misc_string(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_isEmptyString(self):
    self.assertTrue(pyauto_base.misc.isEmptyString(None))
    self.assertTrue(pyauto_base.misc.isEmptyString(u""))
    self.assertTrue(pyauto_base.misc.isEmptyString(""))
    self.assertTrue(pyauto_base.misc.isEmptyString(''))
    #self.assertTrue(pyauto_base.misc.isEmptyString(0))
    #self.assertTrue(pyauto_base.misc.isEmptyString(5.7))
    #self.assertTrue(pyauto_base.misc.isEmptyString(["asdf"]))
    #self.assertTrue(pyauto_base.misc.isEmptyString({"qwery": "asdf"}))

    self.assertFalse(pyauto_base.misc.isEmptyString("hello"))
    self.assertFalse(pyauto_base.misc.isEmptyString(u"hello"))

  #@unittest.skip("")
  def test_isNumberOrString(self):
    #self.assertFalse(pyauto_base.misc.isNumberOrString(None))
    self.assertFalse(pyauto_base.misc.isNumberOrString(""))
    self.assertFalse(pyauto_base.misc.isNumberOrString(u""))
    #self.assertFalse(pyauto_base.misc.isNumberOrString([]))

    self.assertTrue(pyauto_base.misc.isNumberOrString(0))
    self.assertTrue(pyauto_base.misc.isNumberOrString(1))
    self.assertTrue(pyauto_base.misc.isNumberOrString(-1))
    self.assertTrue(pyauto_base.misc.isNumberOrString(0.0))
    self.assertTrue(pyauto_base.misc.isNumberOrString(1.5))
    self.assertTrue(pyauto_base.misc.isNumberOrString(-1.5))
    self.assertTrue(pyauto_base.misc.isNumberOrString("hello"))
    self.assertTrue(pyauto_base.misc.isNumberOrString(u"hello"))

  #@unittest.skip("")
  def test_isTrueString(self):
    #self.assertFalse(pyauto_base.misc.isTrueString(None))
    self.assertFalse(pyauto_base.misc.isTrueString(""))
    self.assertFalse(pyauto_base.misc.isTrueString("no"))
    self.assertFalse(pyauto_base.misc.isTrueString("Bye"))
    self.assertFalse(pyauto_base.misc.isTrueString("#ambiguous"))

    self.assertTrue(pyauto_base.misc.isTrueString("1"))
    self.assertTrue(pyauto_base.misc.isTrueString("true"))
    self.assertTrue(pyauto_base.misc.isTrueString("TRue"))
    self.assertTrue(pyauto_base.misc.isTrueString("ok"))
    self.assertTrue(pyauto_base.misc.isTrueString("OK"))
    self.assertTrue(pyauto_base.misc.isTrueString("YEAH"))

  #@unittest.skip("")
  def test_compareStrings(self):
    str1 = "dsfjljdsf"

    str2 = "clsdnclks"
    self.assertEqual(pyauto_base.misc.compareStrings(str1, str2), "no")

    str2 = "dsfjljdsf"
    self.assertEqual(pyauto_base.misc.compareStrings(str1, str2), "yes")
    str2 = "DSFjljdsf"
    self.assertEqual(pyauto_base.misc.compareStrings(str1, str2), "no")
    str2 = "DSFjljdsf"
    self.assertEqual(pyauto_base.misc.compareStrings(str1, str2, bCase=False), "yes")
    str2 = "DSFjljdsf"
    self.assertEqual(pyauto_base.misc.compareStrings(str1, str2, fuzzyTh=0.6), "fuzzy")

    str2 = "dsfjljds?"
    self.assertEqual(pyauto_base.misc.compareStrings(str1, str2), "fuzzy")
    str2 = "dsfjljd??"
    self.assertEqual(pyauto_base.misc.compareStrings(str1, str2), "no")

  #@unittest.skip("")
  def test_convertStringNonPrint(self):
    self.assertEqual("hello<LF>", pyauto_base.misc.convertStringNonPrint("hello\n"))
    self.assertEqual("hello<CR><LF>", pyauto_base.misc.convertStringNonPrint("hello\r\n"))
    self.assertEqual("<TAB>hello<LF>", pyauto_base.misc.convertStringNonPrint("\thello\n"))
    self.assertEqual("helm<BS>lo<LF>", pyauto_base.misc.convertStringNonPrint("helm\blo\n"))

  #@unittest.skip("")
  def test_parseProtocolString(self):
    dict1 = pyauto_base.misc.parseProtocolString("file:///path/to/folder")
    dict2 = {
        "protocol": "file",
        "user": None,
        "pswd": None,
        "hostname": None,
        "port": None,
        "path": "path/to/folder"
    }
    self.assertEqual(dict1, dict2)
    dict1 = pyauto_base.misc.parseProtocolString("file://hostname/path/to/folder")
    dict2 = {
        "protocol": "file",
        "user": None,
        "pswd": None,
        "hostname": "hostname",
        "port": None,
        "path": "path/to/folder"
    }
    self.assertEqual(dict1, dict2)
    dict1 = pyauto_base.misc.parseProtocolString("file://hostname:port/path/to/folder")
    dict2 = {
        "protocol": "file",
        "user": None,
        "pswd": None,
        "hostname": "hostname",
        "port": "port",
        "path": "path/to/folder"
    }
    self.assertEqual(dict1, dict2)

    dict1 = pyauto_base.misc.parseProtocolString("file:////path/to/folder")
    dict2 = {
        "protocol": "file",
        "user": None,
        "pswd": None,
        "hostname": None,
        "port": None,
        "path": "/path/to/folder"
    }
    self.assertEqual(dict1, dict2)
    dict1 = pyauto_base.misc.parseProtocolString("file:///C:/path/to/folder")
    dict2 = {
        "protocol": "file",
        "user": None,
        "pswd": None,
        "hostname": None,
        "port": None,
        "path": "C:/path/to/folder"
    }
    self.assertEqual(dict1, dict2)

    dict1 = pyauto_base.misc.parseProtocolString("file://user@hostname/path/to/folder")
    dict2 = {
        "protocol": "file",
        "user": "user",
        "pswd": None,
        "hostname": "hostname",
        "port": None,
        "path": "path/to/folder"
    }
    self.assertEqual(dict1, dict2)
    dict1 = pyauto_base.misc.parseProtocolString("file://user:pswd@hostname/path/to/folder")
    dict2 = {
        "protocol": "file",
        "user": "user",
        "pswd": "pswd",
        "hostname": "hostname",
        "port": None,
        "path": "path/to/folder"
    }
    self.assertEqual(dict1, dict2)
    dict1 = pyauto_base.misc.parseProtocolString("file://user@hostname:port/path/to/folder")
    dict2 = {
        "protocol": "file",
        "user": "user",
        "pswd": None,
        "hostname": "hostname",
        "port": "port",
        "path": "path/to/folder"
    }
    self.assertEqual(dict1, dict2)
    dict1 = pyauto_base.misc.parseProtocolString(
        "file://user:pswd@hostname:port/path/to/folder")
    dict2 = {
        "protocol": "file",
        "user": "user",
        "pswd": "pswd",
        "hostname": "hostname",
        "port": "port",
        "path": "path/to/folder"
    }
    self.assertEqual(dict1, dict2)

    dict1 = pyauto_base.misc.parseProtocolString("http://hostname/")
    dict2 = {
        "protocol": "http",
        "user": None,
        "pswd": None,
        "hostname": "hostname",
        "port": None,
        "path": None
    }
    self.assertEqual(dict1, dict2)
    dict1 = pyauto_base.misc.parseProtocolString("http://hostname:port/")
    dict2 = {
        "protocol": "http",
        "user": None,
        "pswd": None,
        "hostname": "hostname",
        "port": "port",
        "path": None
    }
    self.assertEqual(dict1, dict2)
    dict1 = pyauto_base.misc.parseProtocolString("http://user@hostname/")
    dict2 = {
        "protocol": "http",
        "user": "user",
        "pswd": None,
        "hostname": "hostname",
        "port": None,
        "path": None
    }
    self.assertEqual(dict1, dict2)
    dict1 = pyauto_base.misc.parseProtocolString("http://user:pswd@hostname/")
    dict2 = {
        "protocol": "http",
        "user": "user",
        "pswd": "pswd",
        "hostname": "hostname",
        "port": None,
        "path": None
    }
    self.assertEqual(dict1, dict2)
    dict1 = pyauto_base.misc.parseProtocolString("http://user:pswd@hostname:port/")
    dict2 = {
        "protocol": "http",
        "user": "user",
        "pswd": "pswd",
        "hostname": "hostname",
        "port": "port",
        "path": None
    }
    self.assertEqual(dict1, dict2)

  #@unittest.skip("")
  def test_parseRangeString(self):
    self.assertEqual(pyauto_base.misc.parseRangeString("1,2,4,5"), ["1", "2", "4", "5"])
    self.assertEqual(pyauto_base.misc.parseRangeString("4,2,1,5"), ["1", "2", "4", "5"])
    self.assertEqual(pyauto_base.misc.parseRangeString("6-9"), ["6", "7", "8", "9"])
    self.assertEqual(pyauto_base.misc.parseRangeString("11-13,15,20"),
                     ["11", "12", "13", "15", "20"])
    self.assertEqual(pyauto_base.misc.parseRangeString("20,15,11-13"),
                     ["11", "12", "13", "15", "20"])

    self.assertEqual(pyauto_base.misc.parseRangeString("R1,R2,R4,R5"),
                     ["R1", "R2", "R4", "R5"])
    self.assertEqual(pyauto_base.misc.parseRangeString("R1, R2, R4, R5"),
                     ["R1", "R2", "R4", "R5"])
    self.assertEqual(pyauto_base.misc.parseRangeString("TP6-TP9"),
                     ["TP6", "TP7", "TP8", "TP9"])
    self.assertEqual(pyauto_base.misc.parseRangeString("U20,U15,U11-U13"),
                     ["U11", "U12", "U13", "U15", "U20"])
    self.assertEqual(pyauto_base.misc.parseRangeString("U20 ,  U15, U11 - U13"),
                     ["U11", "U12", "U13", "U15", "U20"])

  #@unittest.skip("")
  def test_parseTimedeltaString(self):
    self.assertEqual(pyauto_base.misc.parseTimedeltaString("1"),
                     datetime.timedelta(minutes=1))
    self.assertEqual(pyauto_base.misc.parseTimedeltaString("2.5"),
                     datetime.timedelta(minutes=2, seconds=30))

    self.assertEqual(pyauto_base.misc.parseTimedeltaString("20s"),
                     datetime.timedelta(seconds=20))

    self.assertEqual(pyauto_base.misc.parseTimedeltaString("4m"),
                     datetime.timedelta(minutes=4))
    self.assertEqual(pyauto_base.misc.parseTimedeltaString("4m32s"),
                     datetime.timedelta(minutes=4, seconds=32))

    self.assertEqual(pyauto_base.misc.parseTimedeltaString("1h"),
                     datetime.timedelta(hours=1))
    self.assertEqual(pyauto_base.misc.parseTimedeltaString("2h12m"),
                     datetime.timedelta(hours=2, minutes=12))
    self.assertEqual(pyauto_base.misc.parseTimedeltaString("4h25s"),
                     datetime.timedelta(hours=4, seconds=25))
    self.assertEqual(pyauto_base.misc.parseTimedeltaString("2h33m6s"),
                     datetime.timedelta(hours=2, minutes=33, seconds=6))

    self.assertEqual(pyauto_base.misc.parseTimedeltaString("1d"),
                     datetime.timedelta(days=1))
    self.assertEqual(pyauto_base.misc.parseTimedeltaString("1d4h"),
                     datetime.timedelta(days=1, hours=4))
    self.assertEqual(pyauto_base.misc.parseTimedeltaString("1d34m"),
                     datetime.timedelta(days=1, minutes=34))
    self.assertEqual(pyauto_base.misc.parseTimedeltaString("1d16s"),
                     datetime.timedelta(days=1, seconds=16))
    self.assertEqual(pyauto_base.misc.parseTimedeltaString("2d3h8m"),
                     datetime.timedelta(days=2, hours=3, minutes=8))
    self.assertEqual(pyauto_base.misc.parseTimedeltaString("2d8h44s"),
                     datetime.timedelta(days=2, hours=8, seconds=44))
    self.assertEqual(pyauto_base.misc.parseTimedeltaString("2d12m43s"),
                     datetime.timedelta(days=2, minutes=12, seconds=43))
    self.assertEqual(pyauto_base.misc.parseTimedeltaString("1d2h3m45s"),
                     datetime.timedelta(days=1, hours=2, minutes=3, seconds=45))

  #@unittest.skip("")
  def test_printHexList(self):
    tmp = []
    for i in range(21):
      tmp.append(hex(i))
    print("")
    pyauto_base.misc.printHexList(tmp)
    print("")
    pyauto_base.misc.printHexList(tmp, hexSz="word")
    print("")
    pyauto_base.misc.printHexList(tmp, hexSz="long")
    print("")
    pyauto_base.misc.printHexList(tmp, 2)
    print("")
    pyauto_base.misc.printHexList(tmp, 3)
    print("")
    pyauto_base.misc.printHexList(tmp, 4)
    print("")
    pyauto_base.misc.printHexList(tmp, 16)
    print("")
    pyauto_base.misc.printHexList(tmp, 0)
    pyauto_base.misc.printHexList(tmp, 17)

class Test_misc_file(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  def _createKnownFile(self):
    fOutPath = os.path.join(self.outFolder, "test_known.txt")

    with open(fOutPath, "w") as fOut:
      fOut.write("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")
    return fOutPath

  #@unittest.skip("")
  def test_readCSV(self):
    data = pyauto_base.misc.readCSV_asRows("none.csv")
    self.assertEqual(data, [])

    fPath = os.path.join(self.inFolder, "test_read_CSV.csv")
    data = pyauto_base.misc.readCSV_asRows(fPath)
    self.assertEqual(len(data), 3)
    self.assertEqual(data[0]["a"], "1")
    self.assertEqual(data[1]["b"], "6")
    self.assertEqual(data[2]["c"], "g")

    fPath = os.path.join(self.inFolder, "test_read_CSV.csv")
    data = pyauto_base.misc.readCSV_asCols(fPath)
    self.assertEqual(len(data), 4)
    self.assertEqual(data[0][0], "a")
    self.assertEqual(data[1][1], "2")
    self.assertEqual(data[2][2], "7")
    self.assertEqual(data[3][3], "h")

  #@unittest.skip("")
  def test_read_write_config(self):
    f_in_path = os.path.join(self.inFolder, "test_read.cfg")
    cfg = pyauto_base.misc.readTXT_config(f_in_path)

    dict_cfg = {}
    for s in cfg.sections():
      dict_cfg[s] = dict(cfg.items(s))
    dict_cfg["section2"] = {"key2_1": "value2_1", "key2_2": "value2_2"}

    f_out_path = os.path.join(self.outFolder, "test_misc_write.cfg")
    pyauto_base.misc.writeTXT_config(f_out_path, dict_cfg)
    lstHash = [
        "9c619626d52fd92de80386c1f1d3284b74f1cd18100f8b6e6291d8259bf029db",  # linux
        "50ee8ec4509be29bebee3be65479e0e657cc3608df3e4b62c79ec22b7ea37f66",  # win
    ]
    tt.chkHash_file(self, f_out_path, lstHash)

  #@unittest.skip("")
  def test_writeXML_pretty(self):
    f_in_path = os.path.join(self.inFolder, "test_read.xml")
    dom = ET.parse(f_in_path)
    root = dom.getroot()

    f_out_path = os.path.join(self.outFolder, "test_misc_write.xml")
    pyauto_base.misc.writeXML_pretty(f_out_path, root)
    self.assertEqual(pyauto_base.misc.getFileHash(f_out_path),
                     "2870b8d3992f47a8684c9ccca1e227ed5cf50fe848e6fae1d4e01c1739e25f34")

  #@unittest.skip("")
  def test_getFileHash(self):
    fPath = self._createKnownFile()

    self.assertEqual(pyauto_base.misc.getFileHash(fPath, hashAlg="md5"),
                     "35899082e51edf667f14477ac000cbba")
    self.assertEqual(pyauto_base.misc.getFileHash(fPath),
                     "a58dd8680234c1f8cc2ef2b325a43733605a7f16f288e072de8eae81fd8d6433")

  #@unittest.skip("")
  def test_template(self):
    fInPath = os.path.join(self.inFolder, "test_read_template1.txt")
    fOutPath = os.path.join(self.outFolder, "test_template1.txt")
    dictTpl = {
        "k1": "add",
        "k2": "take away",
        "foo": "fear",
        "bar": "more important than fear"
    }

    pyauto_base.misc.templateSimple(fInPath, fOutPath, dictTpl)
    lstHash = [
        "642662b8f90bad95f3d945012ed0a5b7e023eedb6c4c2182bdedd8b26960d6d9",  # linux
        "43a6fb82d5df0b9b4b220f233acb914fd18450489e1024aad6bb3c4aab29679f",  # win
    ]
    tt.chkHash_file(self, fOutPath, lstHash)

    fInPath = os.path.join(self.inFolder, "test_read_template2.txt")
    fOutPath = os.path.join(self.outFolder, "test_template2.txt")
    dictTpl = {"lstId": ["1", "2", "a", "b"]}

    try:
      import wheezy
      pyauto_base.misc.templateWheezy(fInPath, fOutPath, dictTpl)
      lstHash = [
          "58fe1deb77a4f05d4f07a068f8042d989bfdd6d1974c6c025f86a98b741084d8",
      ]
      tt.chkHash_file(self, fOutPath, lstHash)
    except ImportError:
      pass

  #@unittest.skip("")
  def test_ResultLog(self):
    fOutPath = os.path.join(self.outFolder, "test_resultlog.txt")
    results = pyauto_base.misc.ResultLog(fOutPath, strType="txt")
    results.add("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")
    results.add("Proin lacinia rhoncus orci, nec lacinia ipsum feugiat eu.")
    results.add("Donec lobortis tellus tortor, ut ullamcorper ante imperdiet sit amet.")

    lstHash = [
        "df400fde13ff5c3fdd35e8f097727d5b3899d2b058b565612b4ff6250291488f",  # linux
        "64d8e60f83b8b83233d4d479aad792382abaae260247dc3147792782e0eccb2b",  # win
    ]
    tt.chkHash_file(self, fOutPath, lstHash)

    fOutPath = os.path.join(self.outFolder, "test_resultlog.csv")
    results = pyauto_base.misc.ResultLog(fOutPath)
    results.add(["h1", "h2", "h3"])
    results.add(["1", "2", "3", "4", "5"])
    results.add(["-2.5", "3.6"])

    lstHash = [
        "371afa666bccdc954c7a6a695224f6cfdd3df636e9c9e4abdb5b22c3f9313c10",  # linux
        "d2c8bb57f03e4fe5e3ae0f8055528ace16c8d464a7e40ea54331335004ef5eaf",  # win
    ]
    tt.chkHash_file(self, fOutPath, lstHash)

class Test_StringTree(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_node(self):
    n1 = pyauto_base.misc.StringTreeNode("node1")
    n2 = pyauto_base.misc.StringTreeNode("node2")
    n3 = pyauto_base.misc.StringTreeNode("node3")

    self.assertEqual(n1.name, "node1")
    self.assertEqual(len(n1.parentNodes), 0)
    self.assertEqual(len(n1.childNodes), 0)

    n2.addParent(n1)
    self.assertEqual(len(n1.parentNodes), 0)
    self.assertEqual(len(n1.childNodes), 1)
    self.assertEqual(len(n2.parentNodes), 1)
    self.assertEqual(len(n2.childNodes), 0)
    self.assertCountEqual(n1.childNames, ["node2"])
    self.assertCountEqual(n2.parentNames, ["node1"])

    n2.addChild(n3)
    self.assertEqual(len(n1.parentNodes), 0)
    self.assertEqual(len(n1.childNodes), 1)
    self.assertEqual(len(n2.parentNodes), 1)
    self.assertEqual(len(n2.childNodes), 1)
    self.assertEqual(len(n3.parentNodes), 1)
    self.assertEqual(len(n3.childNodes), 0)
    self.assertCountEqual(n1.childNames, ["node2"])
    self.assertCountEqual(n2.parentNames, ["node1"])
    self.assertCountEqual(n2.childNames, ["node3"])
    self.assertCountEqual(n3.parentNames, ["node2"])

    n2.addChildName("child")
    self.assertEqual(len(n1.parentNodes), 0)
    self.assertEqual(len(n1.childNodes), 1)
    self.assertEqual(len(n2.parentNodes), 1)
    self.assertEqual(len(n2.childNodes), 2)
    self.assertEqual(len(n3.parentNodes), 1)
    self.assertEqual(len(n3.childNodes), 0)
    self.assertCountEqual(n1.childNames, ["node2"])
    self.assertCountEqual(n2.parentNames, ["node1"])
    self.assertCountEqual(n2.childNames, ["child", "node3"])
    self.assertCountEqual(n3.parentNames, ["node2"])

    n2.delParent(n3)
    self.assertEqual(len(n1.parentNodes), 0)
    self.assertEqual(len(n1.childNodes), 1)
    self.assertEqual(len(n2.parentNodes), 1)
    self.assertEqual(len(n2.childNodes), 2)
    self.assertEqual(len(n3.parentNodes), 1)
    self.assertEqual(len(n3.childNodes), 0)
    self.assertCountEqual(n1.childNames, ["node2"])
    self.assertCountEqual(n2.parentNames, ["node1"])
    self.assertCountEqual(n2.childNames, ["child", "node3"])
    self.assertCountEqual(n3.parentNames, ["node2"])

    n2.delParent(n1)
    self.assertEqual(len(n1.parentNodes), 0)
    self.assertEqual(len(n1.childNodes), 0)
    self.assertEqual(len(n2.parentNodes), 0)
    self.assertEqual(len(n2.childNodes), 2)
    self.assertEqual(len(n3.parentNodes), 1)
    self.assertEqual(len(n3.childNodes), 0)
    self.assertCountEqual(n2.childNames, ["child", "node3"])
    self.assertCountEqual(n3.parentNames, ["node2"])

    n2.delChild(n1)
    self.assertEqual(len(n1.parentNodes), 0)
    self.assertEqual(len(n1.childNodes), 0)
    self.assertEqual(len(n2.parentNodes), 0)
    self.assertEqual(len(n2.childNodes), 2)
    self.assertEqual(len(n3.parentNodes), 1)
    self.assertEqual(len(n3.childNodes), 0)
    self.assertCountEqual(n2.childNames, ["child", "node3"])
    self.assertCountEqual(n3.parentNames, ["node2"])

    n2.delChild(n3)
    self.assertEqual(len(n1.parentNodes), 0)
    self.assertEqual(len(n1.childNodes), 0)
    self.assertEqual(len(n2.parentNodes), 0)
    self.assertEqual(len(n2.childNodes), 1)
    self.assertEqual(len(n3.parentNodes), 0)
    self.assertEqual(len(n3.childNodes), 0)
    self.assertCountEqual(n2.childNames, ["child"])

    n2.clear()
    self.assertEqual(len(n1.parentNodes), 0)
    self.assertEqual(len(n1.childNodes), 0)
    self.assertEqual(len(n2.parentNodes), 0)
    self.assertEqual(len(n2.childNodes), 0)
    self.assertEqual(len(n3.parentNodes), 0)
    self.assertEqual(len(n3.childNodes), 0)

  #@unittest.skip("")
  def test_tree01(self):
    """
    tree with multiple roots:
    n1 -> n2
    n3 -> n4
     | -> n5
    :return:
    """
    tree = pyauto_base.misc.StringTree()
    tree.addNode("node1", None)
    self.assertEqual(len(tree), 1)

    tree.addNode("node2", "node1")
    self.assertEqual(len(tree), 2)

    tree.addNode("node3", None)
    tree.addNode("node4", "node3")
    tree.addNode("node5", "node3")
    self.assertEqual(len(tree), 5)

    print("")
    tree.showTree()
    self.assertCountEqual(tree.getTopNodes(), ["node1", "node3"])
    self.assertCountEqual(tree.getParents("node1"), [])
    self.assertCountEqual(tree.getChildren("node1"), ["node2"])
    self.assertCountEqual(tree.getParents("node2"), ["node1"])
    self.assertCountEqual(tree.getChildren("node2"), [])
    self.assertCountEqual(tree.getParents("node3"), [])
    self.assertCountEqual(tree.getChildren("node3"), ["node4", "node5"])
    self.assertCountEqual(tree.getParents("node4"), ["node3"])
    self.assertCountEqual(tree.getChildren("node4"), [])
    self.assertCountEqual(tree.getParents("node5"), ["node3"])
    self.assertCountEqual(tree.getChildren("node5"), [])
    self.assertCountEqual(tree.preorderNames(),
                          ["node1", "node2", "node3", "node4", "node5"])
    self.assertCountEqual(tree.postorderNames(),
                          ["node2", "node1", "node4", "node5", "node3"])

    tree = pyauto_base.misc.StringTree()
    tree.addNode("node1", None)
    tree.addNode("node2", None)
    tree.addNode("node3", None)
    tree.addNode("node4", None)
    tree.addNode("node5", None)
    tree.updateNode("node2", "node1")
    tree.updateNode("node4", "node3")
    tree.updateNode("node5", "node3")

    print("")
    tree.showTree()
    self.assertCountEqual(tree.getTopNodes(), ["node1", "node3"])
    self.assertCountEqual(tree.getParents("node1"), [])
    self.assertCountEqual(tree.getChildren("node1"), ["node2"])
    self.assertCountEqual(tree.getParents("node2"), ["node1"])
    self.assertCountEqual(tree.getChildren("node2"), [])
    self.assertCountEqual(tree.getParents("node3"), [])
    self.assertCountEqual(tree.getChildren("node3"), ["node4", "node5"])
    self.assertCountEqual(tree.getParents("node4"), ["node3"])
    self.assertCountEqual(tree.getChildren("node4"), [])
    self.assertCountEqual(tree.getParents("node5"), ["node3"])
    self.assertCountEqual(tree.getChildren("node5"), [])
    self.assertCountEqual(tree.preorderNames(),
                          ["node1", "node2", "node3", "node4", "node5"])
    self.assertCountEqual(tree.postorderNames(),
                          ["node2", "node1", "node4", "node5", "node3"])

  #@unittest.skip("")
  def test_tree02(self):
    """
    tree with multiple parents:
    n1 -> n2 -> n5
     | -> n3 -> |
     | -> n4 -> |
    :return:
    """
    tree = pyauto_base.misc.StringTree()
    tree.addNode("node1", None)
    tree.addNode("node2", "node1")
    tree.addNode("node3", "node1")
    tree.addNode("node4", "node1")
    tree.addNode("node5", "node4")
    tree.addNode("node5", "node3")
    tree.addNode("node5", "node2")

    print("")
    tree.showTree()
    self.assertCountEqual(tree.getTopNodes(), ["node1"])
    self.assertCountEqual(tree.getParents("node1"), [])
    self.assertCountEqual(tree.getChildren("node1"), ["node2", "node3", "node4"])
    self.assertCountEqual(tree.getParents("node2"), ["node1"])
    self.assertCountEqual(tree.getChildren("node2"), ["node5"])
    self.assertCountEqual(tree.getParents("node3"), ["node1"])
    self.assertCountEqual(tree.getChildren("node3"), ["node5"])
    self.assertCountEqual(tree.getParents("node4"), ["node1"])
    self.assertCountEqual(tree.getChildren("node4"), ["node5"])
    self.assertCountEqual(tree.getParents("node5"), ["node2", "node3", "node4"])
    self.assertCountEqual(tree.getChildren("node5"), [])
    self.assertCountEqual(tree.preorderNames(),
                          ["node1", "node2", "node5", "node3", "node5", "node4", "node5"])
    self.assertCountEqual(tree.postorderNames(),
                          ["node5", "node2", "node5", "node3", "node5", "node4", "node1"])

    tree = pyauto_base.misc.StringTree()
    tree.addNode("node1", None)
    tree.addNode("node2", None)
    tree.addNode("node3", None)
    tree.addNode("node4", None)
    tree.addNode("node5", None)
    tree.updateNode("node2", "node1")
    tree.updateNode("node3", "node1")
    tree.updateNode("node4", "node1")
    tree.updateNode("node5", "node4")
    tree.updateNode("node5", "node3")
    tree.updateNode("node5", "node2")

    print("")
    tree.showTree()
    self.assertCountEqual(tree.getTopNodes(), ["node1"])
    self.assertCountEqual(tree.getParents("node1"), [])
    self.assertCountEqual(tree.getChildren("node1"), ["node2", "node3", "node4"])
    self.assertCountEqual(tree.getParents("node2"), ["node1"])
    self.assertCountEqual(tree.getChildren("node2"), ["node5"])
    self.assertCountEqual(tree.getParents("node3"), ["node1"])
    self.assertCountEqual(tree.getChildren("node3"), ["node5"])
    self.assertCountEqual(tree.getParents("node4"), ["node1"])
    self.assertCountEqual(tree.getChildren("node4"), ["node5"])
    self.assertCountEqual(tree.getParents("node5"), ["node2", "node3", "node4"])
    self.assertCountEqual(tree.getChildren("node5"), [])
    self.assertCountEqual(tree.preorderNames(),
                          ["node1", "node2", "node5", "node3", "node5", "node4", "node5"])
    self.assertCountEqual(tree.postorderNames(),
                          ["node5", "node2", "node5", "node3", "node5", "node4", "node1"])

  #@unittest.skip("")
  def test_tree03(self):
    """
    tree with multiple roots and parents:
    n1 -> |  -> n5
    n2 -> n4 -> n6
    n3 -> |  -> n7
    :return:
    """
    tree = pyauto_base.misc.StringTree()
    tree.addNode("node1", None)
    tree.addNode("node2", None)
    tree.addNode("node3", None)
    tree.addNode("node4", "node1")
    tree.updateNode("node4", "node2")
    tree.updateNode("node4", "node3")
    tree.addNode("node5", "node4")
    tree.addNode("node6", "node4")
    tree.addNode("node7", "node4")

    print("")
    tree.showTree()
    self.assertCountEqual(tree.getTopNodes(), ["node1", "node2", "node3"])
    self.assertCountEqual(tree.getParents("node1"), [])
    self.assertCountEqual(tree.getChildren("node1"), ["node4"])
    self.assertCountEqual(tree.getParents("node2"), [])
    self.assertCountEqual(tree.getChildren("node2"), ["node4"])
    self.assertCountEqual(tree.getParents("node3"), [])
    self.assertCountEqual(tree.getChildren("node3"), ["node4"])
    self.assertCountEqual(tree.getParents("node4"), ["node1", "node2", "node3"])
    self.assertCountEqual(tree.getChildren("node4"), ["node5", "node6", "node7"])
    self.assertCountEqual(tree.getParents("node5"), ["node4"])
    self.assertCountEqual(tree.getChildren("node5"), [])
    self.assertCountEqual(tree.getParents("node6"), ["node4"])
    self.assertCountEqual(tree.getChildren("node6"), [])
    self.assertCountEqual(tree.getParents("node7"), ["node4"])
    self.assertCountEqual(tree.getChildren("node7"), [])
    self.assertCountEqual(tree.preorderNames(), [
        "node1", "node4", "node5", "node6", "node7", "node2", "node4", "node5", "node6",
        "node7", "node3", "node4", "node5", "node6", "node7"
    ])
    self.assertCountEqual(tree.postorderNames(), [
        "node5", "node6", "node7", "node4", "node1", "node5", "node6", "node7", "node4",
        "node2", "node5", "node6", "node7", "node4", "node3"
    ])

    tree = pyauto_base.misc.StringTree()
    tree.addNode("node1", None)
    tree.addNode("node2", None)
    tree.addNode("node3", None)
    tree.addNode("node4", None)
    tree.addNode("node5", None)
    tree.addNode("node6", None)
    tree.addNode("node7", None)
    tree.updateNode("node4", "node1")
    tree.updateNode("node4", "node2")
    tree.updateNode("node4", "node3")
    tree.updateNode("node5", "node4")
    tree.updateNode("node6", "node4")
    tree.updateNode("node7", "node4")

    print("")
    tree.showTree()
    self.assertCountEqual(tree.getTopNodes(), ["node1", "node2", "node3"])
    self.assertCountEqual(tree.getParents("node1"), [])
    self.assertCountEqual(tree.getChildren("node1"), ["node4"])
    self.assertCountEqual(tree.getParents("node2"), [])
    self.assertCountEqual(tree.getChildren("node2"), ["node4"])
    self.assertCountEqual(tree.getParents("node3"), [])
    self.assertCountEqual(tree.getChildren("node3"), ["node4"])
    self.assertCountEqual(tree.getParents("node4"), ["node1", "node2", "node3"])
    self.assertCountEqual(tree.getChildren("node4"), ["node5", "node6", "node7"])
    self.assertCountEqual(tree.getParents("node5"), ["node4"])
    self.assertCountEqual(tree.getChildren("node5"), [])
    self.assertCountEqual(tree.getParents("node6"), ["node4"])
    self.assertCountEqual(tree.getChildren("node6"), [])
    self.assertCountEqual(tree.getParents("node7"), ["node4"])
    self.assertCountEqual(tree.getChildren("node7"), [])
    self.assertCountEqual(tree.preorderNames(), [
        "node1", "node4", "node5", "node6", "node7", "node2", "node4", "node5", "node6",
        "node7", "node3", "node4", "node5", "node6", "node7"
    ])
    self.assertCountEqual(tree.postorderNames(), [
        "node5", "node6", "node7", "node4", "node1", "node5", "node6", "node7", "node4",
        "node2", "node5", "node6", "node7", "node4", "node3"
    ])
