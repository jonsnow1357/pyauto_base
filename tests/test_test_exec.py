#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_base.test_exec"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_base.config
import pyauto_base.db
import pyauto_base.test_exec
import pyauto_base.tests.tools as tt

class Test_TestInfo(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    sqlitePath = os.path.join(self.outFolder, "test_testex.sqlite")
    with open(sqlitePath, "a"):
      pass

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    tInfo = pyauto_base.test_exec.TestInfo()

    self.assertIsInstance(tInfo.id, str)
    self.assertEqual(tInfo.title, None)
    self.assertEqual(tInfo.description, "")
    self.assertEqual(tInfo.status, pyauto_base.test_exec.testStatusNotRun)
    self.assertEqual(tInfo.params, {"manual": False})
    self.assertEqual(tInfo.startTime, None)
    self.assertEqual(tInfo.runTime, None)
    self.assertEqual(tInfo.estimatedTime, None)

    tmp = pyauto_base.misc.getRndStr(32)
    tInfo.id = tmp
    self.assertEqual(tInfo.id, tmp)
    tmp = pyauto_base.misc.getRndStr(32)
    tInfo.title = tmp
    self.assertEqual(tInfo.title, tmp)
    tInfo.id = "test_id"
    tInfo.title = "Test Title"
    logger.info(tInfo)

    tInfo.setStatusSkip()
    self.assertEqual(tInfo.status, pyauto_base.test_exec.testStatusSkip)
    self.assertIsInstance(tInfo.startTime, str)
    self.assertEqual(tInfo.runTime, None)
    self.assertEqual(tInfo.estimatedTime, None)
    logger.info(tInfo)

    tInfo.setStatusNotRun()
    self.assertEqual(tInfo.startTime, None)
    self.assertEqual(tInfo.runTime, None)
    self.assertEqual(tInfo.estimatedTime, None)
    tInfo.setStatusRun()
    self.assertIsInstance(tInfo.startTime, str)
    self.assertEqual(tInfo.runTime, None)
    self.assertEqual(tInfo.estimatedTime, None)
    logger.info(tInfo)
    tInfo.setStatusPass()
    self.assertEqual(tInfo.status, pyauto_base.test_exec.testStatusPass)
    self.assertIsInstance(tInfo.startTime, str)
    self.assertEqual(type(tInfo.runTime), type(datetime.timedelta()))
    self.assertEqual(tInfo.estimatedTime, None)
    logger.info(tInfo)

    tInfo.setStatusNotRun()
    tInfo.setStatusRun()
    tInfo.setStatusFail()
    self.assertEqual(tInfo.status, pyauto_base.test_exec.testStatusFail)
    self.assertIsInstance(tInfo.startTime, str)
    self.assertEqual(type(tInfo.runTime), type(datetime.timedelta()))
    self.assertEqual(tInfo.estimatedTime, None)
    logger.info(tInfo)

    tInfo.setStatusNotRun()
    tInfo.setStatusRun()
    tInfo.setStatusError()
    self.assertEqual(tInfo.status, pyauto_base.test_exec.testStatusError)
    self.assertIsInstance(tInfo.startTime, str)
    self.assertEqual(type(tInfo.runTime), type(datetime.timedelta()))
    self.assertEqual(tInfo.estimatedTime, None)
    logger.info(tInfo)

  #@unittest.skip("")
  def test_estimatedTime(self):
    import random
    tInfo = pyauto_base.test_exec.TestInfo()
    self.assertEqual(tInfo.estimatedTime, None)

    tInfo.estimatedTime = "1"
    self.assertEqual(tInfo.estimatedTime, datetime.timedelta(minutes=1))
    tInfo.estimatedTime = "3.25"
    self.assertEqual(tInfo.estimatedTime, datetime.timedelta(minutes=3, seconds=15))

    nd, nh, nm, ns = [random.randint(1, 59) for _ in range(4)]
    tInfo.estimatedTime = "{}s".format(ns)
    self.assertEqual(tInfo.estimatedTime, datetime.timedelta(seconds=ns))
    nd, nh, nm, ns = [random.randint(1, 59) for _ in range(4)]
    tInfo.estimatedTime = "{}m".format(nm)
    self.assertEqual(tInfo.estimatedTime, datetime.timedelta(minutes=nm))
    nd, nh, nm, ns = [random.randint(1, 59) for _ in range(4)]
    tInfo.estimatedTime = "{}m{}s".format(nm, ns)
    self.assertEqual(tInfo.estimatedTime, datetime.timedelta(minutes=nm, seconds=ns))

    nd, nh, nm, ns = [random.randint(1, 59) for _ in range(4)]
    tInfo.estimatedTime = "{}h{}s".format(nh, ns)
    self.assertEqual(tInfo.estimatedTime, datetime.timedelta(hours=nh, seconds=ns))
    nd, nh, nm, ns = [random.randint(1, 59) for _ in range(4)]
    tInfo.estimatedTime = "{}h{}m".format(nh, nm)
    self.assertEqual(tInfo.estimatedTime, datetime.timedelta(hours=nh, minutes=nm))
    nd, nh, nm, ns = [random.randint(1, 59) for _ in range(4)]
    tInfo.estimatedTime = "{}h{}m{}s".format(nh, nm, ns)
    self.assertEqual(tInfo.estimatedTime,
                     datetime.timedelta(hours=nh, minutes=nm, seconds=ns))

    nd, nh, nm, ns = [random.randint(1, 59) for _ in range(4)]
    tInfo.estimatedTime = "{}d{}s".format(nd, ns)
    self.assertEqual(tInfo.estimatedTime, datetime.timedelta(days=nd, seconds=ns))
    nd, nh, nm, ns = [random.randint(1, 59) for _ in range(4)]
    tInfo.estimatedTime = "{}d{}m".format(nd, nm)
    self.assertEqual(tInfo.estimatedTime, datetime.timedelta(days=nd, minutes=nm))
    nd, nh, nm, ns = [random.randint(1, 59) for _ in range(4)]
    tInfo.estimatedTime = "{}d{}m{}s".format(nd, nm, ns)
    self.assertEqual(tInfo.estimatedTime, datetime.timedelta(days=nd,
                                                             minutes=nm,
                                                             seconds=ns))

    nd, nh, nm, ns = [random.randint(1, 59) for _ in range(4)]
    tInfo.estimatedTime = "{}d{}h{}s".format(nd, nh, ns)
    self.assertEqual(tInfo.estimatedTime, datetime.timedelta(days=nd, hours=nh, seconds=ns))
    nd, nh, nm, ns = [random.randint(1, 59) for _ in range(4)]
    tInfo.estimatedTime = "{}d{}h{}m".format(nd, nh, nm)
    self.assertEqual(tInfo.estimatedTime, datetime.timedelta(days=nd, hours=nh, minutes=nm))
    nd, nh, nm, ns = [random.randint(1, 59) for _ in range(4)]
    tInfo.estimatedTime = "{}d{}h{}m{}s".format(nd, nh, nm, ns)
    self.assertEqual(tInfo.estimatedTime,
                     datetime.timedelta(days=nd, hours=nh, minutes=nm, seconds=ns))

class Test_TestData(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    sqlitePath = os.path.join(self.outFolder, "test_testex.sqlite")
    with open(sqlitePath, "a"):
      pass

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    tData = pyauto_base.test_exec.TestData()

    self.assertEqual(tData.id, None)
    tmp = pyauto_base.misc.getRndStr(32)
    tData.id = tmp
    self.assertEqual(tData.id, tmp)

    self.assertEqual(tData.type, None)
    self.assertFalse(tData.logTimestamp)
    self.assertFalse(tData.logAppName)
    self.assertFalse(tData.logTestId)
    self.assertEqual(len(tData.results), 0)
    logger.info(tData)

  #@unittest.skip("")
  def test_json(self):
    tData = pyauto_base.test_exec.TestData()
    fOutName = "test_testex.jsonl"
    fPath = os.path.join(self.outFolder, fOutName)
    tData.configLog(pyauto_base.test_exec.testLogTypeJson, logPath=fPath, logClear=True)
    tData.log("test1")
    tData.logAppName = True
    tData.log("test2")
    tData.logTestId = True
    tData.log("test3")

    lstHash = [
        "37b905d76ed6b95e1937f21196279de13cc4ac2922215bae78c0b2673f3af5f4",  # pyCharm linux
        "7aeb9c2f2a3b455fe02b3f4a29ef2ab7fb3b9423815c931a3db3b153ec733907",  # py2 unittest linux
        "c9e371218b232d3b5d1218c021be93a46e96d890794ed6100f78b741bf186783",  # py3 unittest linux
        "9c4c4069f2394dc10e7d9c914d13c6d54a4bf8d8349a36a1987fde5cd3b02d61",  # pytest linux
        "2df4fc17689466cd18b949af6f2f22905f1d07c814d3a261cdf83a30d5954719",  # pytest-3 linux
        "2f8a00e0d6e78937076fa9a6981fe7a38a1879ffc38b127044bc4fbb62fa11b6",  # pyCharm win
        "7d534862ba82ad48712cac987035f2eb81074c9d1c91d8aeeea431c9bd44c71e",  # unittest win
        "93a3d9159e12f686fc767c8127e67d75c58cf1f9b70bdf1fdadd43c334d4d6ec",  # pytest win
    ]
    tt.chkHash_file(self, fPath, lstHash)

    tData.logTimestamp = True
    tData.log("test4")

  #@unittest.skip("")
  def test_csv(self):
    tData = pyauto_base.test_exec.TestData()
    fOutName = "test_testex1.csv"
    fPath = os.path.join(self.outFolder, fOutName)
    tData.configLog(pyauto_base.test_exec.testLogTypeCsv, logPath=fPath, logClear=True)
    tData.log("test1")
    tData.logAppName = True
    tData.log("test2")
    tData.logTestId = True
    tData.log("test3")

    lstHash = [
        "1df78f73a93f39b7883b54bd25f90ddfb5e01f62d86edee774fef5b8c5b30c70",  # pyCharm linux
        "f9de75684fb9d3d907f1d691b6c9bf8effa5a0789d6dda46cb1683945c8db449",  # py2 unittest linux
        "4e7f460cb7e8b495f660dd297596ae4682cdc5d019da345460d76ac7c534f965",  # py3 unittest linux
        "a1839bbdfbd9d0ccfdc2483c0a2b38fd230038bd0c88b8c0d446a87d56f2aa9d",  # pytest linux
        "0c2b278a3526855daba89fb28c94ba40fb6553545a6e29ed90af04ee58d8be85",  # pytest-3 linux
        "c6a672bc4bc615159e95777806307c55c9a0f8e39670f9ebe16116ed5ef3ddb0",  # pyCharm win
        "9f31b1de694a8c5ee305a699df2974f3410b97d3974116b9f70f0483db140e42",  # unittest win
        "d7f98a099db0554cce3037e8eed9bf1b7cea185a6dc0bfc2341b38e621526012",  # pytest win
    ]
    tt.chkHash_file(self, fPath, lstHash)

    tData.logTimestamp = True
    tData.log("test4")

    tData.logTimestamp = False
    tData.logAppName = False
    tData.logTestId = False
    fOutName = "test_testex2.csv"
    fPath = os.path.join(self.outFolder, fOutName)
    tData.configLog(pyauto_base.test_exec.testLogTypeCsv,
                    logPath=fPath,
                    logClear=True,
                    logColNames=["VCC [V]", "temp [C]"])
    tData.log("test")
    tData.log(["bite", "me"])
    tData.log({"VCC [V]": "3.31", "temp [C]": "25"})
    tData.logAppName = True
    tData.log({"VCC [V]": "3.32", "temp [C]": "26"})
    tData.logTestId = True
    tData.log({"VCC [V]": "3.33", "temp [C]": "27"})

    lstHash = [
        "eead3e2c14f7804d943d63896677007ca09044f7cc9b8d29050fd0d6e309b8d7",  # pyCharm linux
        "6f509a4648a7d32a51f152fec9e4451c886f5cc79c70ec2b23a167f3a0235692",  # py2 unittest linux
        "f335fd65ea590433c2eb7e0aa400517a7fd1dac5362b924822446fe8d1991c25",  # py3 unittest linux
        "1742edfc16b28f48d518f2d03d637e8fd9692109ce3d3178c5e22c45f190ec87",  # pytest linux
        "dc0f4fbbc358c2edfdaecdd2372285433940faf4a5ac53f08dfe7d48ab2d0ee0",  # pytest-3 linux
        "ff623a6c34577c8fc70275702ea3d8e352882e399550460a69e56712f17c284e",  # pyCharm win
        "1a80ab882cb3e57e3be543abf5dcea579452b7a551789f8b7fd087003f653538",  # unittest win
        "e1a500ca3fbed1b9321d39cfa8b15291b737691c5471dd69be91336c9dda641a",  # pytest win
    ]
    tt.chkHash_file(self, fPath, lstHash)

    tData.logTimestamp = True
    tData.log({"VCC [V]": "3.32", "temp [C]": "28"})

  #@unittest.skip("")
  def test_sqlite(self):
    tData = pyauto_base.test_exec.TestData()
    tData.id = pyauto_base.misc.getRndStr(8)
    fCfgPath = os.path.join(self.inFolder, "test_read_testex.cfg")
    dbCfg = pyauto_base.config.DBConfig()
    dbCfg.loadCfgFile(fCfgPath)

    tData.configLog(pyauto_base.test_exec.testLogTypeSqlite,
                    logPath=dbCfg.dictDBInfo["sqlite"],
                    logClear=True)
    tData.log("test1")
    tData.logTimestamp = True
    tData.log("test2")
    tData.logAppName = True
    tData.log("test3")
    tData.logTestId = True
    tData.log("test4")
    tData.configLog(pyauto_base.test_exec.testLogTypeSqlite,
                    logPath=dbCfg.dictDBInfo["sqlite"],
                    logClear=True)

    tData.configLog(pyauto_base.test_exec.testLogTypeSqlite,
                    logPath=dbCfg.dictDBInfo["sqlite"],
                    logClear=True,
                    logColNames=["VCC [V]", "temp [C]"],
                    logTable="log_measure")
    logger.info(tData)
    tData.log("test")
    tData.log(["bite", "me"])
    tData.log({"VCC [V]": "3.31", "temp [C]": "25"})
    tData.logTimestamp = True
    tData.log({"VCC [V]": "3.32", "temp [C]": "26"})
    tData.logAppName = True
    tData.log({"VCC [V]": "3.33", "temp [C]": "27"})
    tData.logTestId = True
    tData.log({"VCC [V]": "3.32", "temp [C]": "28"})
    tData.configLog(pyauto_base.test_exec.testLogTypeSqlite,
                    logPath=dbCfg.dictDBInfo["sqlite"],
                    logClear=True,
                    logColNames=["VCC [V]", "temp [C]"],
                    logTable="log_measure")

    self.assertGreater(
        os.path.getsize(os.path.join(self.inFolder, dbCfg.dictDBInfo["sqlite"].dbname)),
        1024)

  #@unittest.skip("")
  def test_mysql(self):
    tData = pyauto_base.test_exec.TestData()
    tData.id = pyauto_base.misc.getRndStr(8)
    fCfgPath = os.path.join(self.inFolder, "test_read_testex.cfg")
    dbCfg = pyauto_base.config.DBConfig()
    dbCfg.loadCfgFile(fCfgPath)

    try:
      tData.configLog(pyauto_base.test_exec.testLogTypeMysql,
                      logPath=dbCfg.dictDBInfo["mysql"],
                      logClear=True)
    except pyauto_base.db.DBException:
      self.skipTest("")
    tData.log("test1")
    tData.logTimestamp = True
    tData.log("test2")
    tData.logAppName = True
    tData.log("test3")
    tData.logTestId = True
    tData.log("test4")

    tData.configLog(pyauto_base.test_exec.testLogTypeMysql,
                    logPath=dbCfg.dictDBInfo["mysql"],
                    logClear=True,
                    logColNames=["VCC [V]", "temp [C]"],
                    logTable="log_measure")
    logger.info(tData)
    tData.log("test")
    tData.log(["bite", "me"])
    tData.log({"VCC [V]": "3.31", "temp [C]": "25"})
    tData.logTimestamp = True
    tData.log({"VCC [V]": "3.32", "temp [C]": "26"})
    tData.logAppName = True
    tData.log({"VCC [V]": "3.33", "temp [C]": "27"})
    tData.logTestId = True
    tData.log({"VCC [V]": "3.32", "temp [C]": "28"})

  #@unittest.skip("")
  def test_results(self):
    tData = pyauto_base.test_exec.TestData()
    self.assertEqual(len(tData.results), 0)
    fOutName = "test_results_testex.json"
    fPath = os.path.join(self.outFolder, fOutName)
    tData.addResult("voltage", "3.3")
    self.assertEqual(len(tData.results), 1)
    tData.saveResults(fPath=fPath)

    lstHash = [
        "d9294fc7eb55873590768c95a5628d9a9e68f3ea3c2eb1e24e93a3629f761f1a",  # linux
        "c97eac4130b5b694572265ce027736f8011149449e95e58f8c7031ddb47186e3",  # win
    ]
    tt.chkHash_file(self, fPath, lstHash)

class Test_TestConfig(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    sqlitePath = os.path.join(self.outFolder, "test_testex.sqlite")
    with open(sqlitePath, "a"):
      pass

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    fCfgPath = os.path.join(self.inFolder, "test_read_testex.xml")
    tCfg = pyauto_base.test_exec.TestConfig()
    tCfg.loadXmlFile(fCfgPath)

    tSeq = tCfg.lstTestSeq[0]
    self.assertEqual(len(tSeq.lstTestInfo), 11)
    self.assertEqual(tSeq.lstTestInfo[0].estimatedTime, datetime.timedelta(minutes=1))
    self.assertEqual(tSeq.lstTestInfo[0].params["manual"], True)
    self.assertEqual(tSeq.lstTestInfo[1].estimatedTime,
                     datetime.timedelta(hours=1, minutes=5))
    self.assertEqual(tSeq.lstTestInfo[1].params["manual"], False)

    self.assertEqual(tSeq.lstTestInfo[2].estimatedTime,
                     datetime.timedelta(minutes=0, seconds=30))
    self.assertCountEqual(tSeq.lstTestInfo[2].params["loop"], [0])
    self.assertEqual(tSeq.lstTestInfo[3].estimatedTime,
                     datetime.timedelta(minutes=2, seconds=30))
    self.assertCountEqual(tSeq.lstTestInfo[3].params["loop"], [0, 0])
    self.assertEqual(tSeq.lstTestInfo[4].estimatedTime,
                     datetime.timedelta(minutes=2, seconds=30))
    self.assertCountEqual(tSeq.lstTestInfo[4].params["loop"], [1, 0])

    self.assertEqual(tSeq.lstTestInfo[5].estimatedTime,
                     datetime.timedelta(minutes=0, seconds=30))
    self.assertCountEqual(tSeq.lstTestInfo[5].params["loop"], [1])
    self.assertEqual(tSeq.lstTestInfo[6].estimatedTime,
                     datetime.timedelta(minutes=2, seconds=30))
    self.assertCountEqual(tSeq.lstTestInfo[6].params["loop"], [0, 1])
    self.assertEqual(tSeq.lstTestInfo[7].estimatedTime,
                     datetime.timedelta(minutes=2, seconds=30))
    self.assertCountEqual(tSeq.lstTestInfo[7].params["loop"], [1, 1])
    tCfg.showInfo()
