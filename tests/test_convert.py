#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.CAD.convert"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest
import random

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.convert

_nLoop = 100

class Test_convert(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_value2float(self):
    units = ("p", "n", "u", "m", "", "k", "M", "G", "T")
    scales = (1e-12, 1e-9, 1e-6, 1e-3, 1.0, 1e3, 1e6, 1e9, 1e12)
    for unit, scale in zip(units, scales):
      for tmp_f in (1.0, 10.0, 100.0):
        tmp_fc = pyauto_base.convert.value2float(str(tmp_f) + unit)
        self.assertEqual(tmp_fc, tmp_f * scale)
      for _ in range(_nLoop):
        tmp_f = random.choice([1.0, -1.0]) * round(random.uniform(1.0, 9.99), 2)
        tmp_fc = pyauto_base.convert.value2float(str(tmp_f) + unit)
        self.assertEqual(tmp_fc, tmp_f * scale)
        tmp_f = random.choice([1.0, -1.0]) * round(random.uniform(10.0, 99.99), 2)
        tmp_fc = pyauto_base.convert.value2float(str(tmp_f) + unit)
        self.assertEqual(tmp_fc, tmp_f * scale)
        tmp_f = random.choice([1.0, -1.0]) * round(random.uniform(100.0, 999.99), 2)
        tmp_fc = pyauto_base.convert.value2float(str(tmp_f) + unit)
        self.assertEqual(tmp_fc, tmp_f * scale)

  #@unittest.skip("")
  def test_float2value(self):
    units = ("p", "n", "u", "m", "", "k", "M", "G", "T")
    scales = (1e-12, 1e-9, 1e-6, 1e-3, 1.0, 1e3, 1e6, 1e9, 1e12)
    for unit, scale in zip(units, scales):
      for tmp_f in (1.0, 10.0, 100.0):
        tmp_fc = pyauto_base.convert.float2value(tmp_f * scale)
        self.assertEqual(tmp_fc, str(tmp_f) + unit)
      for _ in range(_nLoop):
        tmp_f = random.choice([1.0, -1.0]) * round(random.uniform(1.0, 9.99), 2)
        tmp_fc = pyauto_base.convert.float2value(tmp_f * scale)
        self.assertEqual(tmp_fc, str(tmp_f) + unit)
        tmp_f = random.choice([1.0, -1.0]) * round(random.uniform(10.0, 99.99), 2)
        tmp_fc = pyauto_base.convert.float2value(tmp_f * scale)
        self.assertEqual(tmp_fc, str(tmp_f) + unit)
        tmp_f = random.choice([1.0, -1.0]) * round(random.uniform(100.0, 999.99), 2)
        tmp_fc = pyauto_base.convert.float2value(tmp_f * scale)
        self.assertEqual(tmp_fc, str(tmp_f) + unit)
