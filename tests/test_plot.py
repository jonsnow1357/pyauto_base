#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_base.plot"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv
import unittest
import numpy

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
skipAll = False
try:
  import pyauto_base.misc
  import pyauto_base.plot
except RuntimeError:
  skipAll = True

class Test_plot_utils(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    if (skipAll):
      self.skipTest("")

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_truncateScale(self):
    for v in range(-9, 10):
      exp = 10**v

      res = pyauto_base.plot.adjustFloat(1.0 * exp)
      self.assertAlmostEqual(res, (1.0 * exp), delta=(exp / 1e6))
      res = pyauto_base.plot.adjustFloat(1.1 * exp)
      self.assertAlmostEqual(res, (1.1 * exp), delta=(exp / 1e6))
      res = pyauto_base.plot.adjustFloat(1.01 * exp)
      self.assertAlmostEqual(res, (1.01 * exp), delta=(exp / 1e6))
      res = pyauto_base.plot.adjustFloat(1.0001 * exp)
      self.assertAlmostEqual(res, (1.01 * exp), delta=(exp / 1e6))
      res = pyauto_base.plot.adjustFloat(9.0 * exp)
      self.assertAlmostEqual(res, (9.0 * exp), delta=(exp / 1e6))
      res = pyauto_base.plot.adjustFloat(9.9 * exp)
      self.assertAlmostEqual(res, (9.9 * exp), delta=(exp / 1e6))
      res = pyauto_base.plot.adjustFloat(9.99 * exp)
      self.assertAlmostEqual(res, (9.99 * exp), delta=(exp / 1e6))
      res = pyauto_base.plot.adjustFloat(9.999 * exp)
      self.assertAlmostEqual(res, (10.0 * exp), delta=(exp / 1e6))

      tmp = pyauto_base.misc.getRndFloat(1.0, 10.0)
      res = pyauto_base.plot.adjustFloat(tmp * exp)
      self.assertAlmostEqual(res, (math.ceil(tmp * 100) / 100) * exp, delta=(exp / 1e6))

class Test_PlotData(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    if (skipAll):
      self.skipTest("")

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    pd = pyauto_base.plot.PlotData()

    self.assertEqual(pd.legend, "")
    self.assertEqual(pd.xunit, "")
    self.assertEqual(pd.yunit, "")
    self.assertEqual(pd.xlabel, "")
    self.assertEqual(pd.ylabel, "")
    self.assertEqual(pd.xmin, None)
    self.assertEqual(pd.xmax, None)
    self.assertEqual(pd.ymin, None)
    self.assertEqual(pd.ymax, None)

    pd.legend = "legend"
    pd.xunit = "s"
    pd.yunit = "V"
    self.assertEqual(pd.xlabel, "[s]")
    self.assertEqual(pd.ylabel, "[V]")
    self.assertEqual(pd.xmin, None)
    self.assertEqual(pd.xmax, None)
    self.assertEqual(pd.ymin, None)
    self.assertEqual(pd.ymax, None)

    pd.clear()
    self.assertEqual(pd.legend, "")
    self.assertEqual(pd.xunit, "")
    self.assertEqual(pd.yunit, "")
    self.assertEqual(pd.xlabel, "")
    self.assertEqual(pd.ylabel, "")
    self.assertEqual(pd.xmin, None)
    self.assertEqual(pd.xmax, None)
    self.assertEqual(pd.ymin, None)
    self.assertEqual(pd.ymax, None)

  #@unittest.skip("")
  def test_limits(self):
    pd = pyauto_base.plot.PlotData()

    x = list(range(0, 11))
    y = list(range(10, 21))
    pd.setXY(x, y)
    self.assertEqual(pd.legend, "")
    self.assertEqual(pd.xunit, "")
    self.assertEqual(pd.yunit, "")
    self.assertEqual(pd.xlabel, "")
    self.assertEqual(pd.ylabel, "")
    self.assertEqual(pd.xmin, 0)
    self.assertEqual(pd.xmax, 10)
    self.assertEqual(pd.ymin, 10)
    self.assertEqual(pd.ymax, 20)

    x = 10 * [pyauto_base.misc.getRndFloat(0.0, 10.0)]
    y = 10 * [pyauto_base.misc.getRndFloat(0.0, 10.0)]
    pd.setXY(x, y, xunit="ms", yunit="u", legend="plot1")
    self.assertEqual(pd.legend, "plot1")
    self.assertEqual(pd.xunit, "ms")
    self.assertEqual(pd.yunit, "u")
    self.assertEqual(pd.xlabel, "[ms]")
    self.assertEqual(pd.ylabel, "[u]")
    self.assertEqual(pd.xmin, min(x))
    self.assertEqual(pd.xmax, max(x))
    self.assertEqual(pd.ymin, min(y))
    self.assertEqual(pd.ymax, max(y))

class Test_plot(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    if (skipAll):
      self.skipTest("")

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_SimplePlot_labels(self):
    x = [float(t) for t in numpy.arange(0, 10.0, 0.1)]
    plot = pyauto_base.plot.SimplePlot()
    self.assertEqual(plot.xlabel, "")
    self.assertEqual(plot.ylabel, "")

    plot.addData(x, x)
    plot.addData(x, x)
    #logger.info(plot)
    self.assertEqual(plot.xlabel, "")
    self.assertEqual(plot.ylabel, "")

    plot.clear()
    plot.addData(x, x)
    plot.addData(x, x)
    #logger.info(plot)
    plot.xlabel = "x label"
    plot.ylabel = "y label"
    self.assertEqual(plot.xlabel, "x label")
    self.assertEqual(plot.ylabel, "y label")

    plot.clear()
    plot.addData(x, x, xunit="s")
    plot.addData(x, x)
    #logger.info(plot)
    self.assertEqual(plot.xlabel, "")
    self.assertEqual(plot.ylabel, "")

    plot.clear()
    plot.addData(x, x, xunit="s")
    plot.addData(x, x)
    #logger.info(plot)
    plot.xlabel = "x label"
    plot.ylabel = "y label"
    self.assertEqual(plot.xlabel, "x label")
    self.assertEqual(plot.ylabel, "y label")

    plot.clear()
    plot.addData(x, x, xunit="s", yunit="V")
    plot.addData(x, x, xunit="s")
    #logger.info(plot)
    self.assertEqual(plot.xlabel, "[s]")
    self.assertEqual(plot.ylabel, "")

    plot.clear()
    plot.addData(x, x, xunit="s", yunit="V")
    plot.addData(x, x, xunit="s")
    #logger.info(plot)
    plot.xlabel = "x label"
    plot.ylabel = "y label"
    self.assertEqual(plot.xlabel, "x label")
    self.assertEqual(plot.ylabel, "y label")

    plot.clear()
    plot.addData(x, x, xunit="s", yunit="V")
    plot.addData(x, x, xunit="s", yunit="A")
    #logger.info(plot)
    self.assertEqual(plot.xlabel, "[s]")
    self.assertEqual(plot.ylabel, "[V],[A]")

    plot.clear()
    plot.addData(x, x, xunit="s", yunit="V")
    plot.addData(x, x, xunit="s", yunit="A")
    #logger.info(plot)
    plot.xlabel = "x label"
    plot.ylabel = "y label"
    self.assertEqual(plot.xlabel, "x label")
    self.assertEqual(plot.ylabel, "y label")

  #@unittest.skip("")
  def test_SimplePlot(self):
    x = numpy.arange(0, (8 * math.pi), 0.1)
    y1 = numpy.sin(x) * numpy.exp(-0.1 * x)
    y2 = numpy.cos(x)
    x = [float(t) for t in x]
    plot = pyauto_base.plot.SimplePlot()

    logger.info(plot)
    plot.addData(x, y1, xunit="s", yunit="dB", legend="plot1")
    logger.info(plot)
    plot.addData(x, y2, xunit="s", yunit="V", legend="plot2")
    logger.info(plot)
    plot.title = "title"

    plot.fPath = os.path.join(self.outFolder, "test_plot_simple1.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

    plot.ax_minticks = True
    plot.fPath = os.path.join(self.outFolder, "test_plot_simple2.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

    plot.ax_grid = "both"
    plot.fPath = os.path.join(self.outFolder, "test_plot_simple3.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

    plot.addHline(0.1)
    plot.addHline(0.25)
    plot.addHline(-0.78)
    plot.fPath = os.path.join(self.outFolder, "test_plot_simple4.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

    plot.addVline(7.1)
    plot.addVline(10.25)
    plot.addVline(20.78)
    plot.fPath = os.path.join(self.outFolder, "test_plot_simple5.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

    plot.clrHline()
    plot.clrVline()
    plot.addMarker(10.5, 0.5)
    plot.fPath = os.path.join(self.outFolder, "test_plot_simple6.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

  #@unittest.skip("")
  def test_GridPlot_time(self):
    plot = pyauto_base.plot.GridPlot()
    x = numpy.arange(-5.0, 5.05, 0.05)
    y1 = 0.5 * numpy.sin(x)
    y2 = 1.0 * numpy.cos(2 * x)
    y3 = 1.8 * numpy.sin(4 * x)

    plot.setTime(0.0, 1e-3)
    plot.addData(y1, 0.0, 0.15, "V", legend="CH1")
    plot.title = "scope plot"
    plot.txt_ur = "upper right"
    plot.txt_ul = "upper left"
    plot.txt_ll = "lower left"
    plot.ts = "timestamp"
    plot.fPath = os.path.join(self.outFolder, "test_plot_time1.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

    plot.addData(y2, 0.0, 0.24925, "V", legend="CH2")
    plot.fPath = os.path.join(self.outFolder, "test_plot_time2.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

    plot.addData(y3, 0.0, 0.5, "A", legend="CH3")
    for pd in plot.data:
      logger.info(pd)
    plot.fPath = os.path.join(self.outFolder, "test_plot_time3.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

    # do a plot with no title since on saving to file it used to cut the top of the figure
    plot.title = ""
    plot.fPath = os.path.join(self.outFolder, "test_plot_time4.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

    plot.addMarker(0.0015, 0.1)
    plot.fPath = os.path.join(self.outFolder, "test_plot_time5.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

  #@unittest.skip("")
  def test_GridPlot_freq(self):
    plot = pyauto_base.plot.GridPlot(ndivY=10)
    f = numpy.arange(0.0, 5.0125e9, 12.5e6)
    fc1 = 2.0e9
    bw1 = 5.0e7
    m1 = -80.0 + 60.0 * numpy.exp(-1 * (f - fc1) * (f - fc1) / (2 * bw1 * bw1))
    fc2 = 3.0e9
    bw2 = 10.0e7
    m2 = -70.0 + 50.0 * numpy.exp(-1 * (f - fc2) * (f - fc2) / (2 * bw2 * bw2))

    plot.setFreq(2.5e9, 5.0e9, mode="center_span")
    plot.addData(m1, -50.0, 10.0, unit="dBm", legend="MAG1")
    plot.title = "scope plot"
    plot.txt_ur = "upper right"
    plot.txt_ul = "upper left"
    plot.txt_ll = "lower left"
    plot.ts = "timestamp"
    plot.fPath = os.path.join(self.outFolder, "test_plot_freq1.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

    plot.addData(m2, -40.0, 10.0, "dBm", legend="MAG2")
    plot.fPath = os.path.join(self.outFolder, "test_plot_freq2.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

    plot = pyauto_base.plot.GridPlot(ndivY=10)
    f = numpy.arange(1.0e9, 4.0125e9, 12.5e6)
    fc3 = 2.5e9
    bw3 = 20.0e7
    m3 = -70.0 + 50.0 * numpy.exp(-1 * (f - fc3) * (f - fc3) / (2 * bw3 * bw3))

    plot.setFreq(1.0e9, 4.0e9, mode="start_stop")
    plot.addData(m3, -50.0, 10.0, unit="dBm", legend="MAG3")
    plot.title = "scope plot"
    plot.txt_ur = "upper right"
    plot.txt_ul = "upper left"
    plot.txt_ll = "lower left"
    plot.ts = "timestamp"
    plot.fPath = os.path.join(self.outFolder, "test_plot_freq3.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

    plot.addMarker(2e9, -60)
    plot.addMarker(3e9, -36.6)
    plot.fPath = os.path.join(self.outFolder, "test_plot_freq4.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))

  #@unittest.skip("")
  def test_BoxPlot(self):
    plot = pyauto_base.plot.BoxPlot()
    v = [float(t) for t in numpy.arange(1.4, 1.7, 0.01)]

    plot.addData(v, yunit="m", legend="distance")
    plot.title = "box plot"
    plot.fPath = os.path.join(self.outFolder, "test_plot_box1.png")
    #plot.show()
    plot.write()
    self.assertTrue(os.path.isfile(plot.fPath),
                    "file DOES NOT exist: {}".format(plot.fPath))
