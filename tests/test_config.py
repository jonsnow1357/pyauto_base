#!/usr/bin/env python
# coding: utf-8
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_base.config"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest
import random

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.config
import pyauto_base.misc
import pyauto_base.db

class Test_DictConfig(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    sqlitePath = os.path.join(self.outFolder, "test_db.sqlite")
    with open(sqlitePath, "a"):
      pass

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    fPath = os.path.join(self.outFolder, "test_DictConfig.cfg")
    cfg = pyauto_base.config.DictConfig()

    nSections = random.randint(1, 4)
    lstSections = []
    for i in range(nSections):
      lstSections.append(pyauto_base.misc.getRndStr_Pat("section_"))
    lstSections = list(set(lstSections))

    dictCfg = {}
    nItems = random.randint(2, 8)
    for s in lstSections:
      dictTmp = {}
      for i in range(nItems):
        dictTmp[pyauto_base.misc.getRndStr_Pat("key_")] = pyauto_base.misc.getRndStr_Pat(
            "val_")
      dictCfg[s] = dictTmp

    cfg.writeCfg(fPath, **dictCfg)
    cfg.readCfg(fPath)

    for s in cfg.configuration.keys():
      self.assertRegex(s, r"section_[0-9]+")
      for k, v in cfg.configuration[s].items():
        self.assertRegex(k, r"key_[0-9]+")
        self.assertRegex(v, r"val_[0-9]+")
    #self.assertEqual(cfg.configuration, self.dictCfg)

class Test_SimpleConfig(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    sqlitePath = os.path.join(self.outFolder, "test_db.sqlite")
    with open(sqlitePath, "a"):
      pass

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_loadXmlFile(self):
    cfg = pyauto_base.config.SimpleConfig()

    self.assertEqual(cfg.default, {})
    self.assertEqual(cfg.path, {})
    self.assertEqual(cfg.vars, {})

    fPath = os.path.join(self.inFolder, "config", "test_read_simple_all.xml")
    cfg.loadXmlFile(fPath)

    self.assertCountEqual(cfg.default, {"key1": "value1", u"键2": u"стоимость2"})
    self.assertCountEqual(cfg.path, {
        "path1": os.path.join(self.inFolder, "config", "some/path"),
        "path2": "/abs/path"
    })
    self.assertCountEqual(cfg.vars, {"var1": "0.123", "var2": "hello"})
    #cfg.showInfo()

    fPath = os.path.join(self.inFolder, "config", "test_read_simple_default.xml")
    cfg.loadXmlFile(fPath)

    self.assertCountEqual(cfg.default, {"key1": "value1", u"键2": u"стоимость2"})
    self.assertEqual(cfg.path, {})
    self.assertEqual(cfg.vars, {})

    fPath = os.path.join(self.inFolder, "config", "test_read_simple_path.xml")
    cfg.loadXmlFile(fPath)

    self.assertEqual(cfg.default, {})
    self.assertCountEqual(cfg.path, {
        "path1": os.path.join(self.inFolder, "config", "some/path"),
        "path2": "/abs/path"
    })
    self.assertEqual(cfg.vars, {})

    fPath = os.path.join(self.inFolder, "config", "test_read_simple_vars.xml")
    cfg.loadXmlFile(fPath)

    self.assertEqual(cfg.default, {})
    self.assertEqual(cfg.path, {})
    self.assertCountEqual(cfg.vars, {"var1": "0.123", "var2": "hello"})

  #@unittest.skip("")
  def test_loadCfgFile(self):
    cfg = pyauto_base.config.SimpleConfig()

    self.assertEqual(cfg.default, {})
    self.assertEqual(cfg.path, {})
    self.assertEqual(cfg.vars, {})

    fPath = os.path.join(self.inFolder, "config", "test_read_simple_all.cfg")
    cfg.loadCfgFile(fPath)

    self.assertCountEqual(cfg.default, {"key1": "value1", u"键2": u"стоимость2"})
    self.assertCountEqual(cfg.path, {
        "path1": os.path.join(self.inFolder, "config", "some/path"),
        "path2": "/abs/path"
    })
    self.assertCountEqual(cfg.vars, {"var1": "0.123", "var2": "hello"})
    #cfg.showInfo()

    fPath = os.path.join(self.inFolder, "config", "test_read_simple_default.cfg")
    cfg.loadCfgFile(fPath)

    self.assertCountEqual(cfg.default, {"key1": "value1", u"键2": u"стоимость2"})
    self.assertEqual(cfg.path, {})
    self.assertEqual(cfg.vars, {})

    fPath = os.path.join(self.inFolder, "config", "test_read_simple_path.cfg")
    cfg.loadCfgFile(fPath)

    self.assertEqual(cfg.default, {})
    self.assertCountEqual(cfg.path, {
        "path1": os.path.join(self.inFolder, "config", "some/path"),
        "path2": "/abs/path"
    })
    self.assertEqual(cfg.vars, {})

    fPath = os.path.join(self.inFolder, "config", "test_read_simple_vars.cfg")
    cfg.loadCfgFile(fPath)

    self.assertEqual(cfg.default, {})
    self.assertEqual(cfg.path, {})
    self.assertCountEqual(cfg.vars, {"var1": "0.123", "var2": "hello"})

class Test_DBConfig(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    sqlitePath = os.path.join(self.outFolder, "test_db.sqlite")
    with open(sqlitePath, "a"):
      pass

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_loadCfgFile(self):
    cfg = pyauto_base.config.DBConfig()
    fPath = os.path.join(self.inFolder, "config", "test_read_db.cfg")
    cfg.loadCfgFile(fPath)
    cfg.showInfo()

    dbId = "sqlite"
    self.assertEqual(cfg.dictDBInfo[dbId].backend, pyauto_base.db.dbBackendSqlite)
    self.assertEqual(cfg.dictDBInfo[dbId].host, "")
    self.assertEqual(cfg.dictDBInfo[dbId].port, "")
    self.assertEqual(cfg.dictDBInfo[dbId].user, "")
    self.assertEqual(os.path.basename(cfg.dictDBInfo[dbId].dbname), "test_db.sqlite")
    self.assertEqual(len(cfg.dictDBInfo[dbId].queries), 1)
    self.assertEqual(cfg.dictDBInfo[dbId].queries["sel1"], "SELECT * FROM [table1]")
    self.assertEqual(cfg.dictDBInfo[dbId].nTables, 1)
    self.assertCountEqual(cfg.dictDBInfo[dbId].getTable("test").colNames, ["comment", "id"])

    dbId = "mysql"
    self.assertEqual(cfg.dictDBInfo[dbId].backend, pyauto_base.db.dbBackendMysql)
    self.assertEqual(cfg.dictDBInfo[dbId].host, "localhost")
    self.assertEqual(cfg.dictDBInfo[dbId].port, "636")
    self.assertEqual(cfg.dictDBInfo[dbId].user, "user")
    self.assertEqual(cfg.dictDBInfo[dbId].dbname, "test")
    self.assertEqual(len(cfg.dictDBInfo[dbId].queries), 1)
    self.assertEqual(cfg.dictDBInfo[dbId].queries["sel1"], "SELECT * FROM `table2`")
    self.assertEqual(cfg.dictDBInfo[dbId].nTables, 1)
    self.assertCountEqual(cfg.dictDBInfo[dbId].getTable("test").colNames, ["comment", "id"])

    dbId = "mssql"
    self.assertEqual(cfg.dictDBInfo[dbId].backend, pyauto_base.db.dbBackendTsql)
    self.assertEqual(cfg.dictDBInfo[dbId].host, "yow1amsqlw03.am.sanm.corp")
    self.assertEqual(cfg.dictDBInfo[dbId].port, "5225")
    self.assertEqual(cfg.dictDBInfo[dbId].user, "caduser")
    self.assertEqual(cfg.dictDBInfo[dbId].dbname, "CADPARTS")
    self.assertEqual(len(cfg.dictDBInfo[dbId].queries), 1)
    self.assertEqual(cfg.dictDBInfo[dbId].queries["sel1"], "SELECT * FROM \"table3\"")
    self.assertEqual(cfg.dictDBInfo[dbId].nTables, 1)
    self.assertCountEqual(cfg.dictDBInfo[dbId].getTable("test").colNames, ["comment", "id"])

    dbId = "csv"
    self.assertEqual(cfg.dictDBInfo[dbId].backend, pyauto_base.db.dbBackendCsv)
    self.assertEqual(cfg.dictDBInfo[dbId].host, "")
    self.assertEqual(cfg.dictDBInfo[dbId].port, "")
    self.assertEqual(cfg.dictDBInfo[dbId].user, "")
    self.assertEqual(os.path.basename(cfg.dictDBInfo[dbId].dbname), "..")
    self.assertEqual(len(cfg.dictDBInfo[dbId].queries), 0)
    self.assertEqual(cfg.dictDBInfo[dbId].nTables, 0)

  #@unittest.skip("")
  def test_loadXmlFile(self):
    cfg = pyauto_base.config.DBConfig()
    fPath = os.path.join(self.inFolder, "config", "test_read_db.xml")
    cfg.loadXmlFile(fPath)
    cfg.showInfo()

    dbId = "sqlite"
    self.assertEqual(cfg.dictDBInfo[dbId].backend, pyauto_base.db.dbBackendSqlite)
    self.assertEqual(cfg.dictDBInfo[dbId].host, "")
    self.assertEqual(cfg.dictDBInfo[dbId].port, "")
    self.assertEqual(cfg.dictDBInfo[dbId].user, "")
    self.assertEqual(os.path.basename(cfg.dictDBInfo[dbId].dbname), "test_db.sqlite")
    self.assertEqual(len(cfg.dictDBInfo[dbId].queries), 2)
    self.assertEqual(cfg.dictDBInfo[dbId].queries["sel1"], "SELECT * FROM [table1]")
    self.assertEqual(cfg.dictDBInfo[dbId].queries["ins1"],
                     "INSERT INTO [table1] VALUES (\"a\", \"b\", 1, 2)")
    self.assertEqual(cfg.dictDBInfo[dbId].nTables, 1)
    self.assertCountEqual(cfg.dictDBInfo[dbId].getTable("test").colNames, ["comment", "id"])

    dbId = "mysql"
    self.assertEqual(cfg.dictDBInfo[dbId].backend, pyauto_base.db.dbBackendMysql)
    self.assertEqual(cfg.dictDBInfo[dbId].host, "localhost")
    self.assertEqual(cfg.dictDBInfo[dbId].port, "636")
    self.assertEqual(cfg.dictDBInfo[dbId].user, "user")
    self.assertEqual(cfg.dictDBInfo[dbId].dbname, "test")
    self.assertEqual(len(cfg.dictDBInfo[dbId].queries), 2)
    self.assertEqual(cfg.dictDBInfo[dbId].queries["sel1"], "SELECT * FROM `table2`")
    self.assertEqual(cfg.dictDBInfo[dbId].queries["ins1"],
                     "INSERT INTO `table2` VALUES (\"a\", \"b\", 1, 2)")
    self.assertEqual(cfg.dictDBInfo[dbId].nTables, 1)
    self.assertCountEqual(cfg.dictDBInfo[dbId].getTable("test").colNames, ["comment", "id"])

    dbId = "mssql"
    self.assertEqual(cfg.dictDBInfo[dbId].backend, pyauto_base.db.dbBackendTsql)
    self.assertEqual(cfg.dictDBInfo[dbId].host, "yow1amsqlw03.am.sanm.corp")
    self.assertEqual(cfg.dictDBInfo[dbId].port, "5225")
    self.assertEqual(cfg.dictDBInfo[dbId].user, "caduser")
    self.assertEqual(cfg.dictDBInfo[dbId].dbname, "CADPARTS")
    self.assertEqual(len(cfg.dictDBInfo[dbId].queries), 2)
    self.assertEqual(cfg.dictDBInfo[dbId].queries["sel1"], "SELECT * FROM \"table3\"")
    self.assertEqual(cfg.dictDBInfo[dbId].queries["ins1"],
                     "INSERT INTO \"table3\" VALUES ('a', 'b', 1, 2)")
    self.assertEqual(cfg.dictDBInfo[dbId].nTables, 1)
    self.assertCountEqual(cfg.dictDBInfo[dbId].getTable("test").colNames, ["comment", "id"])

    dbId = "csv"
    self.assertEqual(cfg.dictDBInfo[dbId].backend, pyauto_base.db.dbBackendCsv)
    self.assertEqual(cfg.dictDBInfo[dbId].host, "")
    self.assertEqual(cfg.dictDBInfo[dbId].port, "")
    self.assertEqual(cfg.dictDBInfo[dbId].user, "")
    self.assertEqual(os.path.basename(cfg.dictDBInfo[dbId].dbname), "..")
    self.assertEqual(len(cfg.dictDBInfo[dbId].queries), 0)
    self.assertEqual(cfg.dictDBInfo[dbId].nTables, 0)

class Test_DocConfig(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  def test_loadXmlFile(self):
    cfg = pyauto_base.config.DocConfig()
    fPath = os.path.join(self.inFolder, "config", "test_read_doc.xml")
    cfg.loadXmlFile(fPath)
    cfg.showInfo()

    estId = "project_1"
    self.assertEqual(cfg.estimate[estId][0], ["task 101", "1d"])
    self.assertEqual(cfg.estimate[estId][1], ["task 102", "2d"])

    estId = "project_2"
    self.assertEqual(cfg.estimate[estId][0], ["task 201", "2d"])
    self.assertEqual(cfg.estimate[estId][1], ["task 202", "0d"])
    self.assertEqual(cfg.estimate[estId][2], ["task 203", "0d"])
