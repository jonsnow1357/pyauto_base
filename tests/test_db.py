#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_base.db"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_base.config
import pyauto_base.db

class Test_DBTable(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    sqlitePath = os.path.join(self.outFolder, "test_db.sqlite")
    with open(sqlitePath, "a"):
      pass

    for f in os.listdir(self.outFolder):
      if (f.lower().endswith(".csv")):
        os.remove(os.path.join(self.outFolder, f))

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    dbTbl = pyauto_base.db.DBTable("table1")

    self.assertEqual(dbTbl.name, "table1")
    self.assertEqual(dbTbl.colNames, [])

class Test_DBInfo(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    sqlitePath = os.path.join(self.outFolder, "test_db.sqlite")
    with open(sqlitePath, "a"):
      pass

    for f in os.listdir(self.outFolder):
      if (f.lower().endswith(".csv")):
        os.remove(os.path.join(self.outFolder, f))

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    dbInfo = pyauto_base.db.DBInfo()

    self.assertEqual(dbInfo.backend, "")
    self.assertEqual(dbInfo.host, "")
    self.assertEqual(dbInfo.port, "")
    self.assertEqual(dbInfo.user, "")
    self.assertEqual(dbInfo.pswd, "")
    self.assertEqual(dbInfo.dbname, "")
    self.assertCountEqual(dbInfo.queries, {})
    self.assertEqual(dbInfo.nTables, 0)
    self.assertEqual(dbInfo.tableNames, [])

  #@unittest.skip("")
  def test_setInfo(self):
    dbInfo = pyauto_base.db.DBInfo()

    dbname = pyauto_base.misc.getRndStr(128)
    dictCfg = {"backend": pyauto_base.db.dbBackendSqlite, "dbname": dbname}
    dbInfo.setInfo(dictCfg)
    self.assertEqual(dbInfo.backend, pyauto_base.db.dbBackendSqlite)
    self.assertEqual(dbInfo.host, "")
    self.assertEqual(dbInfo.port, "")
    self.assertEqual(dbInfo.user, "")
    self.assertEqual(dbInfo.pswd, "")
    self.assertEqual(dbInfo.dbname, dbname)
    self.assertEqual(dbInfo.queries, {})

    dbname = pyauto_base.misc.getRndStr(128)
    dictCfg = {"backend": pyauto_base.db.dbBackendCsv, "dbname": dbname}
    dbInfo.setInfo(dictCfg)
    self.assertEqual(dbInfo.backend, pyauto_base.db.dbBackendCsv)
    self.assertEqual(dbInfo.host, "")
    self.assertEqual(dbInfo.port, "")
    self.assertEqual(dbInfo.user, "")
    self.assertEqual(dbInfo.pswd, "")
    self.assertEqual(dbInfo.dbname, dbname)
    self.assertEqual(dbInfo.queries, {})

    host = pyauto_base.misc.getRndStr(128)
    user = pyauto_base.misc.getRndStr(128)
    pswd = pyauto_base.misc.getRndStr(128)
    dbname = pyauto_base.misc.getRndStr(128)
    dictCfg = {
        "backend": pyauto_base.db.dbBackendMysql,
        "hostname": host,
        "username": user,
        "password": pswd,
        "dbname": dbname
    }
    dbInfo.setInfo(dictCfg)
    self.assertEqual(dbInfo.backend, pyauto_base.db.dbBackendMysql)
    self.assertEqual(dbInfo.host, host)
    self.assertEqual(dbInfo.port, "3306")
    self.assertEqual(dbInfo.user, user)
    self.assertEqual(dbInfo.pswd, pswd)
    self.assertEqual(dbInfo.dbname, dbname)
    self.assertEqual(dbInfo.queries, {})

    host = pyauto_base.misc.getRndStr(128)
    port = pyauto_base.misc.getRndStr(128)
    user = pyauto_base.misc.getRndStr(128)
    dbname = pyauto_base.misc.getRndStr(128)
    dictCfg = {
        "backend": pyauto_base.db.dbBackendMysql,
        "hostname": host,
        "port": port,
        "username": user,
        "dbname": dbname
    }
    dbInfo.setInfo(dictCfg)
    self.assertEqual(dbInfo.backend, pyauto_base.db.dbBackendMysql)
    self.assertEqual(dbInfo.host, host)
    self.assertEqual(dbInfo.port, port)
    self.assertEqual(dbInfo.user, user)
    self.assertEqual(dbInfo.pswd, "")
    self.assertEqual(dbInfo.dbname, dbname)
    self.assertEqual(dbInfo.queries, {})

    host = pyauto_base.misc.getRndStr(128)
    user = pyauto_base.misc.getRndStr(128)
    pswd = pyauto_base.misc.getRndStr(128)
    dbname = pyauto_base.misc.getRndStr(128)
    dictCfg = {
        "backend": pyauto_base.db.dbBackendTsql,
        "hostname": host,
        "username": user,
        "password": pswd,
        "dbname": dbname
    }
    dbInfo.setInfo(dictCfg)
    self.assertEqual(dbInfo.backend, pyauto_base.db.dbBackendTsql)
    self.assertEqual(dbInfo.host, host)
    self.assertEqual(dbInfo.port, "1433")
    self.assertEqual(dbInfo.user, user)
    self.assertEqual(dbInfo.pswd, pswd)
    self.assertEqual(dbInfo.dbname, dbname)
    self.assertEqual(dbInfo.queries, {})

  #@unittest.skip("")
  def test_addTable(self):
    dbInfo = pyauto_base.db.DBInfo()
    self.assertEqual(dbInfo.nTables, 0)
    self.assertEqual(dbInfo.tableNames, [])

    dbTbl = pyauto_base.db.DBTable("table1")
    dbInfo.addTable(dbTbl)
    self.assertEqual(dbInfo.nTables, 1)
    self.assertEqual(dbInfo.tableNames, ["table1"])

    dbTbl = pyauto_base.db.DBTable("table2")
    dbInfo.addTable(dbTbl)
    self.assertEqual(dbInfo.nTables, 2)
    self.assertEqual(dbInfo.tableNames, ["table1", "table2"])

  #@unittest.skip("")
  def test_clrTables(self):
    dbInfo = pyauto_base.db.DBInfo()
    self.assertEqual(dbInfo.nTables, 0)
    self.assertEqual(dbInfo.tableNames, [])

    dbTbl = pyauto_base.db.DBTable("table1")
    dbInfo.addTable(dbTbl)
    dbTbl = pyauto_base.db.DBTable("table2")
    dbInfo.addTable(dbTbl)
    self.assertEqual(dbInfo.nTables, 2)
    self.assertEqual(dbInfo.tableNames, ["table1", "table2"])

    dbInfo.clrTables()
    self.assertEqual(dbInfo.nTables, 0)
    self.assertEqual(dbInfo.tableNames, [])

  #@unittest.skip("")
  def test_getTable(self):
    dbInfo = pyauto_base.db.DBInfo()
    self.assertEqual(dbInfo.nTables, 0)
    self.assertEqual(dbInfo.tableNames, [])

    dbTbl1 = pyauto_base.db.DBTable("table1")
    dbInfo.addTable(dbTbl1)
    dbTbl2 = dbInfo.getTable("table1")

    self.assertEqual(dbTbl1.name, dbTbl2.name)
    self.assertCountEqual(dbTbl1.colNames, dbTbl2.colNames)

class Test_DBQuery(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    sqlitePath = os.path.join(self.outFolder, "test_db.sqlite")
    with open(sqlitePath, "a"):
      pass

    for f in os.listdir(self.outFolder):
      if (f.lower().endswith(".csv")):
        os.remove(os.path.join(self.outFolder, f))

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_DELETE(self):
    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendSqlite)
    qry.delete("TABLE")
    self.assertEqual(qry.sql, "DELETE FROM \"TABLE\";")
    self.assertEqual(qry.params, [])

    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendMysql)
    qry.delete("TABLE")
    self.assertEqual(qry.sql, "DELETE FROM `TABLE`;")
    self.assertEqual(qry.params, [])

    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendTsql)
    qry.delete("TABLE")
    self.assertEqual(qry.sql, "DELETE FROM \"TABLE\";")
    self.assertEqual(qry.params, [])

  #@unittest.skip("")
  def test_DROP(self):
    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendSqlite)
    qry.drop("TABLE")
    self.assertEqual(qry.sql, "DROP TABLE IF EXISTS \"TABLE\";")
    self.assertEqual(qry.params, [])

    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendMysql)
    qry.drop("TABLE")
    self.assertEqual(qry.sql, "DROP TABLE IF EXISTS `TABLE`;")
    self.assertEqual(qry.params, [])

    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendTsql)
    qry.drop("TABLE")
    self.assertEqual(qry.sql, "DROP TABLE \"TABLE\";")
    self.assertEqual(qry.params, [])

  #@unittest.skip("")
  def test_CREATE(self):
    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendSqlite)
    qry.create("TABLE", {"COL1": "INT", "COL2": "TEXT"})
    self.assertEqual(qry.sql, "CREATE TABLE \"TABLE\" (\"COL1\" INT, \"COL2\" TEXT);")
    #self.assertEqual(qry.params, [])

    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendMysql)
    qry.create("TABLE", {"COL1": "INT", "COL2": "TEXT"})
    self.assertEqual(qry.sql, "CREATE TABLE `TABLE` (`COL1` INT, `COL2` TEXT);")
    #self.assertEqual(qry.params, [])

    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendTsql)
    qry.create("TABLE", {"COL1": "INT", "COL2": "TEXT"})
    self.assertEqual(qry.sql, "CREATE TABLE \"TABLE\" (\"COL1\" INT, \"COL2\" TEXT);")
    #self.assertEqual(qry.params, [])

  #@unittest.skip("")
  def test_SELECT(self):
    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendSqlite)
    qry.select("TABLE.SCHEMA")
    self.assertEqual(qry.sql, "SELECT * FROM \"TABLE\".\"SCHEMA\";")
    self.assertEqual(qry.params, [])
    qry.select("TABLE.SCHEMA", ["COL1", "COL_2"])
    self.assertEqual(qry.sql, "SELECT \"COL1\", \"COL_2\" FROM \"TABLE\".\"SCHEMA\";")
    self.assertEqual(qry.params, [])
    qry.select("TABLE.SCHEMA")
    qry.where("COL3")
    self.assertEqual(qry.sql,
                     "SELECT * FROM \"TABLE\".\"SCHEMA\" WHERE \"COL3\" IS NOT NULL;")
    self.assertEqual(qry.params, [])
    qry.where("COL4", test="like", val="me")
    self.assertEqual(
        qry.sql,
        "SELECT * FROM \"TABLE\".\"SCHEMA\" WHERE \"COL3\" IS NOT NULL AND \"COL4\" LIKE ?;"
    )
    self.assertCountEqual(qry.params, ["%me%"])
    qry.params = [None, "C%d"]
    self.assertEqual(
        qry.sql,
        "SELECT * FROM \"TABLE\".\"SCHEMA\" WHERE \"COL3\" IS NOT NULL AND \"COL4\" LIKE ?;"
    )
    self.assertCountEqual(qry.params, ["C%d"])
    qry.where("COL3", test="eq", bClear=True)
    self.assertEqual(qry.sql, "SELECT * FROM \"TABLE\".\"SCHEMA\" WHERE \"COL3\" IS NULL;")
    self.assertEqual(qry.params, [])
    qry.params = ["val"]
    self.assertEqual(qry.sql, "SELECT * FROM \"TABLE\".\"SCHEMA\" WHERE \"COL3\"=?;")
    self.assertCountEqual(qry.params, ["val"])

    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendMysql)
    qry.select("TABLE.SCHEMA")
    self.assertEqual(qry.sql, "SELECT * FROM `TABLE`.`SCHEMA`;")
    self.assertEqual(qry.params, [])
    qry.select("TABLE.SCHEMA", ["COL1", "COL_2"])
    self.assertEqual(qry.sql, "SELECT `COL1`, `COL_2` FROM `TABLE`.`SCHEMA`;")
    self.assertEqual(qry.params, [])
    qry.select("TABLE.SCHEMA")
    qry.where("COL3")
    self.assertEqual(qry.sql, "SELECT * FROM `TABLE`.`SCHEMA` WHERE `COL3` IS NOT NULL;")
    self.assertEqual(qry.params, [])
    qry.where("COL4", test="like", val="me")
    self.assertEqual(
        qry.sql,
        "SELECT * FROM `TABLE`.`SCHEMA` WHERE `COL3` IS NOT NULL AND `COL4` LIKE %s;")
    self.assertCountEqual(qry.params, ["%me%"])
    qry.params = [None, "C%d"]
    self.assertEqual(
        qry.sql,
        "SELECT * FROM `TABLE`.`SCHEMA` WHERE `COL3` IS NOT NULL AND `COL4` LIKE %s;")
    self.assertCountEqual(qry.params, ["C%d"])
    qry.where("COL3", test="eq", bClear=True)
    self.assertEqual(qry.sql, "SELECT * FROM `TABLE`.`SCHEMA` WHERE `COL3` IS NULL;")
    self.assertEqual(qry.params, [])
    qry.params = ["val"]
    self.assertEqual(qry.sql, "SELECT * FROM `TABLE`.`SCHEMA` WHERE `COL3`=%s;")
    self.assertCountEqual(qry.params, ["val"])

    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendTsql)
    qry.select("TABLE.SCHEMA")
    self.assertEqual(qry.sql, "SELECT * FROM \"TABLE\".\"SCHEMA\";")
    self.assertEqual(qry.params, [])
    qry.select("TABLE.SCHEMA", ["COL1", "COL_2"])
    self.assertEqual(qry.sql, "SELECT \"COL1\", \"COL_2\" FROM \"TABLE\".\"SCHEMA\";")
    self.assertEqual(qry.params, [])
    qry.select("TABLE.SCHEMA")
    qry.where("COL3")
    self.assertEqual(qry.sql,
                     "SELECT * FROM \"TABLE\".\"SCHEMA\" WHERE \"COL3\" IS NOT NULL;")
    self.assertEqual(qry.params, [])
    qry.where("COL4", test="like", val="me")
    self.assertEqual(
        qry.sql,
        "SELECT * FROM \"TABLE\".\"SCHEMA\" WHERE \"COL3\" IS NOT NULL AND \"COL4\" LIKE %s;"
    )
    self.assertCountEqual(qry.params, ["%me%"])
    qry.params = [None, "C%d"]
    self.assertEqual(
        qry.sql,
        "SELECT * FROM \"TABLE\".\"SCHEMA\" WHERE \"COL3\" IS NOT NULL AND \"COL4\" LIKE %s;"
    )
    self.assertCountEqual(qry.params, ["C%d"])
    qry.where("COL3", test="eq", bClear=True)
    self.assertEqual(qry.sql, "SELECT * FROM \"TABLE\".\"SCHEMA\" WHERE \"COL3\" IS NULL;")
    self.assertEqual(qry.params, [])
    qry.params = ["val"]
    self.assertEqual(qry.sql, "SELECT * FROM \"TABLE\".\"SCHEMA\" WHERE \"COL3\"=%s;")
    self.assertCountEqual(qry.params, ["val"])

  #@unittest.skip("")
  def test_INSERT(self):
    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendSqlite)
    qry.insert("TABLE", ["COL1", "COL2"])
    self.assertEqual(qry.sql, "INSERT INTO \"TABLE\" (\"COL1\", \"COL2\") VALUES (?, ?);")
    self.assertCountEqual(qry.params, [None, None])
    qry.where("COL3", test="eq")
    self.assertEqual(
        qry.sql,
        "INSERT INTO \"TABLE\" (\"COL1\", \"COL2\") VALUES (?, ?) WHERE \"COL3\" IS NULL;")
    self.assertCountEqual(qry.params, [None, None])
    qry.params = ["c1", None, None]
    self.assertEqual(
        qry.sql,
        "INSERT INTO \"TABLE\" (\"COL1\", \"COL2\") VALUES (?, ?) WHERE \"COL3\" IS NULL;")
    self.assertCountEqual(qry.params, ["c1", None])
    qry.params = [None, "c2", "val"]
    self.assertEqual(
        qry.sql,
        "INSERT INTO \"TABLE\" (\"COL1\", \"COL2\") VALUES (?, ?) WHERE \"COL3\"=?;")
    self.assertCountEqual(qry.params, [None, "c2", "val"])

    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendMysql)
    qry.insert("TABLE", ["COL1", "COL2"])
    self.assertEqual(qry.sql, "INSERT INTO `TABLE` (`COL1`, `COL2`) VALUES (%s, %s);")
    self.assertCountEqual(qry.params, [None, None])
    qry.where("COL3", test="eq")
    self.assertEqual(
        qry.sql,
        "INSERT INTO `TABLE` (`COL1`, `COL2`) VALUES (%s, %s) WHERE `COL3` IS NULL;")
    self.assertCountEqual(qry.params, [None, None])
    qry.params = ["c1", None, None]
    self.assertEqual(
        qry.sql,
        "INSERT INTO `TABLE` (`COL1`, `COL2`) VALUES (%s, %s) WHERE `COL3` IS NULL;")
    self.assertCountEqual(qry.params, ["c1", None])
    qry.params = [None, "c2", "val"]
    self.assertEqual(
        qry.sql, "INSERT INTO `TABLE` (`COL1`, `COL2`) VALUES (%s, %s) WHERE `COL3`=%s;")
    self.assertCountEqual(qry.params, [None, "c2", "val"])

    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendTsql)
    qry.insert("TABLE", ["COL1", "COL2"])
    self.assertEqual(qry.sql, "INSERT INTO \"TABLE\" (\"COL1\", \"COL2\") VALUES (%s, %s);")
    self.assertCountEqual(qry.params, [None, None])
    qry.where("COL3", test="eq")
    self.assertEqual(
        qry.sql,
        "INSERT INTO \"TABLE\" (\"COL1\", \"COL2\") VALUES (%s, %s) WHERE \"COL3\" IS NULL;"
    )
    self.assertCountEqual(qry.params, [None, None])
    qry.params = ["c1", None, None]
    self.assertEqual(
        qry.sql,
        "INSERT INTO \"TABLE\" (\"COL1\", \"COL2\") VALUES (%s, %s) WHERE \"COL3\" IS NULL;"
    )
    self.assertCountEqual(qry.params, ["c1", None])
    qry.params = [None, "c2", "val"]
    self.assertEqual(
        qry.sql,
        "INSERT INTO \"TABLE\" (\"COL1\", \"COL2\") VALUES (%s, %s) WHERE \"COL3\"=%s;")
    self.assertCountEqual(qry.params, [None, "c2", "val"])

  #@unittest.skip("")
  def test_UPDATE(self):
    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendSqlite)
    qry.update("TABLE", ["COL1", "COL2"])
    qry.where("COL3", test="eq")
    self.assertEqual(qry.sql,
                     "UPDATE \"TABLE\" SET \"COL1\"=?, \"COL2\"=? WHERE \"COL3\" IS NULL;")
    self.assertCountEqual(qry.params, [None, None])
    qry.params = [None, "c2", "val"]
    self.assertEqual(qry.sql,
                     "UPDATE \"TABLE\" SET \"COL1\"=?, \"COL2\"=? WHERE \"COL3\"=?;")
    self.assertCountEqual(qry.params, [None, "c2", "val"])
    qry.params = ["c1", None, None]
    self.assertEqual(qry.sql,
                     "UPDATE \"TABLE\" SET \"COL1\"=?, \"COL2\"=? WHERE \"COL3\" IS NULL;")
    self.assertCountEqual(qry.params, ["c1", None])

    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendMysql)
    qry.update("TABLE", ["COL1", "COL2"])
    qry.where("COL3", test="eq")
    self.assertEqual(qry.sql,
                     "UPDATE `TABLE` SET `COL1`=%s, `COL2`=%s WHERE `COL3` IS NULL;")
    self.assertCountEqual(qry.params, [None, None])
    qry.params = [None, "c2", "val"]
    self.assertEqual(qry.sql, "UPDATE `TABLE` SET `COL1`=%s, `COL2`=%s WHERE `COL3`=%s;")
    self.assertCountEqual(qry.params, [None, "c2", "val"])
    qry.params = ["c1", None, None]
    self.assertEqual(qry.sql,
                     "UPDATE `TABLE` SET `COL1`=%s, `COL2`=%s WHERE `COL3` IS NULL;")
    self.assertCountEqual(qry.params, ["c1", None])

    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendTsql)
    qry.update("TABLE", ["COL1", "COL2"])
    qry.where("COL3", test="eq")
    self.assertEqual(
        qry.sql, "UPDATE \"TABLE\" SET \"COL1\"=%s, \"COL2\"=%s WHERE \"COL3\" IS NULL;")
    self.assertCountEqual(qry.params, [None, None])
    qry.params = [None, "c2", "val"]
    self.assertEqual(qry.sql,
                     "UPDATE \"TABLE\" SET \"COL1\"=%s, \"COL2\"=%s WHERE \"COL3\"=%s;")
    self.assertCountEqual(qry.params, [None, "c2", "val"])
    qry.params = ["c1", None, None]
    self.assertEqual(
        qry.sql, "UPDATE \"TABLE\" SET \"COL1\"=%s, \"COL2\"=%s WHERE \"COL3\" IS NULL;")
    self.assertCountEqual(qry.params, ["c1", None])

class Test_DBController(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    sqlitePath = os.path.join(self.outFolder, "test_db.sqlite")
    with open(sqlitePath, "a"):
      pass

    for f in os.listdir(self.outFolder):
      if (f.lower().endswith(".csv")):
        os.remove(os.path.join(self.outFolder, f))

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_sqlite(self):
    dbCfg = pyauto_base.config.DBConfig()
    fPath = os.path.join(self.inFolder, "test_read_db.cfg")
    dbCfg.loadCfgFile(fPath)
    #dbCfg.showInfo()

    dbCtl = pyauto_base.db.DBController()
    dbCtl.dbInfo = dbCfg.dictDBInfo["sqlite"]
    dbCtl.doQuery = True
    logger.info(dbCtl.dbInfo)
    dbCtl.connect()

    qry = pyauto_base.db.DBQuery(dbCtl.backend)
    dbCtl.getTables()
    if ("test" in dbCtl.dbInfo.tableNames):
      qry.drop("test")
      dbCtl.query(qry.sql)
      dbCtl.getTables()
      self.assertNotIn("test", dbCtl.dbInfo.tableNames)

    qry.create("test", {"id": "INTEGER PRIMARY KEY NOT NULL UNIQUE", "comment": "TEXT"})
    dbCtl.query(qry.sql)
    dbCtl.getTables()
    self.assertIn("test", dbCtl.dbInfo.tableNames)
    self.assertCountEqual(dbCtl.dbInfo.getTable("test").colNames, ["comment", "id"])

    qry.insert("test", ["comment"])
    s1 = pyauto_base.misc.getRndStr(16)
    dbCtl.query(qry.sql, [s1])
    s2 = pyauto_base.misc.getRndStr(16)
    dbCtl.query(qry.sql, [s2])

    qry.select("test")
    res = dbCtl.query(qry.sql, bFetch=True)
    self.assertEqual(len(res), 2)
    self.assertIn(s1, res[0])
    self.assertIn(s2, res[1])

    res = dbCtl.selectTable("test")
    self.assertEqual(len(res), 2)
    self.assertIn(s1, res[0])
    self.assertIn(s2, res[1])

    dbCtl.showInfo()
    dbCtl.disconnect()

    self.assertGreater(
        os.path.getsize(os.path.join(self.inFolder, dbCfg.dictDBInfo["sqlite"].dbname)),
        1024)

  #@unittest.skip("")
  def test_csv(self):
    dbCfg = pyauto_base.config.DBConfig()
    fPath = os.path.join(self.inFolder, "test_read_db.cfg")
    dbCfg.loadCfgFile(fPath)
    #dbCfg.showInfo()

    dbCtl = pyauto_base.db.DBController()
    dbCtl.dbInfo = dbCfg.dictDBInfo["csv"]
    logger.info(dbCtl.dbInfo)
    dbCtl.connect()

    dbCtl.getTables()
    self.assertCountEqual(dbCtl.dbInfo.tableNames, ["test_read_CSV"])

    res = dbCtl.selectTable("test_read_CSV")
    self.assertEqual(len(res), 4)
    self.assertIn("a", res[0])
    self.assertIn("2", res[1])
    self.assertIn("7", res[2])
    self.assertIn("h", res[3])

    dbCtl.showInfo()
    dbCtl.disconnect()

  #@unittest.skip("")
  def test_mysql(self):
    dbCfg = pyauto_base.config.DBConfig()
    fPath = os.path.join(self.inFolder, "test_read_db.cfg")
    dbCfg.loadCfgFile(fPath)
    #dbCfg.showInfo()

    dbCtl = pyauto_base.db.DBController()
    dbCtl.dbInfo = dbCfg.dictDBInfo["mysql"]
    dbCtl.doQuery = True
    logger.info(dbCtl.dbInfo)
    try:
      dbCtl.connect()
    except pyauto_base.db.DBException:
      self.skipTest("")

    qry = pyauto_base.db.DBQuery(dbCtl.backend)
    dbCtl.getTables()
    if ("test" in dbCtl.dbInfo.tableNames):
      qry.drop("test")
      dbCtl.query(qry.sql)
      dbCtl.getTables()
      self.assertNotIn("test", dbCtl.dbInfo.tableNames)

    qry.create("test", {"id": "INT NOT NULL DEFAULT 0", "comment": "TEXT"})
    dbCtl.query(qry.sql)
    dbCtl.getTables()
    self.assertIn("test", dbCtl.dbInfo.tableNames)
    self.assertCountEqual(dbCtl.dbInfo.getTable("test").colNames, ["comment", "id"])

    qry.insert("test", ["comment"])
    s1 = pyauto_base.misc.getRndStr(16)
    dbCtl.query(qry.sql, [s1])
    s2 = pyauto_base.misc.getRndStr(16)
    dbCtl.query(qry.sql, [s2])

    qry.select("test")
    res = dbCtl.query(qry.sql, bFetch=True)
    self.assertEqual(len(res), 2)
    self.assertIn(s1, res[0])
    self.assertIn(s2, res[1])

    res = dbCtl.selectTable("test")
    self.assertEqual(len(res), 2)
    self.assertIn(s1, res[0])
    self.assertIn(s2, res[1])

    dbCtl.showInfo()
    dbCtl.disconnect()

  @unittest.skip("")
  def test_mssql(self):
    dbCfg = pyauto_base.config.DBConfig()
    fPath = os.path.join(self.inFolder, "test_read_db.cfg")
    dbCfg.loadCfgFile(fPath)
    #dbCfg.showInfo()

    dbCtl = pyauto_base.db.DBController()
    dbCtl.dbInfo = dbCfg.dictDBInfo["mssql"]
    dbCtl.doQuery = True
    logger.info(dbCtl.dbInfo)
    dbCtl.connect()

    qry = pyauto_base.db.DBQuery(dbCtl.backend)
    dbCtl.getTables()
    if ("test" in dbCtl.dbInfo.tableNames):
      qry.drop("test")
      dbCtl.query(qry.sql)
      dbCtl.getTables()
      self.assertNotIn("test", dbCtl.dbInfo.tableNames)

    qry.create("test", {"id": "INT NOT NULL DEFAULT 0", "comment": "TEXT"})
    dbCtl.query(qry.sql)
    dbCtl.getTables()
    self.assertIn("test", dbCtl.dbInfo.tableNames)
    self.assertCountEqual(dbCtl.dbInfo.getTable("test").colNames, ["comment", "id"])

    qry.insert("test", ["comment"])
    s1 = pyauto_base.misc.getRndStr(16)
    dbCtl.query(qry.sql, [s1])
    s2 = pyauto_base.misc.getRndStr(16)
    dbCtl.query(qry.sql, [s2])

    qry.select("test")
    res = dbCtl.query(qry.sql, bFetch=True)
    self.assertEqual(len(res), 2)
    self.assertIn(s1, res[0])
    self.assertIn(s2, res[1])

    res = dbCtl.selectTable("test")
    self.assertEqual(len(res), 2)
    self.assertIn(s1, res[0])
    self.assertIn(s2, res[1])

    dbCtl.showInfo()
    dbCtl.disconnect()
