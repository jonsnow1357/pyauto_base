#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_base.bin"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.bin

class Test_bin(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_isHexString(self):
    self.assertTrue(pyauto_base.bin.isHexString("0xA", -8))
    self.assertTrue(pyauto_base.bin.isHexString("0xA", 0))
    self.assertTrue(pyauto_base.bin.isHexString("0xA"))
    self.assertTrue(pyauto_base.bin.isHexString("0xA8"))
    self.assertTrue(pyauto_base.bin.isHexString("0xA83"))
    self.assertTrue(pyauto_base.bin.isHexString("0xA83C", 2))

    self.assertFalse(pyauto_base.bin.isHexString("0xA83C", 1))
    self.assertFalse(pyauto_base.bin.isHexString("x45bD"))
    self.assertFalse(pyauto_base.bin.isHexString("0x45bDw"))

  #@unittest.skip("")
  def test_isBinString(self):
    self.assertTrue(pyauto_base.bin.isBinString("b01011", -8))
    self.assertTrue(pyauto_base.bin.isBinString("b01011", 0))
    self.assertTrue(pyauto_base.bin.isBinString("b01011"))
    self.assertTrue(pyauto_base.bin.isBinString("b01011011"))
    self.assertTrue(pyauto_base.bin.isBinString("b010110110001", 12))

    self.assertFalse(pyauto_base.bin.isBinString("b010110110001", 10))
    self.assertFalse(pyauto_base.bin.isBinString("01001011"))
    self.assertFalse(pyauto_base.bin.isBinString("b01001011t"))

  #@unittest.skip("")
  # def test_hex2bin(self):
  #   self.assertEqual(pyauto_base.bin.hex2bin("0x0"), "b0000")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0x1"), "b0001")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0x2"), "b0010")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0x3"), "b0011")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0x4"), "b0100")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0x5"), "b0101")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0x6"), "b0110")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0x7"), "b0111")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0x8"), "b1000")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0x9"), "b1001")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0xa"), "b1010")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0xb"), "b1011")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0xc"), "b1100")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0xd"), "b1101")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0xe"), "b1110")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0xf"), "b1111")
  #   self.assertEqual(pyauto_base.bin.hex2bin("0x16BEEF"), "b000101101011111011101111")

  #@unittest.skip("")
  # def test_int2bin(self):
  #   self.assertEqual(pyauto_base.bin.int2bin(0), "b0")
  #   self.assertEqual(pyauto_base.bin.int2bin(1), "b1")
  #   self.assertEqual(pyauto_base.bin.int2bin(2), "b10")
  #   self.assertEqual(pyauto_base.bin.int2bin(3), "b11")
  #   self.assertEqual(pyauto_base.bin.int2bin(4), "b100")
  #   self.assertEqual(pyauto_base.bin.int2bin(5), "b101")
  #   self.assertEqual(pyauto_base.bin.int2bin(6), "b110")
  #   self.assertEqual(pyauto_base.bin.int2bin(7), "b111")
  #   self.assertEqual(pyauto_base.bin.int2bin(8), "b1000")
  #   self.assertEqual(pyauto_base.bin.int2bin(9), "b1001")
  #   self.assertEqual(pyauto_base.bin.int2bin(10), "b1010")
  #   self.assertEqual(pyauto_base.bin.int2bin(11), "b1011")
  #   self.assertEqual(pyauto_base.bin.int2bin(12), "b1100")
  #   self.assertEqual(pyauto_base.bin.int2bin(13), "b1101")
  #   self.assertEqual(pyauto_base.bin.int2bin(14), "b1110")
  #   self.assertEqual(pyauto_base.bin.int2bin(15), "b1111")
  #
  #   self.assertEqual(pyauto_base.bin.int2bin(16), "b10000")
  #   self.assertEqual(pyauto_base.bin.int2bin(42), "b101010")
  #   self.assertEqual(pyauto_base.bin.int2bin(314), "b100111010")
  #   self.assertEqual(pyauto_base.bin.int2bin(31415), "b111101010110111")
  #   self.assertEqual(pyauto_base.bin.int2bin(314159265), "b10010101110011011000010100001")

  # @unittest.skip("")
  # def test_bin2hex(self):
  #   self.assertEqual(pyauto_base.bin.bin2hex("b0000"), "0x0")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b0001"), "0x1")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b0010"), "0x2")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b0011"), "0x3")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b0100"), "0x4")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b0101"), "0x5")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b0110"), "0x6")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b0111"), "0x7")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b1000"), "0x8")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b1001"), "0x9")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b1010"), "0xA")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b1011"), "0xB")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b1100"), "0xC")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b1101"), "0xD")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b1110"), "0xE")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b1111"), "0xF")
  #   self.assertEqual(pyauto_base.bin.bin2hex("b000101101011111011101111"), "0x16BEEF")

  #@unittest.skip("")
  def test_getBit(self):
    self.assertEqual(pyauto_base.bin.getBit(0, "0x42"), 0)
    self.assertEqual(pyauto_base.bin.getBit(1, "0x42"), 1)
    self.assertEqual(pyauto_base.bin.getBit(2, "0x42"), 0)
    self.assertEqual(pyauto_base.bin.getBit(3, "0x42"), 0)
    self.assertEqual(pyauto_base.bin.getBit(4, "0x42"), 0)
    self.assertEqual(pyauto_base.bin.getBit(5, "0x42"), 0)
    self.assertEqual(pyauto_base.bin.getBit(6, "0x42"), 1)
    self.assertEqual(pyauto_base.bin.getBit(7, "0x42"), 0)

  #@unittest.skip("")
  def test_setBit(self):
    self.assertEqual(pyauto_base.bin.setBit(0, "0x00"), "0x01")
    self.assertEqual(pyauto_base.bin.setBit(1, "0x00"), "0x02")
    self.assertEqual(pyauto_base.bin.setBit(2, "0x00"), "0x04")
    self.assertEqual(pyauto_base.bin.setBit(3, "0x00"), "0x08")
    self.assertEqual(pyauto_base.bin.setBit(4, "0x00"), "0x10")
    self.assertEqual(pyauto_base.bin.setBit(5, "0x00"), "0x20")
    self.assertEqual(pyauto_base.bin.setBit(6, "0x00"), "0x40")
    self.assertEqual(pyauto_base.bin.setBit(7, "0x00"), "0x80")
    self.assertEqual(pyauto_base.bin.setBit(15, "0x00"), "0x8000")

  #@unittest.skip("")
  def test_clrBit(self):
    self.assertEqual(pyauto_base.bin.clrBit(0, "0xFF"), "0xFE")
    self.assertEqual(pyauto_base.bin.clrBit(1, "0xFF"), "0xFD")
    self.assertEqual(pyauto_base.bin.clrBit(2, "0xFF"), "0xFB")
    self.assertEqual(pyauto_base.bin.clrBit(3, "0xFF"), "0xF7")
    self.assertEqual(pyauto_base.bin.clrBit(4, "0xFF"), "0xEF")
    self.assertEqual(pyauto_base.bin.clrBit(5, "0xFF"), "0xDF")
    self.assertEqual(pyauto_base.bin.clrBit(6, "0xFF"), "0xBF")
    self.assertEqual(pyauto_base.bin.clrBit(7, "0xFF"), "0x7F")

  #@unittest.skip("")
  def test_int2bytes(self):
    self.assertEqual(pyauto_base.bin.int2bytes(0), [0x00])
    self.assertEqual(pyauto_base.bin.int2bytes(0, 2), [0x00, 0x00])
    self.assertEqual(pyauto_base.bin.int2bytes(0, 4), [0x00, 0x00, 0x00, 0x00])
    self.assertEqual(pyauto_base.bin.int2bytes(127), [0x7f])
    self.assertEqual(pyauto_base.bin.int2bytes(127, 2), [0x7f, 0x00])
    self.assertEqual(pyauto_base.bin.int2bytes(127, 4), [0x7f, 0x00, 0x00, 0x00])
    self.assertEqual(pyauto_base.bin.int2bytes(43981), [0xcd])
    self.assertEqual(pyauto_base.bin.int2bytes(43981, 2), [0xcd, 0xab])
    self.assertEqual(pyauto_base.bin.int2bytes(43981, 4), [0xcd, 0xab, 0x00, 0x00])

  #@unittest.skip("")
  # def test_hexPad(self):
  #   self.assertEqual(pyauto_base.bin.hexPad("0x12", length=0), "0x12")
  #   self.assertEqual(pyauto_base.bin.hexPad("0x12", length=1), "0x12")
  #   self.assertEqual(pyauto_base.bin.hexPad("0x12", length=2), "0x12")
  #   self.assertEqual(pyauto_base.bin.hexPad("0x12", length=3), "0x012")
  #   self.assertEqual(pyauto_base.bin.hexPad("0x12", length=4), "0x0012")

  #@unittest.skip("")
  # def test_hexAdd(self):
  #   self.assertEqual(pyauto_base.bin.hexAdd("0x00", "0x01"), "0x1")
  #   self.assertEqual(pyauto_base.bin.hexAdd("0xFF", "0x01"), "0x100")
  #   self.assertEqual(pyauto_base.bin.hexAdd("0xFF", "0xFF"), "0x1FE")

  #@unittest.skip("")
  # def test_hexSub(self):
  #   self.assertEqual(pyauto_base.bin.hexSub("0x01", "0x01"), "0x0")
  #   self.assertEqual(pyauto_base.bin.hexSub("0xCD", "0x1D"), "0xB0")
  #   self.assertEqual(pyauto_base.bin.hexSub("0x01", "0x02"), "-0x1")

  #@unittest.skip("")
  # def test_hexNot(self):
  #   self.assertEqual(pyauto_base.bin.hexNot("0x00"), "0xFF")
  #   self.assertEqual(pyauto_base.bin.hexNot("0xFF"), "0x00")
  #   self.assertEqual(pyauto_base.bin.hexNot("0x15"), "0xEA")

  #@unittest.skip("")
  # def test_hexOr(self):
  #   self.assertEqual(pyauto_base.bin.hexOr("0x55", "0xAA"), "0xFF")

  #@unittest.skip("")
  # def test_hexAnd(self):
  #   self.assertEqual(pyauto_base.bin.hexAnd("0x55", "0xAA"), "0x0")

  #@unittest.skip("")
  # def test_hexXor(self):
  #   self.assertEqual(pyauto_base.bin.hexXor("0x55", "0xFF"), "0xAA")

  #@unittest.skip("")
  # def test_hexShl(self):
  #   self.assertEqual(pyauto_base.bin.hexShl("0x55", 1), "0xAA")
  #   self.assertEqual(pyauto_base.bin.hexShl("0x55", 3), "0x2A8")

  #@unittest.skip("")
  # def test_hexShr(self):
  #   self.assertEqual(pyauto_base.bin.hexShr("0x55", 1), "0x2A")
  #   self.assertEqual(pyauto_base.bin.hexShr("0x55", 4), "0x5")

  # #@unittest.skip("")
  # def test_hexGetByte(self):
  #   self.assertEqual(pyauto_base.bin.hexGetByte(0, "0xDEADBEEF"), "0xEF")
  #   self.assertEqual(pyauto_base.bin.hexGetByte(1, "0xDEADBEEF"), "0xBE")
  #   self.assertEqual(pyauto_base.bin.hexGetByte(2, "0xDEADBEEF"), "0xAD")
  #   self.assertEqual(pyauto_base.bin.hexGetByte(3, "0xDEADBEEF"), "0xDE")
  #   self.assertEqual(pyauto_base.bin.hexGetByte(4, "0xDEADBEEF"), "0x00")

  #@unittest.skip("")
  def test_hex2int_2c(self):
    self.assertEqual(pyauto_base.bin.hex2int_2c("0x04"), 4)
    self.assertEqual(pyauto_base.bin.hex2int_2c("0x1F"), 31)
    self.assertEqual(pyauto_base.bin.hex2int_2c("0x3A"), 58)
    self.assertEqual(pyauto_base.bin.hex2int_2c("0xFF"), -1)
    self.assertEqual(pyauto_base.bin.hex2int_2c("0xD2"), -46)
    self.assertEqual(pyauto_base.bin.hex2int_2c("0xA3"), -93)
    self.assertEqual(pyauto_base.bin.hex2int_2c("0x80"), -128)

  #@unittest.skip("")
  def test_int2hex_2c(self):
    self.assertEqual(pyauto_base.bin.int2hex_2c(4), "0x4")
    self.assertEqual(pyauto_base.bin.int2hex_2c(75), "0x4B")
    self.assertEqual(pyauto_base.bin.int2hex_2c(213), "0xD5")
    self.assertEqual(pyauto_base.bin.int2hex_2c(477), "0x1DD")
    self.assertEqual(pyauto_base.bin.int2hex_2c(-1), "0xFF")
    self.assertEqual(pyauto_base.bin.int2hex_2c(-68), "0xBC")
    self.assertEqual(pyauto_base.bin.int2hex_2c(-128), "0x80")
    self.assertEqual(pyauto_base.bin.int2hex_2c(-256), "0x100")
    self.assertEqual(pyauto_base.bin.int2hex_2c(-335), "0x1B1")
    self.assertEqual(pyauto_base.bin.int2hex_2c(-512), "0x200")

  #@unittest.skip("")
  # def test_hexJoin(self):
  #   self.assertEqual(pyauto_base.bin.hexJoin(["0x56", "0x4A"]), "0x4A56")
  #   self.assertEqual(pyauto_base.bin.hexJoin(["0x12", "0x34", "0xAB"], True), "0x1234AB")

  #@unittest.skip("")
  def test_hex2bits(self):
    self.assertCountEqual(pyauto_base.bin.hex2bits("0x0"), [0, 0, 0, 0, 0, 0, 0, 0])
    self.assertCountEqual(pyauto_base.bin.hex2bits("0x6"), [0, 1, 1, 0, 0, 0, 0, 0])
    self.assertCountEqual(pyauto_base.bin.hex2bits("0xE"), [0, 1, 1, 1, 0, 0, 0, 0])
    self.assertCountEqual(pyauto_base.bin.hex2bits("0xE", True), [0, 0, 0, 0, 1, 1, 1, 0])
    self.assertCountEqual(pyauto_base.bin.hex2bits("0xA5"), [1, 0, 1, 0, 0, 1, 0, 1])
    self.assertCountEqual(pyauto_base.bin.hex2bits("0x38D"), [1, 0, 1, 1, 0, 0, 0, 1, 1, 1])
    self.assertCountEqual(pyauto_base.bin.hex2bits("0xBEEF"),
                          [1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1])

  #@unittest.skip("")
  def test_bits2hex(self):
    self.assertCountEqual(pyauto_base.bin.bits2hex([0, 0]), "0x0")
    self.assertCountEqual(pyauto_base.bin.bits2hex([0, 1, 1, 0, 0, 0, 0, 0]), "0x6")
    self.assertCountEqual(pyauto_base.bin.bits2hex([0, 1, 1, 1, 0, 0, 0, 0]), "0xE")
    self.assertCountEqual(pyauto_base.bin.bits2hex([0, 0, 0, 0, 1, 1, 1, 0], True), "0xE")
    self.assertCountEqual(pyauto_base.bin.bits2hex([0, 1, 0, 1, 0, 1, 0, 0]), "0x2A")

  #@unittest.skip("")
  # def test_hexNBits(self):
  #   self.assertEqual(pyauto_base.bin.hexNBits("0x1"), 1)
  #   self.assertEqual(pyauto_base.bin.hexNBits("0x1A"), 5)
  #   self.assertEqual(pyauto_base.bin.hexNBits("0x35FB"), 14)
  #   self.assertEqual(pyauto_base.bin.hexNBits("0x16B78DE0C75"), 41)

  #@unittest.skip("")
  # def test_hexEqual(self):
  #   self.assertTrue(pyauto_base.bin.hexEqual("0x1", "0x0001"))
  #   self.assertTrue(pyauto_base.bin.hexEqual("0xab", "0x00AB"))
  #   self.assertTrue(pyauto_base.bin.hexEqual("0xAB", "0xab"))
  #
  #   self.assertFalse(pyauto_base.bin.hexEqual("0x1", "0x2"))
  #   self.assertFalse(pyauto_base.bin.hexEqual("0x123", "0x1a3"))
  #
  #   self.assertTrue(pyauto_base.bin.hexEqual("0xD717", "0xB7F7", hexMask="0x0F0F"))
  #   self.assertTrue(pyauto_base.bin.hexEqual("0x086B", "0x2286", hexMask="0x5500"))
  #   self.assertTrue(pyauto_base.bin.hexEqual("0x7D15", "0x163F", hexMask="0x0055"))
  #
  #   self.assertFalse(pyauto_base.bin.hexEqual("0xD247", "0x38F5", hexMask="0x7A3B"))
  #   self.assertFalse(pyauto_base.bin.hexEqual("0x0F00", "0xC6D4", hexMask="0x9C09"))

  #@unittest.skip("")
  def test_hexRdModWr(self):
    self.assertEqual(pyauto_base.bin.hexRdModWr("0x0000", "0xEA1E", "0xFF00"), "0xEA00")
    self.assertEqual(pyauto_base.bin.hexRdModWr("0x0000", "0xEA1E", "0x00FF"), "0x1E")
    self.assertEqual(pyauto_base.bin.hexRdModWr("0xE329", "0xBDD7", "0x3333"), "0xF11B")
    self.assertEqual(pyauto_base.bin.hexRdModWr("0xE329", "0xBDD7", "0xCCCC"), "0xAFE5")
