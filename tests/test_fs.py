#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_base.fs"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.tests.tools as tt

class Test_fs(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  def _createKnownFile(self):
    fOutPath = os.path.join(self.outFolder, "test_known.txt")

    with open(fOutPath, "w") as fOut:
      fOut.write("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")
    return fOutPath

  #@unittest.skip("")
  def test_chkPath(self):
    path_file = self._createKnownFile()
    path_dir = self.outFolder

    self.assertEqual(pyauto_base.fs.chkPath_File(path_file), True)
    self.assertEqual(pyauto_base.fs.chkPath_File(path_dir, bDie=False), False)
    self.assertEqual(pyauto_base.fs.chkPath_File(path_dir, msg=None, bDie=False), False)
    with self.assertRaises(IOError):
      pyauto_base.fs.chkPath_File(path_dir)

    self.assertEqual(pyauto_base.fs.chkPath_Dir(path_dir), True)
    self.assertEqual(pyauto_base.fs.chkPath_Dir(path_file, bDie=False), False)
    self.assertEqual(pyauto_base.fs.chkPath_Dir(path_file, msg=None, bDie=False), False)
    with self.assertRaises(IOError):
      pyauto_base.fs.chkPath_Dir(path_file)

  #@unittest.skip("")
  def test_mkAbsolutePath(self):
    path = "WORK/file"
    self.assertEqual(os.path.isabs(path), False)
    absPath = pyauto_base.fs.mkAbsolutePath(path, bExists=False)
    if (sys.platform.startswith("win")):
      self.assertEqual(absPath, os.path.join("C:\\", path))
    elif (sys.platform.startswith("linux")):
      self.assertEqual(absPath, os.path.join(os.environ["HOME"], path))

    path = "."
    self.assertEqual(os.path.isabs(path), False)
    absPath = pyauto_base.fs.mkAbsolutePath(path)
    self.assertEqual(absPath, os.path.abspath(os.getcwd()))
    self.assertEqual(os.path.isabs(absPath), True)

    path = ".."
    self.assertEqual(os.path.isabs(path), False)
    absPath = pyauto_base.fs.mkAbsolutePath(path)
    self.assertEqual(absPath, os.path.dirname(os.path.abspath(os.getcwd())))
    self.assertEqual(os.path.isabs(absPath), True)

    path = "a"
    self.assertEqual(os.path.isabs(path), False)

    rootDir = os.getcwd()
    self.assertEqual(os.path.isabs(rootDir), True)
    self.assertEqual(os.path.isdir(rootDir), True)
    absPath = pyauto_base.fs.mkAbsolutePath(path, rootPath=rootDir, bExists=False)
    self.assertEqual(absPath, os.path.join(rootDir, path))
    self.assertEqual(os.path.isabs(absPath), True)

    rootDir = __file__
    self.assertEqual(os.path.isabs(rootDir), True)
    self.assertEqual(os.path.isdir(rootDir), False)
    absPath = pyauto_base.fs.mkAbsolutePath(path, rootPath=rootDir, bExists=False)
    self.assertEqual(absPath, os.path.join(os.path.dirname(rootDir), path))
    self.assertEqual(os.path.isabs(absPath), True)

    rootDir = "files"
    self.assertEqual(os.path.isabs(rootDir), False)
    self.assertEqual(os.path.isdir(rootDir), True)
    absPath = pyauto_base.fs.mkAbsolutePath(path, rootPath=rootDir, bExists=False)
    self.assertEqual(absPath, os.path.join(os.path.abspath(rootDir), path))
    self.assertEqual(os.path.isabs(absPath), True)

    rootDir = os.path.basename(__file__)
    self.assertEqual(os.path.isabs(rootDir), False)
    self.assertEqual(os.path.isdir(rootDir), False)
    absPath = pyauto_base.fs.mkAbsolutePath(path, rootPath=rootDir, bExists=False)
    self.assertEqual(absPath, os.path.join(os.path.abspath(os.path.dirname(rootDir)), path))
    self.assertEqual(os.path.isabs(absPath), True)

  #@unittest.skip("")
  def test_addTS(self):
    path_file = self._createKnownFile()
    path_dir = self.outFolder

    tmp = pyauto_base.fs.addTS(path_file)
    logger.info(tmp)
    self.assertTrue(tmp.startswith(path_file[:-4] + "_"))
    regex = "^.*_[0-9]{4}-[0-1][0-9]-[0-3][0-9]T[0-9]{4}" + path_file[-4:] + "$"
    self.assertIsNotNone(re.match(regex, tmp), "{} DOES NOT match {}".format(tmp, regex))

    tmp = pyauto_base.fs.addTS(path_dir)
    logger.info(tmp)
    self.assertTrue(tmp.startswith(path_dir + "_"))
    regex = "^.*_[0-9]{4}-[0-1][0-9]-[0-3][0-9]T[0-9]{4}$"
    self.assertIsNotNone(re.match(regex, tmp), "{} DOES NOT match {}".format(tmp, regex))

  #@unittest.skip("")
  def test_rm(self):
    path_file = self._createKnownFile()
    lstHash = ["a58dd8680234c1f8cc2ef2b325a43733605a7f16f288e072de8eae81fd8d6433"]
    tt.chkHash_file(self, path_file, lstHash)

    pyauto_base.fs.rm(path_file)
    self.assertFalse(os.path.exists(path_file))

  #@unittest.skip("")
  def test_cp(self):
    path_file = self._createKnownFile()
    lstHash = ["a58dd8680234c1f8cc2ef2b325a43733605a7f16f288e072de8eae81fd8d6433"]
    tt.chkHash_file(self, path_file, lstHash)

    path_out = pyauto_base.fs.mkOutFolder(os.path.join(self.outFolder, "fs"))
    self.assertTrue(os.path.isdir(path_out))

    pyauto_base.fs.cp(path_file, path_out)
    tt.chkHash_file(self, os.path.join(path_out, os.path.basename(path_file)), lstHash)

    pyauto_base.fs.rm(path_out)
    self.assertFalse(os.path.exists(path_out))

    #path_out = pyauto_base.fs.mkOutFolder(os.path.join(self.outFolder, "fs"))
    #self.assertTrue(os.path.isdir(path_out))

    pyauto_base.fs.cp("files", path_out)
    for f in os.listdir("files"):
      if (os.path.isfile(os.path.join("files", f))):
        self.assertTrue(os.path.isfile(os.path.join(path_out, f)))

    pyauto_base.fs.rm(path_out)
    self.assertFalse(os.path.exists(path_out))

  def test_mv(self):
    path_file = self._createKnownFile()
    lstHash = ["a58dd8680234c1f8cc2ef2b325a43733605a7f16f288e072de8eae81fd8d6433"]
    tt.chkHash_file(self, path_file, lstHash)

    path_out = pyauto_base.fs.mkOutFolder(os.path.join(self.outFolder, "fs"))
    self.assertTrue(os.path.isdir(path_out))

    pyauto_base.fs.mv(path_file, path_out)
    self.assertFalse(os.path.exists(path_file))
    tt.chkHash_file(self, os.path.join(path_out, os.path.basename(path_file)), lstHash)

    pyauto_base.fs.rm(path_out)
    self.assertFalse(os.path.exists(path_out))

  def test_zip(self):
    path_file = self._createKnownFile()
    lstHash = ["a58dd8680234c1f8cc2ef2b325a43733605a7f16f288e072de8eae81fd8d6433"]
    tt.chkHash_file(self, path_file, lstHash)

    path_out = pyauto_base.fs.mkOutFolder(os.path.join(self.outFolder, "fs"))
    self.assertTrue(os.path.isdir(path_out))

    pyauto_base.fs.mkZip(path_file, path_out)
    self.assertTrue(
        os.path.exists(
            os.path.join(path_out,
                         os.path.basename(path_file).replace("txt", "zip"))))

    pyauto_base.fs.rm(path_out)
    self.assertFalse(os.path.exists(path_out))

    path_out = pyauto_base.fs.mkOutFolder(os.path.join(self.outFolder, "fs"))
    self.assertTrue(os.path.isdir(path_out))

    pyauto_base.fs.mkZip("files", path_out)
    self.assertTrue(os.path.exists(os.path.join(path_out, "files.zip")))

    pyauto_base.fs.rm(path_out)
    self.assertFalse(os.path.exists(path_out))

  def test_tar(self):
    path_file = self._createKnownFile()
    lstHash = ["a58dd8680234c1f8cc2ef2b325a43733605a7f16f288e072de8eae81fd8d6433"]
    tt.chkHash_file(self, path_file, lstHash)

    path_out = pyauto_base.fs.mkOutFolder(os.path.join(self.outFolder, "fs"))
    self.assertTrue(os.path.isdir(path_out))

    pyauto_base.fs.mkTar(path_file, path_out)
    self.assertTrue(
        os.path.exists(
            os.path.join(path_out,
                         os.path.basename(path_file).replace("txt", "tar"))))

    pyauto_base.fs.rm(path_out)
    self.assertFalse(os.path.exists(path_out))

    path_out = pyauto_base.fs.mkOutFolder(os.path.join(self.outFolder, "fs"))
    self.assertTrue(os.path.isdir(path_out))

    pyauto_base.fs.mkTar("files", path_out)
    self.assertTrue(os.path.exists(os.path.join(path_out, "files.tar")))

    pyauto_base.fs.rm(path_out)
    self.assertFalse(os.path.exists(path_out))

  def test_gztar(self):
    path_file = self._createKnownFile()
    lstHash = ["a58dd8680234c1f8cc2ef2b325a43733605a7f16f288e072de8eae81fd8d6433"]
    tt.chkHash_file(self, path_file, lstHash)

    path_out = pyauto_base.fs.mkOutFolder(os.path.join(self.outFolder, "fs"))
    self.assertTrue(os.path.isdir(path_out))

    pyauto_base.fs.mkGztar(path_file, path_out)
    self.assertTrue(
        os.path.exists(
            os.path.join(path_out,
                         os.path.basename(path_file).replace("txt", "tar.gz"))))

    pyauto_base.fs.rm(path_out)
    self.assertFalse(os.path.exists(path_out))

    path_out = pyauto_base.fs.mkOutFolder(os.path.join(self.outFolder, "fs"))
    self.assertTrue(os.path.isdir(path_out))

    pyauto_base.fs.mkGztar("files", path_out)
    self.assertTrue(os.path.exists(os.path.join(path_out, "files.tar.gz")))

    pyauto_base.fs.rm(path_out)
    self.assertFalse(os.path.exists(path_out))

  #@unittest.skip("")
  def test_getModulePath(self):
    self.assertTrue(pyauto_base.fs.getModulePath("mock").endswith("mock"))
    self.assertTrue(pyauto_base.fs.getModulePath("numpy").endswith("numpy"))
