#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import hashlib

logger = logging.getLogger("lib")
import pyauto_base.misc

def chkHash_str(tc, strVal, lstHashVal):
  hs = hashlib.sha256()
  hs.update(strVal.encode("utf-8"))
  tmp = hs.hexdigest()
  tc.assertIn(tmp, lstHashVal)

def chkHash_file(tc, fPath, lstHashVal):
  tc.assertTrue(os.path.isfile(fPath), "file DOES NOT exist: {}".format(fPath))
  tmp = pyauto_base.misc.getFileHash(fPath)
  tc.assertIn(tmp, lstHashVal, "INCORRECT hash for {} ({})".format(fPath, tmp))
