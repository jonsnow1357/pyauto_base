#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_base.doc"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.doc
import pyauto_base.tests.tools as tt

class Test_doc(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_formatTable(self):
    lstData = [["asd", "sdfdsfds", "lksd sdf"], ["sd", "jjsd sdf", "ksdjkdsf sdfksd"]]
    colSize = pyauto_base.doc._getTableColSize(lstData)
    self.assertEqual(colSize, [5, 10, 17])

    strVal = pyauto_base.doc._formatTableRow(None, colSize, chFill="-")
    self.assertEqual(strVal, "|-----|----------|-----------------|")
    strVal = pyauto_base.doc._formatTableRow(None,
                                             colSize,
                                             colSep="x",
                                             chFill="=",
                                             bSepOutside=False)
    self.assertEqual(strVal, "=====x==========x=================")

    lstData = ["d1", "val2", "33"]
    strVal = pyauto_base.doc._formatTableRow(lstData, colSize)
    self.assertEqual(strVal, "| d1  |   val2   |       33        |")

  #@unittest.skip("")
  def test_DocInfo(self):
    docPath = os.path.join(self.inFolder, "test_read_template1.txt")
    docInfo = pyauto_base.doc.DocInfo("id1", docPath)
    self.assertEqual(docInfo.id, "id1")
    self.assertEqual(docInfo.docNo, "xxxxxxxx")
    self.assertEqual(docInfo.docVer, "01")

  #@unittest.skip("")
  def test_DocConversion(self):
    docPath = os.path.join(self.inFolder, "test_read_template1.txt")
    docConv = pyauto_base.doc.DocConversion()
    docInfo = pyauto_base.doc.DocInfo("id1", docPath)
    docInfo.convHTML2PDF = True
    docInfo.convOrientation = "landscape"
    docConv.addDocInfo(docInfo)

    if (sys.platform.startswith("win")):
      scriptPath = os.path.join(self.outFolder, "doc.bat")
    elif (sys.platform.startswith("linux")):
      scriptPath = os.path.join(self.outFolder, "doc.sh")
    else:
      msg = "UNSUPPORTED platform: {}".format(sys.platform)
      logger.error(msg)
      raise RuntimeError(msg)
    docConv.writeScript(fPath=self.outFolder)

    lstHash = [
        "59f3ec04eb78af4dd48338813493a3c12ca95b22a7770d711617a71f77dbd173",  # linux
        "4642f3ef795a121a641f1c38d18f29c656f52ca9f64b588930224975e713efc2",  # linux jenkins
        "51ac590e113304a05badbc380e6a393bc8cbdcde34e3c679041df6981cf64b37",  # win
    ]
    tt.chkHash_file(self, scriptPath, lstHash)

    docPath = os.path.join(self.inFolder, "test_read_template2.txt")
    docInfo = pyauto_base.doc.DocInfo("id2", docPath)
    docInfo.convMD2HTML = True
    docConv.addDocInfo(docInfo)

    if (sys.platform.startswith("win")):
      scriptPath = os.path.join(self.outFolder, "doc.bat")
    elif (sys.platform.startswith("linux")):
      scriptPath = os.path.join(self.outFolder, "doc.sh")
    else:
      msg = "UNSUPPORTED platform: {}".format(sys.platform)
      logger.error(msg)
      raise RuntimeError(msg)
    docConv.writeScript(fPath=self.outFolder)

    lstHash = [
        "b232a8fffa9d36ad85105d440549fa0b4513920d19a438ba0b08af268352cadf",  # linux
        "05b4966ca9b64d2b450be5f51a0e4cae11471dbd0fc00188f91f02bbd2c1f1a8",  # linux jenkins
        "f722c12b175af68a55b6c73dc35043dd47a6121840a00998aec36bca0aade29c",  # win
    ]
    tt.chkHash_file(self, scriptPath, lstHash)

class Test_ReSTDocument(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_ReST01(self):
    doc = pyauto_base.doc.ReSTDocument()
    #doc.clear()
    doc.addHeader("Header 1.1", lvl=3)
    doc.addParagraph([
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit, "
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    ])
    doc.addHeader("Header 1.2", lvl=3)
    doc.addParagraph([
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit, "
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    ])
    doc.addHeader("Header 1.2.1", lvl=4)
    doc.addParagraph([
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit, "
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    ])
    doc.addHeader("Header 1.2.1.1", lvl=5)
    doc.addParagraph([
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit, "
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    ])
    doc.addHeader("Header 1.2.1.1.1", lvl=6)
    doc.addParagraph([
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit, "
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    ])

    fPath = os.path.join(self.outFolder, "test_ReST1.txt")
    doc.write(fPath)
    lstHash = [
        "ff6f588aa7a10ccb71fe082de4543c8e3f8b8b2e824ea0e9a9e383ceda3e75f8",  # linux
        "a17104490d3fddecda55107d054200a19dcefbe88c5a1ac77ab1ad2549bfd844",  # win
    ]
    tt.chkHash_file(self, fPath, lstHash)

  #@unittest.skip("")
  def test_ReST02(self):
    doc = pyauto_base.doc.ReSTDocument()
    #doc.clear()
    doc.addHeader("Header 2.1", lvl=3)
    doc.addParagraph("Bullet list:")
    doc.addBulletList(3 * ["item"])
    doc.addParagraph("Numbered list:")
    doc.addNumberedList(5 * ["item"])
    doc.addParagraph("Definition list:")
    doc.addDefinitionList({"term1": "definition of term1", "term2": "definition of term2"})

    doc.addHeader("Header 2.2", lvl=3)
    ln = pyauto_base.doc.ReSTHyperlink("http://docutils.sourceforge.net", "docutils")
    doc.addParagraph("This is an anonymous link to {}.".format(ln.getAnonTitle()))
    doc.addParagraph(ln.getAnonHref())
    ln1 = pyauto_base.doc.ReSTHyperlink(
        "http://docutils.sourceforge.net/docs/user/rst/quickstart.html", "ReST quick-start")
    ln2 = pyauto_base.doc.ReSTHyperlink(
        "http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html",
        "ReST Markup Spec")
    ln3 = pyauto_base.doc.ReSTHyperlink(
        "http://docutils.sourceforge.net/docs/user/rst/quickref.html#hyperlink-targets",
        "Quick ReST")
    doc.addParagraph([
        "And extra links to {}, {} and {}.".format(ln1.getTitle(), ln2.getTitle(),
                                                   ln3.getEmbedded()),
        "Lorem ipsum dolor sit amet ..."
    ])
    doc.addParagraph([ln1.getHref(), ln2.getHref()])

    fPath = os.path.join(self.outFolder, "test_ReST2.txt")
    doc.write(fPath)
    lstHash = [
        "7d596eda08202b2a8b346d457feda39a43ae82b7c47dcdb31959a31757f3a3eb",  # linux
        "35bab8e13c1117ae68999f677a735098e04b344717883baa6f31f5576a40c630",  # win
    ]
    tt.chkHash_file(self, fPath, lstHash)

  #@unittest.skip("")
  def test_ReST03(self):
    doc = pyauto_base.doc.ReSTDocument()
    #doc.clear()
    doc.addHeader("Document Title")

    doc.addHeader("Paragraphs", lvl=2)
    doc.addInclude("test_ReST1.txt")

    doc.addHeader("Links and Lists", lvl=2)
    doc.addInclude("test_ReST2.txt")

    doc.addHeader("Images", lvl=2)
    img1 = pyauto_base.doc.ReSTImage("test_arrow_cross.png.png", width=200, height=200)
    img2 = pyauto_base.doc.ReSTImage("test_lines_mirror.png.png",
                                     strAlt="alt text",
                                     width=200,
                                     height=200)
    doc.addImage(img1)
    doc.addImage(img2)

    doc.addHeader("Tables", lvl=2)
    doc.addHeader("Table 1", lvl=3)
    doc.addTable(
        [["h1", "h2", "h3"], ["v1", "v2", "v3"], ["v4", "v5", "v6"], ["v7", "v8", "v9"]],
        bHdr=True)
    doc.addHeader("Table 2", lvl=3)
    doc.addTable([["v11", "v12", "v13", "v14"], ["v15", "v16", "v17", "v18"]])
    doc.addHeader("Table 3", lvl=3)
    doc.addTable(["v11", "v12", "v13", "v14"], nCols=3)

    fPath = os.path.join(self.outFolder, "test_ReST.txt")
    doc.write(fPath)
    lstHash = [
        "38770a4deb18b941b96b46b7dda02fc44dd4201ffcc25c35900af1332dc60329",  # linux
        "8aa6c07eb0c587f9acaadb204249f174eda95746ec94a2c02d0edbbd84d01b1e",  # win
    ]
    tt.chkHash_file(self, fPath, lstHash)

    os.chdir(self.outFolder)  # TODO: workaround because we include previous ReST files

    fPath = os.path.join(self.outFolder, "test_ReST.html")
    doc.writeAsHTML(fPath)
    self.assertGreater(os.path.getsize(fPath), 1200)
    #lstHash = ["???",  # linux
    #           "???",  # win
    #           ]
    #tt.chkHash_file(self, fPath, lstHash)

    #fPath = os.path.join(self.outFolder, "test_ReST.pdf")
    #doc.writeAsPDF(fPath)
    #lstHash = ["???",  # linux
    #           "???",  # win
    #           ]
    #tt.chkHash_file(self, fPath, lstHash)

    os.chdir("..")

class Test_MarkdownDocument(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    doc = pyauto_base.doc.MarkdownDocument()
    #doc.clear()
    doc.addHeader("Document Title")

    doc.addHeader("Paragraphs", lvl=2)
    doc.addHeader("Header 1.1", lvl=3)
    doc.addParagraph([
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit, "
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    ])
    doc.addHeader("Header 1.2", lvl=3)
    doc.addParagraph([
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit, "
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    ])
    doc.addHeader("Header 1.2.1", lvl=4)
    doc.addParagraph([
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit, "
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    ])
    doc.addHeader("Header 1.2.1.1", lvl=5)
    doc.addParagraph([
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit, "
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    ])
    doc.addHeader("Header 1.2.1.1.1", lvl=6)
    doc.addParagraph([
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit, "
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    ])

    doc.addHeader("Links and Lists", lvl=2)
    doc.addHeader("Header 2.1", lvl=3)
    doc.addParagraph("Bullet list:")
    doc.addBulletList(3 * ["item"])
    doc.addParagraph("Numbered list:")
    doc.addNumberedList(5 * ["item"])
    doc.addParagraph("Definition list:")
    doc.addDefinitionList({"term1": "definition of term1", "term2": "definition of term2"})

    doc.addHeader("Header 2.2", lvl=3)
    ln1 = pyauto_base.doc.MarkdownHyperlink(
        "https://daringfireball.net/projects/markdown/syntax", "markdown")
    ln2 = pyauto_base.doc.MarkdownHyperlink(
        "http://pandoc.org/MANUAL.html#pandocs-markdown", "pandoc's markdown")
    doc.addParagraph([
        "These are inline links to {} and {}.".format(ln1.getInline(), ln2.getInline()),
        "Lorem ipsum dolor sit amet ..."
    ])

    doc.addHeader("Images", lvl=2)
    img1 = pyauto_base.doc.MarkdownImage("test_arrow_cross.png", width=200, height=200)
    img2 = pyauto_base.doc.MarkdownImage("test_lines_mirror.png",
                                         strAlt="alt text",
                                         width=200,
                                         height=200)
    doc.addImage(img1)
    doc.addImage(img2)

    doc.addHeader("Tables", lvl=2)
    doc.addTable(
        [["h1", "h2", "h3"], ["v1", "v2", "v3"], ["v4", "v5", "v6"], ["v7", "v8", "v9"]],
        bHdr=True)
    doc.addTable([["v11", "v12", "v13", "v14"], ["v15", "v16", "v17", "v18"]])
    doc.addTable(["v11", "v12", "v13", "v14"], nCols=3)

    fPath = os.path.join(self.outFolder, "test_Markdown.md")
    doc.write(fPath)
    lstHash = [
        "3d2a058f03cd6e78c00a8bdc2c057c39bff8bca2762d06d161295c6f035ba663",  # linux
        "33390081e4048bf5d67b72c069d8c958ce814f8fee0b80b8ef24ad920961fd30",  # win
    ]
    tt.chkHash_file(self, fPath, lstHash)
