#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

from __future__ import annotations  # for python 3.8
#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import typing
import subprocess

logger = logging.getLogger("lib")

def popenGenericCmd(cmdName: str, cmdArgs: typing.Optional[list[str]] = None) -> list[str]:
  """
  runs a shell command and returns the output.
  :param cmdName:
  :param cmdArgs:
  :return:
  """
  if (not isinstance(cmdName, str)):
    msg = "NOT a string: {}".format(cmdName)
    logger.error(msg)
    raise RuntimeError(msg)
  if (cmdArgs is None):
    cmdArgs = []
  if (not isinstance(cmdArgs, list)):
    msg = "NOT a list: {}".format(cmdArgs)
    logger.error(msg)
    raise RuntimeError(msg)

  cmd = [cmdName] + cmdArgs
  #logger.info(cmd)
  if (sys.platform.startswith("win")):
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
  elif (sys.platform.startswith("linux")):
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
  else:
    msg = "UNSUPPORTED platform: {}".format(sys.platform)
    logger.error(msg)
    raise RuntimeError(msg)
  #proc.wait()
  #out = proc.communicate()[0].decode("utf-8").split("\n")
  stdoutdata, stderrdata = proc.communicate()

  res = []
  for ln in stdoutdata.decode(encoding="utf-8", errors="ignore").split("\n"):
    ln = ln.strip("\r")
    if (len(ln) > 0):
      res.append(ln)
  #logger.info(res)
  return res

def callGenericCmd(cmdName: str, cmdArgs: typing.Optional[list[str]] = None) -> int:
  """
  runs a shell command and returns the status.
  :param cmdName:
  :param cmdArgs:
  :return:
  """
  if (not isinstance(cmdName, str)):
    msg = "NOT a string: {}".format(cmdName)
    logger.error(msg)
    raise RuntimeError(msg)
  if (cmdArgs is None):
    cmdArgs = []
  if (not isinstance(cmdArgs, list)):
    msg = "NOT a list: {}".format(cmdArgs)
    logger.error(msg)
    raise RuntimeError(msg)

  cmd = [cmdName] + cmdArgs
  #logger.info(cmd)
  if (sys.platform.startswith("win")):
    ret = subprocess.call(cmd, shell=True)
  elif (sys.platform.startswith("linux")):
    ret = subprocess.call(cmd)
  else:
    msg = "UNSUPPORTED platform: {}".format(sys.platform)
    logger.error(msg)
    raise RuntimeError(msg)
  return ret
