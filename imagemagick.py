#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv
import shutil

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.cli
import pyauto_base.svg

tempPng = "temp.png"
#logger.info(sys.platform)
if (sys.platform.startswith("win")):
  #progDir = "C:\\Program Files\\ImageMagick-6.7.9-Q16"
  progDir = "C:\\utils\\ImageMagick-6.9.3-8-portable-Q16-x64"
  _cmdConvert = os.path.join(progDir, "convert.exe")
  _cmdMontage = os.path.join(progDir, "montage.exe")
  _cmdIdentify = os.path.join(progDir, "identify.exe")
  cmds = (_cmdConvert, _cmdMontage, _cmdIdentify)
  for c in cmds:
    #logger.info(c)
    pyauto_base.fs.chkPath_File(c)
elif (sys.platform.startswith("linux")):
  _cmdConvert = "convert"
  _cmdMontage = "montage"
  _cmdIdentify = "identify"
  cmds = (_cmdConvert, _cmdMontage, _cmdIdentify)
  for c in cmds:
    #logger.info(c)
    if (pyauto_base.cli.callGenericCmd("which", [c]) != 0):
      _msg = "command {} DOES NOT exist".format(c)
      logger.error(_msg)
      raise RuntimeError(_msg)
else:
  _msg = "unsupported platform: {}".format(sys.platform)
  logger.error(_msg)
  raise RuntimeError(_msg)

def saveTempPic(path: str) -> None:
  shutil.move(tempPng, path)

def showVersion() -> None:
  res = pyauto_base.cli.popenGenericCmd(_cmdConvert, ["-version"])
  for ln in res:
    logger.info(ln.encode("utf-8"))

def identify(imgPath: str) -> dict[str, str]:
  dictRes: dict[str, str] = {}
  res = pyauto_base.cli.popenGenericCmd(_cmdIdentify, [imgPath])
  #print("DBG", imgPath, res)
  if (len(res) == 0):
    logger.warning("CANNOT identify '{}'".format(imgPath))
    return dictRes

  res = res[0].split()
  if (len(res) < 9):
    raise RuntimeError
  dictRes["type"] = res[-8]
  dictRes["size"] = res[-7]
  dictRes["geometry"] = res[-6]
  dictRes["depth"] = res[-5]
  #logger.info(dictRes)
  return dictRes

def makeImg(imgPath: str, p1: complex, color: str) -> None:
  if (not isinstance(p1, complex)):
    msg = "incorrect argument: p1"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("make simple image ...")
  pyauto_base.cli.callGenericCmd(
      _cmdConvert,
      ["-size", "{}x{}".format(p1.real, p1.imag), "xc:{}".format(color), imgPath])

def cropImg(imgPath: str, p1: complex, p2: complex) -> None:
  if (not isinstance(p1, complex)):
    msg = "incorrect argument: p1"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isinstance(p2, complex)):
    msg = "incorrect argument: p2"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("cropping ...")
  size = "{}x{}".format((p2.real - p1.real), (p2.imag - p1.imag))
  offset = "+{}+{}".format(p1.real, p1.imag)
  geom = (size + offset)
  pyauto_base.cli.callGenericCmd(_cmdConvert, [imgPath, "-crop", geom, tempPng])

def mirrorImg(imgPath: str) -> None:
  logger.info("mirroring ...")
  pyauto_base.cli.callGenericCmd(_cmdConvert, [imgPath, "-flop", tempPng])

def getRotationFactor(deg: int) -> complex:
  rad = (deg / 180.0) * math.pi
  return complex(math.cos(rad), (-1.0 * math.sin(rad)))

def drawLine(imgPath: str, p1: complex, p2: complex, color: str, strokeWidth: int) -> None:
  if (not isinstance(p1, complex)):
    msg = "incorrect argument: p1"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isinstance(p2, complex)):
    msg = "incorrect argument: p2"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("draw line ...")
  drawArg = "stroke-linecap round line {},{} {},{}".format(p1.real, p1.imag, p2.real,
                                                           p2.imag)
  pyauto_base.cli.callGenericCmd(_cmdConvert, [
      imgPath, "-stroke", color, "-strokewidth",
      str(strokeWidth), "-draw", drawArg, tempPng
  ])

def drawCircle(imgPath: str,
               pt: complex,
               radius: int,
               color: str,
               strokeWidth: int,
               fillColor: str = "none") -> None:
  if (not isinstance(pt, complex)):
    msg = "incorrect argument: pt"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isinstance(radius, int)):
    msg = "incorrect argument: r"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("draw circle ...")
  pc = pt + complex(0, radius)
  drawArg = "circle {},{} {},{}".format(pt.real, pt.imag, pc.real, pc.imag)
  pyauto_base.cli.callGenericCmd(_cmdConvert, [
      imgPath, "-stroke", color, "-strokewidth",
      str(strokeWidth), "-fill", fillColor, "-draw", drawArg, tempPng
  ])

def drawRect(imgPath: str,
             p1: complex,
             p2: complex,
             color: str,
             strokeWidth: int,
             fillColor: str = "none") -> None:
  if (not isinstance(p1, complex)):
    msg = "incorrect argument: p1"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isinstance(p2, complex)):
    msg = "incorrect argument: p2"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("draw rectangle ...")
  drawArg = "roundrectangle {},{} {},{} 0,0".format(p1.real, p1.imag, p2.real, p2.imag)
  pyauto_base.cli.callGenericCmd(_cmdConvert, [
      imgPath, "-stroke", color, "-strokewidth",
      str(strokeWidth), "-fill", fillColor, "-draw", drawArg, tempPng
  ])

def drawRRect(imgPath: str,
              p1: complex,
              p2: complex,
              color: str,
              strokeWidth: int,
              fillColor: str = "none") -> None:
  if (not isinstance(p1, complex)):
    msg = "incorrect argument: p1"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isinstance(p2, complex)):
    msg = "incorrect argument: p2"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("draw rounded rectangle ...")
  drawArg = "roundrectangle {},{} {},{} 10,10".format(p1.real, p1.imag, p2.real, p2.imag)
  pyauto_base.cli.callGenericCmd(_cmdConvert, [
      imgPath, "-stroke", color, "-strokewidth",
      str(strokeWidth), "-fill", fillColor, "-draw", drawArg, tempPng
  ])

def drawPath(imgPath: str, path: str, color: str, strokeWidth: int) -> None:
  logger.info("draw path ...")
  drawArg = "stroke-linecap round path '{}'".format(path)
  pyauto_base.cli.callGenericCmd(_cmdConvert, [
      imgPath, "-stroke", color, "-strokewidth",
      str(strokeWidth), "-fill", "none", "-draw", drawArg, tempPng
  ])

def drawText(imgPath: str, pt: complex, strText: str, color: str, size: int) -> None:
  if (not isinstance(pt, complex)):
    msg = "incorrect argument: pt"
    logger.error(msg)
    raise RuntimeError(msg)

  if (sys.platform.startswith("win")):
    #fontName = "Verdana"
    fontName = "Verdana-Bold"
    #fontName = "Times-New-Roman"
    #fontName = "Times-New-Roman-Bold"
  elif (sys.platform.startswith("linux")):
    #fontName = "Courier"
    #fontName = "Courier-Bold"
    #fontName = "Helvetica"
    fontName = "Helvetica-Bold"
  else:
    fontName = "Helvetica"

  logger.info("draw text ...")
  drawArg = "font {} font-size {:d} fill {} text {:d},{:d} '{}'".format(
      fontName, size, color, int(pt.real), int(pt.imag), strText)
  logger.info(drawArg)
  pyauto_base.cli.callGenericCmd(_cmdConvert,
                                 [imgPath, "-gravity", "center", "-draw", drawArg, tempPng])
  #pyauto_base.libCLI.callGenericCmd(_cmdConvert, [imgPath, "-draw", drawArg, tempPng])

def drawArrow(imgPath: str, p1: complex, p2: complex, arrowLen: int, color: str,
              strokeWidth: int) -> None:
  if (not isinstance(p1, complex)):
    msg = "incorrect argument: p1"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isinstance(p2, complex)):
    msg = "incorrect argument: p2"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("make arrow ...")
  tip = complex(0.0, 0.0)
  end1 = arrowLen * complex(-0.94, 0.342)
  end2 = arrowLen * complex(-0.94, -0.342)

  if (p1.real == p2.real):
    if (p1.imag > p2.imag):
      rotCoef = complex(0.0, -1.0)  # 180 deg
    else:
      rotCoef = complex(0.0, 1.0)  # 90 deg
  elif (p1.real > p2.real):  # quadrants 2,3
    tmp = math.atan((p2.imag - p1.imag) / (p2.real - p1.real))
    rotCoef = complex((-1.0 * math.cos(tmp)), (-1.0 * math.sin(tmp)))
  else:  # quadrants 1,4
    tmp = math.atan((p2.imag - p1.imag) / (p2.real - p1.real))
    rotCoef = complex(math.cos(tmp), math.sin(tmp))

  end1 = (rotCoef * end1)
  end2 = (rotCoef * end2)
  pe1 = p2 + end1
  pt = p2 + tip
  pe2 = p2 + end2

  svgp = pyauto_base.svg.svgPath(pe1.real, pe1.imag)
  svgp.lineto(pt.real, pt.imag)
  svgp.lineto(pe2.real, pe2.imag)
  #logger.info(svgp.path)
  drawPath(imgPath, svgp.path, color, strokeWidth)

def drawCross(imgPath: str, p1: complex, p2: complex, color: str, strokeWidth: int) -> None:
  if (not isinstance(p1, complex)):
    msg = "incorrect argument: p1"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isinstance(p2, complex)):
    msg = "incorrect argument: p2"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("make cross ...")
  p1p = complex(p2.real, p1.imag)
  p2p = complex(p1.real, p2.imag)

  svgp = pyauto_base.svg.svgPath(p1.real, p1.imag)
  svgp.lineto(p2.real, p2.imag)
  svgp.moveto(p1p.real, p1p.imag)
  svgp.lineto(p2p.real, p2p.imag)
  #logger.info(svgp.path)
  drawPath(imgPath, svgp.path, color, strokeWidth)

def drawRectGeneric(imgPath: str, cnt: complex, length: int, width: int, ang: int,
                    color: str, strokeWidth: int) -> None:
  if (not isinstance(cnt, complex)):
    msg = "incorrect argument: cnt"
    logger.error(msg)
    raise RuntimeError(msg)
  if ((length < 0) or (width < 0)):
    msg = "incorrect argument: [l | w]"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("draw random rectangle ... (known issues with path directive)")
  rotCoef = getRotationFactor(ang)
  p1 = (complex((length / 2.0), (width / 2.0)) * rotCoef) + cnt
  p2 = (complex((-1.0 * (length / 2.0)), (width / 2.0)) * rotCoef) + cnt
  p3 = (complex((-1.0 * (length / 2.0)), (-1.0 * (width / 2.0))) * rotCoef) + cnt
  p4 = (complex((length / 2.0), (-1.0 * (width / 2.0))) * rotCoef) + cnt

  svgp = pyauto_base.svg.svgPath(p1.real, p1.imag)
  svgp.lineto(p2.real, p2.imag)
  svgp.lineto(p3.real, p3.imag)
  svgp.lineto(p4.real, p4.imag)
  svgp.closepath()
  #logger.info(svgp.path)
  drawPath(imgPath, svgp.path, color, strokeWidth)

def colorNegate(imgPath: str) -> None:
  logger.info("negate colors ...")
  pyauto_base.cli.callGenericCmd(_cmdConvert, [imgPath, "-negate", tempPng])

def colorReplace(imgPath: str, color1: str, color2: str) -> None:
  logger.info("color {} -> {} ...".format(color1, color2))
  pyauto_base.cli.callGenericCmd(_cmdConvert,
                                 [imgPath, "-fill", color2, "-opaque", color1, tempPng])

def join2Img_1r2c(lstImgPath: list[str], border: int) -> None:
  row = 1
  col = 2
  if (len(lstImgPath) != (row * col)):
    msg = "incorrect argument: lstImgPath"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("joining images ({:d}x{:d}) ...".format(row, col))
  res = pyauto_base.cli.callGenericCmd(_cmdMontage, [
      lstImgPath[0], lstImgPath[1], "-tile", "x1", "-geometry", "+0+0", "-border",
      str(border), tempPng
  ])
  if (res):
    raise RuntimeError

def join2Img_2r1c(lstImgPath: list[str], border: int) -> None:
  row = 2
  col = 1
  if (len(lstImgPath) != (row * col)):
    msg = "incorrect argument: lstImgPath"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("joining images ({:d}x{:d}) ...".format(row, col))
  res = pyauto_base.cli.callGenericCmd(_cmdMontage, [
      lstImgPath[0], lstImgPath[1], "-tile", "1x", "-geometry", "+0+0", "-border",
      str(border), tempPng
  ])
  if (res):
    raise RuntimeError

def join3Img_3r1c(lstImgPath: list[str], border: int) -> None:
  row = 3
  col = 1
  if (len(lstImgPath) != (row * col)):
    msg = "incorrect argument: lstImgPath"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("joining images ({:d}x{:d}) ...".format(row, col))
  res = pyauto_base.cli.callGenericCmd(_cmdMontage, [
      lstImgPath[0], lstImgPath[1], lstImgPath[2], "-tile", "1x", "-geometry", "+0+0",
      "-border",
      str(border), tempPng
  ])
  if (res):
    raise RuntimeError

def join4Img_4r1c(lstImgPath: list[str], border: int) -> None:
  row = 4
  col = 1
  if (len(lstImgPath) != (row * col)):
    msg = "incorrect argument: lstImgPath"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("joining images ({:d}x{:d}) ...".format(row, col))
  res = pyauto_base.cli.callGenericCmd(_cmdMontage, [
      lstImgPath[0], lstImgPath[1], lstImgPath[2], lstImgPath[3], "-tile", "1x",
      "-geometry", "+0+0", "-border",
      str(border), tempPng
  ])
  if (res):
    raise RuntimeError

def join4Img_2r2c(lstImgPath: list[str], border: int) -> None:
  row = 2
  col = 2
  if (len(lstImgPath) != (row * col)):
    msg = "incorrect argument: lstImgPath"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("joining images ({:d}x{:d}) ...".format(row, col))
  res = pyauto_base.cli.callGenericCmd(_cmdMontage, [
      lstImgPath[0], lstImgPath[1], lstImgPath[2], lstImgPath[3], "-tile", "x2",
      "-geometry", "100%+0+0", "-border",
      str(border), tempPng
  ])
  if (res):
    raise RuntimeError

def join5Img_5r1c(lstImgPath: list[str], border: int) -> None:
  row = 5
  col = 1
  if (len(lstImgPath) != (row * col)):
    msg = "incorrect argument: lstImgPath"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("joining images ({:d}x{:d}) ...".format(row, col))
  res = pyauto_base.cli.callGenericCmd(_cmdMontage, [
      lstImgPath[0], lstImgPath[1], lstImgPath[2], lstImgPath[3], lstImgPath[4], "-tile",
      "1x", "-geometry", "+0+0", "-border",
      str(border), tempPng
  ])
  if (res):
    raise RuntimeError

def join6Img_6r1c(lstImgPath: list[str], border: int) -> None:
  row = 6
  col = 1
  if (len(lstImgPath) != (row * col)):
    msg = "incorrect argument: lstImgPath"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("joining images ({:d}x{:d}) ...".format(row, col))
  res = pyauto_base.cli.callGenericCmd(_cmdMontage, [
      lstImgPath[0], lstImgPath[1], lstImgPath[2], lstImgPath[3], lstImgPath[4],
      lstImgPath[5], "-tile", "1x", "-geometry", "+0+0", "-border",
      str(border), tempPng
  ])
  if (res):
    raise RuntimeError

def join6Img_2r3c(lstImgPath: list[str], border: int) -> None:
  row = 2
  col = 3
  if (len(lstImgPath) != (row * col)):
    msg = "incorrect argument: lstImgPath"
    logger.error(msg)
    raise RuntimeError(msg)

  logger.info("joining images ({:d}x{:d}) ...".format(row, col))
  res = pyauto_base.cli.callGenericCmd(_cmdMontage, [
      lstImgPath[0], lstImgPath[1], lstImgPath[2], lstImgPath[3], lstImgPath[4],
      lstImgPath[5], "-tile", "x2", "-geometry", "100%+0+0", "-border",
      str(border), tempPng
  ])
  if (res):
    raise RuntimeError
