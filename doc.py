#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

from __future__ import annotations  # for python 3.8
#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import stat
import typing
import docutils.core

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.config

class DocInfo(object):

  def __init__(self, val: str, fPath: str):
    self._id: typing.Optional[str] = None
    self.id = val

    if (not os.path.isabs(fPath)):
      msg = "please specify AN ABSOLUTE path: {}".format(fPath)
      logger.error(msg)
      raise RuntimeError(msg)
    pyauto_base.fs.chkPath_File(fPath)

    self.fPath: str = fPath
    self.docNo: str = "xxxxxxxx"
    self.docVer: str = "01"
    self.author: list = []
    self.conv: dict[str, bool] = {
        "MD2HTML": False,
        "MD2DOCX": False,
        "MD2PDF": False,
        "HTML2PDF": False
    }
    self.convOpts: dict[str, str] = {"orientation": "portrait"}

  def __str__(self):
    return "{}: {}, {}_{}, '{}'".format(self.__class__.__name__, self._id, self.docNo,
                                        self.docVer, self.fPath)

  @property
  def id(self) -> typing.Optional[str]:
    return self._id

  @id.setter
  def id(self, val: typing.Optional[str]) -> None:
    if (pyauto_base.misc.isEmptyString(val)):
      msg = "CANNOT assign empty DocInfo id"
      logger.error(msg)
      raise RuntimeError(msg)
    self._id = val

  @property
  def docName(self):
    return ".".join(os.path.basename(self.fPath).split(".")[:-1])

  @property
  def convMD2HTML(self) -> bool:
    return self.conv["MD2HTML"]

  @convMD2HTML.setter
  def convMD2HTML(self, val: bool) -> None:
    self.conv["MD2HTML"] = bool(val)

  @property
  def convMD2DOCX(self) -> bool:
    return self.conv["MD2DOCX"]

  @convMD2DOCX.setter
  def convMD2DOCX(self, val: bool) -> None:
    self.conv["MD2DOCX"] = bool(val)

  @property
  def convMD2PDF(self) -> bool:
    return self.conv["MD2PDF"]

  @convMD2PDF.setter
  def convMD2PDF(self, val: bool) -> None:
    self.conv["MD2PDF"] = bool(val)

  @property
  def convHTML2PDF(self) -> bool:
    return self.conv["HTML2PDF"]

  @convHTML2PDF.setter
  def convHTML2PDF(self, val: bool) -> None:
    self.conv["HTML2PDF"] = bool(val)

  @property
  def convOrientation(self) -> str:
    return self.convOpts["orientation"]

  @convOrientation.setter
  def convOrientation(self, val: str) -> None:
    if (val not in ["portrait", "landscape"]):
      logger.warning("INCORRECT orientation: '{}'".format(val))
      return
    self.convOpts["orientation"] = val

class DocConversion(object):

  def __init__(self):
    _doc_lib_path = pyauto_base.fs.mkAbsolutePath("WORK/doclib")

    self.fPath: str = ""
    self.env: dict[str, str] = {
        "PANDOC_DIR": os.path.abspath(os.path.join(_doc_lib_path, "pandoc")),
        "WKHTML_DIR": os.path.abspath(os.path.join(_doc_lib_path, "wkhtmltopdf"))
    }
    self.doc: dict[str, DocInfo] = {}

  def __str__(self):
    return "{}, {} doc(s), '{}'".format(self.__class__.__name__, len(self.doc), self.fPath)

  def addDocInfo(self, docInfo: DocInfo) -> None:
    if (not isinstance(docInfo, DocInfo)):
      msg = "CANNOT assign empty DocInfo id"
      logger.error(msg)
      raise RuntimeError(msg)
    self.doc[docInfo.id] = docInfo

  def writeScript(self, fPath: str = "") -> None:
    """
    If:
    * 'fPath' is None, the script will be written in the current folder
    * 'fPath' is a folder, the script will be written there
    :param fPath:
    :return:
    """

    if (sys.platform.startswith("win")):
      scrName = "doc.bat"
    elif (sys.platform.startswith("linux")):
      scrName = "doc.sh"
    else:
      msg = "UNSUPPORTED platform: {}".format(sys.platform)
      logger.error(msg)
      raise RuntimeError(msg)

    if (pyauto_base.misc.isEmptyString(fPath)):
      self.fPath = scrName
    else:
      if (os.path.isfile(fPath)):
        self.fPath = fPath
      elif (os.path.isdir(fPath)):
        self.fPath = os.path.join(fPath, scrName)
      else:
        self.fPath = fPath  # path does not exist
        #msg = "UNRECOGNIZED path: {}".format(fPath)
        #logger.error(msg)
        #raise RuntimeError(msg)

    lst_doc = []
    for di in self.doc.values():
      lst_doc.append({
          "path": di.fPath,
          "name": di.docName,
          "no": di.docNo,
          "ver": di.docVer,
          "conv": di.conv
      })
    dict_tpl = {"dictEnv": self.env, "lstDoc": lst_doc}
    if (sys.platform.startswith("win")):
      tpl_path = os.path.join(os.path.dirname(__file__), "templates", "doc.bat.tpl")
    elif (sys.platform.startswith("linux")):
      tpl_path = os.path.join(os.path.dirname(__file__), "templates", "doc.sh.tpl")
    else:
      raise NotImplementedError
    pyauto_base.misc.templateWheezy(tpl_path, self.fPath, dictTpl=dict_tpl)
    logger.info("{} written".format(self.fPath))
    if (sys.platform.startswith("linux")):
      st = os.stat(self.fPath)
      os.chmod(self.fPath, st.st_mode | stat.S_IEXEC)

class BaseHyperlink(object):

  def __init__(self, strHref: str, strTitle: str = ""):
    if (pyauto_base.misc.isEmptyString(strHref)):
      msg = "CANNOT create hyperlink without href"
      logger.error(msg)
      raise RuntimeError(msg)
    self.href = strHref

    if (pyauto_base.misc.isEmptyString(strTitle)):
      msg = "CANNOT create hyperlink with empty title"
      logger.error(msg)
      raise RuntimeError(msg)
    self.title = strTitle

class BaseImage(object):

  def __init__(self,
               strSrc: str,
               strAlt: str = "",
               width: typing.Union[int, str] = "",
               height: typing.Union[int, str] = ""):
    if (pyauto_base.misc.isEmptyString(strSrc)):
      msg = "CANNOT create image without src"
      logger.error(msg)
      raise RuntimeError(msg)
    self.src = strSrc

    if (pyauto_base.misc.isEmptyString(strAlt)):
      self.alt = None
    else:
      self.alt = strAlt
    self.width = str(width)
    self.height = str(height)

class BaseDocument(object):

  def __init__(self):
    self.content = []

  def __str__(self):
    return "{}: {} line(s)".format(self.__class__.__name__, len(self.content))

  def clear(self) -> None:
    self.content = []

  def _addContent(self, lstLn: list[str], bEmptyLine: bool = True) -> None:
    if (len(lstLn) == 0):
      return

    self.content += lstLn
    if (bEmptyLine):
      self.content.append("")

  def addParagraph(self, val: typing.Union[str, list[str]]) -> None:
    if (isinstance(val, str)):
      lst = [val]
    elif (isinstance(val, list)):
      lst = [str(t) for t in val]
    else:
      msg = "INCORRECT type for addParagraph: {}".format(type(val))
      logger.error(msg)
      raise RuntimeError(msg)

    self._addContent(lst)

  def addBulletList(self, lstStr: list[str]) -> None:
    lst = ["* {}".format(t) for t in lstStr]
    self._addContent(lst)

  def addNumberedList(self, lstStr: list[str]) -> None:
    lst = ["{:d}. {}".format((i + 1), v) for i, v in enumerate(lstStr)]
    self._addContent(lst)

  def _addTableWithHdr(self, lstData: list[list[str]]) -> None:
    raise NotImplementedError

  def _addTableNoHdr(self, lstData: list[list[str]]) -> None:
    raise NotImplementedError

  def addTable(self,
               lstData: typing.Union[list[str], list[list[str]]],
               nCols: int = 0,
               bHdr: bool = False) -> None:
    """
    - if lstData is a 1D list and nCols > 1, it will create a table with nCols columns
    - if lstData is a 2D list it will create a table with or without headers
    """
    if (not isinstance(lstData, list)):
      msg = "INCORRECT type for lstData: {}".format(type(lstData))
      logger.error(msg)
      raise RuntimeError(msg)

    if (not isinstance(lstData[0], list)):  # lstData is a 1D list
      if (nCols < 2):
        msg = "INCORRECT nCols: {}".format(nCols)
        logger.error((msg))
        raise RuntimeError(msg)

      for _ in range(nCols - (len(lstData) % nCols)):
        lstData.append("")  # type: ignore
      lstNew: list[list[str]] = []
      for i in range(0, len(lstData), nCols):
        lstNew.append(lstData[i:(i + nCols)])  # type: ignore
      self._addTableNoHdr(lstNew)
    else:  # lstData is a 2D list
      if (bHdr):
        self._addTableWithHdr(lstData)  # type: ignore
      else:
        self._addTableNoHdr(lstData)  # type: ignore

  def showInfo(self) -> None:
    logger.info(self)
    #tmp = docutils.core.publish_parts(source="\n".join(self.content))
    #for k, v in tmp.items():
    #  logger.info("{}: {}".format(k, v))

  def write(self, fPath: str) -> None:
    #if(not fPath.lower().endswith(".txt")):
    #  msg = "INCORRECT file name: {}".format(fPath)
    #  logger.error(msg)
    #  raise RuntimeError(msg)

    with open(fPath, "w") as fOut:
      for ln in self.content:
        #fOut.write("{}{}".format(ln, os.linesep))
        fOut.write("{}\n".format(ln))
    logger.info("{} written".format(fPath))

def _formatTableRow(row: typing.Optional[list[str]],
                    colSize: list[int],
                    colSep: str = "|",
                    chFill: str = " ",
                    bSepOutside: bool = True) -> str:
  if (not isinstance(colSize, list)):
    msg = "colSize has INCORRECT type: {}".format(type(colSize))
    logger.error(msg)
    raise RuntimeError(msg)
  if (len(chFill) != 1):
    msg = "fill MUST BE 1 character: {}".format(type(chFill))
    logger.error(msg)
    raise RuntimeError(msg)

  if (row is None):
    row = [(t * chFill) for t in colSize]
  if (not isinstance(row, list)):
    msg = "row has INCORRECT type: {}".format(type(row))
    logger.error(msg)
    raise RuntimeError(msg)

  align = "^"
  colFmt = ["{{:{}{}{:d}}}".format(chFill, align, t) for t in colSize]
  if (bSepOutside):
    return (colSep + colSep.join([t[0].format(t[1]) for t in zip(colFmt, row)]) + colSep)
  else:
    return colSep.join([t[0].format(t[1]) for t in zip(colFmt, row)])

def _getTableColSize(lstData: list[list[str]]) -> list[int]:
  """
  determines text col size from table data
  """
  nCols = len(lstData[0])
  colSize = [len(str(t)) for t in lstData[0]]

  for row in lstData:
    if (not isinstance(row, list)):
      msg = "table row has INCORRECT type: {}".format(type(row))
      logger.error(msg)
      raise RuntimeError(msg)
    if (len(row) != nCols):
      msg = "table row MUST HAVE {} values : {}".format(nCols, row)
      logger.error(msg)
      raise RuntimeError(msg)

    sz = [len(str(t)) for t in row]
    colSize = [max(i, j) for i, j in zip(sz, colSize)]
  logger.info("table size: {} row(s) x {} col(s)".format(len(lstData), nCols))

  colSize = [(t + 2) for t in colSize]
  logger.info("table cols: {}".format(colSize))
  return colSize

class ReSTHyperlink(BaseHyperlink):

  def getAnonTitle(self) -> str:
    return "`{}`__".format(self.title)

  def getAnonHref(self) -> str:
    return ".. __: {}".format(self.href)

  def getTitle(self) -> str:
    return "`{}`_".format(self.title)

  def getHref(self) -> str:
    return ".. _{}: {}".format(self.title, self.href)

  def getEmbedded(self) -> str:
    return "`{} <{}>`_".format(self.title, self.href)

class ReSTImage(BaseImage):

  def __init__(self,
               strSrc: str,
               strAlt: str = "",
               strSubst: str = "",
               width: typing.Union[int, str] = "",
               height: typing.Union[int, str] = ""):
    super(ReSTImage, self).__init__(strSrc, strAlt, width, height)
    self.subst = strSubst

class ReSTDocument(BaseDocument):

  def addRole(self, val: str) -> None:
    if (isinstance(val, str)):
      lst = [".. role:: {}".format(val)]
    elif (isinstance(val, list)):
      lst = [".. role:: {}".format(t) for t in val]
    else:
      msg = "INCORRECT type for addRole: {}".format(type(val))
      logger.error(msg)
      raise RuntimeError(msg)

    self._addContent(lst)

  def addInclude(self, path: str) -> None:
    lst = [".. include:: {}".format(path)]
    self._addContent(lst)

  def addTitle(self, strVal: str) -> None:
    fmt = "{{:=>{:d}}}".format(len(strVal))
    lst = [fmt.format(""), strVal, fmt.format("")]
    self._addContent(lst)

  def addSubTitle(self, strVal: str) -> None:
    fmt = "{{:->{:d}}}".format(len(strVal))
    lst = [fmt.format(""), strVal, fmt.format("")]
    self._addContent(lst)

  def addHeader(self, strVal: str, lvl: int = 1) -> None:
    if (lvl == 1):
      fmt = "{{:=>{:d}}}".format(len(strVal))
    elif (lvl == 2):
      fmt = "{{:->{:d}}}".format(len(strVal))
    elif (lvl == 3):
      fmt = "{{:~>{:d}}}".format(len(strVal))
    elif (lvl == 4):
      fmt = "{{:+>{:d}}}".format(len(strVal))
    elif (lvl == 5):
      fmt = "{{:#>{:d}}}".format(len(strVal))
    elif (lvl == 6):
      fmt = "{{:*>{:d}}}".format(len(strVal))
    else:
      msg = "addHeader - CANNOT add header level: {}".format(type(lvl))
      logger.error(msg)
      raise RuntimeError(msg)

    lst = [strVal, fmt.format("")]
    self._addContent(lst)

  def addDefinitionList(self, dictString: dict[str, str]) -> None:
    tmp = []
    for k in sorted(dictString.keys()):
      tmp.append(k)
      tmp.append("    {}".format(dictString[k]))

    self._addContent(tmp)

  def addImage(self, img: ReSTImage) -> None:
    if (not isinstance(img, ReSTImage)):
      msg = "INCORRECT image type: {}".format(type(img))
      logger.error(msg)
      raise RuntimeError(msg)

    if (pyauto_base.misc.isEmptyString(img.subst)):
      lst = [".. image:: {}".format(img.src)]
    else:
      lst = [".. |{}| image:: {}".format(img.subst, img.src)]
    if ((img.height is not None) and isinstance(img.height, int)):
      lst.append("   :height: {}px".format(img.height))
    if ((img.width is not None) and isinstance(img.width, int)):
      lst.append("   :width: {}px".format(img.width))
    if (not pyauto_base.misc.isEmptyString(img.alt)):
      lst.append("   :alt: {}".format(img.alt))
    lst.append("   :target: {}".format(img.src))
    self._addContent(lst)

  def _addTableWithHdr(self, lstData: list[list[str]]) -> None:
    colSize = _getTableColSize(lstData)
    lst = [
        _formatTableRow(None, colSize, colSep=" ", chFill="=", bSepOutside=False),
        _formatTableRow(lstData[0], colSize, colSep=" ", chFill=" ", bSepOutside=False),
        _formatTableRow(None, colSize, colSep=" ", chFill="=", bSepOutside=False)
    ]
    for row in lstData[1:]:
      lst.append(_formatTableRow(row, colSize, colSep=" ", chFill=" ", bSepOutside=False))
    lst.append(_formatTableRow(None, colSize, colSep=" ", chFill="=", bSepOutside=False))
    self._addContent(lst)

  def _addTableNoHdr(self, lstData: list[list[str]]) -> None:
    colSize = _getTableColSize(lstData)
    lst = [_formatTableRow(None, colSize, colSep=" ", chFill="=", bSepOutside=False)]
    for row in lstData:
      lst.append(_formatTableRow(row, colSize, colSep=" ", chFill=" ", bSepOutside=False))
    lst.append(_formatTableRow(None, colSize, colSep=" ", chFill="=", bSepOutside=False))
    self._addContent(lst)

  def writeAsHTML(self, fPath: str, cssPath: str = "") -> None:
    if (not fPath.lower().endswith(".html")):
      msg = "INCORRECT file name: {}".format(fPath)
      logger.error(msg)
      raise RuntimeError(msg)

    dictOverride = {}
    if (pyauto_base.misc.isEmptyString(cssPath)):
      with open("local.css", "w"):
        pass
      dictOverride["stylesheet_path"] = "local.css"
    elif (pyauto_base.fs.chkPath_File(cssPath)):
      dictOverride["stylesheet_path"] = cssPath

    with open(fPath, "w") as fOut:
      fOut.write(
          docutils.core.publish_string(source="\n".join(self.content),
                                       writer_name="html",
                                       settings_overrides=dictOverride).decode("utf-8"))
    logger.info("{} written".format(fPath))
    if (cssPath is None):
      os.remove("local.css")

  def writeAsPDF(self, fPath: str) -> None:
    if (not fPath.lower().endswith(".pdf")):
      msg = "INCORRECT file name: {}".format(fPath)
      logger.error(msg)
      raise RuntimeError(msg)

    raise NotImplementedError
    #with open(fPath, "w") as fOut:
    #  fOut.write(docutils.core.publish_string(source="\n".join(self.content), writer_name="pdf"))
    #logger.info("{} written".format(fPath))

class MarkdownHyperlink(BaseHyperlink):

  def getInline(self) -> str:
    return "[{}]({})".format(self.title, self.href)

class MarkdownImage(BaseImage):

  pass

class MarkdownDocument(BaseDocument):

  def addHeader(self, strVal: str, lvl: int = 1) -> None:
    if (lvl == 1):
      lst = ["# {} #".format(strVal)]
    elif (lvl == 2):
      lst = ["## {} ##".format(strVal)]
    elif (lvl == 3):
      lst = ["### {} ###".format(strVal)]
    elif (lvl == 4):
      lst = ["#### {} ####".format(strVal)]
    elif (lvl == 5):
      lst = ["##### {} #####".format(strVal)]
    elif (lvl == 6):
      lst = ["###### {} ######".format(strVal)]
    else:
      msg = "addHeader - CANNOT add header level: {}".format(type(lvl))
      logger.error(msg)
      raise RuntimeError(msg)

    self._addContent(lst)

  def addDefinitionList(self, dictString: dict[str, str]) -> None:
    tmp = []
    for k in sorted(dictString.keys()):
      tmp.append(k)
      tmp.append(":    {}".format(dictString[k]))
      tmp.append("")

    self._addContent(tmp, bEmptyLine=False)

  def addImage(self, img: MarkdownImage) -> None:
    if (not isinstance(img, MarkdownImage)):
      msg = "INCORRECT image type: {}".format(type(img))
      logger.error(msg)
      raise RuntimeError(msg)

    ln = "![{}]({})".format(img.alt if (img.alt is not None) else "", img.src)
    attr: list[str] = []
    if ((img.height is not None) and (isinstance(img.height, int))):
      attr.append("height={}px".format(img.height))
    if ((img.width is not None) and (isinstance(img.width, int))):
      attr.append("width={}px".format(img.width))
    if (attr != []):
      ln += ("{" + " ".join(attr) + "}")
    self._addContent([ln])

  def _addTableWithHdr(self, lstData: list[list[str]]) -> None:
    colSize = _getTableColSize(lstData)
    lst = [
        _formatTableRow(lstData[0], colSize, colSep="|", chFill=" "),
        _formatTableRow(None, colSize, colSep="|", chFill="-")
    ]
    for row in lstData[1:]:
      lst.append(_formatTableRow(row, colSize, colSep="|", chFill=" "))
    self._addContent(lst)

  def _addTableNoHdr(self, lstData: list[list[str]]) -> None:
    colSize = _getTableColSize(lstData)
    lst = [
        _formatTableRow(None, colSize, colSep="|", chFill=" "),
        _formatTableRow(None, colSize, colSep="|", chFill="-")
    ]
    for row in lstData:
      lst.append(_formatTableRow(row, colSize, colSep="|", chFill=" "))
    self._addContent(lst)
