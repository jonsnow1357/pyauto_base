#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

from __future__ import annotations  # for python 3.8
#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv
import typing
import configparser
import xml.etree.ElementTree as ET
import xml.dom.minidom

logger = logging.getLogger("lib")
import pyauto_base.fs

def readCSV_asRows(fPath: str, rowSkip: int = 1) -> list[dict[str, str]]:
  """
  Reads a .csv file, skips rowSkip rows and assumes the next row are headers.
  Returns a list of dictionaries. Each dictionary represents a row and has the header names as keys.

  :param fPath: path to file
  :param rowSkip: number of rows to skip
  :return: list of dictionaries
  """
  if (len(fPath) == 0):
    logger.warning("empty file path")
    return []
  if (not fPath.lower().endswith(".csv")):
    logger.warning("incorrect file path: {}".format(fPath))
    return []
  if (not pyauto_base.fs.chkPath_File(fPath, bDie=False)):
    return []
  if (rowSkip < 1):
    rowSkip = 1
  logger.info("-- reading csv file: {}".format(fPath))

  with open(fPath, "r") as fIn:
    csvIn = csv.reader(fIn)

    keys = []
    res = []
    for row in csvIn:
      if (len(row) == 0):  # ignore empty lines
        continue
      if (csvIn.line_num < rowSkip):
        logger.info("skip line: {}".format(row))
        continue
      if (csvIn.line_num == rowSkip):
        #logger.info("header: {}".format(row))
        keys = row
        continue
      if ((csvIn.line_num > rowSkip) and (len(row) != len(keys))):
        logger.warning("IGNORE line {}: {}".format(csvIn.line_num, row))
        continue
      entry = dict(zip(keys, row))
      res.append(entry)

  logger.info("read {:d} row(s); {:d} col(s)".format(len(res), len(keys)))
  logger.info("-- done reading file")
  return res

def readCSV_asCols(fPath: str, rowSkip: int = 1) -> list[list[str]]:
  """
  Reads a .csv file, skips rowSkip rows.
  Returns a list of lists. Each list represents a column.

  :param fPath: path to file
  :param rowSkip: number of rows to skip
  :return: list of lists
  """
  if (len(fPath) == 0):
    logger.warning("empty file path")
    return []
  if (not fPath.lower().endswith(".csv")):
    logger.warning("incorrect file path: {}".format(fPath))
    return []
  if (not pyauto_base.fs.chkPath_File(fPath, bDie=False)):
    return []
  if (rowSkip < 1):
    rowSkip = 1
  logger.info("-- reading csv file: {}".format(fPath))

  with open(fPath, "r") as fIn:
    csvIn = csv.reader(fIn)

    res = []
    for row in csvIn:
      if (len(row) == 0):  # ignore empty lines
        continue
      if (csvIn.line_num < rowSkip):
        logger.info("skip line: {}".format(row))
        continue
      if (csvIn.line_num == rowSkip):
        #logger.info("header: {}".format(row))
        for v in row:
          res.append([v])
        continue
      if ((csvIn.line_num > rowSkip) and (len(row) != len(res))):
        logger.warning("IGNORE line {}: {}".format(csvIn.line_num, row))
        continue
      for l1, l2 in zip(res, row):
        l1.append(l2)
  logger.info("read {:d} row(s); {:d} col(s)".format(len(res[0]), len(res)))
  logger.info("-- done reading file")
  return res

def readTXT_config(fPath: str) -> configparser.ConfigParser:
  res = configparser.ConfigParser()
  res.optionxform = str  # type: ignore # will be case sensitive
  if (sys.version_info[0] == 2):
    #res.readfp(open(fPath, "r"))
    res.read(fPath)
  else:
    #res.read_file(open(fPath, "r", encoding="utf8"))
    res.read(fPath, encoding="utf-8")

  return res

def writeTXT_config(fPath: str, dictCfg: dict) -> None:
  res = configparser.RawConfigParser()
  res.optionxform = str  # type: ignore # will be case sensitive

  for section, options in dictCfg.items():
    s = str(section)
    res.add_section(s)
    for k, v in options.items():
      res.set(s, str(k), str(v))

  with open(fPath, "w") as fOut:
    res.write(fOut)
  logger.info("{} written".format(fPath))

def writeXML_pretty(fPath: str,
                    elRoot: ET.Element,
                    dtd: typing.Optional[str] = None,
                    bEncoding: bool = False) -> None:
  tmp = ET.tostring(elRoot, encoding="utf-8").decode()
  tmp = tmp.translate(str.maketrans("", "", "\n\r"))

  doc = xml.dom.minidom.parseString(tmp)
  if (dtd == "SVG"):
    doctype = xml.dom.minidom.DocumentType("svg")
    doctype.publicId = "-//W3C//DTD SVG 1.1//EN"
    doctype.systemId = "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"
    doc.insertBefore(doctype, doc.documentElement)
  elif (dtd is not None):
    logger.warning("UNDEFINED DTD: {}".format(dtd))

  if (bEncoding):
    strXML = doc.toprettyxml(indent="  ", encoding="utf-8").decode()
  else:
    strXML = doc.toprettyxml(indent="  ")

  if (sys.version_info[0] == 2):
    import codecs
    with codecs.open(fPath, "w", encoding="utf-8") as fOut:
      fOut.write(strXML)
  else:
    with open(fPath, "w", encoding="utf-8", newline="\n") as fOut:
      fOut.write(strXML)
  logger.info("{} written".format(fPath))

def getFileHash(fPath: str, hashAlg: str = "sha256") -> str:
  """
  :param fPath: file path
  :param hashAlg: [md5|sha256|sha384|sha512]
  :return: hex string
  """
  import hashlib

  if (hashAlg == "md5"):
    hs = hashlib.md5()
  elif (hashAlg == "sha256"):
    hs = hashlib.sha256()
  elif (hashAlg == "sha384"):
    hs = hashlib.sha384()
  elif (hashAlg == "sha512"):
    hs = hashlib.sha512()
  else:
    logger.error("UNRECOGNIZED hash algorithm: {}".format(hashAlg))
    return ""

  with open(fPath, "rb") as fIn:
    buf = fIn.read()
    hs.update(buf)

  return hs.hexdigest()

def templateSimple(fInPath: str, fOutPath: str, dictTpl: dict) -> None:
  pyauto_base.fs.chkPath_File(fInPath)

  import string
  logger.info("template  IN: {}".format(fInPath))
  logger.info("template OUT: {}".format(fOutPath))

  with open(fInPath, "r") as fIn:
    with open(fOutPath, "w") as fOut:
      for ln in fIn.readlines():
        if (ln.startswith("@#")):
          fOut.write(ln)
        else:
          tpl = string.Template(ln)
          fOut.write(tpl.substitute(**dictTpl))

def templateWheezy(fInPath: str,
                   fOutPath: str,
                   dictTpl: typing.Optional[dict] = None) -> None:
  pyauto_base.fs.chkPath_File(fInPath)
  if (dictTpl is None):
    dictTpl = {}

  import json.decoder
  import wheezy.template as whz
  import wheezy.template.ext.core as whz_core
  logger.info("template  IN: {}".format(fInPath))
  logger.info("template OUT: {}".format(fOutPath))

  dictTplFile = {}
  with open(fInPath, "r") as fIn:
    for ln in fIn.readlines():
      if (ln.startswith("@#")):
        try:
          tmp = json.loads(ln[2:])
          if (isinstance(tmp, dict)):
            dictTplFile.update(tmp)
        except json.decoder.JSONDecodeError:
          pass

  if (len(dictTplFile) > 0):
    logger.info("template: extra dictionary defined")
    dictTpl.update(dictTplFile)
  #logger.info(dictTpl)

  eng = whz.engine.Engine(loader=whz.loader.FileLoader([os.path.dirname(fInPath)]),
                          extensions=[whz_core.CoreExtension()])
  # wheezy.template issue https://github.com/akornatskyy/wheezy.template/issues/68
  if (sys.version_info[0:2] == (3, 11)):
    eng.compiler.source_lineno = 0
  tpl = eng.get_template(os.path.basename(fInPath))
  if (sys.version_info[0] == 2):
    with open(fOutPath, "w") as fOut:
      fOut.write(tpl.render(dictTpl))
  else:
    with open(fOutPath, "w", newline='') as fOut:
      fOut.write(tpl.render(dictTpl))

class ResultLog(object):

  def __init__(self, fPath: str, strType: str = "csv"):
    if ((fPath is None) or (fPath == "")):
      msg = "path CANNOT be empty"
      logger.error(msg)
      raise RuntimeError(msg)
    self._path = fPath
    if ((strType is None) or (strType == "")):
      msg = "type CANNOT be empty"
      logger.error(msg)
      raise RuntimeError(msg)
    self._type = strType
    self._append = False

  def _addTxt(self, ln: str) -> None:
    ln = ln.strip(" \n\r")
    if (not self._append):
      with open(self._path, "w") as fOut:
        fOut.write(ln + "\n")
      self._append = True
    else:
      with open(self._path, "a") as fOut:
        fOut.write(ln + "\n")

  def _addCsv(self, lst: list[str]) -> None:
    if (not self._append):
      with open(self._path, "w") as fOut:
        csvOut = csv.writer(fOut, lineterminator="\n")
        csvOut.writerow(lst)
      self._append = True
    else:
      with open(self._path, "a") as fOut:
        csvOut = csv.writer(fOut, lineterminator="\n")
        csvOut.writerow(lst)

  def add(self, val: typing.Union[str, list[str]]) -> None:
    if (isinstance(val, str) and (self._type == "txt")):
      self._addTxt(val)
    elif (isinstance(val, list) and (self._type == "csv")):
      self._addCsv(val)
    else:
      msg = "CANNOT add type '{}' to '{}' log".format(type(val), self._type)
      logger.error(msg)
      raise RuntimeError(msg)
