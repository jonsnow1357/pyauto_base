#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library for test parameters"""

from __future__ import annotations  # for python 3.8
#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv
import typing
import json
import string
import copy
import collections
import unittest
import xml.etree.ElementTree as ET

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.db
import pyauto_base.misc
import pyauto_base.config

testStatusPass = "PASS"
testStatusFail = "FAIL"
testStatusSkip = "SKIP"
testStatusError = "ERROR"
testStatusRun = "run"
testStatusNotRun = "notrun"
lstTestStatus = (testStatusPass, testStatusFail, testStatusSkip, testStatusError,
                 testStatusRun, testStatusNotRun)

testLogTypeJson = "json"
testLogTypeCsv = "csv"
testLogTypeSqlite = "sqlite"
testLogTypeMysql = "mysql"
lstTestLog = (testLogTypeJson, testLogTypeCsv, testLogTypeSqlite, testLogTypeMysql)

class TestInfo(object):
  """
  Class that holds basic information about a test / TestCase.
  """

  def __init__(self, strId: typing.Optional[str] = None):
    self._id: typing.Optional[str] = None
    self.title: typing.Optional[str] = None
    self.description: str = ""
    self.status: typing.Optional[str] = None
    self.params = {"manual": False}  # TODO: fix typing hints
    self._t_start: typing.Optional[datetime.datetime] = None
    self._t_run: typing.Optional[datetime.timedelta] = None
    self._t_est: typing.Optional[datetime.timedelta] = None

    if (strId is None):
      import uuid
      import random
      #self.id = str(uuid.uuid4())
      tmp = random.randint(0, 15)
      self.id = uuid.uuid4().hex[tmp:(tmp + 16)]
    else:
      self.id = strId
    self.setStatusNotRun()

  def __str__(self):
    return "{}: ({}) {: <6} '{}', {} param(s) {}".format(
        self.__class__.__name__, self._id, self.status, self.title, len(self.params),
        ("" if (self._t_start is None) else "start={}".format(self.startTime)))

  @property
  def id(self) -> typing.Optional[str]:
    return self._id

  @id.setter
  def id(self, val: str) -> None:
    if (pyauto_base.misc.isEmptyString(val)):
      logger.warning("CANNOT assign empty id")
      return
    self._id = val

  # yapf: disable
  @property
  def startTime(self):
    return re.sub(r"\.[0-9+:]+$", "", self._t_start.isoformat()) if (self._t_start is not None) else None
  # yapf: enable

  @property
  def runTime(self):
    return self._t_run

  @property
  def estimatedTime(self):
    return self._t_est

  @estimatedTime.setter
  def estimatedTime(self, val: str) -> None:
    self._t_est = pyauto_base.misc.parseTimedeltaString(val)

  def setStatusNotRun(self) -> None:
    self.status = testStatusNotRun
    self._t_start = None
    self._t_run = None

  def setStatusRun(self) -> None:
    if (self.status != testStatusNotRun):
      msg = "CANNOT change {} status from {} to {}".format(self.__class__.__name__,
                                                           self.status, testStatusRun)
      logger.error(msg)
      raise RuntimeError(msg)
    self.status = testStatusRun
    self._t_start = datetime.datetime.now()
    self._t_run = None

  def setStatusSkip(self) -> None:
    if (self.status not in (testStatusNotRun, testStatusRun)):
      msg = "CANNOT change {} status from {} to {}".format(self.__class__.__name__,
                                                           self.status, testStatusSkip)
      logger.error(msg)
      raise RuntimeError(msg)
    self.status = testStatusSkip
    self._t_start = datetime.datetime.now()
    self._t_run = None

  def setStatusPass(self) -> None:
    if (self.status != testStatusRun):
      msg = "CANNOT change {} status from {} to {}".format(self.__class__.__name__,
                                                           self.status, testStatusPass)
      logger.error(msg)
      raise RuntimeError(msg)
    self.status = testStatusPass
    if (self._t_start is None):
      self._t_start = datetime.datetime.now()
      self._t_run = datetime.timedelta(0)
    else:
      self._t_run = datetime.datetime.now() - self._t_start

  def setStatusFail(self) -> None:
    if (self.status != testStatusRun):
      msg = "CANNOT change {} status from {} to {}".format(self.__class__.__name__,
                                                           self.status, testStatusFail)
      logger.error(msg)
      raise RuntimeError(msg)
    self.status = testStatusFail
    if (self._t_start is None):
      self._t_start = datetime.datetime.now()
      self._t_run = datetime.timedelta(0)
    else:
      self._t_run = datetime.datetime.now() - self._t_start

  def setStatusError(self):
    if (self.status != testStatusRun):
      msg = "CANNOT change {} status from {} to {}".format(self.__class__.__name__,
                                                           self.status, testStatusError)
      logger.error(msg)
      raise RuntimeError(msg)
    self.status = testStatusError
    if (self._t_start is None):
      self._t_start = datetime.datetime.now()
      self._t_run = datetime.timedelta(0)
    else:
      self._t_run = datetime.datetime.now() - self._t_start

def _getMainAppName() -> str:
  return os.path.basename(sys.argv[0]).split(".")[0]

class TestData(object):
  """
  object that allows logging values from a test / TestCase execution.
  """

  def __init__(self):
    self._id: typing.Optional[str] = None
    self._type: typing.Optional[str] = None
    self.logTimestamp: bool = False
    self.logAppName: bool = False
    self.logTestId: bool = False
    self._params = {}  # TODO: fix typing hints
    self._email: dict[str, bool] = {"verify": False}
    self._results: dict[str, str] = {}

  def __str__(self):
    return "{}: type {}, {:d} results(s)".format(self.__class__.__name__, self._type,
                                                 len(self._results))

  @property
  def id(self) -> typing.Optional[str]:
    return self._id

  @id.setter
  def id(self, val: str) -> None:
    if (pyauto_base.misc.isEmptyString(val)):
      logger.warning("CANNOT assign empty id")
      return
    self._id = val

  @property
  def type(self) -> typing.Optional[str]:
    return self._type

  @property
  def results(self) -> dict[str, str]:
    return self._results

  def addResult(self, key: str, value: str) -> None:
    self._results[key] = value

  def _configLog_Json(self,
                      logPath: typing.Optional[str],
                      logClear: bool = False,
                      logColNames: typing.Optional[list[str]] = None,
                      logTable: typing.Optional[str] = None) -> None:
    if (pyauto_base.misc.isEmptyString(logPath)):
      self._params["logPath"] = "{}_log.json".format(_getMainAppName())
      self._params["logPath"] = pyauto_base.fs.addTS(self._params["logPath"], bSec=True)
    else:
      self._params["logPath"] = logPath

    self._params["logPath"] = os.path.abspath(self._params["logPath"])
    if (logClear):
      if (os.path.exists(self._params["logPath"])):
        logger.info("CLEAR old log: {}".format(self._params["logPath"]))
    else:
      if (os.path.exists(self._params["logPath"])):
        logger.info("APPEND to log: {}".format(self._params["logPath"]))
        return

    with open(self._params["logPath"], "w"):
      pass

  def _configLog_Csv(self,
                     logPath: typing.Optional[str],
                     logClear: bool = False,
                     logColNames: typing.Optional[list[str]] = None,
                     logTable: typing.Optional[str] = None) -> None:
    if (pyauto_base.misc.isEmptyString(logPath)):
      self._params["logPath"] = "{}_log.csv".format(_getMainAppName())
      self._params["logPath"] = pyauto_base.fs.addTS(self._params["logPath"], bSec=True)
    else:
      self._params["logPath"] = logPath

    self._params["logPath"] = os.path.abspath(self._params["logPath"])
    if (logClear):
      if (os.path.exists(self._params["logPath"])):
        logger.info("CLEAR old log: {}".format(self._params["logPath"]))
    else:
      if (os.path.exists(self._params["logPath"])):
        logger.info("APPEND to log: {}".format(self._params["logPath"]))
        return

    self._params["colNames"] = ["timestamp", "testId", "testApp"]
    if (isinstance(logColNames, list) and (len(logColNames) > 0)):
      self._params["colNames"] += logColNames
    else:
      self._params["colNames"].append("msg")
    with open(self._params["logPath"], "w") as fOut:
      csvWr = csv.writer(fOut, lineterminator="\n")
      csvWr.writerow(self._params["colNames"])
      fOut.flush()

  def _configLog_Sqlite(self,
                        logPath: pyauto_base.db.DBInfo,
                        logClear: bool = False,
                        logColNames: typing.Optional[list[str]] = None,
                        logTable: typing.Optional[str] = None) -> None:
    import pyauto_base.db

    if (not isinstance(logPath, pyauto_base.db.DBInfo)):
      msg = "INCORRECT parameter type: {}".format(type(logPath))
      logger.error(msg)
      raise RuntimeError(msg)

    self._params["dictDBInfo"] = logPath
    if (len(self._params["dictDBInfo"].queries) != 0):
      logger.warning("DELETING UNEXPECTED queries in config file")
      self._params["dictDBInfo"].queries = {}

    self._params["colNames"] = ["timestamp", "testId", "testApp"]
    if (isinstance(logColNames, list) and (len(logColNames) > 0)):
      self._params["colNames"] += logColNames
    else:
      self._params["colNames"].append("msg")
    self._params["tblName"] = "log"
    if (not pyauto_base.misc.isEmptyString(logTable)):
      self._params["tblName"] = logTable

    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendSqlite)
    qry.drop(self._params["tblName"])
    self._params["dictDBInfo"].queries["drop_log"] = qry.sql
    dictCols = collections.OrderedDict()
    for col in self._params["colNames"]:
      if (col == "timestamp"):
        dictCols[col] = "DATETIME DEFAULT CURRENT_TIMESTAMP"
      else:
        dictCols[col] = "TEXT"
    qry.create(self._params["tblName"], dictCols)
    self._params["dictDBInfo"].queries["create_log"] = qry.sql
    qry.insert(self._params["tblName"], self._params["colNames"])
    self._params["dictDBInfo"].queries["ins_log"] = qry.sql
    #logger.info(self._params["dictDBInfo"].queries["drop_log"])
    #logger.info(self._params["dictDBInfo"].queries["create_log"])
    #logger.info(self._params["dictDBInfo"].queries["ins_log"])

    dbCtl = pyauto_base.db.DBController()
    dbCtl.dbInfo = self._params["dictDBInfo"]
    dbCtl.connect()
    dbCtl.doQuery = True

    if (logClear):
      try:
        logger.info("CLEAR old log")
        qry = self._params["dictDBInfo"].queries["drop_log"]
        dbCtl.query(str(qry))
      except pyauto_base.db.DBException:
        pass
    dbCtl.getTables()
    if (self._params["tblName"] not in dbCtl.dbInfo.tableNames):
      qry = self._params["dictDBInfo"].queries["create_log"]
      dbCtl.query(str(qry))

    dbCtl.disconnect()

  def _configLog_Mysql(self,
                       logPath: pyauto_base.db.DBInfo,
                       logClear: bool = False,
                       logColNames: typing.Optional[list[str]] = None,
                       logTable: typing.Optional[str] = None) -> None:
    import pyauto_base.db

    if (not isinstance(logPath, pyauto_base.db.DBInfo)):
      msg = "INCORRECT parameter type: {}".format(type(logPath))
      logger.error(msg)
      raise RuntimeError(msg)

    self._params["dictDBInfo"] = logPath
    if (len(self._params["dictDBInfo"].queries) != 0):
      logger.warning("DELETING UNEXPECTED queries in config file")
      self._params["dictDBInfo"].queries = {}

    self._params["colNames"] = ["timestamp", "testId", "testApp"]
    if (isinstance(logColNames, list) and (len(logColNames) > 0)):
      self._params["colNames"] += logColNames
    else:
      self._params["colNames"].append("msg")
    self._params["tblName"] = "log"
    if (not pyauto_base.misc.isEmptyString(logTable)):
      self._params["tblName"] = logTable

    qry = pyauto_base.db.DBQuery(pyauto_base.db.dbBackendMysql)
    qry.drop(self._params["tblName"])
    self._params["dictDBInfo"].queries["drop_log"] = qry.sql
    dictCols = collections.OrderedDict()
    for col in self._params["colNames"]:
      if (col == "timestamp"):
        dictCols[col] = "DATETIME NULL DEFAULT NULL"
      else:
        dictCols[col] = "TEXT NULL"
    qry.create(self._params["tblName"], dictCols)
    self._params["dictDBInfo"].queries["create_log"] = qry.sql
    qry.insert(self._params["tblName"], self._params["colNames"])
    self._params["dictDBInfo"].queries["ins_log"] = qry.sql
    #logger.info(self._params["dictDBInfo"].queries["drop_log"])
    #logger.info(self._params["dictDBInfo"].queries["create_log"])
    #logger.info(self._params["dictDBInfo"].queries["ins_log"])

    dbCtl = pyauto_base.db.DBController()
    dbCtl.dbInfo = self._params["dictDBInfo"]
    dbCtl.connect()
    dbCtl.doQuery = True

    if (logClear):
      try:
        logger.info("CLEAR old log")
        qry = self._params["dictDBInfo"].queries["drop_log"]
        dbCtl.query(str(qry))
      except pyauto_base.db.DBException:
        pass
    dbCtl.getTables()
    if (self._params["tblName"] not in dbCtl.dbInfo.tableNames):
      qry = self._params["dictDBInfo"].queries["create_log"]
      dbCtl.query(str(qry))

    dbCtl.disconnect()

  def configLog(self,
                logType: str,
                logPath: typing.Union[typing.Optional[str], pyauto_base.db.DBInfo] = None,
                logClear: bool = False,
                logColNames: typing.Optional[list[str]] = None,
                logTable: typing.Optional[str] = None) -> None:
    """
    :param logType: log type
    :param logPath:
    :param logClear: True|False - clear old entries
    :param logColNames: list of col names if more than one message is logged
    :param logTable: table name for database types
    :return:
    """
    #self.clearLog()
    self._params = {}
    if (logType == testLogTypeJson):
      self._type = logType
      if (isinstance(logPath, pyauto_base.db.DBInfo)):
        raise RuntimeError
      self._configLog_Json(logPath, logClear, logColNames, logTable)
    elif (logType == testLogTypeCsv):
      self._type = logType
      if (isinstance(logPath, pyauto_base.db.DBInfo)):
        raise RuntimeError
      self._configLog_Csv(str(logPath), logClear, logColNames, logTable)
    elif (logType == testLogTypeSqlite):
      self._type = logType
      if (not isinstance(logPath, pyauto_base.db.DBInfo)):
        raise RuntimeError
      self._configLog_Sqlite(logPath, logClear, logColNames, logTable)
    elif (logType == testLogTypeMysql):
      self._type = logType
      if (not isinstance(logPath, pyauto_base.db.DBInfo)):
        raise RuntimeError
      self._configLog_Mysql(logPath, logClear, logColNames, logTable)
    else:
      msg = "UNSUPPORTED log type: {}".format(logType)
      logger.error(msg)
      raise RuntimeError(msg)

  def _createLogEntry(
      self, message: typing.Union[str, list[str], dict[str, str]]
  ) -> dict[str, typing.Union[None, str, list[str]]]:
    """
    :param message: message (string or list or dictionary)
    :return: dictionary of
    {"timestamp": <timestamp>, "testId": <testId>, "testApp": <app_name>, ...}
    """
    res: dict[str, typing.Union[None, str, list[str]]] = collections.OrderedDict()
    if (self.logTimestamp):
      res["timestamp"] = pyauto_base.misc.getTimestamp()
    if (self.logTestId):
      res["testId"] = self._id
    if (self.logAppName):
      res["testApp"] = _getMainAppName()

    if (isinstance(message, str)):
      res["msg"] = message
    elif (isinstance(message, list)):
      res["msg"] = message
    elif (isinstance(message, dict)):
      res.update(message)

    logger.info(list(res.items()))
    return res

  def _log_Json(self, message: typing.Union[str, list[str], dict[str, str]]) -> None:
    res = self._createLogEntry(message)

    with open(self._params["logPath"], "a") as fOut:
      fOut.write("{}\n".format(json.dumps(res)))
      fOut.flush()

  def _log_Csv(self, message: typing.Union[str, list[str], dict[str, str]]) -> None:
    res = self._createLogEntry(message)

    row = []
    for colName in self._params["colNames"]:
      if (colName in res):
        row.append(res[colName])
      else:
        row.append("")
    if (len([t for t in row if t != ""]) == 0):
      logger.warning("NOTHING to log ... column name mismatch?")
      return
    with open(self._params["logPath"], "a") as fOut:
      csvWr = csv.writer(fOut, lineterminator="\n")
      csvWr.writerow(row)
      fOut.flush()

  def _log_Sqlite(self, message: typing.Union[str, list[str], dict[str, str]]) -> None:
    import pyauto_base.db
    res = self._createLogEntry(message)

    row: list[typing.Optional[str]] = []
    for colName in self._params["colNames"]:
      if (colName in res.keys()):
        row.append(res[colName])  # type: ignore
      else:
        row.append(None)
    if (len([t for t in row if t is not None]) == 0):
      logger.warning("NOTHING to log ... column name mismatch?")
      return
    dbCtl = pyauto_base.db.DBController()
    dbCtl.dbInfo = self._params["dictDBInfo"]
    dbCtl.connect()
    dbCtl.doQuery = True

    qry = dbCtl.dbInfo.queries["ins_log"]
    dbCtl.query(qry, lstVal=row)

    dbCtl.disconnect()

  def _log_Mysql(self, message: typing.Union[str, list[str], dict[str, str]]) -> None:
    import pyauto_base.db
    res = self._createLogEntry(message)

    row: list[typing.Optional[str]] = []
    for colName in self._params["colNames"]:
      if (colName in res):
        row.append(res[colName])  # type: ignore
      else:
        row.append(None)
    if (len([t for t in row if t is not None]) == 0):
      logger.warning("NOTHING to log ... column name mismatch?")
      return
    dbCtl = pyauto_base.db.DBController()
    dbCtl.dbInfo = self._params["dictDBInfo"]
    dbCtl.connect()
    dbCtl.doQuery = True

    qry = dbCtl.dbInfo.queries["ins_log"]
    dbCtl.query(qry, lstVal=row)

    dbCtl.disconnect()

  def log(self, message: typing.Union[str, list[str], dict[str, str]]) -> None:
    if (self._type not in lstTestLog):
      return
    if (message is None):
      return
    if (isinstance(message, str) and (len(message) == 0)):
      return
    elif ((isinstance(message, list)) and (len(message) == 0)):
      return
    elif ((isinstance(message, dict)) and (len(message) == 0)):
      return

    if (self._type == testLogTypeJson):
      self._log_Json(message)
    elif (self._type == testLogTypeCsv):
      self._log_Csv(message)
    elif (self._type == testLogTypeSqlite):
      self._log_Sqlite(message)
    elif (self._type == testLogTypeSqlite):
      self._log_Sqlite(message)
    elif (self._type == testLogTypeMysql):
      self._log_Mysql(message)
    else:
      msg = "UNSUPPORTED log type: {}".format(self._type)
      logger.error(msg)
      raise RuntimeError(msg)

  #def chgLoggerPath(self, fPath):
  #  self._logPath = fPath

  #def copyLogger(self, bMove=False):
  #  if(self._logPath is None):
  #    return

  #  logger.info("generated on {}".format(pyauto_base.misc.getTimestamp()))
  #  if(bMove):
  #    pyauto_base.misc.moveLoggerFile(self._logPath)
  #  else:
  #    pyauto_base.misc.copyLoggerFile(self._logPath)

  def saveResults(self, fPath: typing.Optional[str] = None) -> None:
    if (fPath is None):
      self._params["resPath"] = "{}_results.json".format(_getMainAppName())
    else:
      self._params["resPath"] = fPath

    if (len(self._results) > 0):
      with open(self._params["resPath"], "w") as fOut:
        fOut.write(json.dumps(self._results, indent=2))

class TestSequence(object):

  def __init__(self):
    self._id: typing.Optional[str] = None
    self.lstTestInfo: list[TestInfo] = []
    self.params = {}  # TODO: fix typing hints

  def __str__(self):
    return "{}: ({})".format(self.__class__.__name__, self._id)

  @property
  def id(self) -> typing.Optional[str]:
    return self._id

  @id.setter
  def id(self, val: str) -> None:
    if (pyauto_base.misc.isEmptyString(val)):
      logger.warning("CANNOT assign empty id")
      return
    self._id = val

  def showInfo(self) -> None:
    for tInfo in self.lstTestInfo:
      logger.info("{: <24} [{: <6}] start: {}, run: {}".format(tInfo.title, tInfo.status,
                                                               tInfo.startTime,
                                                               tInfo.runTime))

class TestContext(object):

  def __init__(self, strId="tContext_0"):
    self._id: typing.Optional[str] = strId
    #self.paramsTestInfo = None

  def __str__(self):
    return "{} {}".format(self.__class__.__name__, self._id)

class ContextTestCase(unittest.TestCase):

  def __init__(self, methodName="runTest", TC=None):
    super(ContextTestCase, self).__init__(methodName)
    self.TC = TC

def _getAllTests(tSuite):
  dictRes = {}

  for t in tSuite:
    if (isinstance(t, unittest.TestSuite)):
      if (t.countTestCases() > 0):
        dictRes.update(_getAllTests(t))
    else:
      testId = t.id().split(".")[-1]
      if (not testId.startswith("test_")):
        msg = "UNEXPECTED test id: {}".format(testId)
        logger.error(msg)
        raise RuntimeError(msg)
      dictRes[testId[5:]] = t

  return dictRes

class TestExecutive(object):

  def __init__(self):
    self.paths: list[str] = []
    self.TC = TestContext()
    self.testSeq: typing.Optional[TestSequence] = None
    self.testStatus: str = testStatusNotRun
    self._dictTests = {}  # all TestCases available in the path

  def parseStatus(self) -> None:
    """
    Assume that the tests are run, and determine the sequence status based on individual test status.
    """
    if (self.testStatus != testStatusRun):
      msg = "UNEXPECTED {} status: {}".format(self.__class__.__name__, self.testStatus)
      logger.error(msg)
      raise RuntimeError(msg)
    if (self.testSeq is None):
      return

    nPass = 0
    nSkip = 0
    nError = 0
    nFail = 0
    for tInfo in self.testSeq.lstTestInfo:
      if (tInfo.status == testStatusPass):
        nPass += 1
      elif (tInfo.status == testStatusSkip):
        nSkip += 1
      elif (tInfo.status == testStatusError):
        nError += 1
      elif (tInfo.status == testStatusFail):
        nFail += 1
    if (nError > 0):
      self.testStatus = testStatusError
    elif (nFail > 0):
      self.testStatus = testStatusFail
    elif (nSkip == len(self.testSeq.lstTestInfo)):
      self.testStatus = testStatusSkip
    elif ((nPass + nSkip) == len(self.testSeq.lstTestInfo)):
      self.testStatus = testStatusPass
    else:
      self.testStatus = testStatusError
    logger.info("{} status: {}".format(self.__class__.__name__, self.testStatus))

  def discover(self) -> None:
    self._dictTests = {}

    for path in self.paths:
      if (os.path.isfile(path)):
        tSuite = unittest.TestLoader().discover(os.path.dirname(path),
                                                pattern=os.path.basename(path))
        self._dictTests.update(_getAllTests(tSuite))
      elif (os.path.isdir(path)):
        tSuite = unittest.TestLoader().discover(os.path.dirname(path))
        self._dictTests.update(_getAllTests(tSuite))
      else:
        msg = "UNRECOGNIZED path: {}".format(path)
        logger.error(msg)
        raise RuntimeError(msg)
    logger.info("found {} Test Cases".format(len(self._dictTests)))

    if (self.testSeq is None):
      return

    for tInfo in self.testSeq.lstTestInfo:
      try:
        self._dictTests[tInfo.id]
      except KeyError:
        msg = "CANNOT find Test Case id: '{}' in path".format(tInfo.id)
        logger.error(msg)
        raise RuntimeError(msg)

  def run(self) -> None:
    #self.discover()

    self.testStatus = testStatusRun

    if (self.testSeq is None):
      return
    for tInfo in self.testSeq.lstTestInfo:
      tInfo.setStatusNotRun()

    for tInfo in self.testSeq.lstTestInfo:
      tc = self._dictTests[tInfo.id]
      tc.TC = self.TC
      tInfo.setStatusRun()
      res = unittest.TextTestRunner().run(tc)
      if (len(res.errors) == 1):
        tInfo.setStatusError()
      elif (len(res.failures) == 1):
        tInfo.setStatusFail()
      elif (len(res.skipped) == 1):
        tInfo.setStatusSkip()
      else:
        tInfo.setStatusPass()

    self.testSeq.showInfo()
    self.parseStatus()

class TestConfig(pyauto_base.config.SimpleConfig):

  def __init__(self):
    super(TestConfig, self).__init__()
    self.lstTestSeq = []

  def _parseXml_TestCase(self, elXml: ET.Element) -> TestInfo:
    tInfo = TestInfo()
    pyauto_base.misc.chkXMLElementHasAttr(elXml, "title", bDie=True)
    pyauto_base.misc.chkXMLElementHasAttrWithVal(elXml, "title", bDie=True)
    tInfo.title = elXml.get("title")

    if (pyauto_base.misc.chkXMLElementHasAttr(elXml, "id")):
      if (pyauto_base.misc.chkXMLElementHasAttrWithVal(elXml, "id")):
        tInfo.id = elXml.get("id")
    if (pyauto_base.misc.chkXMLElementHasAttr(elXml, "duration", bWarn=False)):
      if (pyauto_base.misc.chkXMLElementHasAttrWithVal(elXml, "duration", bWarn=False)):
        tpl = string.Template(str(elXml.get("duration")))
        tInfo.estimatedTime = tpl.substitute(**self.vars)
    if (pyauto_base.misc.chkXMLElementHasAttr(elXml, "manual", bWarn=False)):
      if (pyauto_base.misc.chkXMLElementHasAttrWithVal(elXml, "manual", bWarn=False)):
        tInfo.params["manual"] = True
    return tInfo

  def _parseXml_TestCaseLoop(self, elXml: ET.Element) -> list[TestInfo]:
    lstLoop = None
    if (elXml.tag == "TestSequence"):  # TestSequence is a special TestCaseLoop
      lstLoop = []
    elif (elXml.tag != "TestCaseLoop"):
      msg = "CANNOT handle <{}> as <TestCaseLoop>".format(elXml.tag)
      logger.error(msg)
      raise RuntimeError(msg)

    if (pyauto_base.misc.chkXMLElementHasAttr(elXml, "count", bWarn=False)):
      if (pyauto_base.misc.chkXMLElementHasAttrWithVal(elXml, "count", bWarn=False)):
        lstLoop = [t for t in range(int(str(elXml.get("count"))))]
    if (pyauto_base.misc.chkXMLElementHasAttr(elXml, "list", bWarn=False)):
      if (pyauto_base.misc.chkXMLElementHasAttrWithVal(elXml, "list", bWarn=False)):
        #logger.info(elXml.get("list"))
        tpl = string.Template(str(elXml.get("list")))
        lstLoop = json.loads(tpl.substitute(**self.vars))
    if ((lstLoop is None) or (not isinstance(lstLoop, list))):
      lstLoop = [0]
      logger.warning("loop count SET to 1 for <{}>".format(elXml.tag))
    #logger.info("{}, lstLoop = {}".format(elXml, lstLoop))

    lstTInfo = []
    for el in elXml:
      if (el.tag == "TestCase"):
        tmp1 = self._parseXml_TestCase(el)
        if (tmp1 is not None):
          lstTInfo += [tmp1]
      elif (el.tag == "TestCaseLoop"):
        lstTInfo += self._parseXml_TestCaseLoop(el)
      else:
        logger.warning("UNSUPPORTED <{}>".format(el.tag))

    #logger.info("TestCaseLoop, lstLoop = {}".format(lstLoop))
    lstRes = []
    if (len(lstLoop) == 0):
      return lstTInfo
    else:
      for lp in lstLoop:
        for tInfo in lstTInfo:
          if (tInfo is None):
            continue
          tmp = copy.deepcopy(tInfo)
          #tmp.title += "" if(t == "") else " ({})".format(t)
          if ("loop" not in tmp.params):
            tmp.params["loop"] = []  # type: ignore
          tmp.params["loop"].append(lp)  # type: ignore
          lstRes.append(tmp)
    #for tInfo in lstRes:
    #  logger.info("{}, {}".format(tInfo.title, tInfo.params))
    return lstRes

  def _parseXml_testSequence(self, elXml: ET.Element) -> None:
    self.lstTestSeq = []

    for elSeq in elXml.findall("TestSequence"):
      tSeq = TestSequence()
      if (pyauto_base.misc.chkXMLElementHasAttr(elSeq, "id")):
        tSeq.id = elSeq.get("id")
      if (pyauto_base.misc.chkXMLElementHasAttr(elSeq, "defaultDuration", bWarn=False)):
        if (pyauto_base.misc.chkXMLElementHasAttrWithVal(elSeq,
                                                         "defaultDuration",
                                                         bWarn=False)):
          tSeq.params["defaultDuration"] = elSeq.get("defaultDuration")
      #logger.info(tSeq)

      lstTInfo = self._parseXml_TestCaseLoop(elSeq)

      for tInfo in lstTInfo:
        if (tInfo is None):
          continue
        if (tInfo.estimatedTime is None):
          if ("defaultDuration" in tSeq.params.keys()):
            tInfo.estimatedTime = tSeq.params["defaultDuration"]
        #logger.info(tInfo)
        tSeq.lstTestInfo.append(tInfo)

      if (len(tSeq.lstTestInfo) > 0):
        self.lstTestSeq.append(tSeq)

  def parseXml(self) -> None:
    if (self._domObj is None):
      raise RuntimeError

    super(TestConfig, self).parseXml()
    self._parseXml_testSequence(self._domObj.getroot())

  def _showInfo_TestSequence(self) -> None:
    if (len(self.lstTestSeq) > 0):
      logger.info("test sequence(s):")
      for tSeq in self.lstTestSeq:
        logger.info("  {}".format(tSeq))
        if (len(tSeq.lstTestInfo) > 0):
          logger.info("  test cases:")
          for tInfo in tSeq.lstTestInfo:
            logger.info("    {}".format(tInfo))

  def showInfo(self) -> None:
    super(TestConfig, self).showInfo()
    self._showInfo_TestSequence()
