#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

from __future__ import annotations  # for python 3.8
#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
import csv
import typing
import json

try:
  import numpy
  #import scipy
  import matplotlib
  if (not sys.platform.startswith("win")):
    if ("DISPLAY" not in os.environ):
      matplotlib.use("Agg")
  import matplotlib.pyplot
  import matplotlib.dates
except ImportError as ex:
  raise RuntimeError(ex)

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_base.fs

imgdpi = 200
imgres = {
    "HD_1K": [1920.0, 1080.0],
    "HD_2K": [2048.0, 1152.0],
    "UXGA": [1600.0, 1200.0],
    "WUXGA": [1920.0, 1200.0],
    "WQXGA": [2560.0, 1600.0],
}

def _matplotlib_ver() -> list[int]:
  return [int(t) for t in matplotlib.__version__.split(".")]

def adjustFloat(val: float, adj: str = "+") -> float:
  """
  If val has too many digits return the ceiling/floor of the value with only 2 significant digits.
  - if adj == "+" return math.ceil()
  - if adj == "-" returns math.floor()
  - else returns val
  """
  if (adj not in ("+", "-")):
    return val

  bNeg = False
  if (val < 0.0):
    bNeg = True
    val = -1.0 * val

  n_digits = 2
  val_log = int(math.log10(val))
  #logger.info("val: {} / {}".format(val, val))
  if (val_log < 0):
    log_diff = (val_log - n_digits - 1)
  elif ((val_log == 0) and (val < 1.0)):
    log_diff = -1 - n_digits
  elif ((val_log == 0) and (val >= 1.0)):
    log_diff = 0 - n_digits
  else:
    log_diff = (val_log - n_digits)
  tmp = val / (10**log_diff)
  #print("DBG", tmp - int(tmp))
  if ((tmp - int(tmp)) > 1e-12):
    if (adj == "+"):
      val_adj: float = math.ceil(tmp) * (10**log_diff)
    else:
      val_adj = math.floor(tmp) * (10**log_diff)
    logger.warning("value adjusted from {:g} to {:g}".format(val, val_adj))
    return (-1.0 * val_adj) if (bNeg) else val_adj

  return (-1.0 * val) if (bNeg) else val

def jsonl2tsv(fPath: str) -> None:
  pyauto_base.fs.chkPath_File(fPath)

  fOutPath = ".".join(fPath.split(".")[:-1] + ["tsv"])
  col_names: list[str] = []
  with open(fPath, "r") as fIn:
    with open(fOutPath, "w") as fOut:
      csvOut = csv.writer(fOut, delimiter="\t", lineterminator="\n")

      for ln in fIn.readlines():
        rec = []
        lstVal = json.loads(ln.strip("\r\n"))

        ts = [d["time"] for d in lstVal]
        ts = list(set(ts))
        if (len(ts) == 1):
          rec.append(ts[0])
        else:
          raise NotImplementedError

        if (col_names == []):
          if (len(ts) == 1):
            col_names.append("time")
          col_names += ["{} [{}]".format(d["name"], d["unit"]) for d in lstVal]
          csvOut.writerow(col_names)

        rec += [d["value"] for d in lstVal]
        csvOut.writerow(rec)
  logger.info("{} written".format(fOutPath))

class PlotData(object):

  def __init__(self):
    self.x: typing.Union[list[int], list[float]] = []  # type: ignore
    self.y: typing.Union[list[int], list[float]] = []  # type: ignore
    self._params: dict[str, typing.Union[None, float, str]] = {
        "legend": "",
        "xunit": "",
        "yunit": "",
        "xmin": None,
        "xmax": None,
        "ymin": None,
        "ymax": None
    }

  def __str__(self):
    return "{}: {},{} [{},{}]".format(self.__class__.__name__, self.x if
                                      (self.x is None) else len(self.x), self.y if
                                      (self.y is None) else len(self.y), self.xunit,
                                      self.yunit)

  def clear(self):
    self.x = []  # type: ignore
    self.y = []  # type: ignore
    self._params = {
        "legend": "",
        "xunit": "",
        "yunit": "",
        "xmin": None,
        "xmax": None,
        "ymin": None,
        "ymax": None
    }

  def limits(self) -> None:
    """
    determines data min/max values
    :return:
    """
    if ((self._params["xmin"] is not None) and (self._params["xmax"] is not None)):
      return
    if ((self._params["ymin"] is not None) and (self._params["ymax"] is not None)):
      return

    if ((self.x is None) or (len(self.x) == 0)):
      logger.warning("no x data: CANNOT determine limits")
      return
    if ((self.y is None) or (len(self.y) == 0)):
      logger.warning("no y data: CANNOT determine limits")
      return

    if (isinstance(self.x[0], int)):
      self._params["xmin"] = min(self.x)
      self._params["xmax"] = max(self.x)
    elif (isinstance(self.x[0], float)):
      self._params["xmin"] = min(self.x)
      self._params["xmax"] = max(self.x)
    else:
      self._params["xmin"] = numpy.min(self.x)
      self._params["xmax"] = numpy.max(self.x)
      #msg = "UNSUPPORTED x data: '{}'".format(type(self.x[0]))
      #logger.error(msg)
      #raise RuntimeError(msg)

    if (isinstance(self.y[0], int)):
      self._params["ymin"] = min(self.y)
      self._params["ymax"] = max(self.y)
    elif (isinstance(self.y[0], float)):
      self._params["ymin"] = min(self.y)
      self._params["ymax"] = max(self.y)
    else:
      self._params["ymin"] = numpy.min(self.y)
      self._params["ymax"] = numpy.max(self.y)
      #msg = "UNSUPPORTED y data: '{}'".format(type(self.y[0]))
      #logger.error(msg)
      #raise RuntimeError(msg)

  @property
  def legend(self) -> str:
    return self._params["legend"]  # type: ignore

  @legend.setter
  def legend(self, val: str) -> None:
    if (pyauto_base.misc.isEmptyString(val)):
      logger.warning("CANNOT assign empty legend")
      return
    self._params["legend"] = val

  @property
  def xunit(self) -> str:
    return self._params["xunit"]  # type: ignore

  @xunit.setter
  def xunit(self, val: str) -> None:
    if (pyauto_base.misc.isEmptyString(val)):
      logger.warning("CANNOT assign empty xunit")
      return
    self._params["xunit"] = val

  @property
  def yunit(self) -> str:
    return self._params["yunit"]  # type: ignore

  @yunit.setter
  def yunit(self, val: str) -> None:
    if (pyauto_base.misc.isEmptyString(val)):
      logger.warning("CANNOT assign empty yunit")
      return
    self._params["yunit"] = val

  @property
  def xmin(self) -> float:
    self.limits()
    return self._params["xmin"]  # type: ignore

  @property
  def xmax(self) -> float:
    self.limits()
    return self._params["xmax"]  # type: ignore

  @property
  def ymin(self) -> float:
    self.limits()
    return self._params["ymin"]  # type: ignore

  @property
  def ymax(self) -> float:
    self.limits()
    return self._params["ymax"]  # type: ignore

  @property
  def xlabel(self) -> str:
    res = "[{}]".format(self.xunit)
    if (res == "[]"):
      return ""
    else:
      return res

  @property
  def ylabel(self) -> str:
    res = "[{}]".format(self.yunit)
    if (res == "[]"):
      return ""
    else:
      return res

  def setXY(self,
            xval: typing.Union[list[int], list[float]],
            yval: typing.Union[list[int], list[float]],
            xunit: str = "",
            yunit: str = "",
            legend: str = "") -> None:
    if (len(xval) != len(yval)):
      msg = "MISMATCH x and y lengths: {} != {}".format(len(xval), len(yval))
      logger.error(msg)
      raise RuntimeError(msg)

    self.clear()
    self.x = xval
    self.y = yval
    self.xunit = xunit
    self.yunit = yunit
    self.legend = legend
    self._params["xmin"] = min(xval)
    self._params["xmax"] = max(xval)
    self._params["ymin"] = min(yval)
    self._params["ymax"] = max(yval)

  def setX_min_max(self, np: int, xmin: typing.Union[int, float],
                   xmax: typing.Union[int, float], unit: str) -> None:
    """
    set x data as 'np' equally spaced values between 'xmin' and 'xmax'.
    """
    if ((not isinstance(np, int)) and (np < 1)):
      msg = "INCORRECT np: {}".format(np)
      logger.error(msg)
      raise RuntimeError(msg)

    xmin = pyauto_base.misc.chkFloat(xmin, valId="xmin")
    xmax = pyauto_base.misc.chkFloat(xmax, valId="xmax")

    self._params["xmin"] = xmin
    self._params["xmax"] = xmax
    self.xunit = unit
    step = (self._params["xmax"] - self._params["xmin"]) / (np - 1)  # type: ignore
    self.x = [(self._params["xmin"] + (t * step)) for t in range(np)]  # type: ignore

  def setX_center_span(self, np: int, center: typing.Union[int, float],
                       span: typing.Union[int, float], unit: str):
    """
    set x data as 'np' equally spaced values between ('center' - 'span') and ('center' + 'span').
    :param np:
    :param center:
    :param span:
    :param unit:
    :return:
    """
    if ((not isinstance(np, int)) and (np < 1)):
      msg = "INCORRECT np: {}".format(np)
      logger.error(msg)
      raise RuntimeError(msg)

    center = pyauto_base.misc.chkFloat(center, valId="center")
    span = pyauto_base.misc.chkFloat(span, valId="span")

    self._params["xmin"] = center - span
    self._params["xmax"] = center + span
    self.xunit = unit
    step = (self._params["xmax"] - self._params["xmin"]) / (np - 1)  # type: ignore
    self.x = [(self._params["xmin"] + (t * step)) for t in range(np)]  # type: ignore

  def setY_min_max(self, yval: typing.Union[list[int], list[float]],
                   ymin: typing.Union[int, float], ymax: typing.Union[int,
                                                                      float], unit: str):
    """
    :param yval:
    :param ymin:
    :param ymax:
    :param unit:
    :return:
    """
    if (len(yval) != len(self.x)):
      msg = "MISMATCH data points: {} != {}".format(len(yval), len(self.x))
      logger.error(msg)
      raise RuntimeError(msg)

    ymin = pyauto_base.misc.chkFloat(ymin, valId="xmin")
    ymax = pyauto_base.misc.chkFloat(ymax, valId="xmax")

    self._params["ymin"] = ymin
    self._params["ymax"] = ymax
    self.yunit = unit
    self.y = yval

  def setY_center_span(self, yval: typing.Union[list[int], list[float]],
                       center: typing.Union[int, float], span: typing.Union[int, float],
                       unit: str):
    """
    :param yval:
    :param center:
    :param span:
    :param unit:
    :return:
    """
    if (len(yval) != len(self.x)):
      msg = "MISMATCH data points: {} != {}".format(len(yval), len(self.x))
      logger.error(msg)
      raise RuntimeError(msg)

    center = pyauto_base.misc.chkFloat(center, valId="center")
    span = pyauto_base.misc.chkFloat(span, valId="span")

    self._params["ymin"] = center - span
    self._params["ymax"] = center + span
    self.yunit = unit
    self.y = yval

  def write(self, fPath: str) -> None:
    if (len(self.x) == 0):
      logger.warning("{} NOTHING to write".format(self.__class__.__name__))
      return

    with open(fPath, "w") as fOut:
      csvOut = csv.writer(fOut, lineterminator="\n")
      if ((self.xunit != "") and (self.yunit != "")):
        csvOut.writerow(["[{}]".format(self.xunit), "[{}]".format(self.yunit)])
      for x, y in zip(self.x, self.y):
        csvOut.writerow([x, y])

class BasePlot(object):
  """
  :var data: list of PlotData
  :var fPath: path to save image
  :var title: plot title
  :var _[xy]lim: overwrite plot limits
  :var _[xy]label: overwrite plot label
  """

  def __init__(self):
    self.data: list[PlotData] = []
    self.fPath: str = ""
    self.title: str = ""
    self._xlim = []
    self._ylim = []
    self._xlabel: str = ""
    self._ylabel: str = ""
    self._hline: list[list[typing.Union[float, str]]] = []
    self._vline: list[list[typing.Union[float, str]]] = []
    self._marker: list[list[typing.Union[float, str]]] = []
    self._legend: bool = False
    self._axes = {
        "scale": None,
        "grid": "maj",
        "minticks": False,
        "xticks": None,
        "yticks": None
    }
    self._legend_opts: dict[str, typing.Union[str, int]] = {
        "loc": "best",
        "fontsize": "small"
    }
    self._lbl_opts = {"fontsize": "small"}
    self._plot_opts = {"zorder": 1}
    self._marker_opts = {"zorder": 2, "marker": "d", "color": "m"}
    self._hline_opts = {"color": "#FF0000", "linewidth": 2}
    self._vline_opts = {"color": "#FFA500", "linewidth": 2}
    self._font_opts: dict[str, typing.Union[str, int]] = {"fontsize": "small"}
    #self._extra_artists = []

  def __str__(self):
    return "{}: {:d} plot(s)".format(self.__class__.__name__, len(self.data))

  def clear(self):
    self.data = []
    self.fPath = ""
    self.title = ""
    self._xlim = []
    self._ylim = []
    self._xlabel = ""
    self._ylabel = ""
    self._hline = []
    self._vline = []
    self._marker = []
    self._legend = False
    self._axes = {
        "scale": None,
        "grid": "maj",
        "minticks": False,
        "xticks": None,
        "yticks": None
    }
    self._legend_opts = {"loc": "best", "fontsize": 10}
    self._plot_opts = {"zorder": 1}
    self._marker_opts = {"zorder": 2, "marker": "d", "color": "m"}
    self._hline_opts = {"color": "#FF0000", "linewidth": 2}
    self._vline_opts = {"color": "#FFA500", "linewidth": 2}
    self._font_opts = {"fontsize": 10}

  @property
  def legend(self) -> bool:
    return self._legend

  @legend.setter
  def legend(self, val: bool) -> None:
    if (val in (True, False)):
      self._legend = val
    else:
      logger.warning("INCORRECT legend: {}".format(val))

  @property
  def legend_loc(self) -> str:
    return self._legend_opts["loc"]  # type: ignore

  @legend_loc.setter
  def legend_loc(self, val: str) -> None:
    if (val in ("best", "ur", "cr", "lr", "ul", "cl", "ll")):
      self._legend_opts["loc"] = val
    else:
      logger.warning("INCORRECT legend location: {}".format(val))

  @property
  def ax_minticks(self) -> bool:
    return self._axes["minticks"]  # type: ignore

  @ax_minticks.setter
  def ax_minticks(self, val: bool) -> None:
    if (val in (True, False)):
      self._axes["minticks"] = val
    else:
      logger.warning("INCORRECT axes minticks: {}".format(val))

  @property
  def ax_grid(self) -> str:
    return self._axes["grid"]  # type: ignore

  @ax_grid.setter
  def ax_grid(self, val: str) -> None:
    if (val in (None, "min", "maj", "both")):
      self._axes["grid"] = val
    else:
      logger.warning("INCORRECT axes grid: {}".format(val))

  @property
  def ax_scale(self) -> str:
    return self._axes["scale"]  # type: ignore

  @ax_scale.setter
  def ax_scale(self, val: str) -> None:
    if (val in (None, "log_x", "log_y", "log_both")):
      self._axes["scale"] = val
    else:
      logger.warning("INCORRECT axes scale: {}".format(val))

  @property
  def ax_xticks(self) -> str:
    return self._axes["xticks"]  # type: ignore

  @ax_xticks.setter
  def ax_xticks(self, val: str) -> None:
    if (pyauto_base.misc.isEmptyString(val)):
      logger.warning("CANNOT assign empty axes xticks")
      return
    self._axes["xticks"] = val

  @property
  def ax_yticks(self) -> str:
    return self._axes["yticks"]  # type: ignore

  @ax_yticks.setter
  def ax_yticks(self, val: str) -> None:
    if (pyauto_base.misc.isEmptyString(val)):
      logger.warning("CANNOT assign empty axes yticks")
      return
    self._axes["yticks"] = val

  @property
  def xlabel(self) -> str:
    if (not pyauto_base.misc.isEmptyString(self._xlabel)):
      return self._xlabel

    lstPDlbl = []
    for pd in self.data:
      lstPDlbl.append(pd.xlabel)
    lstPDlbl = [t for t in lstPDlbl if (t != "")]
    if (len(lstPDlbl) == 0):
      return ""
    elif (len(lstPDlbl) != len(self.data)):
      logger.warning("some PlotData have no xlabels, CANNOT auto-generate")
      return ""
    else:
      tmp = list(set(lstPDlbl))
      if (len(tmp) == 1):
        return tmp[0]
      else:
        return ",".join(lstPDlbl)

  @xlabel.setter
  def xlabel(self, val: str) -> None:
    if (pyauto_base.misc.isEmptyString(val)):
      logger.warning("CANNOT assign empty xlabel")
      return
    self._xlabel = val

  @property
  def ylabel(self) -> str:
    if (not pyauto_base.misc.isEmptyString(self._ylabel)):
      return self._ylabel

    lstPDlbl = []
    for pd in self.data:
      lstPDlbl.append(pd.ylabel)
    lstPDlbl = [t for t in lstPDlbl if (t != "")]
    if (len(lstPDlbl) == 0):
      return ""
    elif (len(lstPDlbl) != len(self.data)):
      logger.warning("some PlotData have no ylabels, CANNOT auto-generate")
      return ""
    else:
      tmp = list(set(lstPDlbl))
      if (len(tmp) == 1):
        return tmp[0]
      else:
        return ",".join(lstPDlbl)

  @ylabel.setter
  def ylabel(self, val: str) -> None:
    if (pyauto_base.misc.isEmptyString(val)):
      logger.warning("CANNOT assign empty ylabel")
      return
    self._ylabel = val

  def clrHline(self) -> None:
    self._hline = []

  def clrVline(self) -> None:
    self._vline = []

  def addHline(self, xVal: float, legend: str = "") -> None:
    xVal = pyauto_base.misc.chkFloat(xVal, valId="xVal")

    self._hline.append([xVal, legend])

  def addVline(self, yVal: float, legend: str = "") -> None:
    yVal = pyauto_base.misc.chkFloat(yVal, valId="yVal")

    self._vline.append([yVal, legend])

  def addMarker(self, xVal: float, yVal: float, xunit: str = "", yunit: str = "") -> None:
    xVal = pyauto_base.misc.chkFloat(xVal, valId="xVal")
    yVal = pyauto_base.misc.chkFloat(yVal, valId="yVal")

    self._marker.append([xVal, yVal, "{:e}{}\n{}{}".format(xVal, xunit, yVal, yunit)])

  def limits(self, xExtra: float = 0.0, yExtra: float = 0.0) -> None:
    """
    determine plot limits. If self._[xy]lim is already set use them, if not go though the plot data
    and determine the overall limits.
    :param xExtra: extra percentage added to the x limits
    :param yExtra: extra percentage added to the y limits
    :return:
    """
    dataSampleType: list[typing.Union[int, float, datetime.datetime]] = []
    for pd in self.data:
      if (isinstance(pd.x[0], int)):
        dataSampleType.append(0)
      elif (isinstance(pd.x[0], float)):
        dataSampleType.append(0.0)
      elif (isinstance(pd.x[0], datetime.datetime)):
        dataSampleType.append(datetime.datetime(2000, 1, 1))
      else:
        msg = "UNEXPECTED type for x data: {}".format(type(pd.x[0]))
        logger.error(msg)
        raise RuntimeError(msg)

    dataSampleType = list(set(dataSampleType))
    if (len(dataSampleType) != 1):
      msg = "MULTIPLE types for x data: {}".format(dataSampleType)
      logger.error(msg)
      raise RuntimeError(msg)

    if ((self._xlim is None) or (len(self._xlim) != 2)):
      self._xlim = [None, None]
      for pd in self.data:
        pd.limits()
        if (self._xlim[0] is None):
          self._xlim[0] = pd.xmin
        elif (pd.xmin < self._xlim[0]):
          self._xlim[0] = pd.xmin
        if (self._xlim[1] is None):
          self._xlim[1] = pd.xmax
        elif (pd.xmax > self._xlim[1]):
          self._xlim[1] = pd.xmax
      if (not isinstance(dataSampleType[0], datetime.datetime)):
        xrange = math.fabs(self._xlim[1] - self._xlim[0])
        self._xlim = [self._xlim[0] - (xExtra * xrange), self._xlim[1] + (xExtra * xrange)]

    if ((self._ylim is None) or (len(self._ylim) != 2)):
      self._ylim = [float("inf"), float("-inf")]
      for pd in self.data:
        pd.limits()
        if (pd.ymin < self._ylim[0]):
          self._ylim[0] = pd.ymin
        if (pd.ymax > self._ylim[1]):
          self._ylim[1] = pd.ymax
      yrange = math.fabs(self._ylim[1] - self._ylim[0])
      self._ylim = [self._ylim[0] - (yExtra * yrange), self._ylim[1] + (yExtra * yrange)]

  def _draw_labels(self) -> None:
    matplotlib.pyplot.xlabel(self.xlabel)
    matplotlib.pyplot.ylabel(self.ylabel)

  def _draw_axes_scale(self):
    if (self._axes["scale"] == "log_x"):
      matplotlib.pyplot.xscale("log")
      matplotlib.pyplot.yscale("linear")
    elif (self._axes["scale"] == "log_y"):
      matplotlib.pyplot.xscale("linear")
      matplotlib.pyplot.yscale("log")
    elif (self._axes["scale"] == "log_both"):
      matplotlib.pyplot.xscale("log")
      matplotlib.pyplot.yscale("log")
    else:
      matplotlib.pyplot.xscale("linear")
      matplotlib.pyplot.yscale("linear")

  def _draw_axes_ticks(self) -> None:
    ax = matplotlib.pyplot.gca()
    if (self._axes["xticks"] == "timestamp"):
      ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%Y-%m-%d\n%H:%M:%S'))
      matplotlib.pyplot.xticks(rotation=45)
      #allow timestamp ticks to fit
      box = ax.get_position()
      ax.set_position(
          [box.x0, (box.y0 + (0.1 * box.height)), box.width, (0.9 * box.height)])
    if (self._axes["xticks"] == "rotate"):
      matplotlib.pyplot.xticks(rotation=45)
      '''
      matplotlib.pyplot.axvspan(datetime.datetime(2014, 1, 13, 12, 8, 0),
                                datetime.datetime(2014, 1, 13, 12, 41, 0),
                                facecolor="cyan", alpha=0.5)

      matplotlib.pyplot.axvspan(datetime.datetime(2014, 1, 13, 13, 55, 0),
                                datetime.datetime(2014, 1, 13, 14, 28, 0),
                                facecolor="yellow", alpha=0.5)

      matplotlib.pyplot.axvspan(datetime.datetime(2014, 1, 13, 14, 37, 0),
                                datetime.datetime(2014, 1, 13, 15, 10, 0),
                                facecolor="yellow", alpha=0.5)

      matplotlib.pyplot.axvspan(datetime.datetime(2014, 1, 13, 15, 33, 0),
                                datetime.datetime(2014, 1, 13, 16, 6, 0),
                                facecolor="orange", alpha=0.5)

      matplotlib.pyplot.axvspan(datetime.datetime(2014, 1, 14, 9, 4, 0),
                                datetime.datetime(2014, 1, 14, 9, 36, 0),
                                facecolor="yellow", alpha=0.5)

      matplotlib.pyplot.axvspan(datetime.datetime(2014, 1, 14, 9, 37, 0),
                                datetime.datetime(2014, 1, 14, 10, 10, 0),
                                facecolor="yellow", alpha=0.5)
      '''

    # enable minticks
    if (self._axes["minticks"]):
      matplotlib.pyplot.minorticks_on()
    else:
      matplotlib.pyplot.minorticks_off()

  def _draw_axes_grid(self) -> None:
    if (self._axes["grid"] == "min"):
      matplotlib.pyplot.grid(True, which="minor", axis="both")
    elif (self._axes["grid"] == "maj"):
      matplotlib.pyplot.grid(True, which="major", axis="both")
    elif (self._axes["grid"] == "both"):
      matplotlib.pyplot.grid(True, which="both", axis="both")
    else:
      matplotlib.pyplot.grid(False)

  def _draw_title(self) -> None:
    if (self.title is not None):
      matplotlib.pyplot.title(self.title)

  def _draw_hlines(self) -> None:
    for hline in self._hline:
      if ((hline[0] < self._ylim[0]) or (hline[0] > self._ylim[1])):
        logger.warning("OUT OF BOUNDS hline {}".format(hline))
      if (hline[1] == ""):
        matplotlib.pyplot.axhline(hline[0], **self._hline_opts)
      else:
        self._legend = True
        matplotlib.pyplot.axhline(hline[0], label=hline[1], **self._hline_opts)

  def _draw_vlines(self):
    for vline in self._vline:
      if ((vline[0] < self._xlim[0]) or (vline[0] > self._xlim[1])):
        logger.warning("OUT OF BOUNDS vline {}".format(vline))
        continue
      if (vline[1] == ""):
        matplotlib.pyplot.axvline(vline[0], **self._vline_opts)
      else:
        self._legend = True
        matplotlib.pyplot.axvline(vline[0], label=vline[1], **self._vline_opts)

  def _draw_markers(self):
    for mkr in self._marker:
      if ((mkr[0] < self._xlim[0]) or (mkr[0] > self._xlim[1])):
        logger.warning("OUT OF x BOUNDS marker {}".format(mkr))
        continue
      if ((mkr[1] < self._ylim[0]) or (mkr[1] > self._ylim[1])):
        logger.warning("OUT OF y BOUNDS marker {}".format(mkr))
        continue
      self._legend = True
      matplotlib.pyplot.scatter(x=mkr[0], y=mkr[1], label=mkr[2], **self._marker_opts)

  def _draw_legend(self) -> None:
    if (self._legend):
      matplotlib.pyplot.legend(**self._legend_opts)

  def _draw(self) -> bool:
    raise NotImplementedError

  def show(self) -> None:
    if (self._draw()):
      matplotlib.pyplot.show()

  def write(self, fPath: str = "", xtnd_x: float = 1.0, xtnd_y: float = 1.0) -> None:
    if (pyauto_base.misc.isEmptyString(fPath)):
      fPath = self.fPath
    if (pyauto_base.misc.isEmptyString(fPath)):
      msg = "CANNOT write to empty path"
      logger.error(msg)
      raise RuntimeError(msg)

    if (self._draw()):
      matplotlib.pyplot.gcf().set_size_inches(xtnd_x * (imgres["HD_1K"][0] / imgdpi),
                                              xtnd_y * (imgres["HD_1K"][1] / imgdpi))

      matplotlib.pyplot.savefig(fPath, dpi=imgdpi, bbox_inches="tight")
      logger.info("plot image generated: {}".format(fPath))

class SimplePlot(BasePlot):

  def addData(self,
              xVal: typing.Union[list[int], list[float]],
              yVal: typing.Union[list[int], list[float]],
              xunit: str = "",
              yunit: str = "",
              legend: str = "") -> None:
    if (len(xVal) == 0):
      msg = "X values have 0 length: {}".format(type(xVal))
      logger.error(msg)
      raise RuntimeError(msg)
    if (len(yVal) == 0):
      msg = "Y values have 0 length: {}".format(type(yVal))
      logger.error(msg)
      raise RuntimeError(msg)
    if (len(xVal) != len(yVal)):
      msg = "values have different length: {} != {}".format(type(xVal), len(yVal))
      logger.error(msg)
      raise RuntimeError(msg)

    pd = PlotData()
    pd.x = xVal
    pd.y = yVal
    pd.xunit = xunit
    pd.yunit = yunit
    pd.legend = legend
    self.data.append(pd)

  def _draw(self) -> bool:
    if (len(self.data) == 0):
      logger.warning("NOTHING to plot")
      return False

    matplotlib.pyplot.clf()

    self.limits(xExtra=0.05, yExtra=0.05)

    for pd in self.data:
      logger.info("{}, x=[{:g}, {:g}], y=[{:g}, {:g}]".format(str(pd), pd.xmin, pd.xmax,
                                                              pd.ymin, pd.ymax))
      if (pd.legend == ""):
        matplotlib.pyplot.plot(pd.x, pd.y, **self._plot_opts)
      else:
        matplotlib.pyplot.plot(pd.x, pd.y, label=pd.legend, **self._plot_opts)

    if (_matplotlib_ver()[0] > 2):
      matplotlib.pyplot.xlim(left=self._xlim[0], right=self._xlim[1])
      matplotlib.pyplot.ylim(bottom=self._ylim[0], top=self._ylim[1])
    else:
      matplotlib.pyplot.xlim(xmin=self._xlim[0], xmax=self._xlim[1])
      matplotlib.pyplot.ylim(ymin=self._ylim[0], ymax=self._ylim[1])

    self._draw_hlines()
    self._draw_vlines()
    self._draw_markers()

    self._draw_axes_scale()
    self._draw_axes_ticks()
    self._draw_axes_grid()
    self._draw_labels()
    self._draw_title()
    self._draw_legend()

    return True

class BoxPlot(BasePlot):

  def addData(self,
              yVal: typing.Union[list[int], list[float]],
              yunit: str = "",
              legend: str = "") -> None:
    if (len(yVal) == 0):
      msg = "Y values have 0 length: {}".format(type(yVal))
      logger.error(msg)
      raise RuntimeError(msg)

    pd = PlotData()
    pd.y = yVal
    pd.yunit = yunit
    pd.legend = legend
    self.data.append(pd)

  def _draw(self) -> bool:
    if (len(self.data) == 0):
      logger.warning("NOTHING to plot")
      return False

    matplotlib.pyplot.clf()

    labels = []
    data = []
    for pd in self.data:
      tmp = "{} {}".format(pd.legend, pd.ylabel)
      labels.append(tmp.strip())
      data.append(pd.y)
    matplotlib.pyplot.boxplot(data, tick_labels=labels)

    #self._draw_axes_scale()
    self._draw_axes_ticks()
    self._draw_axes_grid()
    #self._draw_labels()
    self._draw_title()

    return True

class GridPlot(BasePlot):

  def __init__(self, ndivX: int = 10, ndivY: int = 8):
    super(GridPlot, self).__init__()
    if (((ndivX % 2) != 0) and ((ndivY % 2) != 0)):
      msg = "INCORRECT number of divisions (must be even): {}x{}".format(ndivX, ndivY)
      logger.error(msg)
      raise RuntimeError(msg)

    self._ndiv = [ndivX, ndivY]
    self._xaxis: list = []  # TODO: fix typing hints
    self.txt_ul: typing.Optional[str] = None
    self.txt_ur: typing.Optional[str] = None
    self.txt_ll: typing.Optional[str] = None
    self.ts: typing.Optional[str] = None

  def clear(self) -> None:
    super(GridPlot, self).clear()
    self._xaxis = []
    self.txt_ul = None
    self.txt_ur = None
    self.txt_ll = None
    self.ts = None

  def setTime(self, center: float, scale: float) -> None:
    """
    :param center: value at the middle of x axis
    :param scale: increment/decrement per x axis division
    :return:
    """
    if ((len(self.data) > 0) and (len(self._xaxis) > 0)):
      msg = "CANNOT change X axis with existent data"
      logger.error(msg)
      raise RuntimeError(msg)

    center = pyauto_base.misc.chkFloat(center, valId="mid")
    scale = pyauto_base.misc.chkFloat(scale, valId="scale")
    if (scale < 0.0):
      msg = "X axis scale MUST BE positive: {}".format(scale)
      logger.error(msg)
      raise RuntimeError(msg)

    scale = adjustFloat(scale)
    self._xaxis = [
        "s", (center - (scale * self._ndiv[0] / 2)), (center + (scale * self._ndiv[0] / 2))
    ]

  def setFreq(self, f0: float, f1: float, mode: str) -> None:
    """
    :param f0: first freq (center or start)
    :param f1: second freq (span or stop)
    :param mode: [center_span|start_stop]
    :return:
    """
    if (mode not in ("center_span", "start_stop")):
      msg = "INCORRECT mode: {}".format(mode)
      logger.error(msg)
      raise RuntimeError(msg)
    if ((len(self.data) > 0) and (len(self._xaxis) > 0)):
      msg = "CANNOT change X axis with existent data"
      logger.error(msg)
      raise RuntimeError(msg)

    f0 = pyauto_base.misc.chkFloat(f0, valId="f0", minVal=0.0)
    f1 = pyauto_base.misc.chkFloat(f1, valId="f1")

    if (mode == "center_span"):
      #f1 = adjustFloat(f1)
      fmin = f0 - (f1 / 2)
      fmax = f0 + (f1 / 2)
      pyauto_base.misc.chkFloat(fmin, valId="fmin", minVal=0.0)
      self._xaxis = ["Hz", fmin, fmax]
    else:
      if (f0 >= f1):
        msg = "X axis INCORRECT limits: {} < {}".format(f0, f1)
        logger.error(msg)
        raise RuntimeError(msg)
      self._xaxis = ["Hz", f0, f1]

  def addData(self,
              yVal: typing.Union[list[int], list[float]],
              center: float,
              scale: float,
              unit: str,
              legend: str = "") -> None:
    """
    :param yVal: list of data
    :param center: middle of y axis
    :param scale: increment/decrement per y axis division
    :param unit:
    :param legend:
    :return:
    """
    if (len(yVal) == 0):
      msg = "values have 0 length: {}".format(type(yVal))
      logger.error(msg)
      raise RuntimeError(msg)
    if (len(self._xaxis) == 0):
      msg = "UNSPECIFIED X axis"
      logger.error(msg)
      raise RuntimeError(msg)

    pd = PlotData()
    pd.setX_min_max(len(yVal),
                    xmin=self._xaxis[1],
                    xmax=self._xaxis[2],
                    unit=self._xaxis[0])

    scale = adjustFloat(scale)
    ymin = (center - (self._ndiv[1] / 2) * scale)
    ymax = (center + (self._ndiv[1] / 2) * scale)
    pd.setY_min_max(yVal, ymin=ymin, ymax=ymax, unit=unit)
    pd.legend = legend
    self.data.append(pd)

  def _draw(self) -> bool:
    if (len(self.data) == 0):
      logger.warning("NOTHING to plot")
      return False

    colors = ["b", "g", "r", "c"]
    if (len(self.data) > len(colors)):
      msg = "TOO MANY plots for {}".format(self.__class__.__name__)
      logger.error(msg)
      raise RuntimeError(msg)

    matplotlib.pyplot.clf()
    fig, main_ax = matplotlib.pyplot.subplots()
    axes = [main_ax]
    for i in range(len(self.data) - 1):
      axes.append(main_ax.twinx())
    hdl = []

    self.limits()

    for i, pd in enumerate(self.data):
      hdlPlot, = axes[i].plot(pd.x, pd.y, label="???", color=colors[i], **self._plot_opts)
      hdl.append(hdlPlot)

      if (i == 0):
        axes[i].set_xlim(self._xlim[0], self._xlim[1])
        axes[i].locator_params(axis="x", tight=True, nbins=self._ndiv[0])
        axes[i].tick_params(axis="x",
                            labelbottom=True,
                            labeltop=False,
                            labelleft=False,
                            labelright=False,
                            labelsize="x-small")
        axes[i].xaxis.offsetText.set_fontsize("x-small")
        axes[i].grid(True)
      axes[i].set_ylim(pd.ymin, pd.ymax)
      axes[i].yaxis.set_label_position("left")
      #axes[i].locator_params(axis="y", tight=True, nbins=self._ndiv[1])
      axes[i].set_yticks([
          round((pd.ymin + (t * ((pd.ymax - pd.ymin) / self._ndiv[1]))), 2)
          for t in range(self._ndiv[1] + 1)
      ])
      axes[i].tick_params(axis="y",
                          labelbottom=False,
                          labeltop=False,
                          labelleft=True,
                          labelright=False,
                          labelsize="x-small",
                          colors=colors[i],
                          pad=(4 + 32 * i))
      axes[i].yaxis.offsetText.set_fontsize("x-small")

      axes[i].set_ylabel("{}[{}]".format(pd.legend, pd.yunit),
                         color=colors[i],
                         labelpad=-5,
                         **self._lbl_opts)

    if (self._xaxis[0] == "s"):
      # add markers for trigger position
      marker_opts = {"zorder": 2, "marker": "v", "color": "r"}
      matplotlib.pyplot.scatter(x=0.0, y=self.data[-1].ymax, **marker_opts)
      marker_opts = {"zorder": 2, "marker": "^", "color": "r"}
      matplotlib.pyplot.scatter(x=0.0, y=self.data[-1].ymin, **marker_opts)

    self._draw_hlines()
    self._draw_vlines()
    self._draw_markers()

    #axes[-1].legend(hdl, [h.get_label() for h in hdl], **self._legend_opts)
    # add extra legend with y scales
    #lgd = axes[0].legend(hdl, ["{:G}{}/div".format(pd.yscale, pd.yunit) for pd in self.data],
    #               bbox_to_anchor=(0, 1.0), **self._legend_opts)
    #self._extra_artists.append(lgd)

    #axes[0].set_xlabel("{:G}{}/div".format(self.data[0].xscale, self.data[0].xunit), **self._lbl_opts)
    if (self._xaxis[0] == "s"):
      axes[0].set_xlabel("time [{}]".format(self.data[0].xunit), **self._lbl_opts)
    elif (self._xaxis[0] == "Hz"):
      axes[0].set_xlabel("freq [{}]".format(self.data[0].xunit), **self._lbl_opts)

    self._draw_title()

    if (self.ts is not None):
      matplotlib.pyplot.figtext(0.7, 0.04, self.ts, **self._font_opts)
    if (self.txt_ll is not None):
      matplotlib.pyplot.figtext(0.2, 0.04, self.txt_ll, **self._font_opts)
    if (self.txt_ul is not None):
      matplotlib.pyplot.figtext(0.7, 0.92, self.txt_ul, **self._font_opts)
    if (self.txt_ur is not None):
      matplotlib.pyplot.figtext(0.2, 0.92, self.txt_ur, **self._font_opts)

    self._draw_legend()

    return True
