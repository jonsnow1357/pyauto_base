#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

from __future__ import annotations  # for python 3.8
#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")

#yapf: disable
# _dictHex2Bin = {"0": "0000", "1": "0001", "2": "0010", "3": "0011",
#                 "4": "0100", "5": "0101", "6": "0110", "7": "0111",
#                 "8": "1000", "9": "1001", "A": "1010", "B": "1011",
#                 "C": "1100", "D": "1101", "E": "1110", "F": "1111",
#                 "a": "1010", "b": "1011", "c": "1100", "d": "1101",
#                 "e": "1110", "f": "1111"}
# _dictHexNot = {"0": "F", "1": "E", "2": "D", "3": "C",
#                "4": "B", "5": "A", "6": "9", "7": "8",
#                "8": "7", "9": "6", "A": "5", "B": "4",
#                "C": "3", "D": "2", "E": "1", "F": "0",
#                "a": "5", "b": "4", "c": "3", "d": "2",
#                "e": "1", "f": "0"}
#yapf: enable

def isHexString(hexStr: str, maxNB: int = 0) -> bool:
  """
  Checks if a string is a hex number.

  :param str hexStr:
  :param int maxNB: max number of bytes in the string
  :return: True/False
  """
  if (not isinstance(hexStr, str)):
    return False

  if (maxNB > 0):
    regex = r"^0x[0-9a-fA-F]{1," + str(2 * maxNB) + "}$"
  else:
    regex = r"^0x[0-9a-fA-F]+$"
  return (re.match(regex, hexStr) is not None)

def isBinString(binStr: str, maxNB: int = 0) -> bool:
  """
  Checks if a string is a binary number.

  :param str binStr:
  :param int maxNB: max number of bits in the string
  :return: True/False
  """
  if (not isinstance(binStr, str)):
    return False

  if (maxNB > 0):
    regex = r"^b[01]{1," + str(maxNB) + "}$"
  else:
    regex = r"^b[01]+$"
  return (re.match(regex, binStr) is not None)

def _formatHex(val: int, length: int = 0) -> str:
  """
  Converts a integer to hex string and formats it nicely.

  :param int val: integer
  :param int length: required length
  :return: string representing a hex number
  """
  if (length != 0):
    fmt = "{{:0{}X}}".format(length)
  else:
    fmt = "{:X}"
  #logger.info(fmt)
  res = fmt.format(val)
  if (res[0] == "-"):
    return res[0] + "0x" + res[1:]
  else:
    return "0x" + res

# def hex2bin(hexVal):
#   """
#   Returns the binary string representation of the hex value.
#
#   :param str hexVal: string representing a hex number
#   :return: string representing a binary number
#   """
#   if (not isHexString(hexVal)):
#     msg = "NOT a hex number: '{}'".format(hexVal)
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   #logger.info(hexVal)
#   res = ""
#   for c in hexVal[2:]:
#     res += _dictHex2Bin[c]
#   #logger.info(res)
#
#   return ("b" + res)

# def int2bin(n, strlen=0):
#   """
#   Returns the binary string representation of a positive integer.
#   If *strlen* is greater than the converted string length,
#   it will be padded with 0.
#
#   :param int n: positive number
#   :param int strlen: requested length of the result
#   :return: string representing a binary number
#   """
#   if (n < 0):
#     msg = "INCORRECT parameter: {:d}".format(n)
#     logger.error(msg)
#     raise RuntimeError(msg)
#   if (strlen < 0):
#     strlen = 0
#
#   #logger.info(n)
#   res = hex2bin(hex(n))[1:]
#   res = res.lstrip('0')
#   if (res == ""):
#     res = "0"
#
#   if (strlen > len(res)):
#     fmt = "b{:0>" + str(strlen) + "}"
#     return fmt.format(res)
#   else:
#     return ("b" + res)

# def bin2hex(binVal):
#   """
#   Returns the hex value of a binary string.
#
#   :param str binVal: string representing a binary number
#   :return: string representing a hex number
#   """
#   if (not isBinString(binVal)):
#     msg = "NOT a binary number: '{}'".format(binVal)
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   res = 0
#   binVal = binVal[1:][::-1]
#   for i, v in enumerate(binVal):
#     res += int(v) * (2**i)
#   return _formatHex(res)

def getBit(n: int, hexVal: str) -> int:
  """
  Returns the value of the bit on *n*-th position *from hexVal*.

  :param int n: bit position
  :param str hexVal: string representing a hex number
  :return: bit value
  """
  if (n < 0):
    msg = "INCORRECT parameter: {:d}".format(n)
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isHexString(hexVal)):
    msg = "NOT a hex number: '{}'".format(hexVal)
    logger.error(msg)
    raise RuntimeError(msg)

  val = int(hexVal, 16)
  if (val < (2**n)):
    return 0
  return int(val // (2**n)) % 2

def setBit(n: int, hexVal: str) -> str:
  """
  Sets to 1 the value of the bit on *n*-th position from *hexVal*.

  :param int n: bit position
  :param str hexVal: string representing a hex number
  :return: string representing a hex number
  """
  if (getBit(n, hexVal) == 1):
    return hexVal
  val = int(hexVal, 16) + (2**n)
  return _formatHex(val, length=2)

def clrBit(n: int, hexVal: str) -> str:
  """
  Sets to 0 the value of the bit on *n*-th position from *hexVal*.

  :param int n: bit position
  :param str hexVal: string representing a hex number
  :return: string representing a hex number
  """
  if (getBit(n, hexVal) == 0):
    return hexVal
  val = int(hexVal, 16) - (2**n)
  return _formatHex(val, length=2)

# def hexPad(hexVal, length):
#   """
#   Pads with 0 the string representing *hexVal*.
#
#   :param str hexVal: string representing a hex number
#   :param int length:
#   :return: string representing a hex number
#   """
#   if (not isHexString(hexVal)):
#     msg = "NOT a hex number: '{}'".format(hexVal)
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   return _formatHex(int(hexVal, 16), length=length)

# def hexAdd(hexVal1, hexVal2):
#   """
#   Adds 2 hex values and returns the result in hex.
#
#   :param str hexVal1: string representing a hex number
#   :param str hexVal2: string representing a hex number
#   :return: string representing a hex number
#   """
#   if (not isHexString(hexVal1)):
#     msg = "NOT a hex number: '{}'".format(hexVal1)
#     logger.error(msg)
#     raise RuntimeError(msg)
#   if (not isHexString(hexVal2)):
#     msg = "NOT a hex number: '{}'".format(hexVal2)
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   res = int(hexVal1, 16) + int(hexVal2, 16)
#   return _formatHex(res)

# def hexSub(hexVal1, hexVal2):
#   """
#   Subtracts 2 hex values and returns the result in hex.
#
#   :param str hexVal1: string representing a hex number
#   :param str hexVal2: string representing a hex number
#   :return: string representing a hex number
#   """
#   if (not isHexString(hexVal1)):
#     msg = "NOT a hex number: '{}'".format(hexVal1)
#     logger.error(msg)
#     raise RuntimeError(msg)
#   if (not isHexString(hexVal2)):
#     msg = "NOT a hex number: '{}'".format(hexVal2)
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   res = int(hexVal1, 16) - int(hexVal2, 16)
#   return _formatHex(res)

# def hexNot(hexVal):
#   """
#   Returns the bitwise NOT of a hex number.
#
#   :param str hexVal: string representing a hex number
#   :return: string representing a hex number
#   """
#   if (not isHexString(hexVal)):
#     msg = "NOT a hex number: '{}'".format(hexVal)
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   res = "0x"
#   for c in hexVal[2:]:
#     res += _dictHexNot[c]
#   return res

# def hexOr(hexVal1, hexVal2):
#   """
#   ORs 2 hex values and returns the result in hex.
#
#   :param str hexVal1: string representing a hex number
#   :param str hexVal2: string representing a hex number
#   :return: string representing a hex number
#   """
#   if (not isHexString(hexVal1)):
#     msg = "NOT a hex number: '{}'".format(hexVal1)
#     logger.error(msg)
#     raise RuntimeError(msg)
#   if (not isHexString(hexVal2)):
#     msg = "NOT a hex number: '{}'".format(hexVal2)
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   res = int(hexVal1, 16) | int(hexVal2, 16)
#   return _formatHex(res)

# def hexAnd(hexVal1, hexVal2):
#   """
#   ANDs 2 hex values and returns the result in hex.
#
#   :param str hexVal1: string representing a hex number
#   :param str hexVal2: string representing a hex number
#   :return: string representing a hex number
#   """
#   if (not isHexString(hexVal1)):
#     msg = "NOT a hex number: '{}'".format(hexVal1)
#     logger.error(msg)
#     raise RuntimeError(msg)
#   if (not isHexString(hexVal2)):
#     msg = "NOT a hex number: '{}'".format(hexVal2)
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   res = int(hexVal1, 16) & int(hexVal2, 16)
#   return _formatHex(res)

# def hexXor(hexVal1, hexVal2):
#   """
#   XORs 2 hex values and returns the result in hex.
#
#   :param str hexVal1: string representing a hex number
#   :param str hexVal2: string representing a hex number
#   :return: string representing a hex number
#   """
#   if (not isHexString(hexVal1)):
#     msg = "NOT a hex number: '{}'".format(hexVal1)
#     logger.error(msg)
#     raise RuntimeError(msg)
#   if (not isHexString(hexVal2)):
#     msg = "NOT a hex number: '{}'".format(hexVal2)
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   res = int(hexVal1, 16) ^ int(hexVal2, 16)
#   return _formatHex(res)

# def hexShl(hexVal, nb):
#   """
#   Shifts left a hex value by nb bits and returns the result in hex.
#
#   :param str hexVal: string representing a hex number
#   :param int nb:
#   :return: string representing a hex number
#   """
#   if (not isHexString(hexVal)):
#     msg = "NOT a hex number: '{}'".format(hexVal)
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   if (nb < 1):
#     return hexVal
#   res = int(hexVal, 16) * (2**nb)
#   return _formatHex(res)

# def hexShr(hexVal, nb):
#   """
#   Shifts right a hex value by nb bits and returns the result in hex.
#
#   :param str hexVal: string representing a hex number
#   :param int nb:
#   :return: string representing a hex number
#   """
#   if (not isHexString(hexVal)):
#     msg = "NOT a hex number: '{}'".format(hexVal)
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   if (nb < 1):
#     return hexVal
#   res = int(hexVal, 16) // (2**nb)
#   return _formatHex(res)

# def hexGetByte(n, hexVal):
#   """
#   Returns the *n*-th byte from *hexVal*.
#
#   :param int n:
#   :param str hexVal: string representing a hex number
#   :return: string representing a hex number
#   """
#   if (n < 0):
#     msg = "INCORRECT parameter: {:d}".format(n)
#     logger.error(msg)
#     raise RuntimeError(msg)
#   if (not isHexString(hexVal)):
#     msg = "NOT a hex number: '{}'".format(hexVal)
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   res = hexVal[2:]
#   if (len(res) <= (2 * (n + 1))):
#     res = res.rjust(2 * (n + 1), "0")
#     return ("0x" + res[0:2].upper())
#   elif (len(res) > (2 * (n + 1))):
#     return ("0x" + res[(len(res) - (2 * (n + 1))):(len(res) - (2 * n))].upper())

def int2bytes(val: int, nb: int  = 1) -> list[int]:
  res = []
  for i in range(nb):
    res.append(val & 0xff)
    val >>= 8
  return res

def hex2int_2c(hexVal: str, nb: int = 8) -> int:
  """
  Convert to int a 2's complement *hexVal* represented on *nb* bits.

  :param str hexVal: string representing a hex number
  :param int nb:
  :return: converted value
  """
  if (not isHexString(hexVal)):
    msg = "NOT a hex number: '{}'".format(hexVal)
    logger.error(msg)
    raise RuntimeError(msg)

  res = int(hexVal, 16) % (2**nb)
  res = res if (res < 2**(nb - 1)) else (res - 2**nb)
  return int(res)

def int2hex_2c(val: int, nb: int = 8) -> str:
  """
  Convert a int to a 2's complement *hexVal* represented on *nb* bits.

  :param int val:
  :param int nb:
  :return: string representing a hex number
  """
  if (val >= 0):
    return _formatHex(val)

  while (2**nb <= (-1 * val)):
    nb += 1

  res = (2**nb + val)
  if (res < 2**(nb - 1)):  # need sign extension
    res = 2**(nb - 1) + res
  return _formatHex(res)

# def hexJoin(lstHexVal, reverse=False):
#   """
#   Joins a list of hex values into one value.
#   If reverse == False
#     [0xh1, 0xh2, ...] -> 0x...h2h1
#   If reverse == True
#     [0xh1, 0xh2, ...] -> 0xh1h2...
#
#   :param list lstHexVal: list of strings representing hex numbers
#   :param boolean reverse:
#   :return: string representing a hex number
#   """
#   if (not reverse):
#     lstHexVal = lstHexVal[::-1]
#   res = []
#   for h in lstHexVal:
#     if (not isHexString(h)):
#       msg = "NOT a hex number: '{}'".format(h)
#       logger.error(msg)
#       raise RuntimeError(msg)
#
#     res.append(h[2:])
#   return "0x" + "".join(res).upper()

def hex2bits(hexVal: str, reverse: bool = False) -> list[int]:
  """
  Converts a hex string into a list of bits.
  If reverse == False
    returns [b0, b1, b2, ..., bMSB] (normal list ordering)
  If reverse == True
    returns [bMSB, ..., b2, b1, b0]

  :param str hexVal: string representing a hex number
  :param boolean reverse:
  :return: list of 0 and 1s
  """
  if (not isHexString(hexVal)):
    msg = "NOT a hex number: '{}'".format(hexVal)
    logger.error(msg)
    raise RuntimeError(msg)

  res = [int(t) for t in "{:0>8b}".format(int(hexVal, 16))]
  if (not reverse):
    res = res[::-1]
  return res

def bits2hex(lstb: list[int], reverse: bool = False) -> str:
  """
  Converts a list of bits into a hex string.
  If reverse == False
    accepts [b0, b1, b2, ..., bMSB] (normal list ordering)
  If reverse == True
    accepts [bMSB, ..., b2, b1, b0]

  :param list lstb: list of 0s and 1s
  :param boolean reverse:
  :return: string representing a hex number
  """
  for b in lstb:
    if ((b < 0) or (b > 1)):
      msg = "NOT a bit: '{}'".format(lstb)
      logger.error(msg)
      raise RuntimeError(msg)

  if (reverse):
    tmp = sum([t[0] * t[1] for t in zip(lstb[::-1], [(2**i) for i in range(len(lstb))])])
  else:
    tmp = sum([t[0] * t[1] for t in zip(lstb, [(2**i) for i in range(len(lstb))])])
  return "0x{:X}".format(tmp)

# def hexNBits(hexVal):
#   """
#   Returns the number of bits on which *hexVal* can be expressed.
#
#   :param str hexVal: string representing a hex number
#   :return: number of bits
#   """
#   tmp = int(hexVal, 16)
#   if ((tmp == 0) or (tmp == 1)):
#     return 1
#
#   nbits = 1
#   while (tmp > 1):
#     nbits += 1
#     tmp //= 2
#   return nbits

# def hexEqual(hexRefVal, hexVal, hexMask=None):
#   """
#   Compares 2 hex values, with optional mask.
#
#   :param str hexRefVal: string representing a hex number
#   :param str hexVal: string representing a hex number
#   :param str hexMask: string representing a hex number
#   :return: string representing a hex number
#   """
#   if (not isHexString(hexRefVal)):
#     msg = "NOT a hex number: '{}'".format(hexRefVal)
#     logger.error(msg)
#     raise RuntimeError(msg)
#   if (not isHexString(hexVal)):
#     msg = "NOT a hex number: '{}'".format(hexVal)
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   if (hexMask is None):
#     val1 = int(hexRefVal, 16)
#     val2 = int(hexVal, 16)
#     if (val1 != val2):
#       logger.error("data mismatch: {} != {}".format(hexRefVal, hexVal))
#       return False
#   else:
#     if (not isHexString(hexMask)):
#       msg = "NOT a hex number: '{}'".format(hexMask)
#       logger.error(msg)
#       raise RuntimeError(msg)
#
#     val1 = int(hexRefVal, 16) & int(hexMask, 16)
#     val2 = int(hexVal, 16) & int(hexMask, 16)
#     if (val1 != val2):
#       logger.error("data mismatch: {} != {} (mask {})".format(hexRefVal, hexVal, hexMask))
#       return False
#   return True

def hexRdModWr(hexRefVal: str, hexVal: str, hexMask: str) -> str:
  """
  Read-modify-write. Changes *rdVal* with bits from *hexVal* based on *hexMask*.

  :param str hexRefVal: string representing a hex number
  :param str hexVal: string representing a hex number
  :param str hexMask: string representing a hex number
  :return: string representing a hex number
  """
  if (not isHexString(hexRefVal)):
    msg = "NOT a hex number: '{}'".format(hexRefVal)
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isHexString(hexVal)):
    msg = "NOT a hex number: '{}'".format(hexVal)
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isHexString(hexMask)):
    msg = "NOT a hex number: '{}'".format(hexMask)
    logger.error(msg)
    raise RuntimeError(msg)

  rd = int(hexRefVal, 16)
  val = int(hexVal, 16)
  mask = int(hexMask, 16)

  res = (rd & (~mask)) | (val & mask)
  #logger.info("{}, m: {} {}, {}".format(hexRefVal, hexMask, hexVal, hex(res)))
  return _formatHex(res)
