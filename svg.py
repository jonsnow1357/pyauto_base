#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

from __future__ import annotations  # for python 3.8
#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv
import typing
import xml.etree.ElementTree as ET
import xml.dom.minidom

logger = logging.getLogger("lib")
import pyauto_base.misc

unitMM = "mm"
unitCM = "cm"
unitIN = "in"
units = ("", unitMM, unitCM, unitIN)
regexColor1 = r"#[0-9a-fA-F]{6}"
regexColor2 = r"rgb\([12]?[0-9]{1,2}%?, ?[12]?[0-9]{1,2}%?, ?[12]?[0-9]{1,2}%?\)"

def _failAttributeValue(attrName: str, attrVal: str) -> None:
  msg = "INCORRECT '{}': {}".format(attrName, attrVal)
  logger.error(msg)
  raise RuntimeError(msg)

def _chkAttributeValue(attrName: str,
                       attrVal: typing.Union[int, float],
                       minVal: typing.Union[None, int, float] = None,
                       maxVal: typing.Union[None, int, float] = None) -> None:
  if ((minVal is not None) and (attrVal < minVal)):
    _failAttributeValue(attrName, str(attrVal))
  if ((maxVal is not None) and (attrVal > maxVal)):
    _failAttributeValue(attrName, str(attrVal))

class Tag(object):

  def __init__(self):
    self.name: str = ""
    self.attrs: dict[str, str] = {}
    self._text: str = ""

  def __str__(self):
    elDOM = xml.dom.minidom.parseString(ET.tostring(self.getXML(), encoding="utf-8"))
    res = elDOM.toprettyxml(indent="  ")
    #logger.info(res)
    return res

  def getAttrs(self) -> dict[str, str]:
    res = dict([(k, str(v).strip()) for k, v in self.attrs.items() if (v is not None)])
    res = dict([(k, v) for k, v in res.items() if (v != "")])
    return res

  @property
  def text(self) -> str:
    return self._text

  @text.setter
  def text(self, val: str) -> None:
    if (not pyauto_base.misc.isEmptyString(val)):
      self._text = val

  def getXML(self) -> xml.etree.ElementTree.Element:
    if (self.name is None):
      msg = "XML tag CANNOT have None name"
      logger.error(msg)
      raise RuntimeError(msg)

    res = ET.Element(self.name)
    for k, v in self.getAttrs().items():
      res.set(k, v)
    res.text = self._text
    #logger.info(ET.tostring(res, encoding="utf-8"))
    return res

class svgBaseTag(Tag):

  def __init__(self):
    super(svgBaseTag, self).__init__()
    if (not hasattr(self, "attrs")):
      self.attrs = {}
    self.attrs.update({"id": ""})

  @property
  def id(self) -> str:
    return self.attrs["id"]

  @id.setter
  def id(self, val: str) -> None:
    if (not pyauto_base.misc.isEmptyString(val)):
      self.attrs["id"] = val
    else:
      msg = "CANNOT assign empty id"
      logger.error(msg)
      raise RuntimeError(msg)

  # yapf: disable
  def _setAttribute_Number(self,
                           attrName: str,
                           attrVal: typing.Union[int, float, str],
                           minVal: typing.Union[None, int, float, str] = None,
                           maxVal: typing.Union[None, int, float, str] = None) -> None:
    if (isinstance(attrVal, int)):
      _chkAttributeValue(attrName, attrVal,
                         None if (minVal is None) else int(minVal),
                         None if (maxVal is None) else int(maxVal))
      self.attrs[attrName] = str(attrVal)
    elif (isinstance(attrVal, float)):
      _chkAttributeValue(attrName, attrVal,
                         None if (minVal is None) else float(minVal),
                         None if (maxVal is None) else float(maxVal))
      self.attrs[attrName] = str(attrVal)
    elif (not pyauto_base.misc.isEmptyString(attrVal)):
      if (attrVal.endswith(unitMM) or attrVal.endswith(unitCM) or attrVal.endswith(unitIN)):
        try:
          _chkAttributeValue(attrName, int(attrVal[:-2]),
                             None if (minVal is None) else int(minVal),
                             None if (maxVal is None) else int(maxVal))
          self.attrs[attrName] = attrVal
        except ValueError:
          _chkAttributeValue(attrName, float(attrVal[:-2]),
                             None if (minVal is None) else float(minVal),
                             None if (maxVal is None) else float(maxVal))
          self.attrs[attrName] = attrVal
      else:
        try:
          _chkAttributeValue(attrName, int(attrVal),
                             None if (minVal is None) else int(minVal),
                             None if (maxVal is None) else int(maxVal))
          self.attrs[attrName] = attrVal
        except ValueError:
          _failAttributeValue(attrName, attrVal)
    else:
      _failAttributeValue(attrName, attrVal)
  # yapf: enable

class _svgPresentation(object):

  def __init__(self):
    if (not hasattr(self, "attrs")):
      self.attrs: dict[str, str] = {}
    self.attrs.update({
        "fill": "",
        "fill-opacity": "",
        "fill-rule": "",
        "stroke": "",
        "stroke-width": "",
        "stroke-linecap": "",
        "stroke-linejoin": "",
        "stroke-opacity": "",
        "stroke-dasharray": ""
    })

  # yapf: disable
  def _setAttribute_Number(self,
                           attrName: str,
                           attrVal: typing.Union[int, float, str],
                           minVal: typing.Union[None, int, float, str] = None,
                           maxVal: typing.Union[None, int, float, str] = None) -> None:
    if (isinstance(attrVal, int)):
      _chkAttributeValue(attrName, attrVal,
                         None if (minVal is None) else int(minVal),
                         None if (maxVal is None) else int(maxVal))
      self.attrs[attrName] = str(attrVal)
    elif (isinstance(attrVal, float)):
      _chkAttributeValue(attrName, attrVal,
                         None if (minVal is None) else float(minVal),
                         None if (maxVal is None) else float(maxVal))
      self.attrs[attrName] = str(attrVal)
    elif (not pyauto_base.misc.isEmptyString(attrVal)):
      if (attrVal.endswith(unitMM) or attrVal.endswith(unitCM) or attrVal.endswith(unitIN)):
        try:
          _chkAttributeValue(attrName, int(attrVal[:-2]),
                             None if (minVal is None) else int(minVal),
                             None if (maxVal is None) else int(maxVal))
          self.attrs[attrName] = attrVal
        except ValueError:
          _chkAttributeValue(attrName, float(attrVal[:-2]),
                             None if (minVal is None) else float(minVal),
                             None if (maxVal is None) else float(maxVal))
          self.attrs[attrName] = attrVal
      else:
        _failAttributeValue(attrName, attrVal)
    else:
      _failAttributeValue(attrName, attrVal)
  # yapf: enable

  @property
  def fill(self) -> str:
    return self.attrs["fill"]

  @fill.setter
  def fill(self, val: str) -> None:
    if (re.match(regexColor1, val) is not None):
      self.attrs["fill"] = val
    elif (re.match(regexColor2, val) is not None):
      self.attrs["fill"] = val
    elif (val in ["none"]):
      self.attrs["fill"] = val
    else:
      _failAttributeValue("fill", val)

  @property
  def fill_opacity(self) -> typing.Union[float, str]:
    return self.attrs["fill-opacity"]

  @fill_opacity.setter
  def fill_opacity(self, val: typing.Union[float, str]) -> None:
    self._setAttribute_Number("fill-opacity", val, minVal=0.0, maxVal=1.0)

  @property
  def fill_rule(self) -> str:
    return self.attrs["fill-rule"]

  @fill_rule.setter
  def fill_rule(self, val: str) -> None:
    if (val in ["nonzero", "evenodd", "inherit"]):
      self.attrs["fill-rule"] = val
    else:
      _failAttributeValue("fill-rule", val)

  @property
  def stroke(self) -> str:
    return self.attrs["stroke"]

  @stroke.setter
  def stroke(self, val: str) -> None:
    if (re.match(regexColor1, val) is not None):
      self.attrs["stroke"] = val
    elif (re.match(regexColor2, val) is not None):
      self.attrs["stroke"] = val
    else:
      _failAttributeValue("stroke", val)

  @property
  def stroke_width(self) -> typing.Union[int, float, str]:
    return self.attrs["stroke-width"]

  @stroke_width.setter
  def stroke_width(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("stroke-width", val, minVal=0)

  @property
  def stroke_linecap(self) -> str:
    return self.attrs["stroke-linecap"]

  @stroke_linecap.setter
  def stroke_linecap(self, val: str) -> None:
    if (val in ["butt", "round", "square", "inherit"]):
      self.attrs["stroke-linecap"] = val
    else:
      _failAttributeValue("stroke-linecap", val)

  @property
  def stroke_linejoin(self) -> str:
    return self.attrs["stroke-linejoin"]

  @stroke_linejoin.setter
  def stroke_linejoin(self, val: str) -> None:
    if (val in ["miter", "round", "bevel", "inherit"]):
      self.attrs["stroke-linejoin"] = val
    else:
      _failAttributeValue("stroke-linejoin", val)

  @property
  def stroke_opacity(self) -> typing.Union[float, str]:
    return self.attrs["stroke-opacity"]

  @stroke_opacity.setter
  def stroke_opacity(self, val: typing.Union[float, str]) -> None:
    self._setAttribute_Number("stroke-opacity", val, minVal=0.0, maxVal=1.0)

  @property
  def stroke_dasharray(self) -> str:
    return self.attrs["stroke-dasharray"]

  @stroke_dasharray.setter
  def stroke_dasharray(self, val: str) -> None:
    if (not pyauto_base.misc.isEmptyString(val)):
      self.attrs["stroke-dasharray"] = val
    else:
      _failAttributeValue("stroke-dasharray", val)

class svgImage(svgBaseTag):

  def __init__(self,
               fPath: str,
               width: typing.Union[int, str],
               height: typing.Union[int, str],
               unit: str = ""):
    super(svgImage, self).__init__()
    self.name: str = "svg"
    self.attrs.update({"version": "1.1", "xmlns": "http://www.w3.org/2000/svg"})

    if (pyauto_base.misc.isEmptyString(fPath)):
      msg = "INCORRECT file path"
      logger.error(msg)
      raise RuntimeError(msg)
    if (unit not in units):
      msg = "INCORRECT unit"
      logger.error(msg)
      raise RuntimeError(msg)

    self.path: str = fPath
    self._title: str = ""
    if (unit == ""):
      self.width = str(width)
      self.height = str(height)
    else:
      self.width = "{}{}".format(width, unit)
      self.height = "{}{}".format(height, unit)
      self.attrs["viewBox"] = "0 0 {} {}".format(width, height)
    self.content: list[svgBaseTag] = []

  def showInfo(self) -> None:
    logger.info("{}: {}".format(self.__class__.__name__, self.path))
    logger.info("{}: {}x{}".format(self.__class__.__name__, self.attrs["width"],
                                   self.attrs["height"]))

  @property
  def title(self) -> str:
    return self._title

  @title.setter
  def title(self, val: str) -> None:
    if (not pyauto_base.misc.isEmptyString(val)):
      self._title = val

  @property
  def width(self) -> typing.Union[int, str]:
    return self.attrs["width"]

  @width.setter
  def width(self, val: typing.Union[int, str]) -> None:
    self._setAttribute_Number("width", val, minVal=1)

  @property
  def height(self) -> typing.Union[int, str]:
    return self.attrs["height"]

  @height.setter
  def height(self, val: typing.Union[int, str]) -> None:
    self._setAttribute_Number("height", val, minVal=1)

  def addElement(self, el: svgBaseTag) -> None:
    if (not issubclass(el.__class__, svgBaseTag)):
      msg = "CANNOT add UNSUPPORTED object: {}".format(type(el))
      logger.error(msg)
      raise RuntimeError(msg)

    self.content.append(el)

  def getXML(self) -> xml.etree.ElementTree.Element:
    res = super(svgImage, self).getXML()
    if ((self.title is not None) and (len(self.title) > 0)):
      tmpEl = ET.SubElement(res, "title")
      tmpEl.text = self.title
    for t in self.content:
      if (t.name is not None):
        tmpEl = ET.SubElement(res, t.name)
        for k, v in t.getAttrs().items():
          tmpEl.set(k, v)
        tmpEl.text = t.text
    #logger.info(ET.tostring(res, encoding="utf-8"))
    return res

  def write(self) -> None:
    pyauto_base.misc.writeXML_pretty(self.path, self.getXML(), dtd="SVG", bEncoding=True)

class svgRectangle(svgBaseTag, _svgPresentation):

  def __init__(self,
               x: typing.Union[int, float, str],
               y: typing.Union[int, float, str],
               width: typing.Union[int, str],
               height: typing.Union[int, str],
               rx: typing.Union[None, int, float, str] = None,
               ry: typing.Union[None, int, float, str] = None):
    super(svgRectangle, self).__init__()
    self.name = "rect"
    self.x = x
    self.y = y
    self.width = width
    self.height = height
    if (rx is not None):
      self.rx = rx
    if (ry is not None):
      self.ry = ry

  @property
  def x(self) -> typing.Union[int, float, str]:
    return self.attrs["x"]

  @x.setter
  def x(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("x", val)

  @property
  def y(self) -> typing.Union[int, float, str]:
    return self.attrs["y"]

  @y.setter
  def y(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("y", val)

  @property
  def width(self) -> typing.Union[int, str]:
    return self.attrs["width"]

  @width.setter
  def width(self, val: typing.Union[int, str]) -> None:
    self._setAttribute_Number("width", val, minVal=1)

  @property
  def height(self) -> typing.Union[int, str]:
    return self.attrs["height"]

  @height.setter
  def height(self, val: typing.Union[int, str]) -> None:
    self._setAttribute_Number("height", val, minVal=1)

  @property
  def rx(self) -> typing.Union[int, float, str]:
    return self.attrs["rx"]

  @rx.setter
  def rx(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("rx", val)

  @property
  def ry(self) -> typing.Union[int, float, str]:
    return self.attrs["ry"]

  @ry.setter
  def ry(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("ry", val)

class svgCircle(svgBaseTag, _svgPresentation):

  def __init__(self, cx: typing.Union[int, float, str], cy: typing.Union[int, float, str],
               r: typing.Union[int, float, str]):
    super(svgCircle, self).__init__()
    self.name = "circle"
    self.cx = cx
    self.cy = cy
    self.r = r

  @property
  def cx(self) -> typing.Union[int, float, str]:
    return self.attrs["cx"]

  @cx.setter
  def cx(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("cx", val)

  @property
  def cy(self) -> typing.Union[int, float, str]:
    return self.attrs["cy"]

  @cy.setter
  def cy(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("cy", val)

  @property
  def r(self) -> typing.Union[int, float, str]:
    return self.attrs["r"]

  @r.setter
  def r(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("r", val)

class svgEllipse(svgBaseTag, _svgPresentation):

  def __init__(self, cx: typing.Union[int, float, str], cy: typing.Union[int, float, str],
               rx: typing.Union[int, float, str], ry: typing.Union[int, float, str]):
    super(svgEllipse, self).__init__()
    self.name = "ellipse"
    self.cx = cx
    self.cy = cy
    self.rx = rx
    self.ry = ry

  @property
  def cx(self) -> typing.Union[int, float, str]:
    return self.attrs["cx"]

  @cx.setter
  def cx(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("cx", val)

  @property
  def cy(self) -> typing.Union[int, float, str]:
    return self.attrs["cy"]

  @cy.setter
  def cy(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("cy", val)

  @property
  def rx(self) -> typing.Union[int, float, str]:
    return self.attrs["rx"]

  @rx.setter
  def rx(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("rx", val)

  @property
  def ry(self) -> typing.Union[int, float, str]:
    return self.attrs["ry"]

  @ry.setter
  def ry(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("ry", val)

class svgLine(svgBaseTag, _svgPresentation):

  def __init__(self, x1: typing.Union[int, float, str], y1: typing.Union[int, float, str],
               x2: typing.Union[int, float, str], y2: typing.Union[int, float, str]):
    super(svgLine, self).__init__()
    self.name = "line"
    self.x1 = x1
    self.y1 = y1
    self.x2 = x2
    self.y2 = y2
    self.stroke = "rgb(0, 0, 0)"

  @property
  def x1(self) -> typing.Union[int, float, str]:
    return self.attrs["x1"]

  @x1.setter
  def x1(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("x1", val)

  @property
  def y1(self) -> typing.Union[int, float, str]:
    return self.attrs["y1"]

  @y1.setter
  def y1(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("y1", val)

  @property
  def x2(self) -> typing.Union[int, float, str]:
    return self.attrs["x2"]

  @x2.setter
  def x2(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("x2", val)

  @property
  def y2(self) -> typing.Union[int, float, str]:
    return self.attrs["y2"]

  @y2.setter
  def y2(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("y2", val)

class svgPolyLine(svgBaseTag, _svgPresentation):

  def __init__(self):
    super(svgPolyLine, self).__init__()
    self.name = "polyline"
    self.attrs["points"] = ""
    self.fill = "none"
    self.stroke = "rgb(0, 0, 0)"

  def clear(self) -> None:
    self.attrs["points"] = ""
    self.fill = "none"
    self.stroke = "rgb(0, 0, 0)"

  def addPoint(self, x: typing.Union[int, float, str], y: typing.Union[int, float,
                                                                       str]) -> None:
    self.attrs["points"] += "{},{} ".format(x, y)

class svgPolygon(svgBaseTag, _svgPresentation):

  def __init__(self):
    super(svgPolygon, self).__init__()
    self.name = "polygon"
    self.attrs["points"] = ""
    self.fill = "none"
    self.stroke = "rgb(0, 0, 0)"

  def clear(self) -> None:
    self.attrs["points"] = ""
    self.fill = "none"
    self.stroke = "rgb(0, 0, 0)"

  def addPoint(self, x: typing.Union[int, float, str], y: typing.Union[int, float,
                                                                       str]) -> None:
    self.attrs["points"] += "{},{} ".format(x, y)

  def regular_convex(self,
                     x: typing.Union[int, float],
                     y: typing.Union[int, float],
                     n: int,
                     r: typing.Union[int, float],
                     rot: float = 0.0):
    """
    creates a regular convex polygon
    :param x: x coordinate of the center of circumscribed circle
    :param y: y coordinate of the center of circumscribed circle
    :param n: number of sides (> 3)
    :param r: radius of the circumscribed circle
    :param rot: rotation angle (in radians)
    :return:
    """
    if (n < 3):
      msg = "CANNOT generate a regular polygon with {} sides".format(n)
      logger.error(msg)
      raise RuntimeError(msg)
    if (r <= 0.0):
      msg = "CANNOT use radius: {}".format(r)
      logger.error(msg)
      raise RuntimeError(msg)

    self.clear()
    for i in range(n):
      xv = r * math.cos(rot + (2 * i * math.pi) / n)
      yv = r * math.sin(rot + (2 * i * math.pi) / n)
      self.addPoint(x=(x + xv), y=(y + yv))

class svgText(svgBaseTag, _svgPresentation):

  def __init__(self, x: typing.Union[int, float, str], y: typing.Union[int, float, str],
               text: str):
    super(svgText, self).__init__()
    self.name = "text"

    if (pyauto_base.misc.isEmptyString(text)):
      msg = "CANNOT create null text"
      logger.error(msg)
      raise RuntimeError(msg)

    self.x = x
    self.y = y
    self.font_family = "monospace"
    self.font_size = 12
    self.text = text
    self.fill = "rgb(0, 0, 0)"

  @property
  def x(self) -> typing.Union[int, float, str]:
    return self.attrs["x"]

  @x.setter
  def x(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("x", val)

  @property
  def y(self) -> typing.Union[int, float, str]:
    return self.attrs["y"]

  @y.setter
  def y(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("y", val)

  @property
  def font_family(self) -> str:
    return self.attrs["font-family"]

  @font_family.setter
  def font_family(self, val: str):
    if (val in ["monospace", "serif", "sans-serif", "cursive", "fantasy"]):
      self.attrs["font-family"] = val
    else:
      _failAttributeValue("font-family", val)

  @property
  def font_size(self) -> typing.Union[int, float, str]:
    return self.attrs["font-size"]

  @font_size.setter
  def font_size(self, val: typing.Union[int, float, str]) -> None:
    self._setAttribute_Number("font-size", val, minVal=0)

class svgPath(svgBaseTag, _svgPresentation):

  def __init__(self,
               x: typing.Union[None, int, float, str] = None,
               y: typing.Union[None, int, float, str] = None):
    super(svgPath, self).__init__()
    self.name = "path"
    if ((x is not None) and (y is not None)):
      self.attrs["d"] = "M {} {}".format(x, y)
    else:
      self.attrs["d"] = ""
    self.fill = "none"
    self.stroke = "rgb(0, 0, 0)"

  @property
  def path(self) -> str:
    return self.attrs["d"]

  def moveto(self,
             x: typing.Union[int, float, str],
             y: typing.Union[int, float, str],
             bAbs: bool = True):
    _x = float(x)
    _y = float(y)
    if (bAbs):
      self.attrs["d"] += " M {} {}".format(_x, _y)
    else:
      self.attrs["d"] += " m {} {}".format(_x, _y)

  def closepath(self) -> None:
    self.attrs["d"] += " Z"

  def lineto(self,
             x: typing.Union[int, float, str],
             y: typing.Union[int, float, str],
             bAbs: bool = True):
    if (bAbs):
      self.attrs["d"] += " L {} {}".format(x, y)
    else:
      self.attrs["d"] += " l {} {}".format(x, y)

  def lineto_h(self, x: typing.Union[int, float, str], bAbs: bool = True):
    if (bAbs):
      self.attrs["d"] += " H {}".format(x)
    else:
      self.attrs["d"] += " h {}".format(x)

  def lineto_v(self, y: typing.Union[int, float, str], bAbs: bool = True):
    if (bAbs):
      self.attrs["d"] += " V {}".format(y)
    else:
      self.attrs["d"] += " v {}".format(y)

  def arc(self,
          rx: typing.Union[int, float, str],
          ry: typing.Union[int, float, str],
          x: typing.Union[int, float, str],
          y: typing.Union[int, float, str],
          rot: int = 0,
          bLargeArc: bool = False,
          bSweep: bool = False,
          bAbs: bool = True):
    # yapf: disable
    if (bAbs):
      self.attrs["d"] += " A {} {} {} {} {} {} {}".format(rx, ry, rot,
                                                          (1 if (bLargeArc) else 0),
                                                          (1 if (bSweep) else 0),
                                                          x, y)
    else:
      self.attrs["d"] += " a {} {} {} {} {} {} {}".format(rx, ry, rot,
                                                          (1 if (bLargeArc) else 0),
                                                          (1 if (bSweep) else 0),
                                                          x, y)
    # yapf: enable
