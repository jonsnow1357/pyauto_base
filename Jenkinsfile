pipeline {
  agent {node {label "built-in"}}

  environment {
    BUILD_EMAIL_TO = "3a28bdm71w@pomail.net"
    BUILD_EMAIL_BODY = """build info: ${env.BUILD_URL}"""
    PYTHONPATH = "$WORKSPACE"
    PY_MODULE = "pyauto_base"
  }

  options {
    buildDiscarder(logRotator(artifactDaysToKeepStr: "", artifactNumToKeepStr: "", daysToKeepStr: "", numToKeepStr: "8"))
    //disableConcurrentBuilds()
  }

  triggers {
    pollSCM("H/30 * * * *")
  }

  stages {
    stage ("env") {
      steps {
        sh "env"
      }
    }
    stage ("run") {
      steps {
        sh "cd ${PY_MODULE}; tox"
      }
    }
  }

  post {
    always {
      junit allowEmptyResults: true, testResults: "${PY_MODULE}/tests/data_out/test_py*.xml"
    }
    failure {
      emailext (
        subject: "FAILURE: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
        body: "${BUILD_EMAIL_BODY}",
        to: "${BUILD_EMAIL_TO}"
      )
    }
    aborted {
      emailext (
        subject: "ABORTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
        body: "${BUILD_EMAIL_BODY}",
        to: "${BUILD_EMAIL_TO}"
      )
    }
    unstable {
      emailext (
        subject: "UNSTABLE: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
        body: "${BUILD_EMAIL_BODY}",
        to: "${BUILD_EMAIL_TO}"
      )
    }
    cleanup{
      deleteDir()
      dir("${env.WORKSPACE_TMP}")    { deleteDir() }
      dir("${env.WORKSPACE}@script") { deleteDir() }
    }
  }
}
