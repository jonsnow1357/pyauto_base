#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

from __future__ import annotations  # for python 3.8
#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import typing
import random

logger = logging.getLogger("lib")

def getRndStr(n: int, val: str = "ascii") -> str:
  """
  :param int n: length of returned string
  :param str val: [ascii|digits|letters|hex] option for string content
  :return: a random string of length n
  """
  import string
  if (val == "digits"):
    return "".join([random.choice(string.digits) for _ in range(n)])
  elif (val == "hex"):
    return "".join([random.choice(string.hexdigits) for _ in range(n)])
  elif (val == "letters"):
    return "".join([random.choice(string.ascii_letters) for _ in range(n)])
  else:
    return "".join([random.choice(string.ascii_letters + string.digits) for _ in range(n)])

def getRndStr_Pat(prefix: str = "", maxLen: int = 8, bNum: bool = True) -> str:
  """
  Return a random string in the format:
  - <prefix><random_string_of_ascii> if bNum is False
  - <prefix><random_string_of_digits> if bNum is True, and the random string does not start with "0"
  :param prefix:
  :param maxLen:
  :param bNum:
  :return: a random string of max length = len(prefix) + maxLen
  """
  if (bNum):
    n = random.randint(0, (maxLen - 1))
    tmp = random.choice("123456789") + getRndStr(n, val="digits")
  else:
    n = random.randint(1, maxLen)
    tmp = getRndStr(n, val="ascii")
  return "{}{}".format(prefix, tmp)

def isEmptyString(strVal: typing.Optional[str]) -> bool:
  """
  :param strVal:
  :return:
  - True if None, not a string type, not a unicode type, or ""
  - False otherwise
  """
  if (strVal is None):
    return True
  if (not isinstance(strVal, str)):
    return True

  if (strVal == ""):
    return True
  else:
    return False

def isNumberOrString(val: typing.Union[int, float, str]) -> bool:
  """
  :param val:
  :return:
  - True if integer, float or non-empty string
  - False otherwise
  """
  if (isinstance(val, int)):
    return True
  if (isinstance(val, float)):
    return True

  if (not isEmptyString(val)):
    return True
  else:
    return False

def isTrueString(strVal: str) -> bool:
  """
  Checks if the string can be lexically interpreted as True
  :param strVal:
  :return: True|False
  """
  if (not isinstance(strVal, str)):
    return False

  lstTrue = ("1", "true", "yes", "yeah", "yep", "ok", "on")
  if (strVal.lower() in lstTrue):
    return True
  else:
    return False

def compareStrings(str1: str, str2: str, bCase: bool = True, fuzzyTh: float = 0.8) -> str:
  """
  Compare strings (with fuzzy match)
  :param str1:
  :param str2:
  :param bCase: True if case sensitive, else False
  :param fuzzyTh: threshold for the similarity as a float in the range [0, 1],
    * 0.0 means nothing in common
    * 1.0 means identical
  :return:
  """
  if (bCase):
    if (str1 == str2):
      return "yes"
  else:
    if (str1.lower() == str2.lower()):
      return "yes"

  import difflib
  seq: typing.Any = difflib.SequenceMatcher()

  if (bCase):
    seq.set_seqs(str1, str2)
    if (seq.ratio() > fuzzyTh):
      return "fuzzy"
  else:
    seq.set_seqs(str1.lower(), str2.lower())
    if (seq.ratio() > fuzzyTh):
      return "fuzzy"

  return "no"

def convertStringNonPrint(strVal: str) -> str:
  if (sys.version_info[0] == 2):
    if (isinstance(strVal, unicode)):
      strVal = str(strVal)
  else:
    if (isinstance(strVal, bytes)):
      strVal = strVal.decode("utf-8")

  if (not isinstance(strVal, str)):
    msg = "NOT a string: {}".format(strVal)
    logger.error(msg)
    raise RuntimeError(msg)

  return strVal.replace("\r", "<CR>").replace("\n", "<LF>")\
      .replace("\f", "<FF>").replace("\b", "<BS>").replace("\t", "<TAB>")\
      .replace("\x1B", "<ESC>")

def parseProtocolString(strUrl: str) -> dict[str, typing.Optional[str]]:
  """
  parse something like:
    protocol://username:password@hostname:port/path/to/folder
    protocol://hostname:port/path/to/folder
    protocol://hostname/path/to/folder
    protocol:///path/to/folder

  :param strUrl: string
  :return: dictionary of {"protocol": ..., "user": ..., "pswd": ..., "hostname": ..., "port": ..., "path": ...}
  """
  regexProto = r"^([^/]*)://([^/]+@)?([^/@]+)?/(.*)?$"
  #logger.info(strUrl)
  if (not re.match(regexProto, strUrl)):
    msg = "invalid URL: {}".format(strUrl)
    logger.error(msg)
    raise RuntimeError(msg)

  urlsplit = re.split(regexProto, strUrl)[1:-1]
  urlsplit = [t if (t != "") else None for t in urlsplit]
  #logger.info(urlsplit)

  if (len(urlsplit) != 4):
    msg = "invalid URL: {}".format(strUrl)
    logger.error(msg)
    raise RuntimeError(msg)

  dictRes = {
      "protocol": urlsplit[0],
      "user": None,
      "pswd": None,
      "hostname": None,
      "port": None,
      "path": urlsplit[3]
  }

  if (urlsplit[1] is not None):
    regexProto = r"([^:]*):([^@]*)@"
    tmp = re.split(regexProto, urlsplit[1])[1:-1]
    if (tmp == []):
      dictRes["user"] = urlsplit[1][:-1]
    else:
      dictRes["user"] = tmp[0]
      dictRes["pswd"] = tmp[1]

  if (urlsplit[2] is not None):
    regexProto = r"([^:]*):(.*)"
    tmp = re.split(regexProto, urlsplit[2])[1:-1]
    if (tmp == []):
      dictRes["hostname"] = urlsplit[2]
    else:
      dictRes["hostname"] = tmp[0]
      dictRes["port"] = tmp[1]

  #logger.info(dictRes)
  return dictRes

def parseRangeString(strRange: str) -> list[str]:
  """
  parse a range of values:
    "1,2,3" -> [1, 2, 3]
    "5-7" -> [5, 6, 7]
    "11-14,16" -> [11, 12, 13, 14, 16]
    "R1-R5" -> ["R1", "R2", "R3", "R4", "R5"]

  :param strRange:
  :return: list
  """
  if (isEmptyString(strRange)):
    return []

  strRange = re.sub(r" *, *", ",", strRange)
  strRange = re.sub(r" *- *", "-", strRange)

  prefix = None
  res = []
  if (strRange[0] not in (str(t) for t in range(10))):
    #logger.info(("prefix detected"))
    tmp = re.sub(r"[0-9]", "", strRange).replace("-", ",").split(",")
    tmp = list(set(tmp))
    if (len(tmp) != 1):
      logger.error("incorrect range (1): {}".format(strRange))
      return []
    prefix = tmp[0]
    strRange = re.sub(prefix, "", strRange)

  if (re.match(r"^[0-9]+$", strRange)):
    res = [int(strRange)]
  elif (re.match(r"^(([0-9]+|[0-9]+-[0-9]+),)*([0-9]+|[0-9]+-[0-9]+)$", strRange)):
    for t1 in strRange.split(","):
      if (re.match(r"^[0-9]+$", t1)):
        res.append(int(t1))
      elif (re.match(r"^[0-9]+-[0-9]+$", t1)):
        t2 = t1.split("-")
        res += range(int(t2[0]), (int(t2[1]) + 1))
      else:
        logger.error("incorrect range (2): {}".format(strRange))
  else:
    logger.error("incorrect range (3): {}".format(strRange))

  if (prefix is None):
    return [str(t) for t in sorted(res)]
  else:
    return ["{}{}".format(prefix, t) for t in sorted(res)]

def parseTimedeltaString(strVal: str) -> datetime.timedelta:
  """
  Parses a string value to a datetime.timedelta() object. The string must be of the form: [1d2h]3[m45s].
  :param strVal: string value
  :return: datetime.timedelta()
  """
  if (isEmptyString(strVal)):
    msg = "CANNOT parse empty timedelta string: '{}'".format(strVal)
    logger.error(msg)
    raise RuntimeError(msg)

  try:
    tmp: typing.Any = int(strVal)
    if (tmp < 0):
      msg = "CANNOT parse negative timedelta string: '{}'".format(strVal)
      logger.error(msg)
      raise RuntimeError(msg)
    res = datetime.timedelta(minutes=tmp)
  except ValueError:
    try:
      tmp = float(strVal)
      if (tmp < 0):
        msg = "CANNOT parse negative timedelta string: '{}'".format(strVal)
        logger.error(msg)
        raise RuntimeError(msg)
      res = datetime.timedelta(minutes=tmp)
    except ValueError:
      if (re.match(r"^([0-9]+[dmhs])+?$", strVal) is None):
        msg = "CANNOT parse timedelta string: '{}'".format(strVal)
        logger.error(msg)
        raise RuntimeError(msg)
      else:
        delta = [0, 0, 0, 0]
        tmp = re.split(r"([0-9]+d)", strVal)
        if (len(tmp) == 3):
          delta[0] = int(tmp[1][:-1])
        tmp = re.split(r"([0-9]+h)", strVal)
        if (len(tmp) == 3):
          delta[1] = int(tmp[1][:-1])
        tmp = re.split(r"([0-9]+m)", strVal)
        if (len(tmp) == 3):
          delta[2] = int(tmp[1][:-1])
        tmp = re.split(r"([0-9]+s)", strVal)
        if (len(tmp) == 3):
          delta[3] = int(tmp[1][:-1])
        #logger.info(delta)
        res = datetime.timedelta(days=delta[0],
                                 hours=delta[1],
                                 minutes=delta[2],
                                 seconds=delta[3])

  return res

def printHexList(lstHex: list[str], cols: int = 8, hexSz: str = "byte") -> None:
  """
  prints a list of bytes (hex strings) as a matrix (with addresses).
  :param lstHex: list of hex values
  :param cols: number of columns in the matrix [positive integer; 1 < cols < 16]
  :param hexSz: size of hexVal: "byte" - 1 Byte, "word" - 2 Bytes, "long" - 4 Bytes
  """
  if ((cols < 1) | (cols > 16)):
    logger.error("incorrect number of columns (1 < cols < 16): " + str(cols))
    return

  if (hexSz == "byte"):
    valLen = 2
    addrMpy = 1
  elif (hexSz == "word"):
    valLen = 4
    addrMpy = 2
  elif (hexSz == "long"):
    valLen = 8
    addrMpy = 4
  else:
    logger.error("incorrect length: {}".format(hexSz))
    return

  #logger.info("{:d} cols, {}".format(cols, hexSz))
  #logger.info(hexVal)

  printLst = []
  valFormat = "0x{:0>" + str(valLen) + "}"
  for el in lstHex:
    if (el.startswith("0x")):
      if (len(el) > (valLen + 2)):
        logger.error("incorrect value: {}".format(el))
        return
      printLst.append(valFormat.format(el[2:].upper()))
    else:
      if (len(el) > valLen):
        logger.error("incorrect value: {}".format(el))
        return
      printLst.append(valFormat.format(el.upper()))
  #logger.info(printLst)

  addrLen = 8
  bSeparateAddr = True
  if (bSeparateAddr):
    fmt = "0x{:0>" + str(addrLen) + "}: {}"
  else:
    fmt = "0x{:0>" + str(addrLen) + "} {}"
  i = 0
  while i < len(printLst):
    strValues = " ".join(printLst[i:(i + cols)])
    hexAddr = hex(addrMpy * i)
    print(fmt.format(hexAddr[2:].upper(), strValues))
    i += cols

def _chkStringTreeNode(obj) -> None:
  if (not isinstance(obj, StringTreeNode)):
    msg = "INCORRECT StringTreeNode type: '{}".format(type(obj))
    logger.error(msg)
    raise RuntimeError(msg)

class StringTreeNode(object):
  """
  Object representing a tree node.
  :var name: node name
  :var _lstParents: list of parent nodes
  :var _lstChildren: list of child nodes
  """

  def __init__(self, strVal: str):
    if (isEmptyString(strVal)):
      msg = "INCORRECT node value: '{}".format(strVal)
      logger.error(msg)
      raise RuntimeError(msg)

    self.name: typing.Optional[str] = strVal
    self._lstParents: list[StringTreeNode] = []
    self._lstChildren: list[StringTreeNode] = []

  def __str__(self):
    if (self._lstParents is None):
      return "{} -> {} -> {}".format(None, self.name, self.childNames)
    else:
      return "{} -> {} -> {}".format(self.parentNames, self.name, self.childNames)

  def __lt__(self, other):
    if (other.name is None):
      return False
    else:
      return (self.name < other.name)

  def __gt__(self, other):
    if (other.name is None):
      return True
    else:
      return (self.name > other.name)

  @property
  def parentNodes(self):
    return self._lstParents

  @property
  def parentNames(self) -> list[typing.Optional[str]]:
    return [t.name for t in self._lstParents]

  @property
  def childNodes(self):
    return self._lstChildren

  @property
  def childNames(self) -> list[typing.Optional[str]]:
    return [t.name for t in self._lstChildren]

  def clear(self) -> None:
    self._lstParents = []
    self._lstChildren = []

  def addParent(self, node) -> None:
    _chkStringTreeNode(node)

    #logger.debug("addParent {} <- {} {}".format(self.name, node.name, node in self._lstParents))
    if (node not in self._lstParents):
      self._lstParents.append(node)
      node.addChild(self)
      self._lstParents = sorted(self._lstParents)

  def delParent(self, node) -> None:
    _chkStringTreeNode(node)

    #logger.debug("delParent {} <- {} {}".format(self.name, node.name, node in self._lstParents))
    if (node in self._lstParents):
      self._lstParents.remove(node)
      node.delChild(self)
      self._lstParents = sorted(self._lstParents)

  def addChild(self, node) -> None:
    _chkStringTreeNode(node)

    #logger.debug("addChild {} -> {} {}".format(self.name, node.name, node in self._lstChildren))
    if (node not in self._lstChildren):
      self._lstChildren.append(node)
      node.addParent(self)
      self._lstChildren = sorted(self._lstChildren)

  def addChildName(self, strVal: str) -> None:
    node = StringTreeNode(strVal)

    self.addChild(node)

  def delChild(self, node) -> None:
    _chkStringTreeNode(node)

    #logger.debug("delChild {} -> {} {}".format(self.name, node.name, node in self._lstChildren))
    if (node in self._lstChildren):
      self._lstChildren.remove(node)
      node.delParent(self)
      self._lstChildren = sorted(self._lstChildren)

  def postorder(self):
    """returns the list of child StringTreeNode in post-order"""
    lstNodes = []
    for n in self._lstChildren:
      lstNodes += n.postorder()
    return (lstNodes + [self])

  def postorderNames(self, prefix: str = "") -> list[str]:
    lstNodes = []
    for n in self._lstChildren:
      lstNodes += n.postorderNames(prefix=prefix)
    if (self.name is None):
      return ([(prefix + t) for t in lstNodes] + [prefix])
    else:
      return ([(prefix + t) for t in lstNodes] + [(prefix + self.name)])

  def preorder(self):
    """returns the list of child StringTreeNode in pre-order"""
    lstNodes = []
    for n in self._lstChildren:
      lstNodes += n.preorder()
    return ([self] + lstNodes)

  def preorderNames(self, prefix: str = "") -> list[str]:
    lstNodes = []
    for n in self._lstChildren:
      lstNodes += n.preorderNames(prefix=prefix)
    if (self.name is None):
      return ([prefix] + [(prefix + t) for t in lstNodes])
    else:
      return ([(prefix + self.name)] + [(prefix + t) for t in lstNodes])

class StringTree(object):

  def __init__(self):
    self._root: StringTreeNode = StringTreeNode("root")
    self._root.name = None  # _root is a special node
    self._dictNodes: dict[typing.Optional[str], StringTreeNode] = {}

  def __len__(self):
    return len(self._dictNodes)

  def _chkNodeName(self, strVal: str) -> None:
    if (strVal not in self._dictNodes.keys()):
      msg = "'{}' IS NOT in {}".format(strVal, self.__class__.__name__)
      logger.error(msg)
      raise RuntimeError(msg)

  def clear(self) -> None:
    self._root.clear()
    self._dictNodes = {}

  def addNode(self, strVal: str, strParent: typing.Optional[str] = None) -> None:
    if (strVal in self._dictNodes.keys()):
      node = self._dictNodes[strVal]
    else:
      node = StringTreeNode(strVal)

    if (isEmptyString(strParent)):
      self._root.addChild(node)
    else:
      if (strParent not in self._dictNodes.keys()):
        msg = "'{}' IS NOT in {}".format(strParent, self.__class__.__name__)
        logger.error(msg)
        raise RuntimeError(msg)
      self._dictNodes[strParent].addChild(node)
    self._dictNodes[node.name] = node

  def updateNode(self, strVal: str, strParent: str) -> None:
    """create node 'strVal' as child of 'strParent' or move it if in the tree"""
    self._chkNodeName(strVal)
    self._chkNodeName(strParent)

    node = self._dictNodes[strVal]
    if (strParent in node.parentNames):  # node already has the right parent
      #logger.info("no update necessary to node : {}".format(node.name))
      return

    newParent = self._dictNodes[strParent]
    if (strParent not in node.childNames):  # the nodes are not previously related
      oldParents = node.parentNodes
      if (self._root in oldParents):
        node.delParent(self._root)
      node.addParent(newParent)
    else:
      logger.error("parent is in the child list")
      raise NotImplementedError
      #oldParent = node.parentNodes
      #oldParent.delChild(node)
      #oldParent.addChild(newParent)
      #node.delChild(newParent)
      #node.addParent(newParent)
      #newParent.addParent(oldParent)
      #newParent.addChild(node)

  #def postorder(self):
  #  return self._root.postorder()

  def postorderNames(self) -> list[str]:
    return [t for t in self._root.postorderNames() if (t != "")]  # root node has NO name

  #def preorder(self):
  #  return self._root.preorder()

  def preorderNames(self) -> list[str]:
    return [t for t in self._root.preorderNames() if (t != "")]  # root node has NO name

  def showTree(self) -> None:
    lstVal = self._root.preorderNames(prefix=".")
    for v in lstVal[1:]:
      logger.info(v[2:])

  def getTopNodes(self) -> list[typing.Optional[str]]:
    return self._root.childNames

  def getParents(self, strVal: str):
    self._chkNodeName(strVal)

    node = self._dictNodes[strVal]
    return [t for t in node.parentNames if (t is not None)]

  def getChildren(self, strVal: str) -> list[typing.Optional[str]]:
    self._chkNodeName(strVal)

    node = self._dictNodes[strVal]
    return node.childNames
