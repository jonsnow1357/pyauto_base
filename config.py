#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
# coding: utf-8
"""library for config files"""

from __future__ import annotations  # for python 3.8
#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import typing
import configparser
import xml.etree.ElementTree as ET

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.db

class DictConfig(object):

  def __init__(self):
    self.fPath: str = ""
    self._cfgObj: typing.Optional[configparser.ConfigParser] = None
    self._cfg: dict[str, dict[str, str]] = {}

  @property
  def configuration(self) -> dict[str, dict[str, str]]:
    return self._cfg

  def readCfg(self, fPath: str) -> dict[str, dict[str, str]]:
    pyauto_base.fs.chkPath_File(fPath)
    self.fPath = os.path.abspath(fPath)
    #logger.info(self.fPath)

    self._cfgObj = pyauto_base.misc.readTXT_config(self.fPath)

    self._cfg = {}
    #logger.info(self._cfgObj.sections())
    for sName in self._cfgObj.sections():
      self._cfg[sName] = dict(self._cfgObj.items(sName))
    return self._cfg

  def writeCfg(self, fPath: str, **kwargs) -> None:
    dictCfg = {}
    for k, v in kwargs.items():
      if (isinstance(v, dict)):
        dictCfg[k] = v
    if (len(dictCfg) == 0):
      logger.warning("{} NOTHING to write".format(self.__class__.__name__))
      return

    pyauto_base.misc.writeTXT_config(fPath, dictCfg)
    logger.info("{} written".format(fPath))

class SimpleConfig(object):
  """
  parses a ini/xml configuration file and expects the following sections:
  * [default]
  * [paths]
  * [vars]
  """

  def __init__(self):
    self.fPath: str = ""
    self._cfgObj: typing.Optional[configparser.ConfigParser] = None
    self._domObj: typing.Optional[ET.ElementTree] = None
    self.default: dict[str, str] = {}
    self.path: dict[str, str] = {}
    self.vars: dict[str, str] = {}

  def _hasAttribute(self, attrName: str) -> None:
    if (not hasattr(self, attrName)):
      msg = "{} object needs '{}' atribute".format(self.__class__.__name__, attrName)
      logger.error(msg)
      raise RuntimeError(msg)

  def _hasCfgOption(self,
                    section: str,
                    option: str,
                    bWarn: bool = True,
                    bDie: bool = False) -> bool:
    if (self._cfgObj is None):
      raise RuntimeError
    try:
      self._cfgObj.get(section, option)
    except (configparser.NoSectionError, configparser.NoOptionError):
      if (bWarn):
        msg = "cfg HAS NO section '{}', option '{}'".format(section, option)
        if (bDie):
          logger.error(msg)
          raise RuntimeError(msg)
        else:
          logger.warning(msg)
      return False
    return True

  def _parseXml_default(self) -> None:
    if (self._domObj is None):
      raise RuntimeError

    self.default = {}
    for elXml in self._domObj.findall("default"):
      for el in list(elXml):
        self.default[el.tag] = str(el.text)

  def _parseXml_path(self) -> None:
    if (self._domObj is None):
      raise RuntimeError

    self.path = {}
    for el in self._domObj.findall("path"):
      if (not pyauto_base.misc.chkXMLElementHasAttr(el, "id")):
        continue
      if (not pyauto_base.misc.chkXMLElementHasAttrWithVal(el, "id")):
        continue
      self.path[str(el.get("id"))] = pyauto_base.fs.mkAbsolutePath(str(el.text),
                                                                   rootPath=self.fPath,
                                                                   bExists=False)

  def _parseXml_vars(self) -> None:
    if (self._domObj is None):
      raise RuntimeError

    self.vars = {}
    for el in self._domObj.findall("var"):
      if (not pyauto_base.misc.chkXMLElementHasAttr(el, "name")):
        continue
      if (not pyauto_base.misc.chkXMLElementHasAttrWithVal(el, "name")):
        continue
      self.vars[str(el.attrib["name"])] = str(el.text)

  def parseXml(self) -> None:
    self._parseXml_default()
    self._parseXml_path()
    self._parseXml_vars()

  def loadXmlFile(self, fPath: str) -> None:
    pyauto_base.fs.chkPath_File(fPath)
    self.fPath = os.path.abspath(fPath)
    #logger.info(self.fPath)

    self._domObj = ET.parse(fPath)
    self.parseXml()

  def _parseCfg_default(self) -> None:
    if (self._cfgObj is None):
      raise RuntimeError

    self.default = {}
    if ("default" not in self._cfgObj.sections()):
      return

    if (sys.version_info[0] == 2):
      tmp_items = self._cfgObj.items("default")
    else:
      tmp_items = self._cfgObj["default"].items()
    for k, v in tmp_items:
      if (sys.version_info[0] == 2):
        self.default[k.decode("utf-8")] = v.decode("utf-8")
      else:
        self.default[k] = v

  def _parseCfg_path(self) -> None:
    if (self._cfgObj is None):
      raise RuntimeError

    self.path = {}
    if ("path" not in self._cfgObj.sections()):
      return

    if (sys.version_info[0] == 2):
      tmp_items = self._cfgObj.items("path")
    else:
      tmp_items = self._cfgObj["path"].items()
    for k, v in tmp_items:
      tmp = v.strip(" \"")
      if ((tmp is not None) and (len(tmp) > 0)):
        self.path[k] = pyauto_base.fs.mkAbsolutePath(tmp,
                                                     rootPath=self.fPath,
                                                     bExists=False)
      else:
        logger.warning("key with NO value in [path] '{}'")

  def _parseCfg_vars(self) -> None:
    if (self._cfgObj is None):
      raise RuntimeError

    self.vars = {}
    if ("vars" not in self._cfgObj.sections()):
      return

    if (sys.version_info[0] == 2):
      tmp_items = self._cfgObj.items("vars")
    else:
      tmp_items = self._cfgObj["vars"].items()
    for k, v in tmp_items:
      tmp = v.strip(" \"")
      if ((tmp is not None) and (len(tmp) > 0)):
        self.vars[k] = tmp
      else:
        logger.warning("key with NO value in [vars] '{}'")

  def parseCfg(self) -> None:
    self._parseCfg_default()
    self._parseCfg_path()
    self._parseCfg_vars()

  def loadCfgFile(self, fPath: str) -> None:
    pyauto_base.fs.chkPath_File(fPath)
    self.fPath = os.path.abspath(fPath)
    #logger.info(self.fPath)

    self._cfgObj = pyauto_base.misc.readTXT_config(self.fPath)
    #logger.info(self._cfgObj.sections())
    self.parseCfg()

  def _showInfo_default(self) -> None:
    if (len(self.default) == 0):
      return

    logger.info("-- default ({:d})".format(len(self.default)))
    for k in self.default.keys():
      logger.info("  {:<16} -> {}".format(k, self.default[k]))

  def _showInfo_path(self) -> None:
    if (len(self.path) == 0):
      return

    logger.info("-- path ({:d})".format(len(self.path)))
    for k in sorted(self.path.keys()):
      logger.info("  {:<16} -> {}".format(k, self.path[k]))

  def _showInfo_vars(self) -> None:
    if (len(self.vars) == 0):
      return

    logger.info("-- vars ({:d})".format(len(self.vars)))
    for k in sorted(self.vars.keys()):
      logger.info("  {:<16} -> {}".format(k, self.vars[k]))

  def showInfo(self) -> None:
    self._showInfo_default()
    self._showInfo_path()
    self._showInfo_vars()

class DBConfig(SimpleConfig):
  _dbConfigTags = ("backend", "hostname", "port", "username", "password", "dbname")

  def __init__(self):
    super(DBConfig, self).__init__()
    self.dictDBInfo: dict[str, pyauto_base.db.DBInfo] = {}

  def _parseXml_database(self) -> None:
    if (self._domObj is None):
      raise RuntimeError

    self.dictDBInfo = {}
    for elDB in self._domObj.getroot().findall("database"):
      dictCfg: dict[str, str] = {}
      pyauto_base.misc.chkXMLElementHasAttr(elDB, "id", bDie=True)
      dbId = str(elDB.get("id"))
      #logger.info(dbId)
      if (dbId in self.dictDBInfo):
        msg = "DUPLICATE database id: {}".format(dbId)
        logger.error(msg)
        raise RuntimeError(msg)
      self.dictDBInfo[dbId] = pyauto_base.db.DBInfo()

      for tag in self._dbConfigTags:
        lstElTmp = elDB.findall(tag)
        if ((lstElTmp is None) or (len(lstElTmp) == 0)):
          dictCfg[tag] = ""  # missing tag
        elif (len(lstElTmp) != 1):
          msg = "MULTIPLE tags: {}".format(tag)
          logger.error(msg)
          raise RuntimeError(msg)
        else:
          dictCfg[tag] = str(lstElTmp[0].text)
      self.dictDBInfo[dbId].setInfo(dictCfg)

      lstElQry = elDB.findall("queries")
      if ((lstElQry is not None) and (len(lstElQry) > 0)):
        for el in lstElQry[0].findall("query"):
          pyauto_base.misc.chkXMLElementHasAttr(el, "id", bDie=True)
          pyauto_base.misc.chkXMLElementHasText(el, bDie=True)
          self.dictDBInfo[dbId].queries[str(el.get("id"))] = str(el.text)

      lstElQry = elDB.findall("tables")
      if ((lstElQry is not None) and (len(lstElQry) > 0)):
        for el in lstElQry[0].findall("table"):
          pyauto_base.misc.chkXMLElementHasAttr(el, "id", bDie=True)
          pyauto_base.misc.chkXMLElementHasText(el, bDie=True)
          dbt = pyauto_base.db.DBTable(el.get("id"))
          dbt.colNames = str(el.text).split(",")
          self.dictDBInfo[dbId].addTable(dbt)

    #for filesystem databases make path absolute
    for dbId, dbInfo in self.dictDBInfo.items():
      if (dbInfo.backend in pyauto_base.db.dbBackendFS):
        if (dbInfo.dbname is None):
          raise RuntimeError
        dbInfo.dbname = pyauto_base.fs.mkAbsolutePath(dbInfo.dbname,
                                                      rootPath=self.fPath,
                                                      bExists=False)
        if (dbInfo.backend == pyauto_base.db.dbBackendSqlite):
          pyauto_base.fs.chkPath_File(dbInfo.dbname)
        if (dbInfo.backend == pyauto_base.db.dbBackendCsv):
          pyauto_base.fs.chkPath_Dir(dbInfo.dbname)

  def _parseCfg_database(self) -> None:
    if (self._cfgObj is None):
      raise RuntimeError

    self.dictDBInfo = {}
    for s in self._cfgObj.sections():
      if (not s.startswith("database_")):
        continue

      dictCfg: dict[str, str] = {}
      if (s.endswith("_queries")):
        dbId = s[9:-8]
        if ((dbId is None) or (dbId == "")):
          msg = "NO database id in configuration"
          logger.error(msg)
          raise RuntimeError(msg)
        if (dbId not in self.dictDBInfo.keys()):
          msg = "NO database {} defined".format(dbId)
          logger.error(msg)
          raise RuntimeError(msg)

        for k, v in self._cfgObj.items(s):
          self.dictDBInfo[dbId].queries[k] = v
      elif (s.endswith("_tables")):
        dbId = s[9:-7]
        if ((dbId is None) or (dbId == "")):
          msg = "NO database id in configuration"
          logger.error(msg)
          raise RuntimeError(msg)
        if (dbId not in self.dictDBInfo.keys()):
          msg = "NO database {} defined".format(dbId)
          logger.error(msg)
          raise RuntimeError(msg)

        for k, v in self._cfgObj.items(s):
          dbt = pyauto_base.db.DBTable(k)
          dbt.colNames = v.split(",")
          self.dictDBInfo[dbId].addTable(dbt)
      else:
        dbId = s[9:]
        if ((dbId is None) or (dbId == "")):
          msg = "NO database id in configuration"
          logger.error(msg)
          raise RuntimeError(msg)

        for tag in self._dbConfigTags:
          if (self._cfgObj.has_option(s, tag)):
            dictCfg[tag] = self._cfgObj.get(s, tag)
          else:
            dictCfg[tag] = ""
        self.dictDBInfo[dbId] = pyauto_base.db.DBInfo()
        #logger.info("set values for dbId {}: {}".format(dbId, dictCfg))
        self.dictDBInfo[dbId].setInfo(dictCfg)

    #for filesystem databases make path absolute
    for dbId, dbInfo in self.dictDBInfo.items():
      if (dbInfo.backend in pyauto_base.db.dbBackendFS):
        if (dbInfo.dbname is None):
          raise RuntimeError
        dbInfo.dbname = pyauto_base.fs.mkAbsolutePath(dbInfo.dbname,
                                                      rootPath=self.fPath,
                                                      bExists=False)
        if (dbInfo.backend == pyauto_base.db.dbBackendSqlite):
          pyauto_base.fs.chkPath_File(dbInfo.dbname)
        if (dbInfo.backend == pyauto_base.db.dbBackendCsv):
          pyauto_base.fs.chkPath_Dir(dbInfo.dbname)

  def parseXml(self) -> None:
    super(DBConfig, self).parseXml()
    self._parseXml_database()

  def parseCfg(self) -> None:
    super(DBConfig, self).parseCfg()
    self._parseCfg_database()

  def _showInfo_database(self) -> None:
    logger.info("-- {:d} database(s)".format(len(self.dictDBInfo)))
    for k in sorted(self.dictDBInfo.keys()):
      logger.info("  '{}': {}".format(k, self.dictDBInfo[k]))

  def showInfo(self) -> None:
    super(DBConfig, self).showInfo()
    self._showInfo_database()

class DocConfig(DBConfig):

  def __init__(self):
    super(DocConfig, self).__init__()
    self.estimate: dict[str, list[list[typing.Optional[str]]]] = {}

  def _parseXml_devEstimate(self) -> None:
    if (self._domObj is None):
      raise RuntimeError

    self.estimate = {}
    for elDev in self._domObj.getroot().findall("estimate"):
      pyauto_base.misc.chkXMLElementHasAttr(elDev, "id", bDie=True)
      pyauto_base.misc.chkXMLElementHasAttrWithVal(elDev, "id", bDie=True)
      lstEst = []
      for elTask in elDev.findall("task"):
        pyauto_base.misc.chkXMLElementHasAttr(elTask, "time", bDie=True)
        pyauto_base.misc.chkXMLElementHasAttrWithVal(elTask, "time", bDie=True)
        pyauto_base.misc.chkXMLElementHasText(elTask, bDie=True)
        lstEst.append([elTask.text, elTask.get("time")])
      self.estimate[str(elDev.get("id"))] = lstEst

  def parseXml(self) -> None:
    super(DocConfig, self).parseXml()
    self._parseXml_devEstimate()

  def _showInfo_devEstimate(self) -> None:
    logger.info("-- {:d} estimate(s)".format(len(self.estimate)))
    for k in sorted(self.estimate.keys()):
      logger.info("  '{}': {:d} task(s)".format(k, len(self.estimate[k])))

  def showInfo(self) -> None:
    super(DocConfig, self).showInfo()
    self._showInfo_devEstimate()
