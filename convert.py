#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv

logger = logging.getLogger("lib")

def value2float(strVal):
  """converts a string value to a number"""
  regexFloat = r"[+-]?[0-9]*\.?[0-9]*"

  #logger.info(strVal)
  res = 0.0
  if (re.match(r"^{}p$".format(regexFloat), strVal)):
    res = float(strVal[:-1]) * 1.0e-12
  elif (re.match(r"^{}n$".format(regexFloat), strVal)):
    res = float(strVal[:-1]) * 1.0e-9
  elif (re.match(r"^{}u$".format(regexFloat), strVal)):
    res = float(strVal[:-1]) * 1.0e-6
  elif (re.match(r"^{}m$".format(regexFloat), strVal)):
    res = float(strVal[:-1]) * 1.0e-3
  elif (re.match(r"^{}$".format(regexFloat), strVal)):
    res = float(strVal)
  elif (re.match(r"^{}[kK]$".format(regexFloat), strVal)):
    res = float(strVal[:-1]) * 1.0e3
  elif (re.match(r"^{}M$".format(regexFloat), strVal)):
    res = float(strVal[:-1]) * 1.0e6
  elif (re.match(r"^{}G$".format(regexFloat), strVal)):
    res = float(strVal[:-1]) * 1.0e9
  elif (re.match(r"^{}T$".format(regexFloat), strVal)):
    res = float(strVal[:-1]) * 1.0e12
  else:
    raise ValueError("CANNOT convert {}".format(strVal))
  #logger.info(res)
  return res

def float2value(f, nd=3):
  """converts a number to string value"""
  #logger.info(f)
  sign = 1.0
  if (f < 0.0):
    sign = -1.0
    f = sign * f

  res = ""
  if (f < 1.0e-9):
    res = str(round(f * 1.0e12, nd)) + "p"
  elif ((f >= 1.0e-9) and (f < 1.0e-6)):
    res = str(round(f * 1.0e9, nd)) + "n"
  elif ((f >= 1.0e-6) and (f < 1.0e-3)):
    res = str(round(f * 1.0e6, nd)) + "u"
  elif ((f >= 1.0e-3) and (f < 1.0)):
    res = str(round(f * 1.0e3, nd)) + "m"
  elif ((f >= 1.0) and (f < 1.0e3)):
    res = str(round(f, nd))
  elif ((f >= 1.0e3) and (f < 1.0e6)):
    res = str(round(f * 1.0e-3, nd)) + "k"
  elif ((f >= 1.0e6) and (f < 1.0e9)):
    res = str(round(f * 1.0e-6, nd)) + "M"
  elif ((f >= 1.0e9) and (f < 1.0e12)):
    res = str(round(f * 1.0e-9, nd)) + "G"
  elif ((f >= 1.0e12) and (f < 1.0e15)):
    res = str(round(f * 1.0e-12, nd)) + "T"
  else:
    raise ValueError("CANNOT convert {}".format(f))

  if (sign == -1.0):
    res = "-{}".format(res)
  #logger.info(res)
  return res
