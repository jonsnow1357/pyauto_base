#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

from __future__ import annotations  # for python 3.8
#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import shutil
import stat
import typing
import zipfile
import tarfile

logger = logging.getLogger("lib")

class FSException(Exception):
  pass

def chkPath_File(path: (str | bytes), msg: typing.Optional[str] = "",
                 bDie: bool = True) -> bool:
  """
  :param path:
  :param msg: None - no message, "" - default message, or something else
  :param bDie: raise IOError or not
  :return: [True|False]
  """
  bExists = os.path.isfile(path)
  if (msg == ""):
    msg = "file DOES NOT EXIST: {}".format(path)

  if (not bExists):
    if (bDie):
      if (msg is not None):
        logger.error(msg)
        raise IOError(msg)
    else:
      if (msg is not None):
        logger.warning(msg)

  return bExists

def chkPath_Dir(path: (str | bytes), msg: typing.Optional[str] = "", bDie=True) -> bool:
  """
  :param path:
  :param msg: None - no message, "" - default message, or something else
  :param bDie: raise IOError or not
  :return: [True|False]
  """
  bExists = os.path.isdir(path)
  if (msg == ""):
    msg = "path DOES NOT EXIST: {}".format(path)

  if (not bExists):
    if (bDie):
      if (msg is not None):
        logger.error(msg)
        raise IOError(msg)
    else:
      if (msg is not None):
        logger.warning(msg)

  return bExists

def mkAbsolutePath(path: (str | bytes), rootPath: (str | bytes) = "",
                   bExists: bool = True) -> str:
  if (not os.path.isabs(path)):
    if (path.startswith("WORK")):
      if (sys.platform.startswith("win")):
        path = os.path.join("C:\\", path)
      elif (sys.platform.startswith("linux")):
        path = os.path.join(os.environ["HOME"], path)
      else:
        msg = "UNSUPPORTED platform: {}".format(sys.platform)
        logger.error(msg)
        raise FSException(msg)
    elif (path.startswith("HOME")):
      path = os.path.join(os.environ["HOME"], path[5:])
    else:
      if (os.path.isabs(rootPath) and os.path.isdir(rootPath)):
        path = os.path.join(rootPath, path)
      elif (os.path.isabs(rootPath) and (not os.path.isfile(rootPath))):
        path = os.path.join(os.path.dirname(rootPath), path)
      elif (os.path.isdir(rootPath)):
        path = os.path.join(os.path.abspath(rootPath), path)
      elif (os.path.isfile(rootPath)):
        path = os.path.join(os.path.abspath(os.path.dirname(rootPath)), path)
      else:
        path = os.path.abspath(path)
        #logger.warning("mkAbsolutePath: {}".format(path))

  if (bExists):
    if (not os.path.exists(path)):
      msg = "INCORRECT path: {}".format(path)
      logger.error(msg)
      raise FSException(msg)
  return path

def mkOutFolder(path: (str | bytes) = "data_out", bClear: bool = False) -> str:
  """
  Checks if the folder exists and deletes/creates it if required.
  :param path:
  :param bClear: if folder exists delete it first
  """
  if (os.path.isdir(path)):
    if (bClear):
      logger.info("RM -R {}".format(path))
      shutil.rmtree(path)
    else:
      return path

  os.makedirs(path)
  msg = "CANNOT create folder: {}".format(os.path.abspath(path))
  chkPath_Dir(path, msg=msg)
  return path

def _get_ts(bSec: bool) -> str:
  dt = datetime.datetime.now()
  if (bSec):
    return dt.strftime("%Y-%m-%dT%H%M%S")
  else:
    return dt.strftime("%Y-%m-%dT%H%M")

def _addTS_file(path: str, bSec: bool) -> str:
  tmp = path.split(".")
  if (len(tmp) < 2):
    return path + "_" + _get_ts(bSec)
  tmp[-2] += ("_" + _get_ts(bSec))
  return ".".join(tmp)

def _addTS_folder(path: str, bSec: bool) -> str:
  return path + "_" + _get_ts(bSec)

def addTS(path: str, bSec: bool = False) -> str:
  if (os.path.isfile(path)):
    return _addTS_file(path, bSec)
  elif (os.path.isdir(path)):
    return _addTS_folder(path, bSec)
  else:
    raise FSException

def _rm_file(path: str, bShowOnly: bool) -> None:
  logger.info("RM {}".format(path))
  if (not bShowOnly):
    os.remove(path)

def _rm_file_ro(action, name, exc):
  os.chmod(name, stat.S_IWRITE)
  os.remove(name)

def _rm_folder(path: str, bShowOnly: bool) -> None:
  logger.info("RM -R {}".format(path))
  if (not bShowOnly):
    shutil.rmtree(path, onerror=_rm_file_ro)

def rm(path: str, bShowOnly: bool = False) -> None:
  if (os.path.isfile(path)):
    _rm_file(path, bShowOnly)
  elif (os.path.isdir(path)):
    _rm_folder(path, bShowOnly)
  else:
    raise RuntimeError

def _mkdir_dst_file(pathDst: str) -> None:
  """
  Helper function for _[cp|mv|zip|tar|gztar]_file
  :param pathDst: absolute path
  :return:
  """
  if (not os.path.exists(pathDst)):
    tmp = os.path.dirname(pathDst)
    if (not os.path.exists(tmp)):
      os.makedirs(tmp)

def _cp_file(pathSrc: str, pathDst: str, bTS: bool, bSec: bool, bShowOnly: bool) -> None:
  """
  :param pathSrc: absolute path to a file
  :param pathDst: absolute path
  :param bTS:
  :param bSec:
  :return:
  """
  _mkdir_dst_file(pathDst)
  if (os.path.isdir(pathDst)):
    pathDst = os.path.join(pathDst, os.path.basename(pathSrc))

  if (bTS):
    pathDst = _addTS_file(pathDst, bSec)

  logger.info("CP '{}' '{}'".format(pathSrc, pathDst))
  if (not bShowOnly):
    shutil.copyfile(pathSrc, pathDst)

def _cp_folder(pathSrc: str, pathDst: str, bTS: bool, bSec: bool, bShowOnly: bool) -> None:
  """
  :param pathSrc: absolute path to a folder
  :param pathDst: absolute path to a folder
  :param bTS:
  :param bSec:
  :return:
  """
  if (os.path.exists(pathDst)):
    bDstExists = True
  else:
    bDstExists = False
    if (bTS):
      pathDst = _addTS_folder(pathDst, bSec)

  logger.info("CP -R '{}' '{}'".format(pathSrc, pathDst))
  if (not bShowOnly):
    if sys.version_info >= (3, 8):
      shutil.copytree(pathSrc, pathDst, dirs_exist_ok=bDstExists)
    else:
      shutil.copytree(pathSrc, pathDst)

def cp(pathSrc: str,
       pathDst: str,
       bTS: bool = False,
       bSec: bool = False,
       bShowOnly: bool = False) -> None:
  path_dst = mkAbsolutePath(pathDst, bExists=False)
  path_src = mkAbsolutePath(pathSrc)

  if (os.path.isfile(path_src)):
    _cp_file(path_src, path_dst, bTS, bSec, bShowOnly)
  elif (os.path.isdir(path_src)):
    _cp_folder(path_src, path_dst, bTS, bSec, bShowOnly)
  else:
    raise RuntimeError

def cp_list(lstPathSrc: list[str],
            pathDst: str,
            bTS: bool = False,
            bSec: bool = False,
            bShowOnly: bool = False):
  path_dst = mkAbsolutePath(pathDst, bExists=False)

  for path in lstPathSrc:
    path_src = mkAbsolutePath(path)
    cp(path_src, path_dst, bTS, bSec, bShowOnly)

def _mv_file(pathSrc: str, pathDst: str, bTS: bool, bSec: bool, bShowOnly: bool) -> None:
  _mkdir_dst_file(pathDst)
  if (os.path.isdir(pathDst)):
    pathDst = os.path.join(pathDst, os.path.basename(pathSrc))

  if (bTS):
    pathDst = _addTS_file(pathDst, bSec)

  logger.info("MV '{}' '{}'".format(pathSrc, pathDst))
  if (not bShowOnly):
    shutil.move(pathSrc, pathDst)

def _mv_folder(pathSrc: str, pathDst: str, bTS: bool, bSec: bool, bShowOnly: bool) -> None:
  if (not os.path.exists(pathDst)):
    os.makedirs(pathDst)

  if (bTS):
    pathDst = _addTS_folder(pathDst, bSec)

  logger.info("MV -R '{}' '{}'".format(pathSrc, pathDst))
  if (not bShowOnly):
    shutil.move(pathSrc, pathDst)

def mv(pathSrc: str,
       pathDst: str,
       bTS: bool = False,
       bSec: bool = False,
       bShowOnly: bool = False) -> None:
  path_dst = mkAbsolutePath(pathDst, bExists=False)
  path_src = mkAbsolutePath(pathSrc)

  if (os.path.isfile(path_src)):
    _mv_file(path_src, path_dst, bTS, bSec, bShowOnly)
  elif (os.path.isdir(path_src)):
    _mv_folder(path_src, path_dst, bTS, bSec, bShowOnly)
  else:
    raise RuntimeError

def mv_list(lstPathSrc: list[str],
            pathDst: str,
            bTS: bool = False,
            bSec: bool = False,
            bShowOnly: bool = False):
  path_dst = mkAbsolutePath(pathDst, bExists=False)

  for path in lstPathSrc:
    path_src = mkAbsolutePath(path)
    mv(path_src, path_dst, bTS, bSec, bShowOnly)

def _zip_file(pathSrc: str, pathDst: str, bTS: bool, bSec: bool, bShowOnly: bool) -> None:
  _mkdir_dst_file(pathDst)
  if (os.path.isdir(pathDst)):
    tmp = os.path.basename(pathSrc).split(".")
    if (len(tmp) == 1):
      pathDst = os.path.join(pathDst, (tmp[0] + ".zip"))
    else:
      pathDst = os.path.join(pathDst, ".".join(tmp[:-1] + ["zip"]))

  if (bTS):
    pathDst = _addTS_file(pathDst, bSec)

  logger.info("ZIP {} {}".format(pathSrc, pathDst))
  if (not bShowOnly):
    with zipfile.ZipFile(pathDst, mode="w", compression=zipfile.ZIP_DEFLATED) as zipf:
      zipf.write(pathSrc, arcname=os.path.basename(pathSrc))

def _zip_folder(pathSrc: str, pathDst: str, bTS: bool, bSec: bool, bShowOnly: bool) -> None:
  if (not os.path.exists(pathDst)):
    os.makedirs(pathDst)
  if (os.path.isdir(pathDst)):
    pathDst = os.path.join(pathDst, os.path.basename(pathSrc))

  if (bTS):
    pathDst = _addTS_file(pathDst, bSec)

  logger.info("ZIP -R {} {}.zip".format(pathSrc, pathDst))
  if (not bShowOnly):
    shutil.make_archive(base_name=pathDst,
                        format="zip",
                        root_dir=os.path.dirname(pathSrc),
                        base_dir=os.path.basename(pathSrc),
                        logger=logger)

def mkZip(pathSrc: str,
          pathDst: str,
          bTS: bool = False,
          bSec: bool = False,
          bShowOnly: bool = False) -> None:
  path_dst = mkAbsolutePath(pathDst, bExists=False)
  path_src = mkAbsolutePath(pathSrc)

  if (os.path.isfile(path_src)):
    _zip_file(path_src, path_dst, bTS, bSec, bShowOnly)
  elif (os.path.isdir(path_src)):
    _zip_folder(path_src, path_dst, bTS, bSec, bShowOnly)
  else:
    raise RuntimeError

def _tar_file(pathSrc: str, pathDst: str, bTS: bool, bSec: bool, bShowOnly: bool) -> None:
  _mkdir_dst_file(pathDst)
  if (os.path.isdir(pathDst)):
    tmp = os.path.basename(pathSrc).split(".")
    if (len(tmp) == 1):
      pathDst = os.path.join(pathDst, (tmp[0] + ".tar"))
    else:
      pathDst = os.path.join(pathDst, ".".join(tmp[:-1] + ["tar"]))

  if (bTS):
    pathDst = _addTS_file(pathDst, bSec)

  logger.info("TAR {} {}".format(pathSrc, pathDst))
  if (not bShowOnly):
    with tarfile.open(pathDst, mode="w") as tarf:
      tarf.add(pathSrc, arcname=os.path.basename(pathSrc))

def _tar_folder(pathSrc: str, pathDst: str, bTS: bool, bSec: bool, bShowOnly: bool) -> None:
  if (not os.path.exists(pathDst)):
    os.makedirs(pathDst)
  if (os.path.isdir(pathDst)):
    pathDst = os.path.join(pathDst, os.path.basename(pathSrc))

  if (bTS):
    pathDst = _addTS_file(pathDst, bSec)

  logger.info("TAR -R {} {}.tar".format(pathSrc, pathDst))
  if (not bShowOnly):
    shutil.make_archive(base_name=pathDst,
                        format="tar",
                        root_dir=os.path.dirname(pathSrc),
                        base_dir=os.path.basename(pathSrc),
                        logger=logger)

def mkTar(pathSrc: str,
          pathDst: str,
          bTS: bool = False,
          bSec: bool = False,
          bShowOnly: bool = False) -> None:
  path_dst = mkAbsolutePath(pathDst, bExists=False)
  path_src = mkAbsolutePath(pathSrc)

  if (os.path.isfile(path_src)):
    _tar_file(path_src, path_dst, bTS, bSec, bShowOnly)
  elif (os.path.isdir(path_src)):
    _tar_folder(path_src, path_dst, bTS, bSec, bShowOnly)
  else:
    raise RuntimeError

def _gztar_file(pathSrc: str, pathDst: str, bTS: bool, bSec: bool, bShowOnly: bool):
  _mkdir_dst_file(pathDst)
  if (os.path.isdir(pathDst)):
    tmp = os.path.basename(pathSrc).split(".")
    if (len(tmp) == 1):
      pathDst = os.path.join(pathDst, (str(tmp) + ".tar.gz"))
    else:
      pathDst = os.path.join(pathDst, ".".join(tmp[:-1] + ["tar", "gz"]))

  if (bTS):
    pathDst = _addTS_file(pathDst, bSec)

  logger.info("TGZ {} {}".format(pathSrc, pathDst))
  if (not bShowOnly):
    with tarfile.open(pathDst, mode="w:gz") as tarf:
      tarf.add(pathSrc, arcname=os.path.basename(pathSrc))

def _gztar_folder(pathSrc: str, pathDst: str, bTS: bool, bSec: bool,
                  bShowOnly: bool) -> None:
  if (not os.path.exists(pathDst)):
    os.makedirs(pathDst)
  if (os.path.isdir(pathDst)):
    pathDst = os.path.join(pathDst, os.path.basename(pathSrc))

  if (bTS):
    pathDst = _addTS_file(pathDst, bSec)

  logger.info("TGZ -R {} {}.tar.gz".format(pathSrc, pathDst))
  if (not bShowOnly):
    shutil.make_archive(base_name=pathDst,
                        format="gztar",
                        root_dir=os.path.dirname(pathSrc),
                        base_dir=os.path.basename(pathSrc),
                        logger=logger)

def mkGztar(pathSrc: str,
            pathDst: str,
            bTS: bool = False,
            bSec: bool = False,
            bShowOnly: bool = False) -> None:
  path_dst = mkAbsolutePath(pathDst, bExists=False)
  path_src = mkAbsolutePath(pathSrc)

  if (os.path.isfile(path_src)):
    _gztar_file(path_src, path_dst, bTS, bSec, bShowOnly)
  elif (os.path.isdir(path_src)):
    _gztar_folder(path_src, path_dst, bTS, bSec, bShowOnly)
  else:
    raise RuntimeError

def getModulePath(modName: str) -> str:
  import inspect
  import importlib

  mod = importlib.import_module(modName)
  res = os.path.dirname(inspect.getfile(mod))
  #print("DBG", res)
  return res
