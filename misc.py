#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

from __future__ import annotations  # for python 3.8
from __future__ import print_function  # needed because of waitWithPrint()
import datetime
#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import typing
import xml.etree.ElementTree as ET

logger = logging.getLogger("lib")
from ._misc_math import *
from ._misc_string import *
from ._misc_file import *

def _convertTimestampToUTC_pytz(dt: datetime.datetime) -> datetime.datetime:
  import pytz

  if (time.tzname[1] in ("EDT", "Eastern Daylight Time")):
    lcl_tz = pytz.timezone("EST5EDT")
  else:
    lcl_tz = pytz.timezone(time.tzname[-1])
  lcl_dt = lcl_tz.normalize(lcl_tz.localize(dt, is_dst=bool(time.daylight)))
  #print("DBG", lcl_dt)
  utc_dt = lcl_dt.astimezone(pytz.utc)
  #print("DBG", [utc_dt, type(utc_dt)])
  return utc_dt

def _convertTimestampToUTC_arrow(dt: datetime.datetime) -> datetime.datetime:
  import arrow
  import dateutil.tz

  lcl_tz = dateutil.tz.tzlocal()
  lcl_dt = arrow.get(dt, lcl_tz)
  #print("DBG", lcl_dt)
  utc_dt = lcl_dt.to("UTC")
  #print("DBG", [utc_dt, type(utc_dt)])
  return utc_dt.datetime

def convertTimestampToUTC(dt: datetime.datetime) -> datetime.datetime:
  """
  :param dt: datetime (assumed to be local time)
  :return: datetime (UTC)
  """
  utc_dt = None
  try:
    import pytz
    utc_dt = _convertTimestampToUTC_pytz(dt)
  except ImportError:
    try:
      import arrow
      utc_dt = _convertTimestampToUTC_arrow(dt)
    except ImportError:
      msg = "CANNOT convert timestamp to UTC"
      logger.error(msg)
      raise RuntimeError(msg)

  return utc_dt

def getDate(bUTC: bool = False) -> str:
  """
  :return: the current date
  """
  dt = datetime.datetime.now()
  if (bUTC):
    dt = convertTimestampToUTC(dt)
  return dt.strftime("%Y-%m-%d")

def getTime(bUTC: bool = False) -> str:
  """
  :return: the current time
  """
  dt = datetime.datetime.now()
  if (bUTC):
    dt = convertTimestampToUTC(dt)
  return dt.strftime("%H:%M:%S.%f")

def getDateTime(bUTC: bool = False) -> str:
  """
  :return: the current date and time (safe to be used in file names)
  """
  dt = datetime.datetime.now()
  if (bUTC):
    dt = convertTimestampToUTC(dt)
    return dt.strftime("%Y-%m-%dT%H%M%SZ")
  else:
    return dt.strftime("%Y-%m-%dT%H%M%S")

def getTimestamp(bUTC: bool = False) -> str:
  """
  :return: the current date and time in ISO 8601 like format
  """
  dt = datetime.datetime.now()
  if (bUTC):
    dt = convertTimestampToUTC(dt)
    return str(re.sub(r"\.[0-9+:]+$", "Z", dt.isoformat()))
  else:
    return str(re.sub(r"\.[0-9+:]+$", "", dt.isoformat()))

def getTimestamp_4mysql() -> str:
  """
  :return: the current date and time as formatted by MySQL
  """
  return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

def waitWithPrint(n: int, to: int = 1) -> None:
  """
  wait for n*to seconds
  :param n:
  :param to: 1 (default)
  :return:
  """
  print("... waiting for {:d}s".format(n * to), end="")
  sys.stdout.flush()
  for i in range(n, 0, -1):
    time.sleep(to)
    print(" {:d}s".format((i - 1) * to), end="")
    sys.stdout.flush()
  print()

# def chkEqualLists(lst, lstRef):
#   setDiff = set(lstRef) - set(lst)
#   if (setDiff != set([])):
#     logger.error("list mismatch")
#     logger.error("ref  list: " + str(lstRef))
#     logger.error("test list: " + str(lst))
#     logger.error("differences: " + str(setDiff))
#     return False
#   return True

def getListIndex(lst: list[str]) -> dict[str, int]:
  """
  Given a list of strings, returns a dictionary that matches the string
  with its index in the list
  Example:
    lst = ["foo", "bar", "george", "jungle"]
    return {"foo": 0, "george": 2, "bar": 1, "jungle": 3}

  :param lst: list of strings
  :return: dictionary
  """
  res = {}
  for t in lst:
    res[t] = lst.index(t)
  #logger.info(res)
  return res

def getListIndexByRegex(lst: list[str], dictRegex: dict[str, str]) -> dict[str, int]:
  """
  Given a list of strings and a dictionary of regular expressions,
  returns a dictionary that identifies strings that match the regular expressions -> index
  Example:
    lst = ["foo", "bar", "george", "jungle"]
    dictRegex = {"regex1": "fo*", "regex2": "jungle"}
    return {"regex1":0, "regex2":3}

  :param lst: list of names
  :param dictRegex: dictionary of regular expressions that should match the names
  :return: dictionary
  """
  if ((dictRegex is None) or (len(dictRegex) == 0)):
    msg = "dictRegex is empty"
    logger.error(msg)
    raise RuntimeError(msg)

  #logger.info(lst)
  #logger.info(dictRegex)
  res = {}

  for i, v in enumerate(lst):
    for k in dictRegex.keys():
      if (re.match(dictRegex[k], v)):
        res[k] = i

  #logger.info(res)
  return res

def getListIndexByName(lst: list[str],
                       lstRequired: list[str],
                       lstOptional: typing.Optional[list[str]] = None) -> dict[str, int]:
  """
  Given a list of strings and a list of strings to search,
  returns a dictionary that identifies string to search -> index
  Example:
    lst = ["foo", "bar", "george", "jungle"]
    lstRequired = ["foo", "george"]
    lstOptional = ["bar, "good", "evil"]
    return {"foo":0, "george":2, "bar":1}

  Parameters:
    :param lst: list of strings
    :param lstRequired: list of strings to search that are required to be found
    :param lstOptional: list of strings to search that are optional
    :return: dictionary
  """
  if ((lstRequired is None) or (len(lstRequired) == 0)):
    msg = "lstRequired is empty"
    logger.error(msg)
    raise RuntimeError(msg)

  #logger.info(lst)
  #logger.info(lstRequired)
  #logger.info(lstOptional)

  if (lstOptional is None):
    lstOptional = []
  tmp = set(lstRequired) & set(lstOptional)
  if (len(tmp) != 0):
    msg = "common elements in both lists: {}".format(tmp)
    logger.error(msg)
    raise RuntimeError(msg)

  res = {}
  for t in lstRequired:
    res[t] = -1
  #for t in lstOptional:
  #  res[t] = -1
  #logger.info(res)

  for i, v in enumerate(lst):
    if (v in lstRequired):
      res[v] = i
    if (v in lstOptional):
      res[v] = i
  #logger.info(res)

  for t in lstRequired:
    if (res[t] == -1):
      msg = "missing element: {}".format(t)
      logger.error(msg)
      raise RuntimeError(msg)
  return res

def hasAllKeys(dictObj: dict, lstKeys: list, bEmptyValues: bool = True) -> bool:
  """
  Checks that the dictionary has all the keys in the list and optionally if the values are empty.
  :param dictObj:
  :param lstKeys:
  :param bEmptyValues:
  :return: True|False
  """
  for k in lstKeys:
    if (k not in dictObj.keys()):
      logger.warning("key '{}' NOT FOUND in dictionary".format(k))
      return False
    if (not bEmptyValues):
      if ((dictObj[k] is None) or (len(dictObj[k]) == 0)):
        logger.warning("key '{}' with EMPTY VALUE in dictionary".format(k))
        return False
  return True

# def compareLists(lst1, lst2):
#   #logger.info("{} {}".format(type(lst1), type(lst2)))
#   if ((not isinstance(lst1, list)) and (not isinstance(lst1, set))):
#     return -2
#   if ((not isinstance(lst2, list)) and (not isinstance(lst2, set))):
#     return -2
#
#   diff12 = set(lst1) - set(lst2)
#   diff21 = set(lst2) - set(lst1)
#
#   #logger.info("{} {}".format(diff12, diff21))
#   if ((len(diff12) == 0) and (len(diff21) == 0)):
#     return 0
#   elif (len(diff12) > len(diff21)):
#     return 1
#   elif (len(diff21) > len(diff12)):
#     return -1
#   else:
#     return 2

# def checkClassAttributes(obj, lstExpected):
#   """
#   :param obj: class instance
#   :param lstExpected: list of expected attribute names
#   :return: [True|False] if any element in lstExpected is not in obj.__dict__.keys()
#   """
#   if (not isinstance(lstExpected, list)):
#     msg = "INCORRECT type: {}".format(type(lstExpected))
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   res = True
#   lstAttrs = list(obj.__dict__.keys())
#   for s in lstExpected:
#     if (s not in lstAttrs):
#       logger.error("{} NOT in attributes".format(s))
#       res = False
#   return res
#
# def checkAllClassAttributes(obj, setExpected):
#   """
#   :param obj: class instance
#   :param setExpected: set of expected attribute names
#   :return: [True|False] if setNames is identical with obj.__dict__.keys()
#   """
#   if (isinstance(setExpected, list)):
#     setExpected = set(setExpected)
#   elif (not isinstance(setExpected, set)):
#     msg = "INCORRECT type: {}".format(type(setExpected))
#     logger.error(msg)
#     raise RuntimeError(msg)
#
#   setAttrs = set(obj.__dict__.keys())
#   cnt = len(setExpected - setAttrs)
#   if (cnt != 0):
#     logger.error("{} HAS NO attributes: {}".format(obj.__class__.__name__,
#                                                    (setExpected - setAttrs)))
#     return False
#   cnt = len(setAttrs - setExpected)
#   if (cnt != 0):
#     logger.error("{} HAS EXTRA attributes: {}".format(obj.__class__.__name__,
#                                                       (setAttrs - setExpected)))
#     return False
#
#   return True

def chkXMLElementHasTag(el: ET.Element,
                        strTag: str,
                        bWarn: bool = True,
                        bDie: bool = False) -> bool:
  if (el.find(strTag) is None):
    if (bWarn):
      msg = "tag <{}> HAS NO element '{}'".format(el.tag, strTag)
      if (bDie):
        logger.error(msg)
        raise RuntimeError(msg)
      else:
        logger.warning(msg)
    return False
  return True

def chkXMLElementHasAttr(el: ET.Element,
                         strAttr: str,
                         bWarn: bool = True,
                         bDie: bool = False) -> bool:
  if (strAttr not in el.keys()):
    if (bWarn):
      msg = "tag <{}> HAS NO attribute '{}'".format(el.tag, strAttr)
      if (bDie):
        logger.error(msg)
        raise RuntimeError(msg)
      else:
        logger.warning(msg)
    return False
  return True

def chkXMLElementHasAttrWithVal(el: ET.Element,
                                strAttr: str,
                                bWarn: bool = True,
                                bDie: bool = False) -> bool:
  if ((el.get(strAttr) is None) or (el.get(strAttr) == "")):
    if (bWarn):
      msg = "tag <{}> HAS NO attribute with value '{}'".format(el.tag, strAttr)
      if (bDie):
        logger.error(msg)
        raise RuntimeError(msg)
      else:
        logger.warning(msg)
      return False
  return True

def chkXMLElementHasText(el: ET.Element, bWarn: bool = True, bDie: bool = False) -> bool:
  if ((el.text is None) or (el.text == "")):
    if (bWarn):
      msg = "tag <{}> HAS NO text".format(el.tag)
      if (bDie):
        logger.error(msg)
        raise RuntimeError(msg)
      else:
        logger.warning(msg)
    return False
  return True

def changeLoggerName(fPath: str) -> None:
  if (isEmptyString(fPath)):
    return

  for h in logger.handlers:
    if (isinstance(h, logging.FileHandler)):
      h.flush()
      h.baseFilename = os.path.join(os.path.dirname(h.baseFilename), fPath)
      h.close()
      logger.info("log CHANGED to: {}".format(fPath))

def copyLoggerFile(fPath: str) -> None:
  """
  copy the first logger FileHandler to another location
  :param fPath:
  :return:
  """
  if (isEmptyString(fPath)):
    return

  import shutil

  for h in logger.handlers:
    if (isinstance(h, logging.FileHandler)):
      h.flush()
      shutil.copy(h.baseFilename, fPath)
      logger.info("log CP: {} -> {}".format(h.baseFilename, fPath))
      break

def moveLoggerFile(fPath: str) -> None:
  """
  move the first logger FileHandler to another location
  :param fPath:
  :return:
  """
  if (isEmptyString(fPath)):
    return

  import shutil

  for h in logger.handlers:
    if (isinstance(h, logging.FileHandler)):
      h.flush()
      h.close()
      shutil.move(h.baseFilename, fPath)
      logger.info("log MV: {} -> {}".format(h.baseFilename, fPath))
      break

def runScriptMainApp(modName,
                     objLog,
                     appDir=None,
                     appCfgPath=None,
                     dictCLIArgs=None,
                     dictObjGlobal=None,
                     dly=0):
  if (isEmptyString(modName)):
    return

  import importlib

  mod = importlib.import_module(modName)
  #logger.info("== running {}".format(mod.__name__))
  mod.modName = ".".join(os.path.basename(mod.__file__).split(".")[:-1])  # type: ignore
  mod.logger = objLog  # type: ignore[attr-defined]

  if (appDir is not None):
    mod.appDir = appDir  # type: ignore[attr-defined]
    #logger.info("(script {}) overwrite appDir".format(modName))
  #   #else:
  #  if(hasattr(mod, "appDir")):
  #    logger.info("(script {}) has appDir".format(modName))
  #  else:
  #    logger.info("(script {}) has NO appDir".format(modName))

  if (appCfgPath is not None):
    mod.appCfgPath = appCfgPath  # type: ignore[attr-defined]
    #logger.info("(script {}) overwrite appCfgPath".format(modName))
  #else:
  #  if(hasattr(mod, "appCfgPath")):
  #    logger.info("(script {}) has appCfgPath".format(modName))
  #  else:
  #    logger.info("(script {}) has NO appCfgPath".format(modName))

  if (dictCLIArgs is not None):
    mod.cliArgs = dictCLIArgs  # type: ignore[attr-defined]

  if (dictObjGlobal is None):
    dictObjGlobal = {}
  if (hasattr(mod, "appCfg") and ("appCfg" not in dictObjGlobal.keys())):
    #logger.info("creating new appCfg")
    setattr(mod, "appCfg", mod.AppConfig())
  if (hasattr(mod, "appParams") and ("appParams" not in dictObjGlobal.keys())):
    #logger.info("creating new appParams")
    setattr(mod, "appParams", mod.AppParameters())
  for k, obj in dictObjGlobal.items():
    if (hasattr(mod, k)):
      #logger.info("(script {}) overwrite global '{}' with class {}".format(
      #    modName, k, dictObjGlobal[k].__class__.__name__))
      setattr(mod, k, dictObjGlobal[k])
    else:
      msg = "(script {}) CANNOT overwrite global '{}'".format(modName, k)
      logger.error(msg)
      raise RuntimeError(msg)

  res = mod.mainApp()
  if (dly > 0):
    waitWithPrint(dly)
  return res

class StatCounter(object):

  def __init__(self):
    self._total: int = 0
    self._counters: dict[str, int] = {}

  def __str__(self):
    return "{}: {}".format(self.__class__.__name__, [
        "{} {}/{}".format(k, self._counters[k], self._total)
        for k in sorted(self._counters.keys())
    ])

  @property
  def total(self) -> int:
    return self._total

  def reset(self, hard: bool = False) -> None:
    self._total = 0
    if (hard):
      self._counters = {}
    else:
      for k in self._counters.keys():
        self._counters[k] = 0

  def inc(self, strCategory: str) -> None:
    if (strCategory not in self._counters.keys()):
      self._counters[strCategory] = 1
    else:
      self._counters[strCategory] += 1
    self._total += 1

  def get(self, strCategory: str) -> typing.Optional[list[int]]:
    if (strCategory not in self._counters.keys()):
      logger.warning("NOT a category: {}".format(strCategory))
      return None
    return [self._counters[strCategory], self._total]

  def show(self, withTotal: bool = False) -> None:
    logger.info("== stats:")
    for k, v in self._counters.items():
      if (withTotal):
        logger.info("  {}: {:d}/{:d}".format(k, v, self._total))
      else:
        logger.info("  {}: {:d}".format(k, v))

  def test(self, strCategory: str) -> bool:
    if (self._total > 0):
      if (self._counters[strCategory] == self._total):
        return True

    return False
