@require(dictEnv, lstDoc)
@@ECHO OFF

@for k in dictEnv.keys():
set @k="@dictEnv[k]"
@end

@for d in lstDoc:

set DOC_NAME="@d['name']"
set DOC_NO="@d['no']"
set DOC_VER="@d['ver']"
@if d['conv']['MD2HTML'] is True:
echo "== convert MD -> HTML"
pandoc -s --normalize --number-sections --toc --data-dir=%PANDOC_DIR% -c local.css --template=local %DOC_NAME%.md -o %DOC_NAME%.html
echo "== done"
@endif
@if d['conv']['MD2DOCX'] is True:
echo "== convert MD -> DOCX"
pandoc -s --normalize --reference-docx="%PANDOC_DIR%/reference.docx" %DOC_NAME%.md -o %DOC_NAME%.docx
echo "== done"
@endif
@if d['conv']['MD2PDF'] is True:
echo "== convert MD -> PDF"
pandoc -s --normalize --number-sections --toc --data-dir=%DATA_DIR% -c local.css --template=local -t html5 %DOC_NAME%.md -o %DOC_NAME%.pdf
echo "== done"
@endif
@if d['conv']['HTML2PDF'] is True:
echo "== convert HTML -> PDF"
wkhtmltopdf -O landscape --enable-local-file-access --print-media-type --margin-top 22 --margin-bottom 10 --header-html %WKHTML_DIR%/header.html --header-spacing 8 --footer-html %WKHTML_DIR%/footer.html --replace docno %DOC_NO% --replace docver %DOC_VER% %DOC_NAME%.html %DOC_NAME%.pdf
echo "== done"
@endif
@end

@@ECHO ON
