#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library for database abstraction"""

from __future__ import annotations  # for python 3.8
#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv
import typing
import sqlite3

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc

dbBackendSqlite = "sqlite"
dbBackendMysql = "mysql"
dbBackendTsql = "t-sql"
dbBackendCsv = "csv"
dbBackendNames = (
    dbBackendSqlite,
    dbBackendMysql,
    dbBackendTsql,
    dbBackendCsv,
)
dbBackendFS = (
    dbBackendSqlite,
    dbBackendCsv,
)

class DBException(Exception):
  pass

class DBTable(object):

  def __init__(self, strName):
    if (pyauto_base.misc.isEmptyString(strName)):
      msg = "CANNOT set empty table name"
      logger.error(msg)
      raise RuntimeError(msg)

    self.name: str = strName
    self.colNames: list[str] = []

#storm (https://storm.canonical.com/Manual) naming convention for reference:
#  backend://username:password@hostname:port/database_name
class DBInfo(object):

  def __init__(self):
    self._backend: str = ""
    self._host: str = ""
    self._port: str = ""
    self._user: str = ""
    self._pswd: str = ""
    self._dbname: str = ""
    self._tables: dict[str, DBTable] = {}
    self.queries: dict[str, str] = {}

  def __str__(self):
    if (pyauto_base.misc.isEmptyString(self._backend)):
      res = "{} -".format(self.__class__.__name__)
    else:
      res = "{} {}://{}:{}@{}:{}/{} ({} qry, {} tbl)".format(
          self.__class__.__name__, self._backend, "" if
          (self._user is None) else self._user, "" if (self._pswd is None) else self._pswd,
          "" if (self._host is None) else self._host, "" if
          (self._port is None) else self._port, self._dbname, len(self.queries),
          len(self._tables))
    return res

  @property
  def backend(self) -> str:
    return self._backend

  @property
  def host(self) -> str:
    return self._host

  @property
  def port(self) -> str:
    return self._port

  @property
  def user(self) -> str:
    return self._user

  @property
  def pswd(self) -> str:
    return self._pswd

  @property
  def dbname(self) -> str:
    return self._dbname

  @dbname.setter
  def dbname(self, val: str) -> None:
    self._dbname = val

  def setInfo(self, dictCfg: dict[str, str]) -> None:
    #logger.info(dictCfg)
    self._backend = ""
    self._host = ""
    self._port = ""
    self._user = ""
    self._pswd = ""
    self._dbname = ""

    if (dictCfg["backend"] not in dbBackendNames):
      msg = "INCORRECT backend: {}".format(dictCfg["backend"])
      logger.error(msg)
      raise DBException(msg)
    self._backend = dictCfg["backend"]

    if (self._backend not in dbBackendFS):
      if (pyauto_base.misc.isEmptyString(dictCfg["hostname"])):
        msg = "UNSPECIFIED hostname"
        logger.error(msg)
        raise DBException(msg)
      self._host = dictCfg["hostname"]

      if (("port" not in dictCfg.keys()) or (dictCfg["port"] == "")):
        if (self._backend == dbBackendMysql):
          self._port = "3306"
        elif (self._backend == dbBackendTsql):
          self._port = "1433"
        else:
          msg = "UNKNOWN port for {}".format(self._backend)
          logger.error(msg)
          raise DBException(msg)
      else:
        self._port = dictCfg["port"]

      if ("username" in dictCfg.keys()):
        self._user = dictCfg["username"]

      if ("password" in dictCfg.keys()):
        self._pswd = dictCfg["password"]

    if ("dbname" in dictCfg.keys()):
      self._dbname = dictCfg["dbname"]

  @property
  def nTables(self) -> int:
    return len(self._tables)

  @property
  def tableNames(self) -> list:
    return sorted(self._tables.keys())

  def clrTables(self) -> None:
    self._tables = {}

  def addTable(self, obj: DBTable) -> None:
    if (not issubclass(obj.__class__, DBTable)):
      msg = "CANNOT add UNSUPPORTED object: {}".format(type(obj))
      logger.error(msg)
      raise RuntimeError(msg)

    if (obj.name in self._tables.keys()):
      msg = "CANNOT add Existing DBTable: {}".format(obj.name)
      logger.error(msg)
      raise RuntimeError(msg)

    self._tables[obj.name] = obj

  def getTable(self, tblName: str) -> DBTable:
    return self._tables[tblName]

_dictParam = {dbBackendSqlite: "?", dbBackendMysql: "%s", dbBackendTsql: "%s"}
_dictQte = {dbBackendSqlite: "\"", dbBackendMysql: "`", dbBackendTsql: "\""}

def quoteId(dbType: str, strVal: typing.Optional[str]) -> str:
  """
  SQL delimited identifiers.
  :param dbType: any of dbBackendNames
  :param strVal: table name or column name
  :return: table name or column name quoted according to dbType
  """
  if (strVal is None):
    return ""
  else:
    return _dictQte[dbType] + strVal.replace(
        ".", (_dictQte[dbType] + "." + _dictQte[dbType])) + _dictQte[dbType]

class DBQuery(object):

  def __init__(self, backend: str):
    if (backend not in dbBackendNames):
      msg = "INCORRECT backend: {}".format(backend)
      logger.error(msg)
      raise DBException(msg)
    self._backend: str = backend

    self._qry: typing.Optional[str] = None
    self._tbl: typing.Optional[str] = None
    self._what_cols: list[str] = []
    self._what_vals: list[typing.Optional[str]] = []
    self._where_cols: list[str] = []
    self._where_ops: list[typing.Optional[str]] = []
    self._where_tests: list[str] = []
    self._where_vals: list[typing.Optional[str]] = []

  def clear(self) -> None:
    self._qry = None
    self._tbl = None
    self._what_cols = []
    self._what_vals = []
    self._where_cols = []
    self._where_ops = []
    self._where_tests = []
    self._where_vals = []

  def _quoteId(self, strVal: typing.Optional[str]) -> str:
    return quoteId(self._backend, strVal)

  def _sql_where(self) -> str:
    res = ""
    for i, col in enumerate(self._where_cols):
      op = self._where_ops[i]
      test = self._where_tests[i]
      val = self._where_vals[i]
      #print("DBG", op, col, test, val)
      res += ("" if (op is None) else op)
      res += " "
      res += self._quoteId(col)
      if ((val is None) and (test in ("eq", "like"))):
        res += " IS NULL "
      elif ((val is None) and (test == "neq")):
        res += " IS NOT NULL "
      elif (test == "eq"):
        res += ("=" + _dictParam[self._backend] + " ")
      elif (test == "neq"):
        res += ("<>" + _dictParam[self._backend] + " ")
      elif (test == "like"):
        res += (" LIKE " + _dictParam[self._backend] + " ")
      else:
        raise NotImplementedError
    res = res.replace("  ", " ").strip()
    #print("DBG", res)
    return res

  def _sql_delete(self) -> str:
    return "{} FROM {};".format(self._qry, self._quoteId(self._tbl))

  def _sql_drop(self) -> str:
    if (self._backend == dbBackendTsql):
      return "{} TABLE {};".format(self._qry, self._quoteId(self._tbl))
    else:
      return "{} TABLE IF EXISTS {};".format(self._qry, self._quoteId(self._tbl))

  def _sql_create(self) -> str:
    what = ", ".join([
        "{} {}".format(self._quoteId(c), v)
        for c, v in zip(self._what_cols, self._what_vals)
    ])

    return "{} TABLE {} ({});".format(self._qry, self._quoteId(self._tbl), what)

  def _sql_select(self) -> str:
    if (self._what_cols is None):
      what = "*"
    elif (self._what_cols == []):
      what = "*"
    elif (not isinstance(self._what_cols, list)):
      what = "*"
    else:
      what = ", ".join(["{}".format(self._quoteId(t)) for t in self._what_cols])

    if (len(self._where_cols) == 0):
      return "SELECT {} FROM {};".format(what, self._quoteId(self._tbl))
    else:
      return "SELECT {} FROM {} WHERE {};".format(what, self._quoteId(self._tbl),
                                                  self._sql_where())

  def _sql_insert(self) -> str:
    colNames = ", ".join(["{}".format(self._quoteId(t)) for t in self._what_cols])
    colVals = ", ".join(len(self._what_vals) * [_dictParam[self._backend]])

    if (len(self._where_cols) == 0):
      return "INSERT INTO {} ({}) VALUES ({});".format(self._quoteId(self._tbl), colNames,
                                                       colVals)
    else:
      return "INSERT INTO {} ({}) VALUES ({}) WHERE {};".format(self._quoteId(self._tbl),
                                                                colNames, colVals,
                                                                self._sql_where())

  def _sql_update(self) -> str:
    what = ", ".join([
        "{}={}".format(self._quoteId(t), _dictParam[self._backend]) for t in self._what_cols
    ])

    if (len(self._where_cols) == 0):
      return "UPDATE {} SET {};".format(self._quoteId(self._tbl), what)
    else:
      return "UPDATE {} SET {} WHERE {};".format(self._quoteId(self._tbl), what,
                                                 self._sql_where())

  @property
  def sql(self) -> str:
    if (self._qry == "DELETE"):
      return self._sql_delete()
    elif (self._qry == "DROP"):
      return self._sql_drop()
    elif (self._qry == "CREATE"):
      return self._sql_create()
    elif (self._qry == "SELECT"):
      return self._sql_select()
    elif (self._qry == "INSERT"):
      return self._sql_insert()
    elif (self._qry == "UPDATE"):
      return self._sql_update()
    else:
      msg = "UNSUPPORTED query: {}".format(self._qry)
      logger.error(msg)
      raise DBException(msg)

  @property
  def params(self) -> list[typing.Optional[str]]:
    return self._what_vals + [t for t in self._where_vals if (t is not None)]

  @params.setter
  def params(self, lst: list[typing.Optional[str]]):
    if (not isinstance(lst, list)):
      msg = "CANNOT assign {} as params".format(type(lst))
      logger.error(msg)
      raise RuntimeError(msg)

    if (self._qry == "SELECT"):
      if (len(lst) != len(self._where_cols)):
        msg = "INCORRECT length {} for params".format(len(lst))
        logger.error(msg)
        raise RuntimeError(msg)
      self._where_vals = lst
    elif (self._qry in ("INSERT", "UPDATE")):
      if (len(lst) != (len(self._what_cols) + len(self._where_cols))):
        msg = "INCORRECT length {} for params".format(len(lst))
        logger.error(msg)
        raise RuntimeError(msg)
      self._what_vals = lst[0:len(self._what_cols)]
      self._where_vals = lst[len(self._what_cols):]

  def delete(self, tblName: str) -> None:
    self.clear()
    self._qry = "DELETE"
    self._tbl = tblName

  def drop(self, tblName: str) -> None:
    self.clear()
    self._qry = "DROP"
    self._tbl = tblName

  def create(self, tblName: str, dictCols: dict[str, str]) -> None:
    """
    :param tblName:
    :param dictCols: dictionary of {"col_name": "col definition"}
    :return:
    """
    self.clear()
    self._qry = "CREATE"
    self._tbl = tblName
    tmp = sorted(dictCols.keys())
    self._what_cols = tmp
    self._what_vals = [dictCols[t] for t in tmp]

  def select(self, tblName: str, lstColNames: typing.Optional[list[str]] = None) -> None:
    """
    :param tblName:
    :param lstColNames: list of column names to SELECT (* if None)
    :return:
    """
    self.clear()
    self._qry = "SELECT"
    self._tbl = tblName
    if (lstColNames is None):
      self._what_cols = []
    else:
      self._what_cols = lstColNames

  def insert(self, tblName: str, lstColNames: typing.Optional[list[str]] = None) -> None:
    """
    :param tblName:
    :param lstColNames: list of column names to INSERT into
    :return:
    """
    self.clear()
    self._qry = "INSERT"
    self._tbl = tblName
    if (lstColNames is None):
      self._what_cols = []
    else:
      self._what_cols = lstColNames
    self._what_vals = len(lstColNames) * [None]  # type: ignore

  def update(self, tblName: str, lstColNames: typing.Optional[list[str]] = None) -> None:
    """
    :param tblName:
    :param lstColNames: list of column names to INSERT into
    :return:
    """
    self.clear()
    self._qry = "UPDATE"
    self._tbl = tblName
    if (lstColNames is None):
      self._what_cols = []
    else:
      self._what_cols = lstColNames
    self._what_vals = len(lstColNames) * [None]  # type: ignore

  def where(self,
            colName: str,
            op: typing.Optional[str] = None,
            test: str = "neq",
            val: typing.Optional[str] = None,
            bClear: bool = False):
    if (op not in (None, "AND", "OR")):
      msg = "INCORRECT WHERE operator: {}".format(op)
      logger.error(msg)
      raise DBException(msg)
    if (test not in ("eq", "neq", "like")):
      msg = "INCORRECT WHERE test: {}".format(test)
      logger.error(msg)
      raise DBException(msg)

    if (bClear):
      self._where_cols = []
      self._where_ops = []
      self._where_tests = []
      self._where_vals = []

    self._where_cols.append(colName)
    if ((op is None) and (len(self._where_ops) > 0)):
      self._where_ops.append("AND")
    else:
      self._where_ops.append(op)
    self._where_tests.append(test)
    if ((val is not None) and (test == "like")):
      if ("%" in val):
        self._where_vals.append(val)
      else:
        self._where_vals.append("%" + val + "%")
    else:
      self._where_vals.append(val)

class DBController(object):

  def __init__(self):
    self._dbInfo: typing.Optional[DBInfo] = None
    self._dbConn = None
    self.doQuery: bool = False

  @property
  def dbInfo(self) -> DBInfo:
    if (self._dbInfo is None):
      raise RuntimeError
    return self._dbInfo

  @dbInfo.setter
  def dbInfo(self, obj: DBInfo) -> None:
    if (not isinstance(obj, DBInfo)):
      msg = "INCORRECT DBInfo object"
      logger.error(msg)
      raise DBException(msg)
    tmp = obj.backend
    if (tmp not in dbBackendNames):
      msg = "DBInfo object with INCORRECT backend: {}".format(tmp)
      logger.error(msg)
      raise DBException(msg)
    self._dbInfo = obj

  @property
  def backend(self) -> str:
    if (self._dbInfo is None):
      raise RuntimeError

    return self._dbInfo.backend

  def _connect_sqlite(self) -> None:
    if (self._dbInfo is None):
      raise RuntimeError

    try:
      pyauto_base.fs.chkPath_File(str(self._dbInfo.dbname))
    except IOError:
      raise DBException

    self._dbConn = sqlite3.connect(str(self._dbInfo.dbname))

  def _connect_mysql(self) -> None:
    import pymysql
    import pymysql.err

    if (self._dbInfo is None):
      raise RuntimeError

    try:
      self._dbConn = pymysql.connect(host=self._dbInfo.host,
                                     port=int(self._dbInfo.port),
                                     db=self._dbInfo.dbname,
                                     user=self._dbInfo.user,
                                     passwd=self._dbInfo.pswd)
    except pymysql.err.OperationalError as ex:
      logger.error(ex)
      raise DBException(ex)

  def _connect_mssql(self) -> None:
    import pymssql

    if (self._dbInfo is None):
      raise RuntimeError

    try:
      self._dbConn = pymssql.connect(host="{}:{}".format(self._dbInfo.host,
                                                         self._dbInfo.port),
                                     database=self._dbInfo.dbname,
                                     user=self._dbInfo.user,
                                     password=self._dbInfo.pswd)
    except pymssql.OperationalError as ex:
      logger.error(ex)
      raise DBException(ex)

  def connect(self) -> None:
    if (self.backend == dbBackendSqlite):
      self._connect_sqlite()
    elif (self.backend == dbBackendCsv):
      pass  # handle .csv access individually in each child class
    elif (self.backend == dbBackendMysql):
      self._connect_mysql()
    elif (self.backend == dbBackendTsql):
      self._connect_mssql()
    else:
      msg = "UNSUPPORTED db backend: {}".format(self.backend)
      logger.error(msg)
      raise DBException(msg)

    logger.info("connected to database ({})".format(self.backend))

  def disconnect(self) -> None:
    if (self._dbConn is not None):
      self._dbConn.close()

    logger.info("disconnected from database ({})".format(self.backend))
    self._dbConn = None

  def isActive(self) -> bool:
    if (self._dbConn is None):
      return False
    else:
      return True

  def _query_sqlite(self,
                    qry: str,
                    lstVal: typing.Optional[list[typing.Optional[str]]] = None,
                    bFetch: bool = False) -> list[list[str]]:
    if (self._dbConn is None):
      raise RuntimeError

    res = []
    try:
      cr = self._dbConn.cursor()
      if (lstVal is None):
        cr.execute(qry)
      else:
        cr.execute(qry, lstVal)
      #print("DBG", cr._last_executed)
      if (bFetch):
        for row in cr.fetchall():
          res.append(row)
      else:
        self._dbConn.commit()
      cr.close()
    except TypeError as ex:
      logger.error(ex)
      logger.error(qry)
      logger.error(lstVal)
      raise RuntimeError
    #except:
    #  ex = sys.exc_info()[1]
    #  raise DBException(ex)
    return res

  def _query_mysql(self,
                   qry: str,
                   lstVal: typing.Optional[list[typing.Optional[str]]] = None,
                   bFetch: bool = False) -> list[list[str]]:
    if (self._dbConn is None):
      raise RuntimeError

    res = []
    try:
      cr = self._dbConn.cursor()
      if (lstVal is None):
        cr.execute(qry)
      else:
        cr.execute(qry, lstVal)
      #print("DBG", cr._last_executed)
      if (bFetch):
        for row in cr.fetchall():
          res.append(row)
      else:
        self._dbConn.commit()
      cr.close()
    except TypeError as ex:
      logger.error(ex)
      logger.error(qry)
      logger.error(lstVal)
      raise RuntimeError
    #except:
    #  ex = sys.exc_info()[1]
    #  raise DBException(ex)
    return res

  def _query_mssql(self,
                   qry: str,
                   lstVal: typing.Optional[list[typing.Optional[str]]] = None,
                   bFetch: bool = False) -> list[list[str]]:
    if (self._dbConn is None):
      raise RuntimeError

    res = []
    try:
      cr = self._dbConn.cursor()
      if (lstVal is None):
        cr.execute(qry)
      else:
        cr.execute(qry, tuple(lstVal))
      #print("DBG", cr._last_executed)
      if (bFetch):
        for row in cr.fetchall():
          res.append(row)
      else:
        self._dbConn.commit()
      cr.close()
    except TypeError as ex:
      logger.error(ex)
      logger.error(qry)
      logger.error(lstVal)
      raise RuntimeError
    #except:
    #  ex = sys.exc_info()[1]
    #  raise DBException(ex)
    return res

  def query(self,
            qry: str,
            lstVal: typing.Optional[list[typing.Optional[str]]] = None,
            bFetch: bool = False) -> list[list[str]]:
    """
    Execute a query.
    :param qry: query string
    :param lstVal: list of placeholders in the query
    :param bFetch: indicates that results are expected from the query (SELECT vs. INSERT)
    :return: table contents (list of lists or list of tuples)
    """
    if ((lstVal is not None) and (not isinstance(lstVal, list))):
      msg = "INCORRECT type for values"
      logger.error(msg)
      raise DBException(msg)
    if (pyauto_base.misc.isEmptyString(qry)):
      msg = "EMPTY query"
      logger.error(msg)
      raise DBException(msg)

    if (lstVal is None):
      lstVal = []
    #logger.info("[{}] {}".format(self.doQuery, qry))
    if (not qry.startswith("SELECT")):
      if (not self.doQuery):
        if (len(lstVal) == 0):
          logger.info(qry)
        else:
          logger.info([qry] + lstVal)
        return []

    if (self._dbConn is None):
      msg = "INVALID db connection: {}".format(self._dbConn)
      logger.error(msg)
      raise DBException(msg)

    if (self.backend == dbBackendSqlite):
      return self._query_sqlite(qry, lstVal, bFetch)
    elif (self.backend == dbBackendMysql):
      return self._query_mysql(qry, lstVal, bFetch)
    elif (self.backend == dbBackendTsql):
      return self._query_mssql(qry, lstVal, bFetch)
    else:
      msg = "UNSUPPORTED db backend: {}".format(self.backend)
      logger.error(msg)
      raise DBException(msg)

  def selectTable(self, tblName: str) -> list[list[str]]:
    if (self._dbInfo is None):
      raise RuntimeError

    if (self.backend != dbBackendCsv):
      qry = DBQuery(str(self.backend))
      qry.select(tblName)
      return self.query(qry.sql,
                        lstVal=[t for t in qry.params if t is not None],
                        bFetch=True)
    elif (self.backend == dbBackendCsv):
      res = []
      path = os.path.join(str(self._dbInfo.dbname), "{}.csv".format(tblName))
      with open(path, "r") as fIn:
        csvIn = csv.reader(fIn)
        for row in csvIn:
          res.append(row)
      return res
    else:
      msg = "UNSUPPORTED db backend: {}".format(self.backend)
      logger.error(msg)
      raise DBException(msg)

  def _showInfo_sqlite(self) -> None:
    tblInfo = []
    idxInfo = []
    viewInfo = []

    qry = DBQuery(str(self.backend))
    qry.select("sqlite_master")
    for row in self.query(qry.sql, lstVal=qry.params, bFetch=True):
      if (row[0] == "table"):
        tblInfo.append(row[1])
      elif (row[0] == "view"):
        viewInfo.append(row[1])
      elif (row[0] == "index"):
        idxInfo.append([row[1], row[2]])

    if (len(tblInfo) > 0):
      logger.info("tables:\n  " + "\n  ".join(sorted(tblInfo)))
    if (len(viewInfo) > 0):
      logger.info("views:\n  " + "\n  ".join(sorted(viewInfo)))
    if (len(viewInfo) > 0):
      logger.info("index:")
      for v in idxInfo:
        logger.info("  {} -> {}".format(v[0], v[1]))

  def _showInfo_csv(self) -> None:
    if (self._dbInfo is None):
      raise RuntimeError

    logger.info("folder: {}".format(self._dbInfo.dbname))

  def _showInfo_mysql(self) -> None:
    engInfo = []
    tblInfo = []

    for row in self.query("SHOW ENGINES;", bFetch=True):
      engInfo.append("{:<16} [{}]".format(row[0], row[1]))
    if (len(engInfo) > 0):
      logger.info("engines:\n  " + "\n  ".join(sorted(engInfo)))

    for row in self.query("SHOW TABLE STATUS;", bFetch=True):
      if (row[-1] == "VIEW"):
        tblInfo.append("{:<16} [{}]".format(row[0], row[-1]))
      else:
        tblInfo.append("{:<16} [{}] {:>6d} rows {:>8d}kB".format(
            row[0], row[1], int(row[4]), (int(row[6]) // 1024)))
    if (len(tblInfo) > 0):
      logger.info("tables:\n  " + "\n  ".join(sorted(tblInfo)))

  def _showInfo_mssql(self) -> None:
    tblInfo = []
    viewInfo = []

    qry = DBQuery(str(self.backend))
    qry.select("Information_schema.Tables")
    for row in self.query(qry.sql, lstVal=qry.params, bFetch=True):
      if (row[3] == "BASE TABLE"):
        tblInfo.append(row[2])
      elif (row[3] == "VIEW"):
        viewInfo.append(row[2])

    if (len(tblInfo) > 0):
      logger.info("tables:\n  " + "\n  ".join(sorted(tblInfo)))
    if (len(viewInfo) > 0):
      logger.info("views:\n  " + "\n  ".join(sorted(viewInfo)))

  def showInfo(self) -> None:
    doQry = self.doQuery
    self.doQuery = True

    if (self.backend == dbBackendSqlite):
      self._showInfo_sqlite()
    elif (self.backend == dbBackendCsv):
      self._showInfo_csv()
    elif (self.backend == dbBackendMysql):
      self._showInfo_mysql()
    elif (self.backend == dbBackendTsql):
      self._showInfo_mssql()
    else:
      logger.warning("showInfo not available for {}".format(self.backend))

    self.doQuery = doQry

  def _getTables_sqlite(self):
    qres = self.query("SELECT * FROM \"sqlite_master\" WHERE \"type\" == 'table';",
                      bFetch=True)
    if (self._dbInfo is None):
      raise RuntimeError

    for row in qres:
      dbTbl = DBTable(row[1])
      self._dbInfo.addTable(dbTbl)

    for tblId in self._dbInfo.tableNames:
      qres = self.query("PRAGMA table_info({});".format(tblId), bFetch=True)
      for row in qres:
        self._dbInfo.getTable(tblId).colNames.append(row[1])

  def _getTables_csv(self):
    if (self._dbInfo is None):
      raise RuntimeError

    for f in os.listdir(self._dbInfo.dbname):
      if (f.lower().endswith(".csv")):
        dbTbl = DBTable(f[:-4])
        self._dbInfo.addTable(dbTbl)

  def _getTables_mysql(self):
    if (self._dbInfo is None):
      raise RuntimeError

    qres = self.query("SHOW TABLES;", bFetch=True)
    for row in qres:
      dbTbl = DBTable(row[0])
      self._dbInfo.addTable(dbTbl)

    for tblId in self._dbInfo.tableNames:
      qres = self.query("DESCRIBE {};".format(quoteId(self.backend, tblId)), bFetch=True)
      for row in qres:
        self._dbInfo.getTable(tblId).colNames.append(row[0])

  def _getTables_mssql(self):
    if (self._dbInfo is None):
      raise RuntimeError

    qres = self.query(
        "SELECT * FROM \"Information_schema\".\"Tables\" WHERE \"TABLE_TYPE\" = 'BASE TABLE';",
        bFetch=True)
    for row in qres:
      dbTbl = DBTable(row[2])
      self._dbInfo.addTable(dbTbl)

    for tblId in self._dbInfo.tableNames:
      qres = self.query(
          "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbo' AND TABLE_NAME='{}';"
          .format(tblId),
          bFetch=True)
      for row in qres:
        self._dbInfo.getTable(tblId).colNames.append(row[3])

  def getTables(self):
    if (self._dbInfo is None):
      raise RuntimeError

    if (self._dbInfo.nTables > 0):
      logger.info("re-reading table information from database")
      self._dbInfo.clrTables()

    doQry = self.doQuery
    self.doQuery = True

    if (self.backend == dbBackendSqlite):
      self._getTables_sqlite()
    elif (self.backend == dbBackendCsv):
      self._getTables_csv()
    elif (self.backend == dbBackendMysql):
      self._getTables_mysql()
    elif (self.backend == dbBackendTsql):
      self._getTables_mssql()
    else:
      logger.warning("getTables not available for {}".format(self.backend))

    self.doQuery = doQry
